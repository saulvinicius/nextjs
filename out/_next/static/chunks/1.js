(window["webpackJsonp_N_E"] = window["webpackJsonp_N_E"] || []).push([[1],{

/***/ "./components/templates/n49shopv2_trijoia/components/footer.js":
/*!*********************************************************************!*\
  !*** ./components/templates/n49shopv2_trijoia/components/footer.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Footer; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);


var _jsxFileName = "C:\\Users\\sauln49\\Desktop\\nextjs\\components\\templates\\n49shopv2_trijoia\\components\\footer.js";

function Footer(_ref) {
  var _this = this;

  var configs = _ref.configs;

  var loadBlock = function loadBlock(shortcode) {
    var blockList = null;
    var countBlocks = 0;

    if (configs.footerBlocks) {
      blockList = Object.keys(configs.footerBlocks);
      countBlocks = blockList.length;
    }

    var resultBlock = [];
    var linksBlock = null;
    var contentBlock = null;

    for (var i = 1; i <= countBlocks; i++) {
      if (configs.footerBlocks[i] && configs.footerBlocks[i].shortcode == shortcode && configs.footerBlocks[i].status == 1) {
        var arrayLinks = [];

        if (configs.footerBlocks[i].link_interno != null) {
          arrayLinks = Object.keys(configs.footerBlocks[i].link_interno).map(function (item) {
            return configs.footerBlocks[i].link_interno[item];
          });
        }

        resultBlock['links'] = arrayLinks;
        resultBlock['conteudo'] = configs.footerBlocks[i].description[5];
      }
    }

    return resultBlock;
  };

  var decodeHtml = function decodeHtml(str) {
    var translate_re = /&(nbsp|amp|quot|lt|gt|#x27;|#x2F;);/g;
    var result = null;

    var translate = {
      'nbsp': ' ',
      'amp': '&',
      'quot': '"',
      'lt': '<',
      'gt': '>',
      '#x27;': "'",
      '#x2F;': '/'
    },
        translator = function translator($0, $1) {
      return translate[$1];
    };

    if (str) {
      result = str.replace(translate_re, translator);
    }

    return result;
  };

  var blockFooter1 = loadBlock('links_topo_1');
  var blockFooter2 = loadBlock('links_topo_2');
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("footer", {
    className: "footer cp-footer7",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "container",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "row links-footer",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "col-md-3 col-sm-12 v-top",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "title-block",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "area-logo-footer",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                className: "logo-footer",
                src: "https://db7qxt7xxlq5m.cloudfront.net/fit-in/0x0/filters:fill(ffffff)/filters:quality(80)/n49shopv2_trijoia/images/footerspa/logo-footer-trijoiashop.png"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 68,
                columnNumber: 8
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 67,
              columnNumber: 7
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "social-area",
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "square-social col-sm-2 bg-white mr-2",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                  href: "https://www.facebook.com/lojasTriJoia",
                  target: "_blank",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                    className: "fab fa-facebook-f color2"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 73,
                    columnNumber: 73
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 73,
                  columnNumber: 9
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 72,
                columnNumber: 8
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "square-social col-sm-2 bg-white",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                  href: "https://www.instagram.com/trijoiadigital",
                  target: "_blank",
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                    className: "fab fa-instagram color2"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 77,
                    columnNumber: 76
                  }, this), " "]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 77,
                  columnNumber: 9
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 76,
                columnNumber: 8
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 71,
              columnNumber: 7
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 66,
            columnNumber: 6
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 65,
          columnNumber: 5
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "tamanho-tablet col-md-3 col-sm-12 v-top column-links column-lk1",
          children: blockFooter1 != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              dangerouslySetInnerHTML: {
                __html: decodeHtml(blockFooter1.conteudo)
              }
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 85,
              columnNumber: 10
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
              "class": "menufooter",
              children: blockFooter1.links != null && blockFooter1.links.length ? blockFooter1.links.map(function (item, key) {
                return item.tipo_pagina == 'informacoes' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                    href: "/" + item.pagina + "/i",
                    className: "footer-nav-link",
                    children: item.label
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 91,
                    columnNumber: 19
                  }, _this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 91,
                  columnNumber: 15
                }, _this) : item.tipo_pagina == 'minhaconta' && item.pagina != 'contato' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                    href: "/" + item.pagina,
                    className: "footer-nav-link",
                    children: item.label
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 93,
                    columnNumber: 19
                  }, _this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 93,
                  columnNumber: 15
                }, _this) : item.tipo_pagina == 'linkdireto' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                    href: item.pagina,
                    className: "footer-nav-link",
                    children: item.label
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 95,
                    columnNumber: 19
                  }, _this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 95,
                  columnNumber: 15
                }, _this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
                  href: "/" + item.pagina,
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                      className: "footer-nav-link",
                      children: item.label
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 98,
                      columnNumber: 20
                    }, _this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 98,
                    columnNumber: 16
                  }, _this)
                }, key, false, {
                  fileName: _jsxFileName,
                  lineNumber: 97,
                  columnNumber: 15
                }, _this);
              }) : null
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 86,
              columnNumber: 10
            }, this)]
          }, void 0, true) : null
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 81,
          columnNumber: 6
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "col-md-3 col-sm-12 v-top column-links column-lk2",
          children: blockFooter2 != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              dangerouslySetInnerHTML: {
                __html: decodeHtml(blockFooter2.conteudo)
              }
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 116,
              columnNumber: 10
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
              "class": "menufooter",
              children: blockFooter2.links != null && blockFooter2.links.length ? blockFooter2.links.map(function (item, key) {
                return item.tipo_pagina == 'informacoes' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                    href: "/" + item.pagina + "/i",
                    className: "footer-nav-link",
                    children: item.label
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 122,
                    columnNumber: 19
                  }, _this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 122,
                  columnNumber: 15
                }, _this) : item.tipo_pagina == 'minhaconta' && item.pagina != 'contato' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                    href: "/" + item.pagina,
                    className: "footer-nav-link",
                    children: item.label
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 124,
                    columnNumber: 19
                  }, _this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 124,
                  columnNumber: 15
                }, _this) : item.tipo_pagina == 'linkdireto' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                    href: item.pagina,
                    className: "footer-nav-link",
                    children: item.label
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 126,
                    columnNumber: 19
                  }, _this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 126,
                  columnNumber: 15
                }, _this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
                  href: "/" + item.pagina,
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                      className: "footer-nav-link",
                      children: item.label
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 129,
                      columnNumber: 20
                    }, _this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 129,
                    columnNumber: 16
                  }, _this)
                }, key, false, {
                  fileName: _jsxFileName,
                  lineNumber: 128,
                  columnNumber: 15
                }, _this);
              }) : null
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 117,
              columnNumber: 10
            }, this)]
          }, void 0, true) : null
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 111,
          columnNumber: 6
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "col-md-3 col-sm-12 v-top column-links column-lk3",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "title-block",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
              className: "footer-title-menu",
              children: "Contato"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 144,
              columnNumber: 8
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
              children: "Rua Sapiranga, 9595, Imigrante - Campo Bom"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 146,
              columnNumber: 8
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("strong", {
                children: "Fone:\xA0"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 148,
                columnNumber: 11
              }, this), "51 35986100"]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 148,
              columnNumber: 8
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("b", {
                children: "WhatsApp"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 150,
                columnNumber: 11
              }, this), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                href: "https://api.whatsapp.com/send?phone=5551999460836&text=&source=&data=&app_absent=",
                target: "_blank",
                children: "(51) 9 9946 0836"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 150,
                columnNumber: 27
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 150,
              columnNumber: 8
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                href: "mailto:marketing@trijoia.com.br",
                children: "marketing@trijoia.com.br"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 152,
                columnNumber: 11
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 152,
              columnNumber: 8
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 143,
            columnNumber: 7
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 142,
          columnNumber: 6
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 64,
        columnNumber: 4
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "row base-footer",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "tamanho-tablet col-md-4 col-sm-12 v-middle img-footer-1",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "title-block",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
                className: "footer-title-menu",
                children: "Seguran\xE7a"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 161,
                columnNumber: 9
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 160,
              columnNumber: 8
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "content-block",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                className: "d-flex",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                    className: "pr-2",
                    src: "https://db7qxt7xxlq5m.cloudfront.net/fit-in/0x0/filters:fill(ffffff)/filters:quality(80)/n49shopv2_trijoia/images/footerspa/selos.jpg"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 166,
                    columnNumber: 11
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 165,
                  columnNumber: 10
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                  children: "\xA0"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 168,
                  columnNumber: 10
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 164,
                columnNumber: 9
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 163,
              columnNumber: 8
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 159,
            columnNumber: 7
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 158,
          columnNumber: 6
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "tamanho-tablet col-md-4 col-sm-12 v-middle",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "paymentArea",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "title-block",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
                className: "footer-title-menu",
                children: "Formas de Pagamento"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 176,
                columnNumber: 9
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 175,
              columnNumber: 8
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "content-block",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                  src: "https://db7qxt7xxlq5m.cloudfront.net/fit-in/0x0/filters:fill(ffffff)/filters:quality(80)/n49shopv2_trijoia/images/footerspa/formas-pagamento-trijoia.png"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 179,
                  columnNumber: 12
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 179,
                columnNumber: 9
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 178,
              columnNumber: 8
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 174,
            columnNumber: 7
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 173,
          columnNumber: 6
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "col-md-4 col-sm-12 v-middle box-developers",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "paymentArea",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                href: "https://www.n49.com.br",
                target: "_blank",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                  src: "/catalog/view/theme/includes/layouts/images/n49-plataforma-ecommerce.png"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 185,
                  columnNumber: 60
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 185,
                columnNumber: 11
              }, this), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                className: "bra",
                href: "http://www.bradigital.com.br/?gclid=Cj0KCQjw7Nj5BRCZARIsABwxDKLTFqgCjHifMm8och7RQ37U_5p-3YnjIGrssNem5M6K8yrnm9tXN_MaAi04EALw_wcB",
                target: "_blank",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                  src: "https://1ab6290f102b0284.cdn.gocache.net/n49shopv2_trijoia/images/footerspa/logo-bra.png"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 185,
                  columnNumber: 322
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 185,
                columnNumber: 151
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 185,
              columnNumber: 8
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 184,
            columnNumber: 7
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 183,
          columnNumber: 6
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "storeInfo",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "footer-desc t-center",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
              children: "CWD Gestao Digital Com de Prod \xD3pticos @ 2020 - Todos os direitos reservados - CNPJ 36.572.435/0001 58 Ofertas v\xE1lidas enquanto durarem nossos estoques - Vendas sujeitas \xE0 an\xE1lise e confirma\xE7\xE3o de dados pela empresa - Os pre\xE7os, promo\xE7\xF5es e condi\xE7\xF5es de pagamento s\xE3o v\xE1lidos exclusivamente para compras efetuadas em nossa loja virtual."
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 190,
              columnNumber: 8
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 189,
            columnNumber: 7
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 188,
          columnNumber: 6
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 157,
        columnNumber: 5
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 3
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 61,
    columnNumber: 5
  }, this);
}
_c = Footer;

var _c;

$RefreshReg$(_c, "Footer");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy90ZW1wbGF0ZXMvbjQ5c2hvcHYyX3RyaWpvaWEvY29tcG9uZW50cy9mb290ZXIuanMiXSwibmFtZXMiOlsiRm9vdGVyIiwiY29uZmlncyIsImxvYWRCbG9jayIsInNob3J0Y29kZSIsImJsb2NrTGlzdCIsImNvdW50QmxvY2tzIiwiZm9vdGVyQmxvY2tzIiwiT2JqZWN0Iiwia2V5cyIsImxlbmd0aCIsInJlc3VsdEJsb2NrIiwibGlua3NCbG9jayIsImNvbnRlbnRCbG9jayIsImkiLCJzdGF0dXMiLCJhcnJheUxpbmtzIiwibGlua19pbnRlcm5vIiwibWFwIiwiaXRlbSIsImRlc2NyaXB0aW9uIiwiZGVjb2RlSHRtbCIsInN0ciIsInRyYW5zbGF0ZV9yZSIsInJlc3VsdCIsInRyYW5zbGF0ZSIsInRyYW5zbGF0b3IiLCIkMCIsIiQxIiwicmVwbGFjZSIsImJsb2NrRm9vdGVyMSIsImJsb2NrRm9vdGVyMiIsIl9faHRtbCIsImNvbnRldWRvIiwibGlua3MiLCJrZXkiLCJ0aXBvX3BhZ2luYSIsInBhZ2luYSIsImxhYmVsIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFFZSxTQUFTQSxNQUFULE9BQTZCO0FBQUE7O0FBQUEsTUFBWEMsT0FBVyxRQUFYQSxPQUFXOztBQUMzQyxNQUFJQyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFDQyxTQUFELEVBQWU7QUFDOUIsUUFBSUMsU0FBUyxHQUFHLElBQWhCO0FBQ0EsUUFBSUMsV0FBVyxHQUFHLENBQWxCOztBQUVBLFFBQUdKLE9BQU8sQ0FBQ0ssWUFBWCxFQUF3QjtBQUN2QkYsZUFBUyxHQUFHRyxNQUFNLENBQUNDLElBQVAsQ0FBWVAsT0FBTyxDQUFDSyxZQUFwQixDQUFaO0FBQ0FELGlCQUFXLEdBQUdELFNBQVMsQ0FBQ0ssTUFBeEI7QUFDQTs7QUFFRCxRQUFJQyxXQUFXLEdBQUcsRUFBbEI7QUFDQSxRQUFJQyxVQUFVLEdBQUcsSUFBakI7QUFDQSxRQUFJQyxZQUFZLEdBQUcsSUFBbkI7O0FBRUEsU0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxJQUFJUixXQUFyQixFQUFrQ1EsQ0FBQyxFQUFuQyxFQUF1QztBQUN0QyxVQUFJWixPQUFPLENBQUNLLFlBQVIsQ0FBcUJPLENBQXJCLENBQUQsSUFBOEJaLE9BQU8sQ0FBQ0ssWUFBUixDQUFxQk8sQ0FBckIsRUFBd0JWLFNBQXhCLElBQXFDQSxTQUFuRSxJQUFrRkYsT0FBTyxDQUFDSyxZQUFSLENBQXFCTyxDQUFyQixFQUF3QkMsTUFBeEIsSUFBa0MsQ0FBdkgsRUFBMEg7QUFDekgsWUFBSUMsVUFBVSxHQUFHLEVBQWpCOztBQUNBLFlBQUdkLE9BQU8sQ0FBQ0ssWUFBUixDQUFxQk8sQ0FBckIsRUFBd0JHLFlBQXhCLElBQXVDLElBQTFDLEVBQStDO0FBQzlDRCxvQkFBVSxHQUFHUixNQUFNLENBQUNDLElBQVAsQ0FBWVAsT0FBTyxDQUFDSyxZQUFSLENBQXFCTyxDQUFyQixFQUF3QkcsWUFBcEMsRUFBa0RDLEdBQWxELENBQXNELFVBQUFDLElBQUk7QUFBQSxtQkFDdEVqQixPQUFPLENBQUNLLFlBQVIsQ0FBcUJPLENBQXJCLEVBQXdCRyxZQUF4QixDQUFxQ0UsSUFBckMsQ0FEc0U7QUFBQSxXQUExRCxDQUFiO0FBR0E7O0FBQ0RSLG1CQUFXLENBQUMsT0FBRCxDQUFYLEdBQXVCSyxVQUF2QjtBQUNBTCxtQkFBVyxDQUFDLFVBQUQsQ0FBWCxHQUEwQlQsT0FBTyxDQUFDSyxZQUFSLENBQXFCTyxDQUFyQixFQUF3Qk0sV0FBeEIsQ0FBb0MsQ0FBcEMsQ0FBMUI7QUFDQTtBQUNEOztBQUVELFdBQU9ULFdBQVA7QUFDQSxHQTNCRDs7QUE2QkEsTUFBSVUsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ0MsR0FBRCxFQUFTO0FBQ3pCLFFBQU1DLFlBQVksR0FBRyxzQ0FBckI7QUFDQSxRQUFJQyxNQUFNLEdBQUcsSUFBYjs7QUFFQSxRQUFNQyxTQUFTLEdBQUc7QUFDakIsY0FBUSxHQURTO0FBRWpCLGFBQVEsR0FGUztBQUdqQixjQUFRLEdBSFM7QUFJakIsWUFBUSxHQUpTO0FBS2pCLFlBQVEsR0FMUztBQU1qQixlQUFTLEdBTlE7QUFPakIsZUFBUztBQVBRLEtBQWxCO0FBQUEsUUFTQUMsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBU0MsRUFBVCxFQUFhQyxFQUFiLEVBQWlCO0FBQzdCLGFBQU9ILFNBQVMsQ0FBQ0csRUFBRCxDQUFoQjtBQUNBLEtBWEQ7O0FBYUEsUUFBR04sR0FBSCxFQUFPO0FBQ05FLFlBQU0sR0FBR0YsR0FBRyxDQUFDTyxPQUFKLENBQVlOLFlBQVosRUFBMEJHLFVBQTFCLENBQVQ7QUFDQTs7QUFFRCxXQUFPRixNQUFQO0FBQ0EsR0F0QkQ7O0FBd0JBLE1BQUlNLFlBQVksR0FBRzNCLFNBQVMsQ0FBQyxjQUFELENBQTVCO0FBQ0EsTUFBSTRCLFlBQVksR0FBRzVCLFNBQVMsQ0FBQyxjQUFELENBQTVCO0FBRUMsc0JBQ0U7QUFBUSxhQUFTLEVBQUMsbUJBQWxCO0FBQUEsMkJBRUY7QUFBSyxlQUFTLEVBQUMsV0FBZjtBQUFBLDhCQUNDO0FBQUssaUJBQVMsRUFBQyxrQkFBZjtBQUFBLGdDQUNDO0FBQUssbUJBQVMsRUFBQywwQkFBZjtBQUFBLGlDQUNDO0FBQUsscUJBQVMsRUFBQyxhQUFmO0FBQUEsb0NBQ0M7QUFBSyx1QkFBUyxFQUFDLGtCQUFmO0FBQUEscUNBQ0M7QUFBSyx5QkFBUyxFQUFDLGFBQWY7QUFBNkIsbUJBQUcsRUFBQztBQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFERCxlQUtDO0FBQUssdUJBQVMsRUFBQyxhQUFmO0FBQUEsc0NBQ0M7QUFBSyx5QkFBUyxFQUFDLHNDQUFmO0FBQUEsdUNBQ0M7QUFBRyxzQkFBSSxFQUFDLHVDQUFSO0FBQWdELHdCQUFNLEVBQUMsUUFBdkQ7QUFBQSx5Q0FBZ0U7QUFBRyw2QkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFoRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFERCxlQUtDO0FBQUsseUJBQVMsRUFBQyxpQ0FBZjtBQUFBLHVDQUNDO0FBQUcsc0JBQUksRUFBQywwQ0FBUjtBQUFtRCx3QkFBTSxFQUFDLFFBQTFEO0FBQUEsMENBQW1FO0FBQUcsNkJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBQW5FO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBTEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUxEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREQsZUFpQkU7QUFBSyxtQkFBUyxFQUFDLGlFQUFmO0FBQUEsb0JBRUcyQixZQUFZLElBQUksSUFBakIsZ0JBQ0M7QUFBQSxvQ0FDQztBQUFLLHFDQUF1QixFQUFFO0FBQUVFLHNCQUFNLEVBQUVYLFVBQVUsQ0FBQ1MsWUFBWSxDQUFDRyxRQUFkO0FBQXBCO0FBQTlCO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREQsZUFFQztBQUFJLHVCQUFNLFlBQVY7QUFBQSx3QkFFR0gsWUFBWSxDQUFDSSxLQUFiLElBQXNCLElBQXRCLElBQThCSixZQUFZLENBQUNJLEtBQWIsQ0FBbUJ4QixNQUFsRCxHQUNBb0IsWUFBWSxDQUFDSSxLQUFiLENBQW1CaEIsR0FBbkIsQ0FBdUIsVUFBQ0MsSUFBRCxFQUFPZ0IsR0FBUDtBQUFBLHVCQUNwQmhCLElBQUksQ0FBQ2lCLFdBQUwsSUFBb0IsYUFBckIsZ0JBQ0M7QUFBQSx5Q0FBSTtBQUFHLHdCQUFJLEVBQUUsTUFBSWpCLElBQUksQ0FBQ2tCLE1BQVQsR0FBZ0IsSUFBekI7QUFBK0IsNkJBQVMsRUFBQyxpQkFBekM7QUFBQSw4QkFBNERsQixJQUFJLENBQUNtQjtBQUFqRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUo7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFERCxHQUVHbkIsSUFBSSxDQUFDaUIsV0FBTCxJQUFvQixZQUFwQixJQUFvQ2pCLElBQUksQ0FBQ2tCLE1BQUwsSUFBZSxTQUFwRCxnQkFDRDtBQUFBLHlDQUFJO0FBQUcsd0JBQUksRUFBRSxNQUFJbEIsSUFBSSxDQUFDa0IsTUFBbEI7QUFBMEIsNkJBQVMsRUFBQyxpQkFBcEM7QUFBQSw4QkFBdURsQixJQUFJLENBQUNtQjtBQUE1RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUo7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFEQyxHQUVDbkIsSUFBSSxDQUFDaUIsV0FBTCxJQUFvQixZQUFyQixnQkFDRDtBQUFBLHlDQUFJO0FBQUcsd0JBQUksRUFBRWpCLElBQUksQ0FBQ2tCLE1BQWQ7QUFBc0IsNkJBQVMsRUFBQyxpQkFBaEM7QUFBQSw4QkFBbURsQixJQUFJLENBQUNtQjtBQUF4RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUo7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFEQyxnQkFHRCxxRUFBQyxnREFBRDtBQUFnQixzQkFBSSxFQUFFLE1BQUluQixJQUFJLENBQUNrQixNQUEvQjtBQUFBLHlDQUNDO0FBQUEsMkNBQUk7QUFBRywrQkFBUyxFQUFDLGlCQUFiO0FBQUEsZ0NBQWdDbEIsSUFBSSxDQUFDbUI7QUFBckM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERCxtQkFBV0gsR0FBWDtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQVJvQjtBQUFBLGVBQXZCLENBREEsR0FjQztBQWhCSDtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZEO0FBQUEsMEJBREQsR0F3QkM7QUExQkg7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFqQkYsZUErQ0U7QUFBSyxtQkFBUyxFQUFDLGtEQUFmO0FBQUEsb0JBR0dKLFlBQVksSUFBSSxJQUFqQixnQkFDQztBQUFBLG9DQUNDO0FBQUsscUNBQXVCLEVBQUU7QUFBRUMsc0JBQU0sRUFBRVgsVUFBVSxDQUFDVSxZQUFZLENBQUNFLFFBQWQ7QUFBcEI7QUFBOUI7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFERCxlQUVDO0FBQUksdUJBQU0sWUFBVjtBQUFBLHdCQUVHRixZQUFZLENBQUNHLEtBQWIsSUFBc0IsSUFBdEIsSUFBOEJILFlBQVksQ0FBQ0csS0FBYixDQUFtQnhCLE1BQWxELEdBQ0FxQixZQUFZLENBQUNHLEtBQWIsQ0FBbUJoQixHQUFuQixDQUF1QixVQUFDQyxJQUFELEVBQU9nQixHQUFQO0FBQUEsdUJBQ3BCaEIsSUFBSSxDQUFDaUIsV0FBTCxJQUFvQixhQUFyQixnQkFDQztBQUFBLHlDQUFJO0FBQUcsd0JBQUksRUFBRSxNQUFJakIsSUFBSSxDQUFDa0IsTUFBVCxHQUFnQixJQUF6QjtBQUErQiw2QkFBUyxFQUFDLGlCQUF6QztBQUFBLDhCQUE0RGxCLElBQUksQ0FBQ21CO0FBQWpFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBSjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQURELEdBRUduQixJQUFJLENBQUNpQixXQUFMLElBQW9CLFlBQXBCLElBQW9DakIsSUFBSSxDQUFDa0IsTUFBTCxJQUFlLFNBQXBELGdCQUNEO0FBQUEseUNBQUk7QUFBRyx3QkFBSSxFQUFFLE1BQUlsQixJQUFJLENBQUNrQixNQUFsQjtBQUEwQiw2QkFBUyxFQUFDLGlCQUFwQztBQUFBLDhCQUF1RGxCLElBQUksQ0FBQ21CO0FBQTVEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBSjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQURDLEdBRUNuQixJQUFJLENBQUNpQixXQUFMLElBQW9CLFlBQXJCLGdCQUNEO0FBQUEseUNBQUk7QUFBRyx3QkFBSSxFQUFFakIsSUFBSSxDQUFDa0IsTUFBZDtBQUFzQiw2QkFBUyxFQUFDLGlCQUFoQztBQUFBLDhCQUFtRGxCLElBQUksQ0FBQ21CO0FBQXhEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBSjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQURDLGdCQUdELHFFQUFDLGdEQUFEO0FBQWdCLHNCQUFJLEVBQUUsTUFBSW5CLElBQUksQ0FBQ2tCLE1BQS9CO0FBQUEseUNBQ0M7QUFBQSwyQ0FBSTtBQUFHLCtCQUFTLEVBQUMsaUJBQWI7QUFBQSxnQ0FBZ0NsQixJQUFJLENBQUNtQjtBQUFyQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURELG1CQUFXSCxHQUFYO0FBQUE7QUFBQTtBQUFBO0FBQUEseUJBUm9CO0FBQUEsZUFBdkIsQ0FEQSxHQWNDO0FBaEJIO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRkQ7QUFBQSwwQkFERCxHQXdCQztBQTNCSDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQS9DRixlQThFRTtBQUFLLG1CQUFTLEVBQUMsa0RBQWY7QUFBQSxpQ0FDQztBQUFLLHFCQUFTLEVBQUMsYUFBZjtBQUFBLG9DQUNDO0FBQUksdUJBQVMsRUFBQyxtQkFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFERCxlQUdDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUhELGVBS0M7QUFBQSxzQ0FBRztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFBSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBTEQsZUFPQztBQUFBLHNDQUFHO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUFILG9CQUFtQjtBQUFHLG9CQUFJLEVBQUMsbUZBQVI7QUFBNEcsc0JBQU0sRUFBQyxRQUFuSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFBbkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQVBELGVBU0M7QUFBQSxxQ0FBRztBQUFHLG9CQUFJLEVBQUMsaUNBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBSDtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQVREO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBOUVGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURELGVBOEZFO0FBQUssaUJBQVMsRUFBQyxpQkFBZjtBQUFBLGdDQUNDO0FBQUssbUJBQVMsRUFBQyx5REFBZjtBQUFBLGlDQUNDO0FBQUEsb0NBQ0M7QUFBSyx1QkFBUyxFQUFDLGFBQWY7QUFBQSxxQ0FDQztBQUFJLHlCQUFTLEVBQUMsbUJBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERDtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURELGVBSUM7QUFBSyx1QkFBUyxFQUFDLGVBQWY7QUFBQSxxQ0FDQztBQUFJLHlCQUFTLEVBQUMsUUFBZDtBQUFBLHdDQUNDO0FBQUEseUNBQ0M7QUFBSyw2QkFBUyxFQUFDLE1BQWY7QUFBc0IsdUJBQUcsRUFBQztBQUExQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFERCxlQUlDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUpEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBSkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERCxlQWdCQztBQUFLLG1CQUFTLEVBQUMsNENBQWY7QUFBQSxpQ0FDQztBQUFLLHFCQUFTLEVBQUMsYUFBZjtBQUFBLG9DQUNDO0FBQUssdUJBQVMsRUFBQyxhQUFmO0FBQUEscUNBQ0M7QUFBSSx5QkFBUyxFQUFDLG1CQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFERCxlQUlDO0FBQUssdUJBQVMsRUFBQyxlQUFmO0FBQUEscUNBQ0M7QUFBQSx1Q0FBRztBQUFLLHFCQUFHLEVBQUM7QUFBVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBSkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFoQkQsZUEwQkM7QUFBSyxtQkFBUyxFQUFDLDRDQUFmO0FBQUEsaUNBQ0M7QUFBSyxxQkFBUyxFQUFDLGFBQWY7QUFBQSxtQ0FDQztBQUFBLHNDQUFHO0FBQUcsb0JBQUksRUFBQyx3QkFBUjtBQUFpQyxzQkFBTSxFQUFDLFFBQXhDO0FBQUEsdUNBQWlEO0FBQUsscUJBQUcsRUFBQztBQUFUO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBakQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFBSCxvQkFBK0k7QUFBRyx5QkFBUyxFQUFDLEtBQWI7QUFBbUIsb0JBQUksRUFBQyxrSUFBeEI7QUFBMkosc0JBQU0sRUFBQyxRQUFsSztBQUFBLHVDQUEySztBQUFLLHFCQUFHLEVBQUM7QUFBVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTNLO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBQS9JO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQTFCRCxlQStCQztBQUFLLG1CQUFTLEVBQUMsV0FBZjtBQUFBLGlDQUNDO0FBQUsscUJBQVMsRUFBQyxzQkFBZjtBQUFBLG1DQUNDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBL0JEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQTlGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGRTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUF5SUQ7S0FsTXVCbEMsTSIsImZpbGUiOiJzdGF0aWMvY2h1bmtzLzEuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgTGluayBmcm9tIFwibmV4dC9saW5rXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBGb290ZXIoeyBjb25maWdzIH0pIHtcclxuXHR2YXIgbG9hZEJsb2NrID0gKHNob3J0Y29kZSkgPT4ge1xyXG5cdFx0bGV0IGJsb2NrTGlzdCA9IG51bGxcclxuXHRcdGxldCBjb3VudEJsb2NrcyA9IDBcclxuXHRcdFxyXG5cdFx0aWYoY29uZmlncy5mb290ZXJCbG9ja3Mpe1xyXG5cdFx0XHRibG9ja0xpc3QgPSBPYmplY3Qua2V5cyhjb25maWdzLmZvb3RlckJsb2NrcylcclxuXHRcdFx0Y291bnRCbG9ja3MgPSBibG9ja0xpc3QubGVuZ3RoXHRcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0bGV0IHJlc3VsdEJsb2NrID0gW11cclxuXHRcdGxldCBsaW5rc0Jsb2NrID0gbnVsbFxyXG5cdFx0bGV0IGNvbnRlbnRCbG9jayA9IG51bGxcclxuXHRcdFxyXG5cdFx0Zm9yICh2YXIgaSA9IDE7IGkgPD0gY291bnRCbG9ja3M7IGkrKykge1xyXG5cdFx0XHRpZigoY29uZmlncy5mb290ZXJCbG9ja3NbaV0pICYmIChjb25maWdzLmZvb3RlckJsb2Nrc1tpXS5zaG9ydGNvZGUgPT0gc2hvcnRjb2RlKSAmJiAoY29uZmlncy5mb290ZXJCbG9ja3NbaV0uc3RhdHVzID09IDEpKXtcclxuXHRcdFx0XHRsZXQgYXJyYXlMaW5rcyA9IFtdXHRcdFxyXG5cdFx0XHRcdGlmKGNvbmZpZ3MuZm9vdGVyQmxvY2tzW2ldLmxpbmtfaW50ZXJubyE9IG51bGwpe1xyXG5cdFx0XHRcdFx0YXJyYXlMaW5rcyA9IE9iamVjdC5rZXlzKGNvbmZpZ3MuZm9vdGVyQmxvY2tzW2ldLmxpbmtfaW50ZXJubykubWFwKGl0ZW0gPT4gXHJcblx0XHRcdFx0XHRcdGNvbmZpZ3MuZm9vdGVyQmxvY2tzW2ldLmxpbmtfaW50ZXJub1tpdGVtXVxyXG5cdFx0XHRcdFx0KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmVzdWx0QmxvY2tbJ2xpbmtzJ10gPSBhcnJheUxpbmtzXHJcblx0XHRcdFx0cmVzdWx0QmxvY2tbJ2NvbnRldWRvJ10gPSBjb25maWdzLmZvb3RlckJsb2Nrc1tpXS5kZXNjcmlwdGlvbls1XVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdHJldHVybiByZXN1bHRCbG9jaztcclxuXHR9XHJcblxyXG5cdHZhciBkZWNvZGVIdG1sID0gKHN0cikgPT4ge1xyXG5cdFx0Y29uc3QgdHJhbnNsYXRlX3JlID0gLyYobmJzcHxhbXB8cXVvdHxsdHxndHwjeDI3O3wjeDJGOyk7L2dcclxuXHRcdGxldCByZXN1bHQgPSBudWxsXHJcblx0XHJcblx0XHRjb25zdCB0cmFuc2xhdGUgPSB7XHJcblx0XHRcdCduYnNwJzogJyAnLCBcclxuXHRcdFx0J2FtcCcgOiAnJicsIFxyXG5cdFx0XHQncXVvdCc6ICdcIicsXHJcblx0XHRcdCdsdCcgIDogJzwnLCBcclxuXHRcdFx0J2d0JyAgOiAnPicsXHJcblx0XHRcdCcjeDI3Oyc6IFwiJ1wiLFxyXG5cdFx0XHQnI3gyRjsnOiAnLycgXHJcblx0XHR9LFxyXG5cdFx0dHJhbnNsYXRvciA9IGZ1bmN0aW9uKCQwLCAkMSkgeyBcclxuXHRcdFx0cmV0dXJuIHRyYW5zbGF0ZVskMV07IFxyXG5cdFx0fTtcclxuXHRcdFxyXG5cdFx0aWYoc3RyKXtcclxuXHRcdFx0cmVzdWx0ID0gc3RyLnJlcGxhY2UodHJhbnNsYXRlX3JlLCB0cmFuc2xhdG9yKVx0XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdHJldHVybiByZXN1bHRcclxuXHR9XHJcblxyXG5cdHZhciBibG9ja0Zvb3RlcjEgPSBsb2FkQmxvY2soJ2xpbmtzX3RvcG9fMScpXHJcblx0dmFyIGJsb2NrRm9vdGVyMiA9IGxvYWRCbG9jaygnbGlua3NfdG9wb18yJylcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxmb290ZXIgY2xhc3NOYW1lPVwiZm9vdGVyIGNwLWZvb3RlcjdcIj5cclxuXHRcdFx0XHRcclxuXHRcdDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XHRcdFxyXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInJvdyBsaW5rcy1mb290ZXJcIj5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC1tZC0zIGNvbC1zbS0xMiB2LXRvcFwiPlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJ0aXRsZS1ibG9ja1wiPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImFyZWEtbG9nby1mb290ZXJcIj5cclxuXHRcdFx0XHRcdFx0XHQ8aW1nIGNsYXNzTmFtZT1cImxvZ28tZm9vdGVyXCIgc3JjPVwiaHR0cHM6Ly9kYjdxeHQ3eHhscTVtLmNsb3VkZnJvbnQubmV0L2ZpdC1pbi8weDAvZmlsdGVyczpmaWxsKGZmZmZmZikvZmlsdGVyczpxdWFsaXR5KDgwKS9uNDlzaG9wdjJfdHJpam9pYS9pbWFnZXMvZm9vdGVyc3BhL2xvZ28tZm9vdGVyLXRyaWpvaWFzaG9wLnBuZ1wiIC8+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJzb2NpYWwtYXJlYVwiPlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwic3F1YXJlLXNvY2lhbCBjb2wtc20tMiBiZy13aGl0ZSBtci0yXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8YSBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL2xvamFzVHJpSm9pYVwiIHRhcmdldD1cIl9ibGFua1wiPjxpIGNsYXNzTmFtZT1cImZhYiBmYS1mYWNlYm9vay1mIGNvbG9yMlwiPjwvaT48L2E+XHJcblx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwic3F1YXJlLXNvY2lhbCBjb2wtc20tMiBiZy13aGl0ZVwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0PGEgaHJlZj1cImh0dHBzOi8vd3d3Lmluc3RhZ3JhbS5jb20vdHJpam9pYWRpZ2l0YWxcIiB0YXJnZXQ9XCJfYmxhbmtcIj48aSBjbGFzc05hbWU9XCJmYWIgZmEtaW5zdGFncmFtIGNvbG9yMlwiPjwvaT4gPC9hPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJ0YW1hbmhvLXRhYmxldCBjb2wtbWQtMyBjb2wtc20tMTIgdi10b3AgY29sdW1uLWxpbmtzIGNvbHVtbi1sazFcIj5cclxuXHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdChibG9ja0Zvb3RlcjEgIT0gbnVsbCkgP1xyXG5cdFx0XHRcdFx0XHRcdFx0PD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBkYW5nZXJvdXNseVNldElubmVySFRNTD17eyBfX2h0bWw6IGRlY29kZUh0bWwoYmxvY2tGb290ZXIxLmNvbnRldWRvKSB9fSAvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dWwgY2xhc3M9XCJtZW51Zm9vdGVyXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0KGJsb2NrRm9vdGVyMS5saW5rcyAhPSBudWxsICYmIGJsb2NrRm9vdGVyMS5saW5rcy5sZW5ndGgpID9cdFx0XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRibG9ja0Zvb3RlcjEubGlua3MubWFwKChpdGVtLCBrZXkpID0+IFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdChpdGVtLnRpcG9fcGFnaW5hID09ICdpbmZvcm1hY29lcycpID9cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxsaT48YSBocmVmPXtcIi9cIitpdGVtLnBhZ2luYStcIi9pXCJ9IGNsYXNzTmFtZT1cImZvb3Rlci1uYXYtbGlua1wiPntpdGVtLmxhYmVsfTwvYT48L2xpPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDogKGl0ZW0udGlwb19wYWdpbmEgPT0gJ21pbmhhY29udGEnICYmIGl0ZW0ucGFnaW5hICE9ICdjb250YXRvJykgP1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGxpPjxhIGhyZWY9e1wiL1wiK2l0ZW0ucGFnaW5hfSBjbGFzc05hbWU9XCJmb290ZXItbmF2LWxpbmtcIj57aXRlbS5sYWJlbH08L2E+PC9saT5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ6IChpdGVtLnRpcG9fcGFnaW5hID09ICdsaW5rZGlyZXRvJykgP1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGxpPjxhIGhyZWY9e2l0ZW0ucGFnaW5hfSBjbGFzc05hbWU9XCJmb290ZXItbmF2LWxpbmtcIj57aXRlbS5sYWJlbH08L2E+PC9saT5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ6XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8TGluayBrZXk9e2tleX0gaHJlZj17XCIvXCIraXRlbS5wYWdpbmF9PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8bGk+PGEgY2xhc3NOYW1lPVwiZm9vdGVyLW5hdi1saW5rXCI+e2l0ZW0ubGFiZWx9PC9hPjwvbGk+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8L0xpbms+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdClcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDpcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0bnVsbFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC91bD5cclxuXHRcdFx0XHRcdFx0XHRcdDwvPlx0XHJcblx0XHRcdFx0XHRcdFx0OlxyXG5cdFx0XHRcdFx0XHRcdFx0bnVsbFxyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC1tZC0zIGNvbC1zbS0xMiB2LXRvcCBjb2x1bW4tbGlua3MgY29sdW1uLWxrMlwiPlxyXG5cdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdFx0KGJsb2NrRm9vdGVyMiAhPSBudWxsKSA/XHJcblx0XHRcdFx0XHRcdFx0XHQ8PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXt7IF9faHRtbDogZGVjb2RlSHRtbChibG9ja0Zvb3RlcjIuY29udGV1ZG8pIH19IC8+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDx1bCBjbGFzcz1cIm1lbnVmb290ZXJcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQoYmxvY2tGb290ZXIyLmxpbmtzICE9IG51bGwgJiYgYmxvY2tGb290ZXIyLmxpbmtzLmxlbmd0aCkgP1x0XHRcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGJsb2NrRm9vdGVyMi5saW5rcy5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0KGl0ZW0udGlwb19wYWdpbmEgPT0gJ2luZm9ybWFjb2VzJykgP1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGxpPjxhIGhyZWY9e1wiL1wiK2l0ZW0ucGFnaW5hK1wiL2lcIn0gY2xhc3NOYW1lPVwiZm9vdGVyLW5hdi1saW5rXCI+e2l0ZW0ubGFiZWx9PC9hPjwvbGk+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0OiAoaXRlbS50aXBvX3BhZ2luYSA9PSAnbWluaGFjb250YScgJiYgaXRlbS5wYWdpbmEgIT0gJ2NvbnRhdG8nKSA/XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8bGk+PGEgaHJlZj17XCIvXCIraXRlbS5wYWdpbmF9IGNsYXNzTmFtZT1cImZvb3Rlci1uYXYtbGlua1wiPntpdGVtLmxhYmVsfTwvYT48L2xpPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDogKGl0ZW0udGlwb19wYWdpbmEgPT0gJ2xpbmtkaXJldG8nKSA/XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8bGk+PGEgaHJlZj17aXRlbS5wYWdpbmF9IGNsYXNzTmFtZT1cImZvb3Rlci1uYXYtbGlua1wiPntpdGVtLmxhYmVsfTwvYT48L2xpPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDpcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxMaW5rIGtleT17a2V5fSBocmVmPXtcIi9cIitpdGVtLnBhZ2luYX0+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxsaT48YSBjbGFzc05hbWU9XCJmb290ZXItbmF2LWxpbmtcIj57aXRlbS5sYWJlbH08L2E+PC9saT5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDwvTGluaz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0KVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0OlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRudWxsXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L3VsPlxyXG5cdFx0XHRcdFx0XHRcdFx0PC8+XHRcclxuXHRcdFx0XHRcdFx0XHQ6XHJcblx0XHRcdFx0XHRcdFx0XHRudWxsXHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwiY29sLW1kLTMgY29sLXNtLTEyIHYtdG9wIGNvbHVtbi1saW5rcyBjb2x1bW4tbGszXCI+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwidGl0bGUtYmxvY2tcIj5cclxuXHRcdFx0XHRcdFx0XHQ8aDMgY2xhc3NOYW1lPVwiZm9vdGVyLXRpdGxlLW1lbnVcIj5Db250YXRvPC9oMz5cclxuXHJcblx0XHRcdFx0XHRcdFx0PHA+UnVhIFNhcGlyYW5nYSwgOTU5NSwgSW1pZ3JhbnRlIC0gQ2FtcG8gQm9tPC9wPlxyXG5cclxuXHRcdFx0XHRcdFx0XHQ8cD48c3Ryb25nPkZvbmU6Jm5ic3A7PC9zdHJvbmc+NTEgMzU5ODYxMDA8L3A+XHJcblxyXG5cdFx0XHRcdFx0XHRcdDxwPjxiPldoYXRzQXBwPC9iPiA8YSBocmVmPVwiaHR0cHM6Ly9hcGkud2hhdHNhcHAuY29tL3NlbmQ/cGhvbmU9NTU1MTk5OTQ2MDgzNiZhbXA7dGV4dD0mYW1wO3NvdXJjZT0mYW1wO2RhdGE9JmFtcDthcHBfYWJzZW50PVwiIHRhcmdldD1cIl9ibGFua1wiPig1MSkgOSA5OTQ2IDA4MzY8L2E+PC9wPlxyXG5cclxuXHRcdFx0XHRcdFx0XHQ8cD48YSBocmVmPVwibWFpbHRvOm1hcmtldGluZ0B0cmlqb2lhLmNvbS5iclwiPm1hcmtldGluZ0B0cmlqb2lhLmNvbS5icjwvYT48L3A+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0PC9kaXY+XHJcblxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93IGJhc2UtZm9vdGVyXCI+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInRhbWFuaG8tdGFibGV0IGNvbC1tZC00IGNvbC1zbS0xMiB2LW1pZGRsZSBpbWctZm9vdGVyLTFcIj5cclxuXHRcdFx0XHRcdFx0PGRpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInRpdGxlLWJsb2NrXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8aDMgY2xhc3NOYW1lPVwiZm9vdGVyLXRpdGxlLW1lbnVcIj5TZWd1cmFuw6dhPC9oMz5cclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbnRlbnQtYmxvY2tcIj5cclxuXHRcdFx0XHRcdFx0XHRcdDx1bCBjbGFzc05hbWU9XCJkLWZsZXhcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGxpPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxpbWcgY2xhc3NOYW1lPVwicHItMlwiIHNyYz1cImh0dHBzOi8vZGI3cXh0N3h4bHE1bS5jbG91ZGZyb250Lm5ldC9maXQtaW4vMHgwL2ZpbHRlcnM6ZmlsbChmZmZmZmYpL2ZpbHRlcnM6cXVhbGl0eSg4MCkvbjQ5c2hvcHYyX3RyaWpvaWEvaW1hZ2VzL2Zvb3RlcnNwYS9zZWxvcy5qcGdcIiAvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2xpPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8bGk+Jm5ic3A7PC9saT5cclxuXHRcdFx0XHRcdFx0XHRcdDwvdWw+XHJcblx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInRhbWFuaG8tdGFibGV0IGNvbC1tZC00IGNvbC1zbS0xMiB2LW1pZGRsZVwiPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInBheW1lbnRBcmVhXCI+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJ0aXRsZS1ibG9ja1wiPlxyXG5cdFx0XHRcdFx0XHRcdFx0PGgzIGNsYXNzTmFtZT1cImZvb3Rlci10aXRsZS1tZW51XCI+Rm9ybWFzIGRlIFBhZ2FtZW50bzwvaDM+XHJcblx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb250ZW50LWJsb2NrXCI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8cD48aW1nIHNyYz1cImh0dHBzOi8vZGI3cXh0N3h4bHE1bS5jbG91ZGZyb250Lm5ldC9maXQtaW4vMHgwL2ZpbHRlcnM6ZmlsbChmZmZmZmYpL2ZpbHRlcnM6cXVhbGl0eSg4MCkvbjQ5c2hvcHYyX3RyaWpvaWEvaW1hZ2VzL2Zvb3RlcnNwYS9mb3JtYXMtcGFnYW1lbnRvLXRyaWpvaWEucG5nXCIgLz48L3A+XHJcblx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC1tZC00IGNvbC1zbS0xMiB2LW1pZGRsZSBib3gtZGV2ZWxvcGVyc1wiPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInBheW1lbnRBcmVhXCI+XHJcblx0XHRcdFx0XHRcdFx0PHA+PGEgaHJlZj1cImh0dHBzOi8vd3d3Lm40OS5jb20uYnJcIiB0YXJnZXQ9XCJfYmxhbmtcIj48aW1nIHNyYz1cIi9jYXRhbG9nL3ZpZXcvdGhlbWUvaW5jbHVkZXMvbGF5b3V0cy9pbWFnZXMvbjQ5LXBsYXRhZm9ybWEtZWNvbW1lcmNlLnBuZ1wiIC8+PC9hPiA8YSBjbGFzc05hbWU9XCJicmFcIiBocmVmPVwiaHR0cDovL3d3dy5icmFkaWdpdGFsLmNvbS5ici8/Z2NsaWQ9Q2owS0NRanc3Tmo1QlJDWkFSSXNBQnd4REtMVEZxZ0NqSGlmTW04b2NoN1JRMzdVXzVwLTNZbmpJR3Jzc05lbTVNNks4eXJubTl0WE5fTWFBaTA0RUFMd193Y0JcIiB0YXJnZXQ9XCJfYmxhbmtcIj48aW1nIHNyYz1cImh0dHBzOi8vMWFiNjI5MGYxMDJiMDI4NC5jZG4uZ29jYWNoZS5uZXQvbjQ5c2hvcHYyX3RyaWpvaWEvaW1hZ2VzL2Zvb3RlcnNwYS9sb2dvLWJyYS5wbmdcIiAvPjwvYT48L3A+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInN0b3JlSW5mb1wiPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImZvb3Rlci1kZXNjIHQtY2VudGVyXCI+XHJcblx0XHRcdFx0XHRcdFx0PHA+Q1dEIEdlc3RhbyBEaWdpdGFsIENvbSBkZSBQcm9kIMOTcHRpY29zIEAgMjAyMCAtIFRvZG9zIG9zIGRpcmVpdG9zIHJlc2VydmFkb3MgLSBDTlBKIDM2LjU3Mi40MzUvMDAwMSA1OCBPZmVydGFzIHbDoWxpZGFzIGVucXVhbnRvIGR1cmFyZW0gbm9zc29zIGVzdG9xdWVzIC0gVmVuZGFzIHN1amVpdGFzIMOgIGFuw6FsaXNlIGUgY29uZmlybWHDp8OjbyBkZSBkYWRvcyBwZWxhIGVtcHJlc2EgLSBPcyBwcmXDp29zLCBwcm9tb8Onw7VlcyBlIGNvbmRpw6fDtWVzIGRlIHBhZ2FtZW50byBzw6NvIHbDoWxpZG9zIGV4Y2x1c2l2YW1lbnRlIHBhcmEgY29tcHJhcyBlZmV0dWFkYXMgZW0gbm9zc2EgbG9qYSB2aXJ0dWFsLjwvcD5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHQ8L2Rpdj5cclxuPC9mb290ZXI+XHJcbiAgKTtcclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9