webpackHotUpdate_N_E("pages/[slug]",{

/***/ "./pages/[slug].js":
/*!*************************!*\
  !*** ./pages/[slug].js ***!
  \*************************/
/*! exports provided: __N_SSG, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__N_SSG", function() { return __N_SSG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Product; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/error */ "./node_modules/next/error.js");
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_error__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_image_magnify__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-image-magnify */ "./node_modules/react-image-magnify/dist/es/ReactImageMagnify.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_container__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/components/container */ "./components/container.js");
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @/components/layout */ "./components/layout.js");





var _jsxFileName = "C:\\Users\\sauln49\\Desktop\\nextjs\\pages\\[slug].js",
    _s3 = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }










var idStoreApp = 'n49shopv2_trijoia';
var TemplateHeader = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c = function _c() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/header$")("./" + idStoreApp + "/components/header");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/header$").resolve("./" + idStoreApp + "/components/header"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/header']
  }
});
_c2 = TemplateHeader;
var TemplateFooter = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c3 = function _c3() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/footer$")("./" + idStoreApp + "/components/footer");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/footer$").resolve("./" + idStoreApp + "/components/footer"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/footer']
  }
});
_c4 = TemplateFooter;
var sliderThumbs = {
  infinite: false,
  vertical: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  speed: 500,
  responsive: [{
    breakpoint: 415,
    settings: {
      vertical: false,
      slidesToShow: 3
    }
  }]
};
var settingsMobileSlide = {
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  dots: true
};
var __N_SSG = true;
function Product(_ref) {
  _s3();

  var _s = $RefreshSig$(),
      _s2 = $RefreshSig$(),
      _this = this;

  var configs = _ref.configs,
      mainMenu = _ref.mainMenu,
      dadosProduto = _ref.dadosProduto;
  var initialImages = [];

  function getInitialImage(dadosProduto) {
    var imgInicial = [];
    var tempImgInicial = [];

    for (var i in dadosProduto.images) {
      tempImgInicial.push([i, dadosProduto.images[i]]);
    }

    var initialKey = dadosProduto.image.substring(dadosProduto.image.lastIndexOf('/') + 1);

    if (dadosProduto.images != undefined && dadosProduto.images[initialKey] != null) {
      imgInicial['show'] = dadosProduto.images[initialKey].show;
      imgInicial['popup'] = dadosProduto.images[initialKey].popup;
      imgInicial['thumb'] = dadosProduto.images[initialKey].thumb;
    } else if (tempImgInicial[0] != null) {
      imgInicial = tempImgInicial[0][1];
    }

    return imgInicial;
  }

  if (dadosProduto) {
    initialImages = getInitialImage(dadosProduto);
  }

  var selectedQtdParcel = 1;
  var selectedValParcel = 1;
  var quantityBuy = 1;
  var shippingMethods = null;
  var selectedOptionBuy = 1;

  function getSpecialValue(dadosProduto) {
    _s();

    var initialSpecialValue = null;

    if (dadosProduto != undefined) {
      if (parseInt(dadosProduto.has_option) == 0) {
        // PRODUTO SEM OPCOES
        if (dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        } else {
          initialSpecialValue = dadosProduto.price;
        }
      } else {
        if (initialSpecialValue == null && dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        }
      }
    }

    var specialValue = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(initialSpecialValue);
    return specialValue;
  }

  _s(getSpecialValue, "uOCI/u7Rhc0OEPJW7t3Rh1DIVCE=");

  var _getSpecialValue = getSpecialValue(dadosProduto),
      _getSpecialValue2 = Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_getSpecialValue, 2),
      specialValue = _getSpecialValue2[0],
      setSpecialValue = _getSpecialValue2[1];

  function getMainValue(dadosProduto) {
    _s2();

    var initialMainValue = null;

    if (dadosProduto != undefined) {
      if (parseInt(dadosProduto.has_option) == 0) {
        // PRODUTO SEM OPCOES
        if (dadosProduto.special) {
          initialMainValue = dadosProduto.price;
        } else {
          initialMainValue = null;
        }
      } else {
        if (initialMainValue == null && dadosProduto.price) {
          initialMainValue = dadosProduto.price;
        }
      }
    }

    var mainValue = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(initialMainValue);
    return mainValue;
  }

  _s2(getMainValue, "lLKpnft/kQh7/d03zECM13mtmpA=");

  var _getMainValue = getMainValue(dadosProduto),
      _getMainValue2 = Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_getMainValue, 2),
      mainValue = _getMainValue2[0],
      setMainValue = _getMainValue2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(function () {
    var initialState = 0;
    return initialState;
  }),
      soldOutStock = _useState[0],
      setSoldOutStock = _useState[1];
  /*if(parseInt(dadosProduto.has_option) == 0){ // PRODUTO SEM OPCOES
      
    const valorAtual = dadosProduto.price
    if(dadosProduto.special){
      setSelectedMainValue(valorAtual);
      setSelectedSpecialValue(dadosProduto.special);
    }else{
      setSelectedMainValue(null);
      setSelectedSpecialValue(valorAtual);
    }
      if(!dadosProduto.sold_out){
      setSoldOutStock(0);
    }else{
      setSoldOutStock(1);
    }
  }else{ // PRODUTO COM OPCOES
    setSoldOutStock(0);
        if(selectedMainValue == null){
        setSelectedMainValue(dadosProduto.price)
      }
      if(selectedSpecialValue == null && dadosProduto.special){
        setSelectedSpecialValue(dadosProduto.special)
      }*/

  /*for(var i in dadosProduto.options){
    grupoAtual = dadosProduto.options[i]
    for(var opt in grupoAtual.option_value){
      if(grupoAtual.option_value[opt].option_value_id==dadosProduto.opcao_selecionada){
        initialOption = grupoAtual.product_option_id+'_'+grupoAtual.option_value[opt].product_option_value_id
        this.SelectOption(initialOption, null)
      }
    }
  }
        
  }*/


  var changeZoom = function changeZoom(ev) {
    var imgShow = ev.currentTarget.dataset.srcshow;
    var imgPopup = ev.currentTarget.dataset.srcpopup;
    var tempImage = {};
    tempImage['show'] = imgShow;
    tempImage['popup'] = imgPopup; //setInitialImage(tempImage);
  };

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])();

  if (!router.isFallback && !(dadosProduto !== null && dadosProduto !== void 0 && dadosProduto.product_id)) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_error__WEBPACK_IMPORTED_MODULE_4___default.a, {
      statusCode: 404
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 173,
      columnNumber: 12
    }, this);
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_layout__WEBPACK_IMPORTED_MODULE_11__["default"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_container__WEBPACK_IMPORTED_MODULE_10__["default"], {
      children: router.isFallback ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        children: "Loading\u2026"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 179,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_5___default.a, {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
            children: dadosProduto.name
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 183,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "description",
            content: "lelele"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 184,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "og:image",
            content: "lilili"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 185,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 182,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateHeader, {
          configs: configs.resposta,
          mainMenu: mainMenu.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 187,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          clas: "main-content",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "page-products",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "container-padding light-background nproduct-breadcrumb",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "container",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ol", {
                  className: "breadcrumb",
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                      href: "/",
                      title: "P\xE1gina inicial",
                      children: "Home"
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 194,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 193,
                    columnNumber: 24
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: dadosProduto.name
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 196,
                    columnNumber: 24
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 192,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                  className: "cp-preview3",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                    className: "container",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                      className: "nproduct-page",
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-gallery",
                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-thumbnails ",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images-container",
                            children: dadosProduto.images ? Object.keys(dadosProduto.images).length > 4 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "sliderThumbs"
                            }, sliderThumbs), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 218,
                                    columnNumber: 57
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 215,
                                  columnNumber: 53
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 232,
                                      columnNumber: 51
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 233,
                                      columnNumber: 51
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 231,
                                    columnNumber: 49
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 230,
                                  columnNumber: 47
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 211,
                              columnNumber: 39
                            }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 249,
                                    columnNumber: 53
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 246,
                                  columnNumber: 49
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 264,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 265,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 263,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 262,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }, void 0, true) : null
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 207,
                            columnNumber: 32
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 206,
                          columnNumber: 31
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-images min-width-415px",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "areaZoom",
                            children: [dadosProduto.labels.promo_top_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 283,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 282,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_top_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 291,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 290,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 299,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 298,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 307,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 306,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_image_magnify__WEBPACK_IMPORTED_MODULE_8__["default"], _objectSpread({}, {
                              smallImage: {
                                alt: dadosProduto.name,
                                isFluidWidth: true,
                                src: initialImages.show
                              },
                              largeImage: {
                                src: initialImages.popup,
                                width: configs.widthZoom,
                                height: configs.widthZoom
                              }
                            }), void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 313,
                              columnNumber: 37
                            }, this)]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 279,
                            columnNumber: 35
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 278,
                          columnNumber: 33
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "max-width-414px",
                          children: Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 1 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "slideVitrine"
                            }, settingsMobileSlide), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 336,
                                    columnNumber: 49
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 335,
                                  columnNumber: 47
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 345,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 346,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 344,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 343,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 332,
                              columnNumber: 37
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 331,
                            columnNumber: 35
                          }, this) : Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                            children: [Object.keys(dadosProduto.images).map(function (item, key) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                  src: dadosProduto.images[item].popup,
                                  alt: dadosProduto.name
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 360,
                                  columnNumber: 45
                                }, _this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 359,
                                columnNumber: 43
                              }, _this);
                            }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom item-video",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  onClick: "",
                                  "data-videoid": itemVideo.id_video,
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 369,
                                    columnNumber: 49
                                  }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-youtube-play",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 370,
                                    columnNumber: 49
                                  }, _this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 368,
                                  columnNumber: 47
                                }, _this)
                              }, keyVideo, false, {
                                fileName: _jsxFileName,
                                lineNumber: 367,
                                columnNumber: 45
                              }, _this);
                            }) : null]
                          }, void 0, true) : null
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 329,
                          columnNumber: 31
                        }, this)]
                      }, void 0, true, {
                        fileName: _jsxFileName,
                        lineNumber: 205,
                        columnNumber: 29
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-info",
                        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-actions container-padding container-padding-top",
                          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "nproduct-header",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
                              className: "nproduct-title",
                              children: dadosProduto.name
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 386,
                              columnNumber: 38
                            }, this), dadosProduto.model ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              className: "infosProduct",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "titleInfo",
                                  children: "REF:"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 390,
                                  columnNumber: 49
                                }, this), " ", dadosProduto.model]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 390,
                                columnNumber: 45
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 389,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "rateBox",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                className: "lk-avaliar",
                                onClick: "",
                                children: "Avaliar agora"
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 397,
                                columnNumber: 41
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 396,
                              columnNumber: 38
                            }, this), dadosProduto["short"] != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "infosArea",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "resumeProduct",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  dangerouslySetInnerHTML: {
                                    __html: dadosProduto["short"]
                                  }
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 404,
                                  columnNumber: 43
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 403,
                                columnNumber: 42
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 402,
                              columnNumber: 41
                            }, this) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 385,
                            columnNumber: 35
                          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "buyArea",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "nprodct-price",
                                children: [specialValue ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "oldPrice",
                                  children: ["De ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "nproduct-price-max",
                                    children: getMainValue(dadosProduto)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 419,
                                    columnNumber: 50
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 418,
                                  columnNumber: 45
                                }, this) : '', /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "nproduct-price-value",
                                  children: specialValue ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                                    children: ["Por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "specialValue",
                                      children: getSpecialValue(dadosProduto)
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 427,
                                      columnNumber: 53
                                    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "item-discount",
                                      children: dadosProduto.discount_percent
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 428,
                                      columnNumber: 48
                                    }, this)]
                                  }, void 0, true) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                    children: ["por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      children: mainValue
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 431,
                                      columnNumber: 54
                                    }, this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 431,
                                    columnNumber: 47
                                  }, this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 424,
                                  columnNumber: 43
                                }, this), selectedQtdParcel != '' && selectedValParcel != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                  className: "selectedParcel",
                                  children: ["Ou ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "numParc",
                                    children: [selectedQtdParcel, "x"]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 436,
                                    columnNumber: 78
                                  }, this), " de ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "valParc",
                                    children: selectedValParcel
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 436,
                                    columnNumber: 135
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 436,
                                  columnNumber: 45
                                }, this) : null]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 416,
                                columnNumber: 39
                              }, this), dadosProduto != '' ? parseInt(dadosProduto.has_option) != 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "optionsArea",
                                children: dadosProduto.options.map(function (item, key) {
                                  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                    className: "box-option",
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                      children: item.name
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 449,
                                      columnNumber: 53
                                    }, _this), item.type == 'text' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                      className: "txtOption",
                                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                        id: "fieldOption",
                                        maxlength: "3",
                                        type: "text",
                                        "data-group": item.product_option_id,
                                        className: "field",
                                        name: "txt-option"
                                      }, void 0, false, {
                                        fileName: _jsxFileName,
                                        lineNumber: 453,
                                        columnNumber: 57
                                      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                        className: "help",
                                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                          className: "fa fa-question-circle color2",
                                          "aria-hidden": "true"
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 455,
                                          columnNumber: 61
                                        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                          className: "infoHelp",
                                          children: "Insira at\xE9 3 letras para personalizar a camisa com um bordado exclusivo."
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 457,
                                          columnNumber: 61
                                        }, _this)]
                                      }, void 0, true, {
                                        fileName: _jsxFileName,
                                        lineNumber: 454,
                                        columnNumber: 57
                                      }, _this)]
                                    }, void 0, true, {
                                      fileName: _jsxFileName,
                                      lineNumber: 452,
                                      columnNumber: 55
                                    }, _this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                                      className: "listOptions",
                                      children: item.option_value.map(function (itemOption, key) {
                                        return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                            className: selectedOptionBuy !== item.product_option_id + '_' + itemOption.product_option_value_id ? 'option' : 'option selected',
                                            onClick: "",
                                            href: "javascript:;",
                                            children: itemOption.name
                                          }, void 0, false, {
                                            fileName: _jsxFileName,
                                            lineNumber: 467,
                                            columnNumber: 63
                                          }, _this)
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 466,
                                          columnNumber: 61
                                        }, _this);
                                      })
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 463,
                                      columnNumber: 55
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 448,
                                    columnNumber: 51
                                  }, _this);
                                })
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 445,
                                columnNumber: 44
                              }, this) : '' : '']
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 415,
                              columnNumber: 37
                            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "quantityArea",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                  children: "Quantidade"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 485,
                                  columnNumber: 44
                                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "buttonsQuantity",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnLess",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-minus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 487,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 487,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                    id: "txtQuantity",
                                    type: "text",
                                    name: "txt-quantity",
                                    value: quantityBuy,
                                    className: "txtQuantity"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 488,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnMore",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-plus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 489,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 489,
                                    columnNumber: 47
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 486,
                                  columnNumber: 44
                                }, this)]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 484,
                                columnNumber: 41
                              }, this), soldOutStock != null && !soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton btn_buy",
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fas fa fa-shopping-cart"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 496,
                                    columnNumber: 52
                                  }, this), " ", 'Comprar']
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 495,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 494,
                                columnNumber: 43
                              }, this) : soldOutStock != null && soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton notifyButton",
                                  "data-productid": dadosProduto.product_id,
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-envelope",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 502,
                                    columnNumber: 52
                                  }, this), " Avise-me"]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 501,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 500,
                                columnNumber: 43
                              }, this) : null]
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 483,
                              columnNumber: 39
                            }, this), dadosProduto.text_prevenda != null && dadosProduto.text_prevenda != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                className: "checkPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                  id: "ckprevenda",
                                  type: "checkbox",
                                  onChange: ""
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 512,
                                  columnNumber: 70
                                }, this), " Concordo com o prazo de entrega descrito abaixo."]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 512,
                                columnNumber: 41
                              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "infoPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
                                  className: "tit",
                                  children: "TERMO DE ACEITA\xC7\xC3O"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 514,
                                  columnNumber: 43
                                }, this), dadosProduto.text_prevenda]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 513,
                                columnNumber: 41
                              }, this)]
                            }, void 0, true) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 413,
                            columnNumber: 35
                          }, this), dadosProduto.guia_medidas ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "guiasMedida",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              children: dadosProduto.guia_medidas.map(function (itemGuia, keyGuia) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                  className: "itemGuia",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    className: "color2",
                                    title: itemGuia.title,
                                    "data-tituloguia": "",
                                    "data-conteudoguia": "",
                                    onClick: "",
                                    children: itemGuia.title
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 530,
                                    columnNumber: 47
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 529,
                                  columnNumber: 43
                                }, _this);
                              })
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 526,
                              columnNumber: 39
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 525,
                            columnNumber: 37
                          }, this) : null]
                        }, void 0, true, {
                          fileName: _jsxFileName,
                          lineNumber: 384,
                          columnNumber: 33
                        }, this)
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 383,
                        columnNumber: 29
                      }, this)]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 203,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 202,
                    columnNumber: 23
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 201,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 191,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 190,
              columnNumber: 17
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 189,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 188,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateFooter, {
          configs: configs.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 555,
          columnNumber: 13
        }, this)]
      }, void 0, true)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 177,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 176,
    columnNumber: 5
  }, this);
}

_s3(Product, "9ivUjH6RR1GoW/oyO3lSZCyjhfw=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"]];
});

_c5 = Product;
;

var _c, _c2, _c3, _c4, _c5;

$RefreshReg$(_c, "TemplateHeader$dynamic");
$RefreshReg$(_c2, "TemplateHeader");
$RefreshReg$(_c3, "TemplateFooter$dynamic");
$RefreshReg$(_c4, "TemplateFooter");
$RefreshReg$(_c5, "Product");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvW3NsdWddLmpzIl0sIm5hbWVzIjpbImlkU3RvcmVBcHAiLCJUZW1wbGF0ZUhlYWRlciIsImR5bmFtaWMiLCJUZW1wbGF0ZUZvb3RlciIsInNsaWRlclRodW1icyIsImluZmluaXRlIiwidmVydGljYWwiLCJzbGlkZXNUb1Nob3ciLCJzbGlkZXNUb1Njcm9sbCIsInNwZWVkIiwicmVzcG9uc2l2ZSIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsInNldHRpbmdzTW9iaWxlU2xpZGUiLCJkb3RzIiwiUHJvZHVjdCIsImNvbmZpZ3MiLCJtYWluTWVudSIsImRhZG9zUHJvZHV0byIsImluaXRpYWxJbWFnZXMiLCJnZXRJbml0aWFsSW1hZ2UiLCJpbWdJbmljaWFsIiwidGVtcEltZ0luaWNpYWwiLCJpIiwiaW1hZ2VzIiwicHVzaCIsImluaXRpYWxLZXkiLCJpbWFnZSIsInN1YnN0cmluZyIsImxhc3RJbmRleE9mIiwidW5kZWZpbmVkIiwic2hvdyIsInBvcHVwIiwidGh1bWIiLCJzZWxlY3RlZFF0ZFBhcmNlbCIsInNlbGVjdGVkVmFsUGFyY2VsIiwicXVhbnRpdHlCdXkiLCJzaGlwcGluZ01ldGhvZHMiLCJzZWxlY3RlZE9wdGlvbkJ1eSIsImdldFNwZWNpYWxWYWx1ZSIsImluaXRpYWxTcGVjaWFsVmFsdWUiLCJwYXJzZUludCIsImhhc19vcHRpb24iLCJzcGVjaWFsIiwicHJpY2UiLCJzcGVjaWFsVmFsdWUiLCJ1c2VTdGF0ZSIsInNldFNwZWNpYWxWYWx1ZSIsImdldE1haW5WYWx1ZSIsImluaXRpYWxNYWluVmFsdWUiLCJtYWluVmFsdWUiLCJzZXRNYWluVmFsdWUiLCJpbml0aWFsU3RhdGUiLCJzb2xkT3V0U3RvY2siLCJzZXRTb2xkT3V0U3RvY2siLCJjaGFuZ2Vab29tIiwiZXYiLCJpbWdTaG93IiwiY3VycmVudFRhcmdldCIsImRhdGFzZXQiLCJzcmNzaG93IiwiaW1nUG9wdXAiLCJzcmNwb3B1cCIsInRlbXBJbWFnZSIsInJvdXRlciIsInVzZVJvdXRlciIsImlzRmFsbGJhY2siLCJwcm9kdWN0X2lkIiwibmFtZSIsInJlc3Bvc3RhIiwiT2JqZWN0Iiwia2V5cyIsImxlbmd0aCIsIm1hcCIsIml0ZW0iLCJrZXkiLCJ2aWRlb3MiLCJpdGVtVmlkZW8iLCJrZXlWaWRlbyIsImlkX3ZpZGVvIiwibGFiZWxzIiwicHJvbW9fdG9wX2xlZnQiLCJwcm9tb190b3BfcmlnaHQiLCJwcm9tb19ib3R0b21fbGVmdCIsInByb21vX2JvdHRvbV9yaWdodCIsInNtYWxsSW1hZ2UiLCJhbHQiLCJpc0ZsdWlkV2lkdGgiLCJzcmMiLCJsYXJnZUltYWdlIiwid2lkdGgiLCJ3aWR0aFpvb20iLCJoZWlnaHQiLCJtb2RlbCIsIl9faHRtbCIsImRpc2NvdW50X3BlcmNlbnQiLCJvcHRpb25zIiwidHlwZSIsInByb2R1Y3Rfb3B0aW9uX2lkIiwib3B0aW9uX3ZhbHVlIiwiaXRlbU9wdGlvbiIsInByb2R1Y3Rfb3B0aW9uX3ZhbHVlX2lkIiwidGV4dF9wcmV2ZW5kYSIsImd1aWFfbWVkaWRhcyIsIml0ZW1HdWlhIiwia2V5R3VpYSIsInRpdGxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUdBLElBQUlBLFVBQVUsR0FBRyxtQkFBakI7QUFDQSxJQUFJQyxjQUFjLEdBQUdDLG1EQUFPLE1BQUM7QUFBQSxTQUFNLDhGQUFPLElBQXlCLEdBQUNGLFVBQTFCLEdBQXFDLG9CQUE1QyxDQUFOO0FBQUEsQ0FBRDtBQUFBO0FBQUE7QUFBQSxrQ0FBYywwR0FBeUIsR0FBQ0EsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxjQUFjLDRCQUEwQkEsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxFQUE1QjtNQUFJQyxjO0FBQ0osSUFBSUUsY0FBYyxHQUFHRCxtREFBTyxPQUFDO0FBQUEsU0FBTSw4RkFBTyxJQUF5QixHQUFDRixVQUExQixHQUFxQyxvQkFBNUMsQ0FBTjtBQUFBLENBQUQ7QUFBQTtBQUFBO0FBQUEsa0NBQWMsMEdBQXlCLEdBQUNBLFVBQTFCLEdBQXFDLG9CQUFuRDtBQUFBO0FBQUEsY0FBYyw0QkFBMEJBLFVBQTFCLEdBQXFDLG9CQUFuRDtBQUFBO0FBQUEsRUFBNUI7TUFBSUcsYztBQUVKLElBQUlDLFlBQVksR0FBRztBQUNqQkMsVUFBUSxFQUFFLEtBRE87QUFFakJDLFVBQVEsRUFBRSxJQUZPO0FBR2pCQyxjQUFZLEVBQUUsQ0FIRztBQUlqQkMsZ0JBQWMsRUFBRSxDQUpDO0FBS2pCQyxPQUFLLEVBQUUsR0FMVTtBQU1qQkMsWUFBVSxFQUFFLENBQ1Y7QUFDRUMsY0FBVSxFQUFFLEdBRGQ7QUFFRUMsWUFBUSxFQUFFO0FBQ1JOLGNBQVEsRUFBRSxLQURGO0FBRVJDLGtCQUFZLEVBQUU7QUFGTjtBQUZaLEdBRFU7QUFOSyxDQUFuQjtBQWlCQSxJQUFJTSxtQkFBbUIsR0FBRztBQUN4Qk4sY0FBWSxFQUFFLENBRFU7QUFFeEJDLGdCQUFjLEVBQUUsQ0FGUTtBQUd4QkMsT0FBSyxFQUFFLEdBSGlCO0FBSXhCSyxNQUFJLEVBQUU7QUFKa0IsQ0FBMUI7O0FBT2UsU0FBU0MsT0FBVCxPQUFzRDtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQSxNQUFuQ0MsT0FBbUMsUUFBbkNBLE9BQW1DO0FBQUEsTUFBMUJDLFFBQTBCLFFBQTFCQSxRQUEwQjtBQUFBLE1BQWhCQyxZQUFnQixRQUFoQkEsWUFBZ0I7QUFDbkUsTUFBSUMsYUFBYSxHQUFHLEVBQXBCOztBQUNBLFdBQVNDLGVBQVQsQ0FBeUJGLFlBQXpCLEVBQXVDO0FBRXJDLFFBQUlHLFVBQVUsR0FBRyxFQUFqQjtBQUNBLFFBQUlDLGNBQWMsR0FBRyxFQUFyQjs7QUFFQSxTQUFJLElBQUlDLENBQVIsSUFBYUwsWUFBWSxDQUFDTSxNQUExQixFQUFpQztBQUMvQkYsb0JBQWMsQ0FBQ0csSUFBZixDQUFvQixDQUFDRixDQUFELEVBQUlMLFlBQVksQ0FBQ00sTUFBYixDQUFxQkQsQ0FBckIsQ0FBSixDQUFwQjtBQUNEOztBQUVELFFBQUlHLFVBQVUsR0FBR1IsWUFBWSxDQUFDUyxLQUFiLENBQW1CQyxTQUFuQixDQUE2QlYsWUFBWSxDQUFDUyxLQUFiLENBQW1CRSxXQUFuQixDQUErQixHQUEvQixJQUFvQyxDQUFqRSxDQUFqQjs7QUFFQSxRQUFHWCxZQUFZLENBQUNNLE1BQWIsSUFBdUJNLFNBQXZCLElBQW9DWixZQUFZLENBQUNNLE1BQWIsQ0FBb0JFLFVBQXBCLEtBQW1DLElBQTFFLEVBQStFO0FBQzdFTCxnQkFBVSxDQUFDLE1BQUQsQ0FBVixHQUFxQkgsWUFBWSxDQUFDTSxNQUFiLENBQW9CRSxVQUFwQixFQUFnQ0ssSUFBckQ7QUFDQVYsZ0JBQVUsQ0FBQyxPQUFELENBQVYsR0FBc0JILFlBQVksQ0FBQ00sTUFBYixDQUFvQkUsVUFBcEIsRUFBZ0NNLEtBQXREO0FBQ0FYLGdCQUFVLENBQUMsT0FBRCxDQUFWLEdBQXNCSCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JFLFVBQXBCLEVBQWdDTyxLQUF0RDtBQUNELEtBSkQsTUFJTSxJQUFHWCxjQUFjLENBQUMsQ0FBRCxDQUFkLElBQXFCLElBQXhCLEVBQTZCO0FBQ2pDRCxnQkFBVSxHQUFHQyxjQUFjLENBQUMsQ0FBRCxDQUFkLENBQWtCLENBQWxCLENBQWI7QUFDRDs7QUFFRCxXQUFPRCxVQUFQO0FBQ0Q7O0FBQ0QsTUFBR0gsWUFBSCxFQUFnQjtBQUNkQyxpQkFBYSxHQUFHQyxlQUFlLENBQUNGLFlBQUQsQ0FBL0I7QUFDRDs7QUFFRCxNQUFJZ0IsaUJBQWlCLEdBQUcsQ0FBeEI7QUFDQSxNQUFJQyxpQkFBaUIsR0FBRyxDQUF4QjtBQUNBLE1BQUlDLFdBQVcsR0FBRyxDQUFsQjtBQUNBLE1BQUlDLGVBQWUsR0FBRyxJQUF0QjtBQUNBLE1BQUlDLGlCQUFpQixHQUFHLENBQXhCOztBQUVBLFdBQVNDLGVBQVQsQ0FBeUJyQixZQUF6QixFQUF1QztBQUFBOztBQUNyQyxRQUFJc0IsbUJBQW1CLEdBQUcsSUFBMUI7O0FBQ0EsUUFBR3RCLFlBQVksSUFBSVksU0FBbkIsRUFBNkI7QUFDM0IsVUFBR1csUUFBUSxDQUFDdkIsWUFBWSxDQUFDd0IsVUFBZCxDQUFSLElBQXFDLENBQXhDLEVBQTBDO0FBQUU7QUFDMUMsWUFBR3hCLFlBQVksQ0FBQ3lCLE9BQWhCLEVBQXdCO0FBQ3RCSCw2QkFBbUIsR0FBR3RCLFlBQVksQ0FBQ3lCLE9BQW5DO0FBQ0QsU0FGRCxNQUVLO0FBQ0hILDZCQUFtQixHQUFHdEIsWUFBWSxDQUFDMEIsS0FBbkM7QUFDRDtBQUNGLE9BTkQsTUFNSztBQUNILFlBQUdKLG1CQUFtQixJQUFJLElBQXZCLElBQStCdEIsWUFBWSxDQUFDeUIsT0FBL0MsRUFBdUQ7QUFDckRILDZCQUFtQixHQUFHdEIsWUFBWSxDQUFDeUIsT0FBbkM7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsUUFBTUUsWUFBWSxHQUFHQyxzREFBUSxDQUFDTixtQkFBRCxDQUE3QjtBQUNBLFdBQU9LLFlBQVA7QUFDRDs7QUFuRGtFLEtBaUMxRE4sZUFqQzBEOztBQUFBLHlCQW9EM0JBLGVBQWUsQ0FBQ3JCLFlBQUQsQ0FwRFk7QUFBQTtBQUFBLE1Bb0Q1RDJCLFlBcEQ0RDtBQUFBLE1Bb0Q5Q0UsZUFwRDhDOztBQXNEbkUsV0FBU0MsWUFBVCxDQUFzQjlCLFlBQXRCLEVBQW9DO0FBQUE7O0FBQ2xDLFFBQUkrQixnQkFBZ0IsR0FBRyxJQUF2Qjs7QUFDQSxRQUFHL0IsWUFBWSxJQUFJWSxTQUFuQixFQUE2QjtBQUMzQixVQUFHVyxRQUFRLENBQUN2QixZQUFZLENBQUN3QixVQUFkLENBQVIsSUFBcUMsQ0FBeEMsRUFBMEM7QUFBRTtBQUMxQyxZQUFHeEIsWUFBWSxDQUFDeUIsT0FBaEIsRUFBd0I7QUFDdEJNLDBCQUFnQixHQUFHL0IsWUFBWSxDQUFDMEIsS0FBaEM7QUFDRCxTQUZELE1BRUs7QUFDSEssMEJBQWdCLEdBQUcsSUFBbkI7QUFDRDtBQUNGLE9BTkQsTUFNSztBQUNILFlBQUdBLGdCQUFnQixJQUFJLElBQXBCLElBQTRCL0IsWUFBWSxDQUFDMEIsS0FBNUMsRUFBa0Q7QUFDaERLLDBCQUFnQixHQUFHL0IsWUFBWSxDQUFDMEIsS0FBaEM7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsUUFBTU0sU0FBUyxHQUFHSixzREFBUSxDQUFDRyxnQkFBRCxDQUExQjtBQUNBLFdBQU9DLFNBQVA7QUFDRDs7QUF4RWtFLE1Bc0QxREYsWUF0RDBEOztBQUFBLHNCQXlFakNBLFlBQVksQ0FBQzlCLFlBQUQsQ0F6RXFCO0FBQUE7QUFBQSxNQXlFNURnQyxTQXpFNEQ7QUFBQSxNQXlFakRDLFlBekVpRDs7QUFBQSxrQkEyRTNCTCxzREFBUSxDQUFDLFlBQU07QUFDckQsUUFBTU0sWUFBWSxHQUFHLENBQXJCO0FBQ0EsV0FBT0EsWUFBUDtBQUNELEdBSCtDLENBM0VtQjtBQUFBLE1BMkU1REMsWUEzRTREO0FBQUEsTUEyRTlDQyxlQTNFOEM7QUFpRm5FO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBSU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUUsTUFBSUMsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ0MsRUFBRCxFQUFRO0FBQ3ZCLFFBQU1DLE9BQU8sR0FBR0QsRUFBRSxDQUFDRSxhQUFILENBQWlCQyxPQUFqQixDQUF5QkMsT0FBekM7QUFDQSxRQUFNQyxRQUFRLEdBQUdMLEVBQUUsQ0FBQ0UsYUFBSCxDQUFpQkMsT0FBakIsQ0FBeUJHLFFBQTFDO0FBQ0EsUUFBSUMsU0FBUyxHQUFHLEVBQWhCO0FBQ0FBLGFBQVMsQ0FBQyxNQUFELENBQVQsR0FBb0JOLE9BQXBCO0FBQ0FNLGFBQVMsQ0FBQyxPQUFELENBQVQsR0FBcUJGLFFBQXJCLENBTHVCLENBTXZCO0FBRUQsR0FSRDs7QUFXQSxNQUFNRyxNQUFNLEdBQUdDLDZEQUFTLEVBQXhCOztBQUNBLE1BQUksQ0FBQ0QsTUFBTSxDQUFDRSxVQUFSLElBQXNCLEVBQUNoRCxZQUFELGFBQUNBLFlBQUQsZUFBQ0EsWUFBWSxDQUFFaUQsVUFBZixDQUExQixFQUFxRDtBQUNuRCx3QkFBTyxxRUFBQyxpREFBRDtBQUFXLGdCQUFVLEVBQUU7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUFQO0FBQ0Q7O0FBQ0Qsc0JBQ0UscUVBQUMsMkRBQUQ7QUFBQSwyQkFDRSxxRUFBQyw4REFBRDtBQUFBLGdCQUNHSCxNQUFNLENBQUNFLFVBQVAsZ0JBQ0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERCxnQkFHQztBQUFBLGdDQUNFLHFFQUFDLGdEQUFEO0FBQUEsa0NBQ0U7QUFBQSxzQkFBUWhELFlBQVksQ0FBQ2tEO0FBQXJCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREYsZUFFRTtBQUFNLGdCQUFJLEVBQUMsYUFBWDtBQUF5QixtQkFBTyxFQUFDO0FBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRkYsZUFHRTtBQUFNLGdCQUFJLEVBQUMsVUFBWDtBQUFzQixtQkFBTyxFQUFDO0FBQTlCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBSEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBTUUscUVBQUMsY0FBRDtBQUFnQixpQkFBTyxFQUFFcEQsT0FBTyxDQUFDcUQsUUFBakM7QUFBMkMsa0JBQVEsRUFBRXBELFFBQVEsQ0FBQ29EO0FBQTlEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBTkYsZUFPRTtBQUFLLGNBQUksRUFBQyxjQUFWO0FBQUEsaUNBQ0U7QUFBSyxxQkFBUyxFQUFDLGVBQWY7QUFBQSxtQ0FDRTtBQUFLLHVCQUFTLEVBQUMsd0RBQWY7QUFBQSxxQ0FDRTtBQUFLLHlCQUFTLEVBQUMsV0FBZjtBQUFBLHdDQUNFO0FBQUksMkJBQVMsRUFBQyxZQUFkO0FBQUEsMENBQ0c7QUFBSSw2QkFBUyxFQUFDLGlCQUFkO0FBQUEsMkNBQ0c7QUFBRywwQkFBSSxFQUFDLEdBQVI7QUFBWSwyQkFBSyxFQUFDLG1CQUFsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURIO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBREgsZUFJRztBQUFJLDZCQUFTLEVBQUMsaUJBQWQ7QUFBQSw4QkFDSW5ELFlBQVksQ0FBQ2tEO0FBRGpCO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBSkg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURGLGVBVUU7QUFBSywyQkFBUyxFQUFDLGFBQWY7QUFBQSx5Q0FDRTtBQUFLLDZCQUFTLEVBQUMsV0FBZjtBQUFBLDJDQUNJO0FBQUssK0JBQVMsRUFBQyxlQUFmO0FBQUEsOENBRUU7QUFBSyxpQ0FBUyxFQUFDLGtCQUFmO0FBQUEsZ0RBQ0U7QUFBSyxtQ0FBUyxFQUFDLHFCQUFmO0FBQUEsaURBQ0M7QUFBSyxxQ0FBUyxFQUFDLDBCQUFmO0FBQUEsc0NBRUlsRCxZQUFZLENBQUNNLE1BQWQsR0FDRzhDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZckQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ2dELE1BQWpDLEdBQTBDLENBQTNDLGdCQUNFLHFFQUFDLGtEQUFEO0FBQVEsdUNBQVMsRUFBQztBQUFsQiwrQkFBcUNwRSxZQUFyQztBQUFBLHlDQUdNa0UsTUFBTSxDQUFDQyxJQUFQLENBQVlyRCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDaUQsR0FBakMsQ0FBcUMsVUFBQ0MsSUFBRCxFQUFPQyxHQUFQO0FBQUEsb0RBQzdCO0FBQ0UsMkNBQVMsRUFBQyw2RUFEWjtBQUFBLHlEQUdJO0FBQUssdUNBQUcsRUFBRXpELFlBQVksQ0FBQ00sTUFBYixDQUFvQmtELElBQXBCLEVBQTBCMUMsS0FBcEM7QUFDRSx1Q0FBRyxFQUFFZCxZQUFZLENBQUNrRCxJQURwQjtBQUVFLG9EQUFjbEQsWUFBWSxDQUFDTSxNQUFiLENBQW9Ca0QsSUFBcEIsRUFBMEIzQyxJQUYxQztBQUdFLHFEQUFlYixZQUFZLENBQUNNLE1BQWIsQ0FBb0JrRCxJQUFwQixFQUEwQjFDLEtBSDNDO0FBSUUseUNBQUssRUFBQyxJQUpSO0FBSWEsMENBQU0sRUFBQyxJQUpwQjtBQUtFLDJDQUFPLEVBQUM7QUFMVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSEo7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FENkI7QUFBQSwrQkFBckMsQ0FITixFQWlCS2QsWUFBWSxDQUFDMEQsTUFBYixDQUFvQkosTUFBckIsR0FDRXRELFlBQVksQ0FBQzBELE1BQWIsQ0FBb0JILEdBQXBCLENBQXdCLFVBQUNJLFNBQUQsRUFBWUMsUUFBWjtBQUFBLG9EQUN0QjtBQUFLLDJDQUFTLEVBQUMsWUFBZjtBQUFBLHlEQUNFO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0Msb0RBQWNELFNBQVMsQ0FBQ0UsUUFBMUQ7QUFBQSw0REFDRTtBQUFLLHlDQUFHLHVDQUFnQ0YsU0FBUyxDQUFDRSxRQUExQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREYsZUFFRTtBQUFHLCtDQUFTLEVBQUMsb0JBQWI7QUFBa0MscURBQVk7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixtQ0FBaUNELFFBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRHNCO0FBQUEsK0JBQXhCLENBREYsR0FVRSxJQTNCTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREYsZ0JBaUNFO0FBQUEseUNBRUVSLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZckQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ2lELEdBQWpDLENBQXFDLFVBQUNDLElBQUQsRUFBT0MsR0FBUDtBQUFBLG9EQUM3QjtBQUNFLDJDQUFTLEVBQUMsNkVBRFo7QUFBQSx5REFHSTtBQUFLLHVDQUFHLEVBQUV6RCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JrRCxJQUFwQixFQUEwQjFDLEtBQXBDO0FBQ0UsdUNBQUcsRUFBRWQsWUFBWSxDQUFDa0QsSUFEcEI7QUFFRSxvREFBY2xELFlBQVksQ0FBQ00sTUFBYixDQUFvQmtELElBQXBCLEVBQTBCM0MsSUFGMUM7QUFHRSxxREFBZWIsWUFBWSxDQUFDTSxNQUFiLENBQW9Ca0QsSUFBcEIsRUFBMEIxQyxLQUgzQztBQUlFLHlDQUFLLEVBQUMsSUFKUjtBQUlhLDBDQUFNLEVBQUMsSUFKcEI7QUFLRSwyQ0FBTyxFQUFDO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRDZCO0FBQUEsK0JBQXJDLENBRkYsRUFpQkdkLFlBQVksQ0FBQzBELE1BQWIsQ0FBb0JKLE1BQXJCLEdBQ0V0RCxZQUFZLENBQUMwRCxNQUFiLENBQW9CSCxHQUFwQixDQUF3QixVQUFDSSxTQUFELEVBQVlDLFFBQVo7QUFBQSxvREFDdEI7QUFBSywyQ0FBUyxFQUFDLFlBQWY7QUFBQSx5REFDRTtBQUFHLHdDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBTyxFQUFDLEVBQS9CO0FBQWtDLG9EQUFjRCxTQUFTLENBQUNFLFFBQTFEO0FBQUEsNERBQ0U7QUFBSyx5Q0FBRyx1Q0FBZ0NGLFNBQVMsQ0FBQ0UsUUFBMUM7QUFBUjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQURGLGVBRUU7QUFBRywrQ0FBUyxFQUFDLG9CQUFiO0FBQWtDLHFEQUFZO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREYsbUNBQWlDRCxRQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQURzQjtBQUFBLCtCQUF4QixDQURGLEdBVUUsSUEzQko7QUFBQSw0Q0FsQ0osR0FnRUU7QUFsRUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0NBREYsZUF5RUk7QUFBSyxtQ0FBUyxFQUFDLGdDQUFmO0FBQUEsaURBQ0U7QUFBSyxxQ0FBUyxFQUFDLFVBQWY7QUFBQSx1Q0FFSzVELFlBQVksQ0FBQzhELE1BQWIsQ0FBb0JDLGNBQXBCLENBQW1DdEQsS0FBbkMsSUFBNEMsSUFBN0MsZ0JBQ0U7QUFBSyx1Q0FBUyxFQUFDLHdCQUFmO0FBQUEscURBQ0U7QUFBSyxtQ0FBRyxFQUFFVCxZQUFZLENBQUM4RCxNQUFiLENBQW9CQyxjQUFwQixDQUFtQ3REO0FBQTdDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLEdBS0UsSUFQTixFQVVLVCxZQUFZLENBQUM4RCxNQUFiLENBQW9CRSxlQUFwQixDQUFvQ3ZELEtBQXBDLElBQTZDLElBQTlDLGdCQUNFO0FBQUssdUNBQVMsRUFBQyx5QkFBZjtBQUFBLHFEQUNFO0FBQUssbUNBQUcsRUFBRVQsWUFBWSxDQUFDOEQsTUFBYixDQUFvQkUsZUFBcEIsQ0FBb0N2RDtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixHQUtFLElBZk4sRUFrQktULFlBQVksQ0FBQzhELE1BQWIsQ0FBb0JHLGlCQUFwQixDQUFzQ3hELEtBQXRDLElBQStDLElBQWhELGdCQUNFO0FBQUssdUNBQVMsRUFBQywyQkFBZjtBQUFBLHFEQUNFO0FBQUssbUNBQUcsRUFBRVQsWUFBWSxDQUFDOEQsTUFBYixDQUFvQkcsaUJBQXBCLENBQXNDeEQ7QUFBaEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREYsR0FLRSxJQXZCTixFQTBCS1QsWUFBWSxDQUFDOEQsTUFBYixDQUFvQkksa0JBQXBCLENBQXVDekQsS0FBdkMsSUFBZ0QsSUFBakQsZ0JBQ0U7QUFBSyx1Q0FBUyxFQUFDLDRCQUFmO0FBQUEscURBQ0U7QUFBSyxtQ0FBRyxFQUFFVCxZQUFZLENBQUM4RCxNQUFiLENBQW9CSSxrQkFBcEIsQ0FBdUN6RDtBQUFqRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixHQUtFLElBL0JOLGVBa0NFLHFFQUFDLDJEQUFELG9CQUF1QjtBQUNmMEQsd0NBQVUsRUFBRTtBQUNaQyxtQ0FBRyxFQUFFcEUsWUFBWSxDQUFDa0QsSUFETjtBQUVabUIsNENBQVksRUFBRSxJQUZGO0FBR1pDLG1DQUFHLEVBQUVyRSxhQUFhLENBQUNZO0FBSFAsK0JBREc7QUFNbkIwRCx3Q0FBVSxFQUFFO0FBQ1JELG1DQUFHLEVBQUVyRSxhQUFhLENBQUNhLEtBRFg7QUFFUjBELHFDQUFLLEVBQUUxRSxPQUFPLENBQUMyRSxTQUZQO0FBR1JDLHNDQUFNLEVBQUU1RSxPQUFPLENBQUMyRTtBQUhSO0FBTk8sNkJBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBbENGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0NBekVKLGVBNEhFO0FBQUssbUNBQVMsRUFBQyxpQkFBZjtBQUFBLG9DQUNJckIsTUFBTSxDQUFDQyxJQUFQLENBQVlyRCxZQUFZLENBQUNNLE1BQXpCLEtBQW9DOEMsTUFBTSxDQUFDQyxJQUFQLENBQVlyRCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDZ0QsTUFBakMsR0FBMEMsQ0FBL0UsZ0JBQ0M7QUFBSyxxQ0FBUyxFQUFDLGdCQUFmO0FBQUEsbURBQ0UscUVBQUMsa0RBQUQ7QUFBUSx1Q0FBUyxFQUFDO0FBQWxCLCtCQUFxQzNELG1CQUFyQztBQUFBLHlDQUVNeUQsTUFBTSxDQUFDQyxJQUFQLENBQVlyRCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDaUQsR0FBakMsQ0FBcUMsVUFBQ0MsSUFBRCxFQUFPQyxHQUFQO0FBQUEsb0RBQ2pDO0FBQUssMkNBQVMsRUFBQyxVQUFmO0FBQUEseURBQ0U7QUFBSyx1Q0FBRyxFQUFFekQsWUFBWSxDQUFDTSxNQUFiLENBQW9Ca0QsSUFBcEIsRUFBMEIxQyxLQUFwQztBQUEyQyx1Q0FBRyxFQUFFZCxZQUFZLENBQUNrRDtBQUE3RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FEaUM7QUFBQSwrQkFBckMsQ0FGTixFQVNLbEQsWUFBWSxDQUFDMEQsTUFBYixDQUFvQkosTUFBckIsR0FDRXRELFlBQVksQ0FBQzBELE1BQWIsQ0FBb0JILEdBQXBCLENBQXdCLFVBQUNJLFNBQUQsRUFBWUMsUUFBWjtBQUFBLG9EQUN0QjtBQUFLLDJDQUFTLEVBQUMscUJBQWY7QUFBQSx5REFDRTtBQUFHLHdDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBTyxFQUFDLEVBQS9CO0FBQWtDLG9EQUFjRCxTQUFTLENBQUNFLFFBQTFEO0FBQUEsNERBQ0U7QUFBSyx5Q0FBRyx1Q0FBZ0NGLFNBQVMsQ0FBQ0UsUUFBMUM7QUFBUjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQURGLGVBRUU7QUFBRywrQ0FBUyxFQUFDLG9CQUFiO0FBQWtDLHFEQUFZO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREYsbUNBQTBDRCxRQUExQztBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQURzQjtBQUFBLCtCQUF4QixDQURGLEdBVUUsSUFuQk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQ0FERCxHQXlCRVIsTUFBTSxDQUFDQyxJQUFQLENBQVlyRCxZQUFZLENBQUNNLE1BQXpCLEtBQW9DOEMsTUFBTSxDQUFDQyxJQUFQLENBQVlyRCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDZ0QsTUFBakMsR0FBMEMsQ0FBL0UsZ0JBQ0U7QUFBQSx1Q0FFSUYsTUFBTSxDQUFDQyxJQUFQLENBQVlyRCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDaUQsR0FBakMsQ0FBcUMsVUFBQ0MsSUFBRCxFQUFPQyxHQUFQO0FBQUEsa0RBQ25DO0FBQUsseUNBQVMsRUFBQyxVQUFmO0FBQUEsdURBQ0U7QUFBSyxxQ0FBRyxFQUFFekQsWUFBWSxDQUFDTSxNQUFiLENBQW9Ca0QsSUFBcEIsRUFBMEIxQyxLQUFwQztBQUEyQyxxQ0FBRyxFQUFFZCxZQUFZLENBQUNrRDtBQUE3RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSx1Q0FEbUM7QUFBQSw2QkFBckMsQ0FGSixFQVNLbEQsWUFBWSxDQUFDMEQsTUFBYixDQUFvQkosTUFBckIsR0FDRXRELFlBQVksQ0FBQzBELE1BQWIsQ0FBb0JILEdBQXBCLENBQXdCLFVBQUNJLFNBQUQsRUFBWUMsUUFBWjtBQUFBLGtEQUN0QjtBQUFLLHlDQUFTLEVBQUMscUJBQWY7QUFBQSx1REFDRTtBQUFHLHNDQUFJLEVBQUMsY0FBUjtBQUF1Qix5Q0FBTyxFQUFDLEVBQS9CO0FBQWtDLGtEQUFjRCxTQUFTLENBQUNFLFFBQTFEO0FBQUEsMERBQ0U7QUFBSyx1Q0FBRyx1Q0FBZ0NGLFNBQVMsQ0FBQ0UsUUFBMUM7QUFBUjtBQUFBO0FBQUE7QUFBQTtBQUFBLDJDQURGLGVBRUU7QUFBRyw2Q0FBUyxFQUFDLG9CQUFiO0FBQWtDLG1EQUFZO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkNBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREYsaUNBQTBDRCxRQUExQztBQUFBO0FBQUE7QUFBQTtBQUFBLHVDQURzQjtBQUFBLDZCQUF4QixDQURGLEdBVUUsSUFuQk47QUFBQSwwQ0FERixHQXdCQTtBQWxESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdDQTVIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsOEJBRkYsZUFvTEU7QUFBSyxpQ0FBUyxFQUFDLGVBQWY7QUFBQSwrQ0FDSTtBQUFLLG1DQUFTLEVBQUMseURBQWY7QUFBQSxrREFDRTtBQUFLLHFDQUFTLEVBQUMsaUJBQWY7QUFBQSxvREFDRztBQUFJLHVDQUFTLEVBQUMsZ0JBQWQ7QUFBQSx3Q0FBZ0M1RCxZQUFZLENBQUNrRDtBQUE3QztBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURILEVBR01sRCxZQUFZLENBQUMyRSxLQUFkLGdCQUNDO0FBQUksdUNBQVMsRUFBQyxjQUFkO0FBQUEscURBQ0k7QUFBQSx3REFBSTtBQUFNLDJDQUFTLEVBQUMsV0FBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBQUosT0FBNkMzRSxZQUFZLENBQUMyRSxLQUExRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURELEdBS0MsSUFSTixlQVdHO0FBQUssdUNBQVMsRUFBQyxTQUFmO0FBQUEscURBQ0c7QUFBRyx5Q0FBUyxFQUFDLFlBQWI7QUFBMEIsdUNBQU8sRUFBQyxFQUFsQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURIO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBWEgsRUFnQk0zRSxZQUFZLFNBQVosSUFBc0IsRUFBdkIsZ0JBQ0M7QUFBSyx1Q0FBUyxFQUFDLFdBQWY7QUFBQSxxREFDQztBQUFLLHlDQUFTLEVBQUMsZUFBZjtBQUFBLHVEQUNDO0FBQUsseURBQXVCLEVBQUU7QUFBRTRFLDBDQUFNLEVBQUU1RSxZQUFZO0FBQXRCO0FBQTlCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERCxHQU9DLElBdkJOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQ0FERixlQTZCRTtBQUFLLHFDQUFTLEVBQUMsU0FBZjtBQUFBLG9EQUVFO0FBQUssdUNBQVMsRUFBQyxZQUFmO0FBQUEsc0RBQ0U7QUFBSyx5Q0FBUyxFQUFDLGVBQWY7QUFBQSwyQ0FDTTJCLFlBQUQsZ0JBQ0M7QUFBSywyQ0FBUyxFQUFDLFVBQWY7QUFBQSxpRUFDSztBQUFNLDZDQUFTLEVBQUMsb0JBQWhCO0FBQUEsOENBQXNDRyxZQUFZLENBQUM5QixZQUFEO0FBQWxEO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBREw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQURELEdBS0MsRUFOTixlQVFJO0FBQUssMkNBQVMsRUFBQyxzQkFBZjtBQUFBLDRDQUNJMkIsWUFBRCxnQkFDQztBQUFBLG9FQUNNO0FBQU0sK0NBQVMsRUFBQyxjQUFoQjtBQUFBLGdEQUFnQ04sZUFBZSxDQUFDckIsWUFBRDtBQUEvQztBQUFBO0FBQUE7QUFBQTtBQUFBLDRDQUROLGVBRUM7QUFBTSwrQ0FBUyxFQUFDLGVBQWhCO0FBQUEsZ0RBQWlDQSxZQUFZLENBQUM2RTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDRDQUZEO0FBQUEsa0RBREQsZ0JBTUM7QUFBQSxvRUFBTztBQUFBLGdEQUFPN0M7QUFBUDtBQUFBO0FBQUE7QUFBQTtBQUFBLDRDQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBUkosRUFtQk1oQixpQkFBaUIsSUFBSSxFQUFyQixJQUEyQkMsaUJBQWlCLElBQUksRUFBakQsZ0JBQ0M7QUFBRywyQ0FBUyxFQUFDLGdCQUFiO0FBQUEsaUVBQWlDO0FBQU0sNkNBQVMsRUFBQyxTQUFoQjtBQUFBLCtDQUEyQkQsaUJBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FBakMsdUJBQTBGO0FBQU0sNkNBQVMsRUFBQyxTQUFoQjtBQUFBLDhDQUEyQkM7QUFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FBMUY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQURELEdBR0MsSUF0Qk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURGLEVBNEJLakIsWUFBWSxJQUFJLEVBQWpCLEdBQ0d1QixRQUFRLENBQUN2QixZQUFZLENBQUN3QixVQUFkLENBQVIsSUFBcUMsQ0FBdEMsZ0JBQ0M7QUFBSyx5Q0FBUyxFQUFDLGFBQWY7QUFBQSwwQ0FFS3hCLFlBQVksQ0FBQzhFLE9BQWIsQ0FBcUJ2QixHQUFyQixDQUF5QixVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxzREFDdkI7QUFBSyw2Q0FBUyxFQUFDLFlBQWY7QUFBQSw0REFDRTtBQUFBLGdEQUFRRCxJQUFJLENBQUNOO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FERixFQUdJTSxJQUFJLENBQUN1QixJQUFMLElBQWEsTUFBZCxnQkFDQztBQUFLLCtDQUFTLEVBQUMsV0FBZjtBQUFBLDhEQUNFO0FBQU8sMENBQUUsRUFBQyxhQUFWO0FBQXdCLGlEQUFTLEVBQUMsR0FBbEM7QUFBc0MsNENBQUksRUFBQyxNQUEzQztBQUFrRCxzREFBWXZCLElBQUksQ0FBQ3dCLGlCQUFuRTtBQUFzRixpREFBUyxFQUFDLE9BQWhHO0FBQXdHLDRDQUFJLEVBQUM7QUFBN0c7QUFBQTtBQUFBO0FBQUE7QUFBQSwrQ0FERixlQUVFO0FBQUssaURBQVMsRUFBQyxNQUFmO0FBQUEsZ0VBQ0k7QUFBRyxtREFBUyxFQUFDLDhCQUFiO0FBQTRDLHlEQUFZO0FBQXhEO0FBQUE7QUFBQTtBQUFBO0FBQUEsaURBREosZUFHSTtBQUFLLG1EQUFTLEVBQUMsVUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpREFISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsK0NBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQURELGdCQVlDO0FBQUksK0NBQVMsRUFBQyxhQUFkO0FBQUEsZ0RBRUl4QixJQUFJLENBQUN5QixZQUFMLENBQWtCMUIsR0FBbEIsQ0FBc0IsVUFBQzJCLFVBQUQsRUFBYXpCLEdBQWI7QUFBQSw0REFDcEI7QUFBQSxpRUFDRTtBQUFPLHFEQUFTLEVBQUVyQyxpQkFBaUIsS0FBS29DLElBQUksQ0FBQ3dCLGlCQUFMLEdBQXVCLEdBQXZCLEdBQTJCRSxVQUFVLENBQUNDLHVCQUE1RCxHQUFzRixRQUF0RixHQUFpRyxpQkFBbkg7QUFBc0ksbURBQU8sRUFBQyxFQUE5STtBQUFpSixnREFBSSxFQUFDLGNBQXRKO0FBQUEsc0RBQXNLRCxVQUFVLENBQUNoQztBQUFqTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpREFEb0I7QUFBQSx1Q0FBdEI7QUFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQWZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQ0FEdUI7QUFBQSxpQ0FBekI7QUFGTDtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURELEdBa0NBLEVBbkNGLEdBb0NELEVBaEVIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FGRixlQXNFSTtBQUFLLHVDQUFTLEVBQUMsWUFBZjtBQUFBLHNEQUNFO0FBQUsseUNBQVMsRUFBQyxjQUFmO0FBQUEsd0RBQ0c7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBREgsZUFFRztBQUFLLDJDQUFTLEVBQUMsaUJBQWY7QUFBQSwwREFDRztBQUFHLHdDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBTyxFQUFDLEVBQS9CO0FBQWtDLDZDQUFTLEVBQUMsU0FBNUM7QUFBQSwyREFBc0Q7QUFBRywrQ0FBUyxFQUFDLGFBQWI7QUFBMkIscURBQVk7QUFBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF0RDtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQURILGVBRUc7QUFBTyxzQ0FBRSxFQUFDLGFBQVY7QUFBd0Isd0NBQUksRUFBQyxNQUE3QjtBQUFvQyx3Q0FBSSxFQUFDLGNBQXpDO0FBQXdELHlDQUFLLEVBQUdoQyxXQUFoRTtBQUE4RSw2Q0FBUyxFQUFDO0FBQXhGO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBRkgsZUFHRztBQUFHLHdDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBTyxFQUFDLEVBQS9CO0FBQWtDLDZDQUFTLEVBQUMsU0FBNUM7QUFBQSwyREFBc0Q7QUFBRywrQ0FBUyxFQUFDLFlBQWI7QUFBMEIscURBQVk7QUFBdEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF0RDtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUhIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FGSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREYsRUFVSWlCLFlBQVksSUFBSSxJQUFoQixJQUF3QixDQUFDQSxZQUExQixnQkFDQztBQUFLLHlDQUFTLEVBQUMsZUFBZjtBQUFBLHVEQUNNO0FBQUcsc0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFTLEVBQUMsbUJBQWpDO0FBQXFELHlDQUFPLEVBQUMsRUFBN0Q7QUFBQSwwREFDRztBQUFHLDZDQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQURILE9BQ2dELFNBRGhEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUROO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREQsR0FNRUEsWUFBWSxJQUFJLElBQWhCLElBQXdCQSxZQUF6QixnQkFDQTtBQUFLLHlDQUFTLEVBQUMsZUFBZjtBQUFBLHVEQUNNO0FBQUcsc0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFTLEVBQUMsd0JBQWpDO0FBQTBELG9EQUFnQm5DLFlBQVksQ0FBQ2lELFVBQXZGO0FBQW1HLHlDQUFPLEVBQUMsRUFBM0c7QUFBQSwwREFDRztBQUFHLDZDQUFTLEVBQUMsZ0JBQWI7QUFBOEIsbURBQVk7QUFBMUM7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FESDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFETjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURBLEdBT0EsSUF2Qko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQXRFSixFQWlHSWpELFlBQVksQ0FBQ29GLGFBQWIsSUFBNkIsSUFBN0IsSUFBcUNwRixZQUFZLENBQUNvRixhQUFiLElBQThCLEVBQXBFLGdCQUNDO0FBQUEsc0RBQ0U7QUFBRyx5Q0FBUyxFQUFDLGVBQWI7QUFBQSx3REFBNkI7QUFBTyxvQ0FBRSxFQUFDLFlBQVY7QUFBdUIsc0NBQUksRUFBQyxVQUE1QjtBQUF1QywwQ0FBUSxFQUFDO0FBQWhEO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBQTdCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERixlQUVFO0FBQUsseUNBQVMsRUFBQyxjQUFmO0FBQUEsd0RBQ0U7QUFBSSwyQ0FBUyxFQUFDLEtBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBREYsRUFFR3BGLFlBQVksQ0FBQ29GLGFBRmhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FGRjtBQUFBLDRDQURELEdBU0MsSUExR0o7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQTdCRixFQTRJSXBGLFlBQVksQ0FBQ3FGLFlBQWQsZ0JBQ0M7QUFBSyxxQ0FBUyxFQUFDLGFBQWY7QUFBQSxtREFDRTtBQUFBLHdDQUVFckYsWUFBWSxDQUFDcUYsWUFBYixDQUEwQjlCLEdBQTFCLENBQThCLFVBQUMrQixRQUFELEVBQVdDLE9BQVg7QUFBQSxvREFDNUI7QUFBSSwyQ0FBUyxFQUFDLFVBQWQ7QUFBQSx5REFDSTtBQUFHLDZDQUFTLEVBQUMsUUFBYjtBQUFzQix5Q0FBSyxFQUFFRCxRQUFRLENBQUNFLEtBQXRDO0FBQTZDLHVEQUFnQixFQUE3RDtBQUFnRSx5REFBa0IsRUFBbEY7QUFBcUYsMkNBQU8sRUFBQyxFQUE3RjtBQUFBLDhDQUFpR0YsUUFBUSxDQUFDRTtBQUExRztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FENEI7QUFBQSwrQkFBOUI7QUFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQ0FERCxHQWFDLElBekpKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsOEJBcExGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQVZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFQRixlQXNYRSxxRUFBQyxjQUFEO0FBQWdCLGlCQUFPLEVBQUUxRixPQUFPLENBQUNxRDtBQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQXRYRjtBQUFBO0FBSko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQWtZRDs7SUF4Z0J1QnRELE87VUFrSVBrRCxxRDs7O01BbElPbEQsTztBQXdnQnZCIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL1tzbHVnXS42ZTA0MTA2OWEwYWUxMThiMjkwZC5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSBcIm5leHQvcm91dGVyXCI7XHJcbmltcG9ydCBFcnJvclBhZ2UgZnJvbSBcIm5leHQvZXJyb3JcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgZHluYW1pYyBmcm9tICduZXh0L2R5bmFtaWMnXHJcbmltcG9ydCB7IHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnXHJcbmltcG9ydCBSZWFjdEltYWdlTWFnbmlmeSBmcm9tICdyZWFjdC1pbWFnZS1tYWduaWZ5J1xyXG5pbXBvcnQgU2xpZGVyIGZyb20gXCJyZWFjdC1zbGlja1wiO1xyXG5cclxuaW1wb3J0IENvbnRhaW5lciBmcm9tIFwiQC9jb21wb25lbnRzL2NvbnRhaW5lclwiO1xyXG5pbXBvcnQgTGF5b3V0IGZyb20gXCJAL2NvbXBvbmVudHMvbGF5b3V0XCI7XHJcbmltcG9ydCB7IGdldENvbmZpZ3MsIGdldE1haW5NZW51LCBnZXRQcm9kdWN0QnlTbHVnLCBnZXRQcm9kdWN0c0J5Q2F0ZWdvcnkgfSBmcm9tIFwiQC9saWIvYXBpXCI7XHJcblxyXG52YXIgaWRTdG9yZUFwcCA9ICduNDlzaG9wdjJfdHJpam9pYSc7XHJcbnZhciBUZW1wbGF0ZUhlYWRlciA9IGR5bmFtaWMoKCkgPT4gaW1wb3J0KCdAL2NvbXBvbmVudHMvdGVtcGxhdGVzLycraWRTdG9yZUFwcCsnL2NvbXBvbmVudHMvaGVhZGVyJykpXHJcbnZhciBUZW1wbGF0ZUZvb3RlciA9IGR5bmFtaWMoKCkgPT4gaW1wb3J0KCdAL2NvbXBvbmVudHMvdGVtcGxhdGVzLycraWRTdG9yZUFwcCsnL2NvbXBvbmVudHMvZm9vdGVyJykpXHJcblxyXG5sZXQgc2xpZGVyVGh1bWJzID0ge1xyXG4gIGluZmluaXRlOiBmYWxzZSxcclxuICB2ZXJ0aWNhbDogdHJ1ZSxcclxuICBzbGlkZXNUb1Nob3c6IDQsXHJcbiAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgc3BlZWQ6IDUwMCxcclxuICByZXNwb25zaXZlOiBbXHJcbiAgICB7XHJcbiAgICAgIGJyZWFrcG9pbnQ6IDQxNSxcclxuICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICB2ZXJ0aWNhbDogZmFsc2UsXHJcbiAgICAgICAgc2xpZGVzVG9TaG93OiAzXHJcbiAgICAgIH1cclxuICAgIH1cclxuICBdXHJcbn1cclxuXHJcbnZhciBzZXR0aW5nc01vYmlsZVNsaWRlID0ge1xyXG4gIHNsaWRlc1RvU2hvdzogMSxcclxuICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICBzcGVlZDogNTAwLFxyXG4gIGRvdHM6IHRydWVcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gUHJvZHVjdCh7IGNvbmZpZ3MsIG1haW5NZW51LCBkYWRvc1Byb2R1dG8gfSkge1xyXG4gIHZhciBpbml0aWFsSW1hZ2VzID0gW107XHJcbiAgZnVuY3Rpb24gZ2V0SW5pdGlhbEltYWdlKGRhZG9zUHJvZHV0bykge1xyXG4gICAgXHJcbiAgICB2YXIgaW1nSW5pY2lhbCA9IFtdO1xyXG4gICAgdmFyIHRlbXBJbWdJbmljaWFsID0gW107XHJcbiAgICBcclxuICAgIGZvcih2YXIgaSBpbiBkYWRvc1Byb2R1dG8uaW1hZ2VzKXtcclxuICAgICAgdGVtcEltZ0luaWNpYWwucHVzaChbaSwgZGFkb3NQcm9kdXRvLmltYWdlcyBbaV1dKTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgaW5pdGlhbEtleSA9IGRhZG9zUHJvZHV0by5pbWFnZS5zdWJzdHJpbmcoZGFkb3NQcm9kdXRvLmltYWdlLmxhc3RJbmRleE9mKCcvJykrMSlcclxuICAgIFxyXG4gICAgaWYoZGFkb3NQcm9kdXRvLmltYWdlcyAhPSB1bmRlZmluZWQgJiYgZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XSAhPSBudWxsKXtcclxuICAgICAgaW1nSW5pY2lhbFsnc2hvdyddID0gZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XS5zaG93IFxyXG4gICAgICBpbWdJbmljaWFsWydwb3B1cCddID0gZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XS5wb3B1cFxyXG4gICAgICBpbWdJbmljaWFsWyd0aHVtYiddID0gZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XS50aHVtYlxyXG4gICAgfWVsc2UgaWYodGVtcEltZ0luaWNpYWxbMF0gIT0gbnVsbCl7XHJcbiAgICAgIGltZ0luaWNpYWwgPSB0ZW1wSW1nSW5pY2lhbFswXVsxXVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBpbWdJbmljaWFsO1xyXG4gIH1cclxuICBpZihkYWRvc1Byb2R1dG8pe1xyXG4gICAgaW5pdGlhbEltYWdlcyA9IGdldEluaXRpYWxJbWFnZShkYWRvc1Byb2R1dG8pO1xyXG4gIH1cclxuXHJcbiAgdmFyIHNlbGVjdGVkUXRkUGFyY2VsID0gMTtcclxuICB2YXIgc2VsZWN0ZWRWYWxQYXJjZWwgPSAxO1xyXG4gIHZhciBxdWFudGl0eUJ1eSA9IDE7XHJcbiAgdmFyIHNoaXBwaW5nTWV0aG9kcyA9IG51bGw7XHJcbiAgdmFyIHNlbGVjdGVkT3B0aW9uQnV5ID0gMTtcclxuICBcclxuICBmdW5jdGlvbiBnZXRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvKSB7XHJcbiAgICB2YXIgaW5pdGlhbFNwZWNpYWxWYWx1ZSA9IG51bGw7XHJcbiAgICBpZihkYWRvc1Byb2R1dG8gIT0gdW5kZWZpbmVkKXtcclxuICAgICAgaWYocGFyc2VJbnQoZGFkb3NQcm9kdXRvLmhhc19vcHRpb24pID09IDApeyAvLyBQUk9EVVRPIFNFTSBPUENPRVNcclxuICAgICAgICBpZihkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgICBpbml0aWFsU3BlY2lhbFZhbHVlID0gZGFkb3NQcm9kdXRvLnNwZWNpYWw7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICBpbml0aWFsU3BlY2lhbFZhbHVlID0gZGFkb3NQcm9kdXRvLnByaWNlO1xyXG4gICAgICAgIH1cclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgaWYoaW5pdGlhbFNwZWNpYWxWYWx1ZSA9PSBudWxsICYmIGRhZG9zUHJvZHV0by5zcGVjaWFsKXtcclxuICAgICAgICAgIGluaXRpYWxTcGVjaWFsVmFsdWUgPSBkYWRvc1Byb2R1dG8uc3BlY2lhbFxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBjb25zdCBzcGVjaWFsVmFsdWUgPSB1c2VTdGF0ZShpbml0aWFsU3BlY2lhbFZhbHVlKTtcclxuICAgIHJldHVybiBzcGVjaWFsVmFsdWU7XHJcbiAgfVxyXG4gIGNvbnN0IFtzcGVjaWFsVmFsdWUsIHNldFNwZWNpYWxWYWx1ZV0gPSBnZXRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvKTtcclxuXHJcbiAgZnVuY3Rpb24gZ2V0TWFpblZhbHVlKGRhZG9zUHJvZHV0bykge1xyXG4gICAgdmFyIGluaXRpYWxNYWluVmFsdWUgPSBudWxsO1xyXG4gICAgaWYoZGFkb3NQcm9kdXRvICE9IHVuZGVmaW5lZCl7XHJcbiAgICAgIGlmKHBhcnNlSW50KGRhZG9zUHJvZHV0by5oYXNfb3B0aW9uKSA9PSAwKXsgLy8gUFJPRFVUTyBTRU0gT1BDT0VTXHJcbiAgICAgICAgaWYoZGFkb3NQcm9kdXRvLnNwZWNpYWwpe1xyXG4gICAgICAgICAgaW5pdGlhbE1haW5WYWx1ZSA9IGRhZG9zUHJvZHV0by5wcmljZTtcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgIGluaXRpYWxNYWluVmFsdWUgPSBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgaWYoaW5pdGlhbE1haW5WYWx1ZSA9PSBudWxsICYmIGRhZG9zUHJvZHV0by5wcmljZSl7XHJcbiAgICAgICAgICBpbml0aWFsTWFpblZhbHVlID0gZGFkb3NQcm9kdXRvLnByaWNlXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGNvbnN0IG1haW5WYWx1ZSA9IHVzZVN0YXRlKGluaXRpYWxNYWluVmFsdWUpO1xyXG4gICAgcmV0dXJuIG1haW5WYWx1ZTtcclxuICB9XHJcbiAgY29uc3QgW21haW5WYWx1ZSwgc2V0TWFpblZhbHVlXSA9IGdldE1haW5WYWx1ZShkYWRvc1Byb2R1dG8pO1xyXG5cclxuICBjb25zdCBbc29sZE91dFN0b2NrLCBzZXRTb2xkT3V0U3RvY2tdID0gdXNlU3RhdGUoKCkgPT4ge1xyXG4gICAgY29uc3QgaW5pdGlhbFN0YXRlID0gMDtcclxuICAgIHJldHVybiBpbml0aWFsU3RhdGU7XHJcbiAgfSk7XHJcbiAgXHJcbiAgXHJcbiAgLyppZihwYXJzZUludChkYWRvc1Byb2R1dG8uaGFzX29wdGlvbikgPT0gMCl7IC8vIFBST0RVVE8gU0VNIE9QQ09FU1xyXG4gICAgICBcclxuICAgIGNvbnN0IHZhbG9yQXR1YWwgPSBkYWRvc1Byb2R1dG8ucHJpY2VcclxuICAgIGlmKGRhZG9zUHJvZHV0by5zcGVjaWFsKXtcclxuICAgICAgc2V0U2VsZWN0ZWRNYWluVmFsdWUodmFsb3JBdHVhbCk7XHJcbiAgICAgIHNldFNlbGVjdGVkU3BlY2lhbFZhbHVlKGRhZG9zUHJvZHV0by5zcGVjaWFsKTtcclxuICAgIH1lbHNle1xyXG4gICAgICBzZXRTZWxlY3RlZE1haW5WYWx1ZShudWxsKTtcclxuICAgICAgc2V0U2VsZWN0ZWRTcGVjaWFsVmFsdWUodmFsb3JBdHVhbCk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYoIWRhZG9zUHJvZHV0by5zb2xkX291dCl7XHJcbiAgICAgIHNldFNvbGRPdXRTdG9jaygwKTtcclxuICAgIH1lbHNle1xyXG4gICAgICBzZXRTb2xkT3V0U3RvY2soMSk7XHJcbiAgICB9XHJcbiAgfWVsc2V7IC8vIFBST0RVVE8gQ09NIE9QQ09FU1xyXG4gICAgc2V0U29sZE91dFN0b2NrKDApO1xyXG5cclxuICAgICAgaWYoc2VsZWN0ZWRNYWluVmFsdWUgPT0gbnVsbCl7XHJcbiAgICAgICAgc2V0U2VsZWN0ZWRNYWluVmFsdWUoZGFkb3NQcm9kdXRvLnByaWNlKVxyXG4gICAgICB9XHJcbiAgICAgIGlmKHNlbGVjdGVkU3BlY2lhbFZhbHVlID09IG51bGwgJiYgZGFkb3NQcm9kdXRvLnNwZWNpYWwpe1xyXG4gICAgICAgIHNldFNlbGVjdGVkU3BlY2lhbFZhbHVlKGRhZG9zUHJvZHV0by5zcGVjaWFsKVxyXG4gICAgICB9Ki9cclxuXHJcbiAgICAgIC8qZm9yKHZhciBpIGluIGRhZG9zUHJvZHV0by5vcHRpb25zKXtcclxuICAgICAgICBncnVwb0F0dWFsID0gZGFkb3NQcm9kdXRvLm9wdGlvbnNbaV1cclxuICAgICAgICBmb3IodmFyIG9wdCBpbiBncnVwb0F0dWFsLm9wdGlvbl92YWx1ZSl7XHJcbiAgICAgICAgICBpZihncnVwb0F0dWFsLm9wdGlvbl92YWx1ZVtvcHRdLm9wdGlvbl92YWx1ZV9pZD09ZGFkb3NQcm9kdXRvLm9wY2FvX3NlbGVjaW9uYWRhKXtcclxuICAgICAgICAgICAgaW5pdGlhbE9wdGlvbiA9IGdydXBvQXR1YWwucHJvZHVjdF9vcHRpb25faWQrJ18nK2dydXBvQXR1YWwub3B0aW9uX3ZhbHVlW29wdF0ucHJvZHVjdF9vcHRpb25fdmFsdWVfaWRcclxuICAgICAgICAgICAgdGhpcy5TZWxlY3RPcHRpb24oaW5pdGlhbE9wdGlvbiwgbnVsbClcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgfSovXHJcbiBcclxuICB2YXIgY2hhbmdlWm9vbSA9IChldikgPT4ge1xyXG4gICAgY29uc3QgaW1nU2hvdyA9IGV2LmN1cnJlbnRUYXJnZXQuZGF0YXNldC5zcmNzaG93XHJcbiAgICBjb25zdCBpbWdQb3B1cCA9IGV2LmN1cnJlbnRUYXJnZXQuZGF0YXNldC5zcmNwb3B1cFxyXG4gICAgdmFyIHRlbXBJbWFnZSA9IHt9O1xyXG4gICAgdGVtcEltYWdlWydzaG93J10gPSBpbWdTaG93XHJcbiAgICB0ZW1wSW1hZ2VbJ3BvcHVwJ10gPSBpbWdQb3B1cFxyXG4gICAgLy9zZXRJbml0aWFsSW1hZ2UodGVtcEltYWdlKTtcclxuICAgIFxyXG4gIH1cclxuICBcclxuXHJcbiAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcbiAgaWYgKCFyb3V0ZXIuaXNGYWxsYmFjayAmJiAhZGFkb3NQcm9kdXRvPy5wcm9kdWN0X2lkKSB7XHJcbiAgICByZXR1cm4gPEVycm9yUGFnZSBzdGF0dXNDb2RlPXs0MDR9IC8+O1xyXG4gIH1cclxuICByZXR1cm4gKFxyXG4gICAgPExheW91dD5cclxuICAgICAgPENvbnRhaW5lcj5cclxuICAgICAgICB7cm91dGVyLmlzRmFsbGJhY2sgPyAoXHJcbiAgICAgICAgICA8ZGl2PkxvYWRpbmfigKY8L2Rpdj5cclxuICAgICAgICApIDogKFxyXG4gICAgICAgICAgPD5cclxuICAgICAgICAgICAgPEhlYWQ+XHJcbiAgICAgICAgICAgICAgPHRpdGxlPntkYWRvc1Byb2R1dG8ubmFtZX08L3RpdGxlPlxyXG4gICAgICAgICAgICAgIDxtZXRhIG5hbWU9XCJkZXNjcmlwdGlvblwiIGNvbnRlbnQ9XCJsZWxlbGVcIiAvPlxyXG4gICAgICAgICAgICAgIDxtZXRhIG5hbWU9XCJvZzppbWFnZVwiIGNvbnRlbnQ9XCJsaWxpbGlcIiAvPlxyXG4gICAgICAgICAgICA8L0hlYWQ+XHJcbiAgICAgICAgICAgIDxUZW1wbGF0ZUhlYWRlciBjb25maWdzPXtjb25maWdzLnJlc3Bvc3RhfSBtYWluTWVudT17bWFpbk1lbnUucmVzcG9zdGF9PjwvVGVtcGxhdGVIZWFkZXI+IFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXM9XCJtYWluLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBhZ2UtcHJvZHVjdHNcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyLXBhZGRpbmcgbGlnaHQtYmFja2dyb3VuZCBucHJvZHVjdC1icmVhZGNydW1iXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPG9sIGNsYXNzTmFtZT1cImJyZWFkY3J1bWJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwiYnJlYWRjcnVtYi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cIi9cIiB0aXRsZT1cIlDDoWdpbmEgaW5pY2lhbFwiPkhvbWU8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwiYnJlYWRjcnVtYi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAge2RhZG9zUHJvZHV0by5uYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9vbD5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjcC1wcmV2aWV3M1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LXBhZ2VcIj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LWdhbGxlcnlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LXRodW1ibmFpbHMgXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2VzLWNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLmltYWdlcykgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubGVuZ3RoID4gNCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNsaWRlciBjbGFzc05hbWU9XCJzbGlkZXJUaHVtYnNcIiB7Li4uc2xpZGVyVGh1bWJzfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZS10aHVtYiBqcy1jYXJvdXNlbC1jb250cm9sLWl0ZW0gcG9pbnRlciBqcy1wcm9kdWN0LWltYWdlLXRodW1iXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ9e2RhZG9zUHJvZHV0by5uYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1zcmNzaG93PXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnNob3d9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLXNyY3BvcHVwPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCIxMFwiIGhlaWdodD1cIjEwXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9XCJcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8udmlkZW9zLmxlbmd0aCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLnZpZGVvcy5tYXAoKGl0ZW1WaWRlbywga2V5VmlkZW8pID0+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaXRlbS12aWRlb1wiIGtleT17a2V5VmlkZW99PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgZGF0YS12aWRlb2lkPXtpdGVtVmlkZW8uaWRfdmlkZW99PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvJHtpdGVtVmlkZW8uaWRfdmlkZW99L21xZGVmYXVsdC5qcGdgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXlvdXR1YmUtcGxheVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU2xpZGVyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2UtdGh1bWIganMtY2Fyb3VzZWwtY29udHJvbC1pdGVtIHBvaW50ZXIganMtcHJvZHVjdC1pbWFnZS10aHVtYlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWx0PXtkYWRvc1Byb2R1dG8ubmFtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1zcmNzaG93PXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnNob3d9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEtc3JjcG9wdXA9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPVwiMTBcIiBoZWlnaHQ9XCIxMFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9XCJcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8udmlkZW9zLmxlbmd0aCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by52aWRlb3MubWFwKChpdGVtVmlkZW8sIGtleVZpZGVvKSA9PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpdGVtLXZpZGVvXCIga2V5PXtrZXlWaWRlb30+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgZGF0YS12aWRlb2lkPXtpdGVtVmlkZW8uaWRfdmlkZW99PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17YGh0dHBzOi8vaW1nLnlvdXR1YmUuY29tL3ZpLyR7aXRlbVZpZGVvLmlkX3ZpZGVvfS9tcWRlZmF1bHQuanBnYH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEteW91dHViZS1wbGF5XCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LWltYWdlcyBtaW4td2lkdGgtNDE1cHhcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb21cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX3RvcF9sZWZ0LmltYWdlICE9IG51bGwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsYWJlbEl0ZW0gbGFiZWxUb3BMZWZ0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX3RvcF9sZWZ0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX3RvcF9yaWdodC5pbWFnZSAhPSBudWxsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGFiZWxJdGVtIGxhYmVsVG9wUmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fdG9wX3JpZ2h0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX2JvdHRvbV9sZWZ0LmltYWdlICE9IG51bGwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsYWJlbEl0ZW0gbGFiZWxCb3R0b21MZWZ0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX2JvdHRvbV9sZWZ0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX2JvdHRvbV9yaWdodC5pbWFnZSAhPSBudWxsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGFiZWxJdGVtIGxhYmVsQm90dG9tUmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fYm90dG9tX3JpZ2h0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFJlYWN0SW1hZ2VNYWduaWZ5IHsuLi57XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc21hbGxJbWFnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdDogZGFkb3NQcm9kdXRvLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNGbHVpZFdpZHRoOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYzogaW5pdGlhbEltYWdlcy5zaG93XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFyZ2VJbWFnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYzogaW5pdGlhbEltYWdlcy5wb3B1cCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogY29uZmlncy53aWR0aFpvb20sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBjb25maWdzLndpZHRoWm9vbVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fSAvPiAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWF4LXdpZHRoLTQxNHB4XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKSAmJiBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5sZW5ndGggPiAxKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2VzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTbGlkZXIgY2xhc3NOYW1lPVwic2xpZGVWaXRyaW5lXCIgey4uLnNldHRpbmdzTW9iaWxlU2xpZGV9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfSBhbHQ9e2RhZG9zUHJvZHV0by5uYW1lfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by52aWRlb3MubGVuZ3RoKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLnZpZGVvcy5tYXAoKGl0ZW1WaWRlbywga2V5VmlkZW8pID0+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tIGl0ZW0tdmlkZW9cIiBrZXk9e2tleVZpZGVvfT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBkYXRhLXZpZGVvaWQ9e2l0ZW1WaWRlby5pZF92aWRlb30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvJHtpdGVtVmlkZW8uaWRfdmlkZW99L21xZGVmYXVsdC5qcGdgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS15b3V0dWJlLXBsYXlcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TbGlkZXI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IChPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKSAmJiBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5sZW5ndGggPiAwKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9IGFsdD17ZGFkb3NQcm9kdXRvLm5hbWV9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLnZpZGVvcy5sZW5ndGgpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8udmlkZW9zLm1hcCgoaXRlbVZpZGVvLCBrZXlWaWRlbykgPT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb20gaXRlbS12aWRlb1wiIGtleT17a2V5VmlkZW99PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGRhdGEtdmlkZW9pZD17aXRlbVZpZGVvLmlkX3ZpZGVvfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2BodHRwczovL2ltZy55b3V0dWJlLmNvbS92aS8ke2l0ZW1WaWRlby5pZF92aWRlb30vbXFkZWZhdWx0LmpwZ2B9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXlvdXR1YmUtcGxheVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZHVjdC1pbmZvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LWFjdGlvbnMgY29udGFpbmVyLXBhZGRpbmcgY29udGFpbmVyLXBhZGRpbmctdG9wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LWhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGgxIGNsYXNzTmFtZT1cIm5wcm9kdWN0LXRpdGxlXCI+e2RhZG9zUHJvZHV0by5uYW1lfTwvaDE+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoZGFkb3NQcm9kdXRvLm1vZGVsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJpbmZvc1Byb2R1Y3RcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+PHNwYW4gY2xhc3NOYW1lPVwidGl0bGVJbmZvXCI+UkVGOjwvc3Bhbj4ge2RhZG9zUHJvZHV0by5tb2RlbH08L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmF0ZUJveFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwibGstYXZhbGlhclwiIG9uQ2xpY2s9XCJcIj5BdmFsaWFyIGFnb3JhPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGRhZG9zUHJvZHV0by5zaG9ydCAhPSAnJyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImluZm9zQXJlYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmVzdW1lUHJvZHVjdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXt7IF9faHRtbDogZGFkb3NQcm9kdXRvLnNob3J0IH19IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJ1eUFyZWFcIj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sU2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2RjdC1wcmljZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KHNwZWNpYWxWYWx1ZSkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwib2xkUHJpY2VcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIERlIDxzcGFuIGNsYXNzTmFtZT1cIm5wcm9kdWN0LXByaWNlLW1heFwiPntnZXRNYWluVmFsdWUoZGFkb3NQcm9kdXRvKX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2R1Y3QtcHJpY2UtdmFsdWVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KHNwZWNpYWxWYWx1ZSkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUG9yIDxzcGFuIGNsYXNzTmFtZT1cInNwZWNpYWxWYWx1ZVwiPntnZXRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvKX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiaXRlbS1kaXNjb3VudFwiPntkYWRvc1Byb2R1dG8uZGlzY291bnRfcGVyY2VudH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cD5wb3IgPHNwYW4+e21haW5WYWx1ZX08L3NwYW4+PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KHNlbGVjdGVkUXRkUGFyY2VsICE9ICcnICYmIHNlbGVjdGVkVmFsUGFyY2VsICE9ICcnKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJzZWxlY3RlZFBhcmNlbFwiPk91IDxzcGFuIGNsYXNzTmFtZT1cIm51bVBhcmNcIj57c2VsZWN0ZWRRdGRQYXJjZWx9eDwvc3Bhbj4gZGUgPHNwYW4gY2xhc3NOYW1lPVwidmFsUGFyY1wiPntzZWxlY3RlZFZhbFBhcmNlbH08L3NwYW4+PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvICE9ICcnKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKHBhcnNlSW50KGRhZG9zUHJvZHV0by5oYXNfb3B0aW9uKSAhPSAwKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm9wdGlvbnNBcmVhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8ub3B0aW9ucy5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJib3gtb3B0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+e2l0ZW0ubmFtZX08L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGl0ZW0udHlwZSA9PSAndGV4dCcpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInR4dE9wdGlvblwiPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IGlkPVwiZmllbGRPcHRpb25cIiBtYXhsZW5ndGg9XCIzXCIgdHlwZT1cInRleHRcIiBkYXRhLWdyb3VwPXtpdGVtLnByb2R1Y3Rfb3B0aW9uX2lkfSBjbGFzc05hbWU9XCJmaWVsZFwiIG5hbWU9XCJ0eHQtb3B0aW9uXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhlbHBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtcXVlc3Rpb24tY2lyY2xlIGNvbG9yMlwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaW5mb0hlbHBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEluc2lyYSBhdMOpIDMgbGV0cmFzIHBhcmEgcGVyc29uYWxpemFyIGEgY2FtaXNhIGNvbSB1bSBib3JkYWRvIGV4Y2x1c2l2by4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwibGlzdE9wdGlvbnNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtLm9wdGlvbl92YWx1ZS5tYXAoKGl0ZW1PcHRpb24sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPXtzZWxlY3RlZE9wdGlvbkJ1eSAhPT0gaXRlbS5wcm9kdWN0X29wdGlvbl9pZCsnXycraXRlbU9wdGlvbi5wcm9kdWN0X29wdGlvbl92YWx1ZV9pZCA/ICdvcHRpb24nIDogJ29wdGlvbiBzZWxlY3RlZCd9IG9uQ2xpY2s9XCJcIiBocmVmPVwiamF2YXNjcmlwdDo7XCI+e2l0ZW1PcHRpb24ubmFtZX08L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6ICAnJyAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbFNlY3Rpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicXVhbnRpdHlBcmVhXCI+ICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+UXVhbnRpZGFkZTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJ1dHRvbnNRdWFudGl0eVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBjbGFzc05hbWU9XCJidG5MZXNzXCI+PGkgY2xhc3NOYW1lPVwiZmEgZmEtbWludXNcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IGlkPVwidHh0UXVhbnRpdHlcIiB0eXBlPVwidGV4dFwiIG5hbWU9XCJ0eHQtcXVhbnRpdHlcIiB2YWx1ZT17IHF1YW50aXR5QnV5IH0gY2xhc3NOYW1lPVwidHh0UXVhbnRpdHlcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBjbGFzc05hbWU9XCJidG5Nb3JlXCI+PGkgY2xhc3NOYW1lPVwiZmEgZmEtcGx1c1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT48L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoc29sZE91dFN0b2NrICE9IG51bGwgJiYgIXNvbGRPdXRTdG9jayk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV5QnV0dG9uQXJlYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgY2xhc3NOYW1lPVwiYnV5QnV0dG9uIGJ0bl9idXlcIiBvbkNsaWNrPVwiXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhcyBmYSBmYS1zaG9wcGluZy1jYXJ0XCI+PC9pPiB7J0NvbXByYXInfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiAoc29sZE91dFN0b2NrICE9IG51bGwgJiYgc29sZE91dFN0b2NrKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXlCdXR0b25BcmVhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBjbGFzc05hbWU9XCJidXlCdXR0b24gbm90aWZ5QnV0dG9uXCIgZGF0YS1wcm9kdWN0aWQ9e2RhZG9zUHJvZHV0by5wcm9kdWN0X2lkfSBvbkNsaWNrPVwiXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLWVudmVsb3BlXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPiBBdmlzZS1tZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhkYWRvc1Byb2R1dG8udGV4dF9wcmV2ZW5kYSE9IG51bGwgJiYgZGFkb3NQcm9kdXRvLnRleHRfcHJldmVuZGEgIT0gJycpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJjaGVja1ByZXZlbmRhXCI+PGlucHV0IGlkPVwiY2twcmV2ZW5kYVwiIHR5cGU9XCJjaGVja2JveFwiIG9uQ2hhbmdlPVwiXCIgLz4gQ29uY29yZG8gY29tIG8gcHJhem8gZGUgZW50cmVnYSBkZXNjcml0byBhYmFpeG8uPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpbmZvUHJldmVuZGFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cInRpdFwiPlRFUk1PIERFIEFDRUlUQcOHw4NPPC9oND5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2RhZG9zUHJvZHV0by50ZXh0X3ByZXZlbmRhfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoZGFkb3NQcm9kdXRvLmd1aWFfbWVkaWRhcyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZ3VpYXNNZWRpZGFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLmd1aWFfbWVkaWRhcy5tYXAoKGl0ZW1HdWlhLCBrZXlHdWlhKSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT1cIml0ZW1HdWlhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJjb2xvcjJcIiB0aXRsZT17aXRlbUd1aWEudGl0bGV9IGRhdGEtdGl0dWxvZ3VpYT1cIlwiIGRhdGEtY29udGV1ZG9ndWlhPVwiXCIgb25DbGljaz1cIlwiPntpdGVtR3VpYS50aXRsZX08L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8VGVtcGxhdGVGb290ZXIgY29uZmlncz17Y29uZmlncy5yZXNwb3N0YX0gLz5cclxuICAgICAgICAgIDwvPlxyXG4gICAgICAgICl9XHJcbiAgICAgIDwvQ29udGFpbmVyPlxyXG4gICAgPC9MYXlvdXQ+XHJcbiAgKTtcclxufTtcclxuXHJcbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBnZXRTdGF0aWNQcm9wcyh7IHBhcmFtcyB9KSB7XHJcbiAgdmFyIGNvbmZpZ3MgPSBhd2FpdCBnZXRDb25maWdzKCk7XHJcbiAgdmFyIG1haW5NZW51ID0gYXdhaXQgZ2V0TWFpbk1lbnUoKTtcclxuICB2YXIgcmVzUHJvZHVjdCA9IGF3YWl0IGdldFByb2R1Y3RCeVNsdWcocGFyYW1zLnNsdWcpO1xyXG5cclxuICB2YXIgZGFkb3NQcm9kdXRvID0gbnVsbDtcclxuICBpZihyZXNQcm9kdWN0LnN1Y2Nlc3Mpe1xyXG4gICAgZGFkb3NQcm9kdXRvID0gcmVzUHJvZHVjdC5yZXNwb3N0YS5wcm9kdXRvO1xyXG4gIH1cclxuICByZXR1cm4ge1xyXG4gICAgcHJvcHM6IHtcclxuICAgICAgY29uZmlncyxcclxuICAgICAgZGFkb3NQcm9kdXRvLFxyXG4gICAgICBtYWluTWVudVxyXG4gICAgfSxcclxuICAgIHJldmFsaWRhdGU6IDYwXHJcbiAgfTtcclxufVxyXG5cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFN0YXRpY1BhdGhzKCkge1xyXG4gIGNvbnN0IHByb2R1dG9zID0gYXdhaXQgZ2V0UHJvZHVjdHNCeUNhdGVnb3J5KCk7XHJcblxyXG4gIHJldHVybiB7XHJcbiAgICBwYXRoczpcclxuICAgICAgW10sXHJcbiAgICBmYWxsYmFjazogdHJ1ZSxcclxuICB9O1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=