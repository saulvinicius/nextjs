webpackHotUpdate_N_E("pages/[slug]",{

/***/ "./pages/[slug].js":
/*!*************************!*\
  !*** ./pages/[slug].js ***!
  \*************************/
/*! exports provided: __N_SSG, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__N_SSG", function() { return __N_SSG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Product; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/error */ "./node_modules/next/error.js");
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_error__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_image_magnify__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-image-magnify */ "./node_modules/react-image-magnify/dist/es/ReactImageMagnify.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_container__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/components/container */ "./components/container.js");
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @/components/layout */ "./components/layout.js");





var _jsxFileName = "C:\\Users\\sauln49\\Desktop\\nextjs\\pages\\[slug].js",
    _s3 = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }










var idStoreApp = 'n49shopv2_trijoia';
var TemplateHeader = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c = function _c() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/header$")("./" + idStoreApp + "/components/header");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/header$").resolve("./" + idStoreApp + "/components/header"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/header']
  }
});
_c2 = TemplateHeader;
var TemplateFooter = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c3 = function _c3() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/footer$")("./" + idStoreApp + "/components/footer");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/footer$").resolve("./" + idStoreApp + "/components/footer"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/footer']
  }
});
_c4 = TemplateFooter;
var sliderThumbs = {
  infinite: false,
  vertical: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  speed: 500,
  responsive: [{
    breakpoint: 415,
    settings: {
      vertical: false,
      slidesToShow: 3
    }
  }]
};
var settingsMobileSlide = {
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  dots: true
};
var __N_SSG = true;
function Product(_ref) {
  _s3();

  var _s = $RefreshSig$(),
      _s2 = $RefreshSig$(),
      _this = this;

  var configs = _ref.configs,
      mainMenu = _ref.mainMenu,
      dadosProduto = _ref.dadosProduto;
  var initialImages = [];

  function getInitialImage(dadosProduto) {
    var imgInicial = [];
    var tempImgInicial = [];

    for (var i in dadosProduto.images) {
      tempImgInicial.push([i, dadosProduto.images[i]]);
    }

    var initialKey = dadosProduto.image.substring(dadosProduto.image.lastIndexOf('/') + 1);

    if (dadosProduto.images != undefined && dadosProduto.images[initialKey] != null) {
      imgInicial['show'] = dadosProduto.images[initialKey].show;
      imgInicial['popup'] = dadosProduto.images[initialKey].popup;
      imgInicial['thumb'] = dadosProduto.images[initialKey].thumb;
    } else if (tempImgInicial[0] != null) {
      imgInicial = tempImgInicial[0][1];
    }

    return imgInicial;
  }

  if (dadosProduto) {
    initialImages = getInitialImage(dadosProduto);
  }

  var selectedQtdParcel = 1;
  var selectedValParcel = 1;
  var quantityBuy = 1;
  var shippingMethods = null;
  var selectedOptionBuy = 1;

  function getSpecialValue(dadosProduto) {
    _s();

    var initialSpecialValue = null;

    if (dadosProduto != undefined) {
      if (parseInt(dadosProduto.has_option) == 0) {
        // PRODUTO SEM OPCOES
        if (dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        } else {
          initialSpecialValue = dadosProduto.price;
        }
      } else {
        if (initialSpecialValue == null && dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        }
      }
    }

    var specialValue = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(initialSpecialValue);
    return specialValue;
  }

  _s(getSpecialValue, "uOCI/u7Rhc0OEPJW7t3Rh1DIVCE=");

  var _getSpecialValue = getSpecialValue(dadosProduto),
      _getSpecialValue2 = Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_getSpecialValue, 2),
      specialValue = _getSpecialValue2[0],
      setSpecialValue = _getSpecialValue2[1];

  function getMainValue(dadosProduto) {
    _s2();

    var initialMainValue = null;

    if (dadosProduto != undefined) {
      if (parseInt(dadosProduto.has_option) == 0) {
        // PRODUTO SEM OPCOES
        if (dadosProduto.special) {
          initialMainValue = dadosProduto.price;
        } else {
          initialMainValue = null;
        }
      } else {
        if (initialMainValue == null && dadosProduto.price) {
          initialMainValue = dadosProduto.price;
        }
      }
    }

    var mainValueState = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(initialMainValue);
    return mainValueState;
  }

  _s2(getMainValue, "loXbXvMqcS2N0dBaSjwn2V8iaXI=");

  var _getMainValue = getMainValue(dadosProduto),
      _getMainValue2 = Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_getMainValue, 2),
      mainValue = _getMainValue2[0],
      setMainValue = _getMainValue2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(function () {
    var initialState = 0;
    return initialState;
  }),
      soldOutStock = _useState[0],
      setSoldOutStock = _useState[1];
  /*if(parseInt(dadosProduto.has_option) == 0){ // PRODUTO SEM OPCOES
      
    const valorAtual = dadosProduto.price
    if(dadosProduto.special){
      setSelectedMainValue(valorAtual);
      setSelectedSpecialValue(dadosProduto.special);
    }else{
      setSelectedMainValue(null);
      setSelectedSpecialValue(valorAtual);
    }
      if(!dadosProduto.sold_out){
      setSoldOutStock(0);
    }else{
      setSoldOutStock(1);
    }
  }else{ // PRODUTO COM OPCOES
    setSoldOutStock(0);
        if(selectedMainValue == null){
        setSelectedMainValue(dadosProduto.price)
      }
      if(selectedSpecialValue == null && dadosProduto.special){
        setSelectedSpecialValue(dadosProduto.special)
      }*/

  /*for(var i in dadosProduto.options){
    grupoAtual = dadosProduto.options[i]
    for(var opt in grupoAtual.option_value){
      if(grupoAtual.option_value[opt].option_value_id==dadosProduto.opcao_selecionada){
        initialOption = grupoAtual.product_option_id+'_'+grupoAtual.option_value[opt].product_option_value_id
        this.SelectOption(initialOption, null)
      }
    }
  }
        
  }*/


  var changeZoom = function changeZoom(ev) {
    var imgShow = ev.currentTarget.dataset.srcshow;
    var imgPopup = ev.currentTarget.dataset.srcpopup;
    var tempImage = {};
    tempImage['show'] = imgShow;
    tempImage['popup'] = imgPopup; //setInitialImage(tempImage);
  };

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])();

  if (!router.isFallback && !(dadosProduto !== null && dadosProduto !== void 0 && dadosProduto.product_id)) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_error__WEBPACK_IMPORTED_MODULE_4___default.a, {
      statusCode: 404
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 173,
      columnNumber: 12
    }, this);
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_layout__WEBPACK_IMPORTED_MODULE_11__["default"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_container__WEBPACK_IMPORTED_MODULE_10__["default"], {
      children: router.isFallback ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        children: "Loading\u2026"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 179,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_5___default.a, {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
            children: dadosProduto.name
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 183,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "description",
            content: "lelele"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 184,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "og:image",
            content: "lilili"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 185,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 182,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateHeader, {
          configs: configs.resposta,
          mainMenu: mainMenu.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 187,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          clas: "main-content",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "page-products",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "container-padding light-background nproduct-breadcrumb",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "container",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ol", {
                  className: "breadcrumb",
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                      href: "/",
                      title: "P\xE1gina inicial",
                      children: "Home"
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 194,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 193,
                    columnNumber: 24
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: dadosProduto.name
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 196,
                    columnNumber: 24
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 192,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                  className: "cp-preview3",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                    className: "container",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                      className: "nproduct-page",
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-gallery",
                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-thumbnails ",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images-container",
                            children: dadosProduto.images ? Object.keys(dadosProduto.images).length > 4 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "sliderThumbs"
                            }, sliderThumbs), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 218,
                                    columnNumber: 57
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 215,
                                  columnNumber: 53
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 232,
                                      columnNumber: 51
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 233,
                                      columnNumber: 51
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 231,
                                    columnNumber: 49
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 230,
                                  columnNumber: 47
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 211,
                              columnNumber: 39
                            }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 249,
                                    columnNumber: 53
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 246,
                                  columnNumber: 49
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 264,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 265,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 263,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 262,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }, void 0, true) : null
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 207,
                            columnNumber: 32
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 206,
                          columnNumber: 31
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-images min-width-415px",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "areaZoom",
                            children: [dadosProduto.labels.promo_top_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 283,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 282,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_top_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 291,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 290,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 299,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 298,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 307,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 306,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_image_magnify__WEBPACK_IMPORTED_MODULE_8__["default"], _objectSpread({}, {
                              smallImage: {
                                alt: dadosProduto.name,
                                isFluidWidth: true,
                                src: initialImages.show
                              },
                              largeImage: {
                                src: initialImages.popup,
                                width: configs.widthZoom,
                                height: configs.widthZoom
                              }
                            }), void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 313,
                              columnNumber: 37
                            }, this)]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 279,
                            columnNumber: 35
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 278,
                          columnNumber: 33
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "max-width-414px",
                          children: Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 1 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "slideVitrine"
                            }, settingsMobileSlide), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 336,
                                    columnNumber: 49
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 335,
                                  columnNumber: 47
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 345,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 346,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 344,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 343,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 332,
                              columnNumber: 37
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 331,
                            columnNumber: 35
                          }, this) : Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                            children: [Object.keys(dadosProduto.images).map(function (item, key) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                  src: dadosProduto.images[item].popup,
                                  alt: dadosProduto.name
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 360,
                                  columnNumber: 45
                                }, _this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 359,
                                columnNumber: 43
                              }, _this);
                            }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom item-video",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  onClick: "",
                                  "data-videoid": itemVideo.id_video,
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 369,
                                    columnNumber: 49
                                  }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-youtube-play",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 370,
                                    columnNumber: 49
                                  }, _this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 368,
                                  columnNumber: 47
                                }, _this)
                              }, keyVideo, false, {
                                fileName: _jsxFileName,
                                lineNumber: 367,
                                columnNumber: 45
                              }, _this);
                            }) : null]
                          }, void 0, true) : null
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 329,
                          columnNumber: 31
                        }, this)]
                      }, void 0, true, {
                        fileName: _jsxFileName,
                        lineNumber: 205,
                        columnNumber: 29
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-info",
                        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-actions container-padding container-padding-top",
                          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "nproduct-header",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
                              className: "nproduct-title",
                              children: dadosProduto.name
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 386,
                              columnNumber: 38
                            }, this), dadosProduto.model ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              className: "infosProduct",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "titleInfo",
                                  children: "REF:"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 390,
                                  columnNumber: 49
                                }, this), " ", dadosProduto.model]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 390,
                                columnNumber: 45
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 389,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "rateBox",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                className: "lk-avaliar",
                                onClick: "",
                                children: "Avaliar agora"
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 397,
                                columnNumber: 41
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 396,
                              columnNumber: 38
                            }, this), dadosProduto["short"] != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "infosArea",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "resumeProduct",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  dangerouslySetInnerHTML: {
                                    __html: dadosProduto["short"]
                                  }
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 404,
                                  columnNumber: 43
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 403,
                                columnNumber: 42
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 402,
                              columnNumber: 41
                            }, this) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 385,
                            columnNumber: 35
                          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "buyArea",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "nprodct-price",
                                children: [specialValue ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "oldPrice",
                                  children: ["De ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "nproduct-price-max",
                                    children: getMainValue(dadosProduto)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 419,
                                    columnNumber: 50
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 418,
                                  columnNumber: 45
                                }, this) : '', /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "nproduct-price-value",
                                  children: ["Por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "specialValue",
                                    children: getSpecialValue(dadosProduto)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 426,
                                    columnNumber: 53
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "item-discount",
                                    children: dadosProduto.discount_percent
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 427,
                                    columnNumber: 48
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 424,
                                  columnNumber: 43
                                }, this), selectedQtdParcel != '' && selectedValParcel != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                  className: "selectedParcel",
                                  children: ["Ou ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "numParc",
                                    children: [selectedQtdParcel, "x"]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 432,
                                    columnNumber: 78
                                  }, this), " de ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "valParc",
                                    children: selectedValParcel
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 432,
                                    columnNumber: 135
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 432,
                                  columnNumber: 45
                                }, this) : null]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 416,
                                columnNumber: 39
                              }, this), dadosProduto != '' ? parseInt(dadosProduto.has_option) != 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "optionsArea",
                                children: dadosProduto.options.map(function (item, key) {
                                  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                    className: "box-option",
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                      children: item.name
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 445,
                                      columnNumber: 53
                                    }, _this), item.type == 'text' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                      className: "txtOption",
                                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                        id: "fieldOption",
                                        maxlength: "3",
                                        type: "text",
                                        "data-group": item.product_option_id,
                                        className: "field",
                                        name: "txt-option"
                                      }, void 0, false, {
                                        fileName: _jsxFileName,
                                        lineNumber: 449,
                                        columnNumber: 57
                                      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                        className: "help",
                                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                          className: "fa fa-question-circle color2",
                                          "aria-hidden": "true"
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 451,
                                          columnNumber: 61
                                        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                          className: "infoHelp",
                                          children: "Insira at\xE9 3 letras para personalizar a camisa com um bordado exclusivo."
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 453,
                                          columnNumber: 61
                                        }, _this)]
                                      }, void 0, true, {
                                        fileName: _jsxFileName,
                                        lineNumber: 450,
                                        columnNumber: 57
                                      }, _this)]
                                    }, void 0, true, {
                                      fileName: _jsxFileName,
                                      lineNumber: 448,
                                      columnNumber: 55
                                    }, _this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                                      className: "listOptions",
                                      children: item.option_value.map(function (itemOption, key) {
                                        return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                            className: selectedOptionBuy !== item.product_option_id + '_' + itemOption.product_option_value_id ? 'option' : 'option selected',
                                            onClick: "",
                                            href: "javascript:;",
                                            children: itemOption.name
                                          }, void 0, false, {
                                            fileName: _jsxFileName,
                                            lineNumber: 463,
                                            columnNumber: 63
                                          }, _this)
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 462,
                                          columnNumber: 61
                                        }, _this);
                                      })
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 459,
                                      columnNumber: 55
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 444,
                                    columnNumber: 51
                                  }, _this);
                                })
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 441,
                                columnNumber: 44
                              }, this) : '' : '']
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 415,
                              columnNumber: 37
                            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "quantityArea",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                  children: "Quantidade"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 481,
                                  columnNumber: 44
                                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "buttonsQuantity",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnLess",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-minus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 483,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 483,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                    id: "txtQuantity",
                                    type: "text",
                                    name: "txt-quantity",
                                    value: quantityBuy,
                                    className: "txtQuantity"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 484,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnMore",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-plus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 485,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 485,
                                    columnNumber: 47
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 482,
                                  columnNumber: 44
                                }, this)]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 480,
                                columnNumber: 41
                              }, this), soldOutStock != null && !soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton btn_buy",
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fas fa fa-shopping-cart"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 492,
                                    columnNumber: 52
                                  }, this), " ", 'Comprar']
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 491,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 490,
                                columnNumber: 43
                              }, this) : soldOutStock != null && soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton notifyButton",
                                  "data-productid": dadosProduto.product_id,
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-envelope",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 498,
                                    columnNumber: 52
                                  }, this), " Avise-me"]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 497,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 496,
                                columnNumber: 43
                              }, this) : null]
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 479,
                              columnNumber: 39
                            }, this), dadosProduto.text_prevenda != null && dadosProduto.text_prevenda != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                className: "checkPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                  id: "ckprevenda",
                                  type: "checkbox",
                                  onChange: ""
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 508,
                                  columnNumber: 70
                                }, this), " Concordo com o prazo de entrega descrito abaixo."]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 508,
                                columnNumber: 41
                              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "infoPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
                                  className: "tit",
                                  children: "TERMO DE ACEITA\xC7\xC3O"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 510,
                                  columnNumber: 43
                                }, this), dadosProduto.text_prevenda]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 509,
                                columnNumber: 41
                              }, this)]
                            }, void 0, true) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 413,
                            columnNumber: 35
                          }, this), dadosProduto.guia_medidas ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "guiasMedida",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              children: dadosProduto.guia_medidas.map(function (itemGuia, keyGuia) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                  className: "itemGuia",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    className: "color2",
                                    title: itemGuia.title,
                                    "data-tituloguia": "",
                                    "data-conteudoguia": "",
                                    onClick: "",
                                    children: itemGuia.title
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 526,
                                    columnNumber: 47
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 525,
                                  columnNumber: 43
                                }, _this);
                              })
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 522,
                              columnNumber: 39
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 521,
                            columnNumber: 37
                          }, this) : null]
                        }, void 0, true, {
                          fileName: _jsxFileName,
                          lineNumber: 384,
                          columnNumber: 33
                        }, this)
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 383,
                        columnNumber: 29
                      }, this)]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 203,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 202,
                    columnNumber: 23
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 201,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 191,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 190,
              columnNumber: 17
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 189,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 188,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateFooter, {
          configs: configs.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 551,
          columnNumber: 13
        }, this)]
      }, void 0, true)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 177,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 176,
    columnNumber: 5
  }, this);
}

_s3(Product, "9ivUjH6RR1GoW/oyO3lSZCyjhfw=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"]];
});

_c5 = Product;
;

var _c, _c2, _c3, _c4, _c5;

$RefreshReg$(_c, "TemplateHeader$dynamic");
$RefreshReg$(_c2, "TemplateHeader");
$RefreshReg$(_c3, "TemplateFooter$dynamic");
$RefreshReg$(_c4, "TemplateFooter");
$RefreshReg$(_c5, "Product");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvW3NsdWddLmpzIl0sIm5hbWVzIjpbImlkU3RvcmVBcHAiLCJUZW1wbGF0ZUhlYWRlciIsImR5bmFtaWMiLCJUZW1wbGF0ZUZvb3RlciIsInNsaWRlclRodW1icyIsImluZmluaXRlIiwidmVydGljYWwiLCJzbGlkZXNUb1Nob3ciLCJzbGlkZXNUb1Njcm9sbCIsInNwZWVkIiwicmVzcG9uc2l2ZSIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsInNldHRpbmdzTW9iaWxlU2xpZGUiLCJkb3RzIiwiUHJvZHVjdCIsImNvbmZpZ3MiLCJtYWluTWVudSIsImRhZG9zUHJvZHV0byIsImluaXRpYWxJbWFnZXMiLCJnZXRJbml0aWFsSW1hZ2UiLCJpbWdJbmljaWFsIiwidGVtcEltZ0luaWNpYWwiLCJpIiwiaW1hZ2VzIiwicHVzaCIsImluaXRpYWxLZXkiLCJpbWFnZSIsInN1YnN0cmluZyIsImxhc3RJbmRleE9mIiwidW5kZWZpbmVkIiwic2hvdyIsInBvcHVwIiwidGh1bWIiLCJzZWxlY3RlZFF0ZFBhcmNlbCIsInNlbGVjdGVkVmFsUGFyY2VsIiwicXVhbnRpdHlCdXkiLCJzaGlwcGluZ01ldGhvZHMiLCJzZWxlY3RlZE9wdGlvbkJ1eSIsImdldFNwZWNpYWxWYWx1ZSIsImluaXRpYWxTcGVjaWFsVmFsdWUiLCJwYXJzZUludCIsImhhc19vcHRpb24iLCJzcGVjaWFsIiwicHJpY2UiLCJzcGVjaWFsVmFsdWUiLCJ1c2VTdGF0ZSIsInNldFNwZWNpYWxWYWx1ZSIsImdldE1haW5WYWx1ZSIsImluaXRpYWxNYWluVmFsdWUiLCJtYWluVmFsdWVTdGF0ZSIsIm1haW5WYWx1ZSIsInNldE1haW5WYWx1ZSIsImluaXRpYWxTdGF0ZSIsInNvbGRPdXRTdG9jayIsInNldFNvbGRPdXRTdG9jayIsImNoYW5nZVpvb20iLCJldiIsImltZ1Nob3ciLCJjdXJyZW50VGFyZ2V0IiwiZGF0YXNldCIsInNyY3Nob3ciLCJpbWdQb3B1cCIsInNyY3BvcHVwIiwidGVtcEltYWdlIiwicm91dGVyIiwidXNlUm91dGVyIiwiaXNGYWxsYmFjayIsInByb2R1Y3RfaWQiLCJuYW1lIiwicmVzcG9zdGEiLCJPYmplY3QiLCJrZXlzIiwibGVuZ3RoIiwibWFwIiwiaXRlbSIsImtleSIsInZpZGVvcyIsIml0ZW1WaWRlbyIsImtleVZpZGVvIiwiaWRfdmlkZW8iLCJsYWJlbHMiLCJwcm9tb190b3BfbGVmdCIsInByb21vX3RvcF9yaWdodCIsInByb21vX2JvdHRvbV9sZWZ0IiwicHJvbW9fYm90dG9tX3JpZ2h0Iiwic21hbGxJbWFnZSIsImFsdCIsImlzRmx1aWRXaWR0aCIsInNyYyIsImxhcmdlSW1hZ2UiLCJ3aWR0aCIsIndpZHRoWm9vbSIsImhlaWdodCIsIm1vZGVsIiwiX19odG1sIiwiZGlzY291bnRfcGVyY2VudCIsIm9wdGlvbnMiLCJ0eXBlIiwicHJvZHVjdF9vcHRpb25faWQiLCJvcHRpb25fdmFsdWUiLCJpdGVtT3B0aW9uIiwicHJvZHVjdF9vcHRpb25fdmFsdWVfaWQiLCJ0ZXh0X3ByZXZlbmRhIiwiZ3VpYV9tZWRpZGFzIiwiaXRlbUd1aWEiLCJrZXlHdWlhIiwidGl0bGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBR0EsSUFBSUEsVUFBVSxHQUFHLG1CQUFqQjtBQUNBLElBQUlDLGNBQWMsR0FBR0MsbURBQU8sTUFBQztBQUFBLFNBQU0sOEZBQU8sSUFBeUIsR0FBQ0YsVUFBMUIsR0FBcUMsb0JBQTVDLENBQU47QUFBQSxDQUFEO0FBQUE7QUFBQTtBQUFBLGtDQUFjLDBHQUF5QixHQUFDQSxVQUExQixHQUFxQyxvQkFBbkQ7QUFBQTtBQUFBLGNBQWMsNEJBQTBCQSxVQUExQixHQUFxQyxvQkFBbkQ7QUFBQTtBQUFBLEVBQTVCO01BQUlDLGM7QUFDSixJQUFJRSxjQUFjLEdBQUdELG1EQUFPLE9BQUM7QUFBQSxTQUFNLDhGQUFPLElBQXlCLEdBQUNGLFVBQTFCLEdBQXFDLG9CQUE1QyxDQUFOO0FBQUEsQ0FBRDtBQUFBO0FBQUE7QUFBQSxrQ0FBYywwR0FBeUIsR0FBQ0EsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxjQUFjLDRCQUEwQkEsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxFQUE1QjtNQUFJRyxjO0FBRUosSUFBSUMsWUFBWSxHQUFHO0FBQ2pCQyxVQUFRLEVBQUUsS0FETztBQUVqQkMsVUFBUSxFQUFFLElBRk87QUFHakJDLGNBQVksRUFBRSxDQUhHO0FBSWpCQyxnQkFBYyxFQUFFLENBSkM7QUFLakJDLE9BQUssRUFBRSxHQUxVO0FBTWpCQyxZQUFVLEVBQUUsQ0FDVjtBQUNFQyxjQUFVLEVBQUUsR0FEZDtBQUVFQyxZQUFRLEVBQUU7QUFDUk4sY0FBUSxFQUFFLEtBREY7QUFFUkMsa0JBQVksRUFBRTtBQUZOO0FBRlosR0FEVTtBQU5LLENBQW5CO0FBaUJBLElBQUlNLG1CQUFtQixHQUFHO0FBQ3hCTixjQUFZLEVBQUUsQ0FEVTtBQUV4QkMsZ0JBQWMsRUFBRSxDQUZRO0FBR3hCQyxPQUFLLEVBQUUsR0FIaUI7QUFJeEJLLE1BQUksRUFBRTtBQUprQixDQUExQjs7QUFPZSxTQUFTQyxPQUFULE9BQXNEO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBLE1BQW5DQyxPQUFtQyxRQUFuQ0EsT0FBbUM7QUFBQSxNQUExQkMsUUFBMEIsUUFBMUJBLFFBQTBCO0FBQUEsTUFBaEJDLFlBQWdCLFFBQWhCQSxZQUFnQjtBQUNuRSxNQUFJQyxhQUFhLEdBQUcsRUFBcEI7O0FBQ0EsV0FBU0MsZUFBVCxDQUF5QkYsWUFBekIsRUFBdUM7QUFFckMsUUFBSUcsVUFBVSxHQUFHLEVBQWpCO0FBQ0EsUUFBSUMsY0FBYyxHQUFHLEVBQXJCOztBQUVBLFNBQUksSUFBSUMsQ0FBUixJQUFhTCxZQUFZLENBQUNNLE1BQTFCLEVBQWlDO0FBQy9CRixvQkFBYyxDQUFDRyxJQUFmLENBQW9CLENBQUNGLENBQUQsRUFBSUwsWUFBWSxDQUFDTSxNQUFiLENBQXFCRCxDQUFyQixDQUFKLENBQXBCO0FBQ0Q7O0FBRUQsUUFBSUcsVUFBVSxHQUFHUixZQUFZLENBQUNTLEtBQWIsQ0FBbUJDLFNBQW5CLENBQTZCVixZQUFZLENBQUNTLEtBQWIsQ0FBbUJFLFdBQW5CLENBQStCLEdBQS9CLElBQW9DLENBQWpFLENBQWpCOztBQUVBLFFBQUdYLFlBQVksQ0FBQ00sTUFBYixJQUF1Qk0sU0FBdkIsSUFBb0NaLFlBQVksQ0FBQ00sTUFBYixDQUFvQkUsVUFBcEIsS0FBbUMsSUFBMUUsRUFBK0U7QUFDN0VMLGdCQUFVLENBQUMsTUFBRCxDQUFWLEdBQXFCSCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JFLFVBQXBCLEVBQWdDSyxJQUFyRDtBQUNBVixnQkFBVSxDQUFDLE9BQUQsQ0FBVixHQUFzQkgsWUFBWSxDQUFDTSxNQUFiLENBQW9CRSxVQUFwQixFQUFnQ00sS0FBdEQ7QUFDQVgsZ0JBQVUsQ0FBQyxPQUFELENBQVYsR0FBc0JILFlBQVksQ0FBQ00sTUFBYixDQUFvQkUsVUFBcEIsRUFBZ0NPLEtBQXREO0FBQ0QsS0FKRCxNQUlNLElBQUdYLGNBQWMsQ0FBQyxDQUFELENBQWQsSUFBcUIsSUFBeEIsRUFBNkI7QUFDakNELGdCQUFVLEdBQUdDLGNBQWMsQ0FBQyxDQUFELENBQWQsQ0FBa0IsQ0FBbEIsQ0FBYjtBQUNEOztBQUVELFdBQU9ELFVBQVA7QUFDRDs7QUFDRCxNQUFHSCxZQUFILEVBQWdCO0FBQ2RDLGlCQUFhLEdBQUdDLGVBQWUsQ0FBQ0YsWUFBRCxDQUEvQjtBQUNEOztBQUVELE1BQUlnQixpQkFBaUIsR0FBRyxDQUF4QjtBQUNBLE1BQUlDLGlCQUFpQixHQUFHLENBQXhCO0FBQ0EsTUFBSUMsV0FBVyxHQUFHLENBQWxCO0FBQ0EsTUFBSUMsZUFBZSxHQUFHLElBQXRCO0FBQ0EsTUFBSUMsaUJBQWlCLEdBQUcsQ0FBeEI7O0FBRUEsV0FBU0MsZUFBVCxDQUF5QnJCLFlBQXpCLEVBQXVDO0FBQUE7O0FBQ3JDLFFBQUlzQixtQkFBbUIsR0FBRyxJQUExQjs7QUFDQSxRQUFHdEIsWUFBWSxJQUFJWSxTQUFuQixFQUE2QjtBQUMzQixVQUFHVyxRQUFRLENBQUN2QixZQUFZLENBQUN3QixVQUFkLENBQVIsSUFBcUMsQ0FBeEMsRUFBMEM7QUFBRTtBQUMxQyxZQUFHeEIsWUFBWSxDQUFDeUIsT0FBaEIsRUFBd0I7QUFDdEJILDZCQUFtQixHQUFHdEIsWUFBWSxDQUFDeUIsT0FBbkM7QUFDRCxTQUZELE1BRUs7QUFDSEgsNkJBQW1CLEdBQUd0QixZQUFZLENBQUMwQixLQUFuQztBQUNEO0FBQ0YsT0FORCxNQU1LO0FBQ0gsWUFBR0osbUJBQW1CLElBQUksSUFBdkIsSUFBK0J0QixZQUFZLENBQUN5QixPQUEvQyxFQUF1RDtBQUNyREgsNkJBQW1CLEdBQUd0QixZQUFZLENBQUN5QixPQUFuQztBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxRQUFNRSxZQUFZLEdBQUdDLHNEQUFRLENBQUNOLG1CQUFELENBQTdCO0FBQ0EsV0FBT0ssWUFBUDtBQUNEOztBQW5Ea0UsS0FpQzFETixlQWpDMEQ7O0FBQUEseUJBb0QzQkEsZUFBZSxDQUFDckIsWUFBRCxDQXBEWTtBQUFBO0FBQUEsTUFvRDVEMkIsWUFwRDREO0FBQUEsTUFvRDlDRSxlQXBEOEM7O0FBc0RuRSxXQUFTQyxZQUFULENBQXNCOUIsWUFBdEIsRUFBb0M7QUFBQTs7QUFDbEMsUUFBSStCLGdCQUFnQixHQUFHLElBQXZCOztBQUNBLFFBQUcvQixZQUFZLElBQUlZLFNBQW5CLEVBQTZCO0FBQzNCLFVBQUdXLFFBQVEsQ0FBQ3ZCLFlBQVksQ0FBQ3dCLFVBQWQsQ0FBUixJQUFxQyxDQUF4QyxFQUEwQztBQUFFO0FBQzFDLFlBQUd4QixZQUFZLENBQUN5QixPQUFoQixFQUF3QjtBQUN0Qk0sMEJBQWdCLEdBQUcvQixZQUFZLENBQUMwQixLQUFoQztBQUNELFNBRkQsTUFFSztBQUNISywwQkFBZ0IsR0FBRyxJQUFuQjtBQUNEO0FBQ0YsT0FORCxNQU1LO0FBQ0gsWUFBR0EsZ0JBQWdCLElBQUksSUFBcEIsSUFBNEIvQixZQUFZLENBQUMwQixLQUE1QyxFQUFrRDtBQUNoREssMEJBQWdCLEdBQUcvQixZQUFZLENBQUMwQixLQUFoQztBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxRQUFJTSxjQUFjLEdBQUdKLHNEQUFRLENBQUNHLGdCQUFELENBQTdCO0FBQ0EsV0FBT0MsY0FBUDtBQUNEOztBQXhFa0UsTUFzRDFERixZQXREMEQ7O0FBQUEsc0JBeUVuQ0EsWUFBWSxDQUFDOUIsWUFBRCxDQXpFdUI7QUFBQTtBQUFBLE1BeUU5RGlDLFNBekU4RDtBQUFBLE1BeUVuREMsWUF6RW1EOztBQUFBLGtCQTJFM0JOLHNEQUFRLENBQUMsWUFBTTtBQUNyRCxRQUFNTyxZQUFZLEdBQUcsQ0FBckI7QUFDQSxXQUFPQSxZQUFQO0FBQ0QsR0FIK0MsQ0EzRW1CO0FBQUEsTUEyRTVEQyxZQTNFNEQ7QUFBQSxNQTJFOUNDLGVBM0U4QztBQWlGbkU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFJTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFRSxNQUFJQyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDQyxFQUFELEVBQVE7QUFDdkIsUUFBTUMsT0FBTyxHQUFHRCxFQUFFLENBQUNFLGFBQUgsQ0FBaUJDLE9BQWpCLENBQXlCQyxPQUF6QztBQUNBLFFBQU1DLFFBQVEsR0FBR0wsRUFBRSxDQUFDRSxhQUFILENBQWlCQyxPQUFqQixDQUF5QkcsUUFBMUM7QUFDQSxRQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFDQUEsYUFBUyxDQUFDLE1BQUQsQ0FBVCxHQUFvQk4sT0FBcEI7QUFDQU0sYUFBUyxDQUFDLE9BQUQsQ0FBVCxHQUFxQkYsUUFBckIsQ0FMdUIsQ0FNdkI7QUFFRCxHQVJEOztBQVdBLE1BQU1HLE1BQU0sR0FBR0MsNkRBQVMsRUFBeEI7O0FBQ0EsTUFBSSxDQUFDRCxNQUFNLENBQUNFLFVBQVIsSUFBc0IsRUFBQ2pELFlBQUQsYUFBQ0EsWUFBRCxlQUFDQSxZQUFZLENBQUVrRCxVQUFmLENBQTFCLEVBQXFEO0FBQ25ELHdCQUFPLHFFQUFDLGlEQUFEO0FBQVcsZ0JBQVUsRUFBRTtBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBQVA7QUFDRDs7QUFDRCxzQkFDRSxxRUFBQywyREFBRDtBQUFBLDJCQUNFLHFFQUFDLDhEQUFEO0FBQUEsZ0JBQ0dILE1BQU0sQ0FBQ0UsVUFBUCxnQkFDQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURELGdCQUdDO0FBQUEsZ0NBQ0UscUVBQUMsZ0RBQUQ7QUFBQSxrQ0FDRTtBQUFBLHNCQUFRakQsWUFBWSxDQUFDbUQ7QUFBckI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQUVFO0FBQU0sZ0JBQUksRUFBQyxhQUFYO0FBQXlCLG1CQUFPLEVBQUM7QUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFGRixlQUdFO0FBQU0sZ0JBQUksRUFBQyxVQUFYO0FBQXNCLG1CQUFPLEVBQUM7QUFBOUI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFNRSxxRUFBQyxjQUFEO0FBQWdCLGlCQUFPLEVBQUVyRCxPQUFPLENBQUNzRCxRQUFqQztBQUEyQyxrQkFBUSxFQUFFckQsUUFBUSxDQUFDcUQ7QUFBOUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFORixlQU9FO0FBQUssY0FBSSxFQUFDLGNBQVY7QUFBQSxpQ0FDRTtBQUFLLHFCQUFTLEVBQUMsZUFBZjtBQUFBLG1DQUNFO0FBQUssdUJBQVMsRUFBQyx3REFBZjtBQUFBLHFDQUNFO0FBQUsseUJBQVMsRUFBQyxXQUFmO0FBQUEsd0NBQ0U7QUFBSSwyQkFBUyxFQUFDLFlBQWQ7QUFBQSwwQ0FDRztBQUFJLDZCQUFTLEVBQUMsaUJBQWQ7QUFBQSwyQ0FDRztBQUFHLDBCQUFJLEVBQUMsR0FBUjtBQUFZLDJCQUFLLEVBQUMsbUJBQWxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREg7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFESCxlQUlHO0FBQUksNkJBQVMsRUFBQyxpQkFBZDtBQUFBLDhCQUNJcEQsWUFBWSxDQUFDbUQ7QUFEakI7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFKSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREYsZUFVRTtBQUFLLDJCQUFTLEVBQUMsYUFBZjtBQUFBLHlDQUNFO0FBQUssNkJBQVMsRUFBQyxXQUFmO0FBQUEsMkNBQ0k7QUFBSywrQkFBUyxFQUFDLGVBQWY7QUFBQSw4Q0FFRTtBQUFLLGlDQUFTLEVBQUMsa0JBQWY7QUFBQSxnREFDRTtBQUFLLG1DQUFTLEVBQUMscUJBQWY7QUFBQSxpREFDQztBQUFLLHFDQUFTLEVBQUMsMEJBQWY7QUFBQSxzQ0FFSW5ELFlBQVksQ0FBQ00sTUFBZCxHQUNHK0MsTUFBTSxDQUFDQyxJQUFQLENBQVl0RCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDaUQsTUFBakMsR0FBMEMsQ0FBM0MsZ0JBQ0UscUVBQUMsa0RBQUQ7QUFBUSx1Q0FBUyxFQUFDO0FBQWxCLCtCQUFxQ3JFLFlBQXJDO0FBQUEseUNBR01tRSxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNrRCxHQUFqQyxDQUFxQyxVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxvREFDN0I7QUFDRSwyQ0FBUyxFQUFDLDZFQURaO0FBQUEseURBR0k7QUFBSyx1Q0FBRyxFQUFFMUQsWUFBWSxDQUFDTSxNQUFiLENBQW9CbUQsSUFBcEIsRUFBMEIzQyxLQUFwQztBQUNFLHVDQUFHLEVBQUVkLFlBQVksQ0FBQ21ELElBRHBCO0FBRUUsb0RBQWNuRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjVDLElBRjFDO0FBR0UscURBQWViLFlBQVksQ0FBQ00sTUFBYixDQUFvQm1ELElBQXBCLEVBQTBCM0MsS0FIM0M7QUFJRSx5Q0FBSyxFQUFDLElBSlI7QUFJYSwwQ0FBTSxFQUFDLElBSnBCO0FBS0UsMkNBQU8sRUFBQztBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFISjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQUQ2QjtBQUFBLCtCQUFyQyxDQUhOLEVBaUJLZCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSixNQUFyQixHQUNFdkQsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkgsR0FBcEIsQ0FBd0IsVUFBQ0ksU0FBRCxFQUFZQyxRQUFaO0FBQUEsb0RBQ3RCO0FBQUssMkNBQVMsRUFBQyxZQUFmO0FBQUEseURBQ0U7QUFBRyx3Q0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQU8sRUFBQyxFQUEvQjtBQUFrQyxvREFBY0QsU0FBUyxDQUFDRSxRQUExRDtBQUFBLDREQUNFO0FBQUsseUNBQUcsdUNBQWdDRixTQUFTLENBQUNFLFFBQTFDO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FERixlQUVFO0FBQUcsK0NBQVMsRUFBQyxvQkFBYjtBQUFrQyxxREFBWTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLG1DQUFpQ0QsUUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FEc0I7QUFBQSwrQkFBeEIsQ0FERixHQVVFLElBM0JOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixnQkFpQ0U7QUFBQSx5Q0FFRVIsTUFBTSxDQUFDQyxJQUFQLENBQVl0RCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDa0QsR0FBakMsQ0FBcUMsVUFBQ0MsSUFBRCxFQUFPQyxHQUFQO0FBQUEsb0RBQzdCO0FBQ0UsMkNBQVMsRUFBQyw2RUFEWjtBQUFBLHlEQUdJO0FBQUssdUNBQUcsRUFBRTFELFlBQVksQ0FBQ00sTUFBYixDQUFvQm1ELElBQXBCLEVBQTBCM0MsS0FBcEM7QUFDRSx1Q0FBRyxFQUFFZCxZQUFZLENBQUNtRCxJQURwQjtBQUVFLG9EQUFjbkQsWUFBWSxDQUFDTSxNQUFiLENBQW9CbUQsSUFBcEIsRUFBMEI1QyxJQUYxQztBQUdFLHFEQUFlYixZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjNDLEtBSDNDO0FBSUUseUNBQUssRUFBQyxJQUpSO0FBSWEsMENBQU0sRUFBQyxJQUpwQjtBQUtFLDJDQUFPLEVBQUM7QUFMVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSEo7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FENkI7QUFBQSwrQkFBckMsQ0FGRixFQWlCR2QsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkosTUFBckIsR0FDRXZELFlBQVksQ0FBQzJELE1BQWIsQ0FBb0JILEdBQXBCLENBQXdCLFVBQUNJLFNBQUQsRUFBWUMsUUFBWjtBQUFBLG9EQUN0QjtBQUFLLDJDQUFTLEVBQUMsWUFBZjtBQUFBLHlEQUNFO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0Msb0RBQWNELFNBQVMsQ0FBQ0UsUUFBMUQ7QUFBQSw0REFDRTtBQUFLLHlDQUFHLHVDQUFnQ0YsU0FBUyxDQUFDRSxRQUExQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREYsZUFFRTtBQUFHLCtDQUFTLEVBQUMsb0JBQWI7QUFBa0MscURBQVk7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixtQ0FBaUNELFFBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRHNCO0FBQUEsK0JBQXhCLENBREYsR0FVRSxJQTNCSjtBQUFBLDRDQWxDSixHQWdFRTtBQWxFTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0FERixlQXlFSTtBQUFLLG1DQUFTLEVBQUMsZ0NBQWY7QUFBQSxpREFDRTtBQUFLLHFDQUFTLEVBQUMsVUFBZjtBQUFBLHVDQUVLN0QsWUFBWSxDQUFDK0QsTUFBYixDQUFvQkMsY0FBcEIsQ0FBbUN2RCxLQUFuQyxJQUE0QyxJQUE3QyxnQkFDRTtBQUFLLHVDQUFTLEVBQUMsd0JBQWY7QUFBQSxxREFDRTtBQUFLLG1DQUFHLEVBQUVULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JDLGNBQXBCLENBQW1DdkQ7QUFBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREYsR0FLRSxJQVBOLEVBVUtULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JFLGVBQXBCLENBQW9DeEQsS0FBcEMsSUFBNkMsSUFBOUMsZ0JBQ0U7QUFBSyx1Q0FBUyxFQUFDLHlCQUFmO0FBQUEscURBQ0U7QUFBSyxtQ0FBRyxFQUFFVCxZQUFZLENBQUMrRCxNQUFiLENBQW9CRSxlQUFwQixDQUFvQ3hEO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLEdBS0UsSUFmTixFQWtCS1QsWUFBWSxDQUFDK0QsTUFBYixDQUFvQkcsaUJBQXBCLENBQXNDekQsS0FBdEMsSUFBK0MsSUFBaEQsZ0JBQ0U7QUFBSyx1Q0FBUyxFQUFDLDJCQUFmO0FBQUEscURBQ0U7QUFBSyxtQ0FBRyxFQUFFVCxZQUFZLENBQUMrRCxNQUFiLENBQW9CRyxpQkFBcEIsQ0FBc0N6RDtBQUFoRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixHQUtFLElBdkJOLEVBMEJLVCxZQUFZLENBQUMrRCxNQUFiLENBQW9CSSxrQkFBcEIsQ0FBdUMxRCxLQUF2QyxJQUFnRCxJQUFqRCxnQkFDRTtBQUFLLHVDQUFTLEVBQUMsNEJBQWY7QUFBQSxxREFDRTtBQUFLLG1DQUFHLEVBQUVULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JJLGtCQUFwQixDQUF1QzFEO0FBQWpEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLEdBS0UsSUEvQk4sZUFrQ0UscUVBQUMsMkRBQUQsb0JBQXVCO0FBQ2YyRCx3Q0FBVSxFQUFFO0FBQ1pDLG1DQUFHLEVBQUVyRSxZQUFZLENBQUNtRCxJQUROO0FBRVptQiw0Q0FBWSxFQUFFLElBRkY7QUFHWkMsbUNBQUcsRUFBRXRFLGFBQWEsQ0FBQ1k7QUFIUCwrQkFERztBQU1uQjJELHdDQUFVLEVBQUU7QUFDUkQsbUNBQUcsRUFBRXRFLGFBQWEsQ0FBQ2EsS0FEWDtBQUVSMkQscUNBQUssRUFBRTNFLE9BQU8sQ0FBQzRFLFNBRlA7QUFHUkMsc0NBQU0sRUFBRTdFLE9BQU8sQ0FBQzRFO0FBSFI7QUFOTyw2QkFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FsQ0Y7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0F6RUosZUE0SEU7QUFBSyxtQ0FBUyxFQUFDLGlCQUFmO0FBQUEsb0NBQ0lyQixNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsS0FBb0MrQyxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNpRCxNQUFqQyxHQUEwQyxDQUEvRSxnQkFDQztBQUFLLHFDQUFTLEVBQUMsZ0JBQWY7QUFBQSxtREFDRSxxRUFBQyxrREFBRDtBQUFRLHVDQUFTLEVBQUM7QUFBbEIsK0JBQXFDNUQsbUJBQXJDO0FBQUEseUNBRU0wRCxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNrRCxHQUFqQyxDQUFxQyxVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxvREFDakM7QUFBSywyQ0FBUyxFQUFDLFVBQWY7QUFBQSx5REFDRTtBQUFLLHVDQUFHLEVBQUUxRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjNDLEtBQXBDO0FBQTJDLHVDQUFHLEVBQUVkLFlBQVksQ0FBQ21EO0FBQTdEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQURpQztBQUFBLCtCQUFyQyxDQUZOLEVBU0tuRCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSixNQUFyQixHQUNFdkQsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkgsR0FBcEIsQ0FBd0IsVUFBQ0ksU0FBRCxFQUFZQyxRQUFaO0FBQUEsb0RBQ3RCO0FBQUssMkNBQVMsRUFBQyxxQkFBZjtBQUFBLHlEQUNFO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0Msb0RBQWNELFNBQVMsQ0FBQ0UsUUFBMUQ7QUFBQSw0REFDRTtBQUFLLHlDQUFHLHVDQUFnQ0YsU0FBUyxDQUFDRSxRQUExQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREYsZUFFRTtBQUFHLCtDQUFTLEVBQUMsb0JBQWI7QUFBa0MscURBQVk7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixtQ0FBMENELFFBQTFDO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRHNCO0FBQUEsK0JBQXhCLENBREYsR0FVRSxJQW5CTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQURELEdBeUJFUixNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsS0FBb0MrQyxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNpRCxNQUFqQyxHQUEwQyxDQUEvRSxnQkFDRTtBQUFBLHVDQUVJRixNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNrRCxHQUFqQyxDQUFxQyxVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxrREFDbkM7QUFBSyx5Q0FBUyxFQUFDLFVBQWY7QUFBQSx1REFDRTtBQUFLLHFDQUFHLEVBQUUxRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjNDLEtBQXBDO0FBQTJDLHFDQUFHLEVBQUVkLFlBQVksQ0FBQ21EO0FBQTdEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLHVDQURtQztBQUFBLDZCQUFyQyxDQUZKLEVBU0tuRCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSixNQUFyQixHQUNFdkQsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkgsR0FBcEIsQ0FBd0IsVUFBQ0ksU0FBRCxFQUFZQyxRQUFaO0FBQUEsa0RBQ3RCO0FBQUsseUNBQVMsRUFBQyxxQkFBZjtBQUFBLHVEQUNFO0FBQUcsc0NBQUksRUFBQyxjQUFSO0FBQXVCLHlDQUFPLEVBQUMsRUFBL0I7QUFBa0Msa0RBQWNELFNBQVMsQ0FBQ0UsUUFBMUQ7QUFBQSwwREFDRTtBQUFLLHVDQUFHLHVDQUFnQ0YsU0FBUyxDQUFDRSxRQUExQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkNBREYsZUFFRTtBQUFHLDZDQUFTLEVBQUMsb0JBQWI7QUFBa0MsbURBQVk7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQ0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixpQ0FBMENELFFBQTFDO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUNBRHNCO0FBQUEsNkJBQXhCLENBREYsR0FVRSxJQW5CTjtBQUFBLDBDQURGLEdBd0JBO0FBbERKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0NBNUhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw4QkFGRixlQW9MRTtBQUFLLGlDQUFTLEVBQUMsZUFBZjtBQUFBLCtDQUNJO0FBQUssbUNBQVMsRUFBQyx5REFBZjtBQUFBLGtEQUNFO0FBQUsscUNBQVMsRUFBQyxpQkFBZjtBQUFBLG9EQUNHO0FBQUksdUNBQVMsRUFBQyxnQkFBZDtBQUFBLHdDQUFnQzdELFlBQVksQ0FBQ21EO0FBQTdDO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREgsRUFHTW5ELFlBQVksQ0FBQzRFLEtBQWQsZ0JBQ0M7QUFBSSx1Q0FBUyxFQUFDLGNBQWQ7QUFBQSxxREFDSTtBQUFBLHdEQUFJO0FBQU0sMkNBQVMsRUFBQyxXQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FBSixPQUE2QzVFLFlBQVksQ0FBQzRFLEtBQTFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREQsR0FLQyxJQVJOLGVBV0c7QUFBSyx1Q0FBUyxFQUFDLFNBQWY7QUFBQSxxREFDRztBQUFHLHlDQUFTLEVBQUMsWUFBYjtBQUEwQix1Q0FBTyxFQUFDLEVBQWxDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREg7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FYSCxFQWdCTTVFLFlBQVksU0FBWixJQUFzQixFQUF2QixnQkFDQztBQUFLLHVDQUFTLEVBQUMsV0FBZjtBQUFBLHFEQUNDO0FBQUsseUNBQVMsRUFBQyxlQUFmO0FBQUEsdURBQ0M7QUFBSyx5REFBdUIsRUFBRTtBQUFFNkUsMENBQU0sRUFBRTdFLFlBQVk7QUFBdEI7QUFBOUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERDtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURELEdBT0MsSUF2Qk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQURGLGVBNkJFO0FBQUsscUNBQVMsRUFBQyxTQUFmO0FBQUEsb0RBRUU7QUFBSyx1Q0FBUyxFQUFDLFlBQWY7QUFBQSxzREFDRTtBQUFLLHlDQUFTLEVBQUMsZUFBZjtBQUFBLDJDQUNNMkIsWUFBRCxnQkFDQztBQUFLLDJDQUFTLEVBQUMsVUFBZjtBQUFBLGlFQUNLO0FBQU0sNkNBQVMsRUFBQyxvQkFBaEI7QUFBQSw4Q0FBc0NHLFlBQVksQ0FBQzlCLFlBQUQ7QUFBbEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FETDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBREQsR0FLQyxFQU5OLGVBUUk7QUFBSywyQ0FBUyxFQUFDLHNCQUFmO0FBQUEsa0VBRVU7QUFBTSw2Q0FBUyxFQUFDLGNBQWhCO0FBQUEsOENBQWdDcUIsZUFBZSxDQUFDckIsWUFBRDtBQUEvQztBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUZWLGVBR0s7QUFBTSw2Q0FBUyxFQUFDLGVBQWhCO0FBQUEsOENBQWlDQSxZQUFZLENBQUM4RTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUhMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FSSixFQWVNOUQsaUJBQWlCLElBQUksRUFBckIsSUFBMkJDLGlCQUFpQixJQUFJLEVBQWpELGdCQUNDO0FBQUcsMkNBQVMsRUFBQyxnQkFBYjtBQUFBLGlFQUFpQztBQUFNLDZDQUFTLEVBQUMsU0FBaEI7QUFBQSwrQ0FBMkJELGlCQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBQWpDLHVCQUEwRjtBQUFNLDZDQUFTLEVBQUMsU0FBaEI7QUFBQSw4Q0FBMkJDO0FBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBQTFGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FERCxHQUdDLElBbEJOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERixFQXdCS2pCLFlBQVksSUFBSSxFQUFqQixHQUNHdUIsUUFBUSxDQUFDdkIsWUFBWSxDQUFDd0IsVUFBZCxDQUFSLElBQXFDLENBQXRDLGdCQUNDO0FBQUsseUNBQVMsRUFBQyxhQUFmO0FBQUEsMENBRUt4QixZQUFZLENBQUMrRSxPQUFiLENBQXFCdkIsR0FBckIsQ0FBeUIsVUFBQ0MsSUFBRCxFQUFPQyxHQUFQO0FBQUEsc0RBQ3ZCO0FBQUssNkNBQVMsRUFBQyxZQUFmO0FBQUEsNERBQ0U7QUFBQSxnREFBUUQsSUFBSSxDQUFDTjtBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREYsRUFHSU0sSUFBSSxDQUFDdUIsSUFBTCxJQUFhLE1BQWQsZ0JBQ0M7QUFBSywrQ0FBUyxFQUFDLFdBQWY7QUFBQSw4REFDRTtBQUFPLDBDQUFFLEVBQUMsYUFBVjtBQUF3QixpREFBUyxFQUFDLEdBQWxDO0FBQXNDLDRDQUFJLEVBQUMsTUFBM0M7QUFBa0Qsc0RBQVl2QixJQUFJLENBQUN3QixpQkFBbkU7QUFBc0YsaURBQVMsRUFBQyxPQUFoRztBQUF3Ryw0Q0FBSSxFQUFDO0FBQTdHO0FBQUE7QUFBQTtBQUFBO0FBQUEsK0NBREYsZUFFRTtBQUFLLGlEQUFTLEVBQUMsTUFBZjtBQUFBLGdFQUNJO0FBQUcsbURBQVMsRUFBQyw4QkFBYjtBQUE0Qyx5REFBWTtBQUF4RDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlEQURKLGVBR0k7QUFBSyxtREFBUyxFQUFDLFVBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaURBSEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLCtDQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FERCxnQkFZQztBQUFJLCtDQUFTLEVBQUMsYUFBZDtBQUFBLGdEQUVJeEIsSUFBSSxDQUFDeUIsWUFBTCxDQUFrQjFCLEdBQWxCLENBQXNCLFVBQUMyQixVQUFELEVBQWF6QixHQUFiO0FBQUEsNERBQ3BCO0FBQUEsaUVBQ0U7QUFBTyxxREFBUyxFQUFFdEMsaUJBQWlCLEtBQUtxQyxJQUFJLENBQUN3QixpQkFBTCxHQUF1QixHQUF2QixHQUEyQkUsVUFBVSxDQUFDQyx1QkFBNUQsR0FBc0YsUUFBdEYsR0FBaUcsaUJBQW5IO0FBQXNJLG1EQUFPLEVBQUMsRUFBOUk7QUFBaUosZ0RBQUksRUFBQyxjQUF0SjtBQUFBLHNEQUFzS0QsVUFBVSxDQUFDaEM7QUFBakw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsaURBRG9CO0FBQUEsdUNBQXRCO0FBRko7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FmSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkNBRHVCO0FBQUEsaUNBQXpCO0FBRkw7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERCxHQWtDQSxFQW5DRixHQW9DRCxFQTVESDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBRkYsZUFrRUk7QUFBSyx1Q0FBUyxFQUFDLFlBQWY7QUFBQSxzREFDRTtBQUFLLHlDQUFTLEVBQUMsY0FBZjtBQUFBLHdEQUNHO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQURILGVBRUc7QUFBSywyQ0FBUyxFQUFDLGlCQUFmO0FBQUEsMERBQ0c7QUFBRyx3Q0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQU8sRUFBQyxFQUEvQjtBQUFrQyw2Q0FBUyxFQUFDLFNBQTVDO0FBQUEsMkRBQXNEO0FBQUcsK0NBQVMsRUFBQyxhQUFiO0FBQTJCLHFEQUFZO0FBQXZDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBdEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FESCxlQUVHO0FBQU8sc0NBQUUsRUFBQyxhQUFWO0FBQXdCLHdDQUFJLEVBQUMsTUFBN0I7QUFBb0Msd0NBQUksRUFBQyxjQUF6QztBQUF3RCx5Q0FBSyxFQUFHakMsV0FBaEU7QUFBOEUsNkNBQVMsRUFBQztBQUF4RjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUZILGVBR0c7QUFBRyx3Q0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQU8sRUFBQyxFQUEvQjtBQUFrQyw2Q0FBUyxFQUFDLFNBQTVDO0FBQUEsMkRBQXNEO0FBQUcsK0NBQVMsRUFBQyxZQUFiO0FBQTBCLHFEQUFZO0FBQXRDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBdEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FISDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBRkg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURGLEVBVUlrQixZQUFZLElBQUksSUFBaEIsSUFBd0IsQ0FBQ0EsWUFBMUIsZ0JBQ0M7QUFBSyx5Q0FBUyxFQUFDLGVBQWY7QUFBQSx1REFDTTtBQUFHLHNDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBUyxFQUFDLG1CQUFqQztBQUFxRCx5Q0FBTyxFQUFDLEVBQTdEO0FBQUEsMERBQ0c7QUFBRyw2Q0FBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FESCxPQUNnRCxTQURoRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFETjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURELEdBTUVBLFlBQVksSUFBSSxJQUFoQixJQUF3QkEsWUFBekIsZ0JBQ0E7QUFBSyx5Q0FBUyxFQUFDLGVBQWY7QUFBQSx1REFDTTtBQUFHLHNDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBUyxFQUFDLHdCQUFqQztBQUEwRCxvREFBZ0JwQyxZQUFZLENBQUNrRCxVQUF2RjtBQUFtRyx5Q0FBTyxFQUFDLEVBQTNHO0FBQUEsMERBQ0c7QUFBRyw2Q0FBUyxFQUFDLGdCQUFiO0FBQThCLG1EQUFZO0FBQTFDO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBREg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRE47QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FEQSxHQU9BLElBdkJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FsRUosRUE2RklsRCxZQUFZLENBQUNxRixhQUFiLElBQTZCLElBQTdCLElBQXFDckYsWUFBWSxDQUFDcUYsYUFBYixJQUE4QixFQUFwRSxnQkFDQztBQUFBLHNEQUNFO0FBQUcseUNBQVMsRUFBQyxlQUFiO0FBQUEsd0RBQTZCO0FBQU8sb0NBQUUsRUFBQyxZQUFWO0FBQXVCLHNDQUFJLEVBQUMsVUFBNUI7QUFBdUMsMENBQVEsRUFBQztBQUFoRDtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQUE3QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREYsZUFFRTtBQUFLLHlDQUFTLEVBQUMsY0FBZjtBQUFBLHdEQUNFO0FBQUksMkNBQVMsRUFBQyxLQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQURGLEVBRUdyRixZQUFZLENBQUNxRixhQUZoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBRkY7QUFBQSw0Q0FERCxHQVNDLElBdEdKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQ0E3QkYsRUF3SUlyRixZQUFZLENBQUNzRixZQUFkLGdCQUNDO0FBQUsscUNBQVMsRUFBQyxhQUFmO0FBQUEsbURBQ0U7QUFBQSx3Q0FFRXRGLFlBQVksQ0FBQ3NGLFlBQWIsQ0FBMEI5QixHQUExQixDQUE4QixVQUFDK0IsUUFBRCxFQUFXQyxPQUFYO0FBQUEsb0RBQzVCO0FBQUksMkNBQVMsRUFBQyxVQUFkO0FBQUEseURBQ0k7QUFBRyw2Q0FBUyxFQUFDLFFBQWI7QUFBc0IseUNBQUssRUFBRUQsUUFBUSxDQUFDRSxLQUF0QztBQUE2Qyx1REFBZ0IsRUFBN0Q7QUFBZ0UseURBQWtCLEVBQWxGO0FBQXFGLDJDQUFPLEVBQUMsRUFBN0Y7QUFBQSw4Q0FBaUdGLFFBQVEsQ0FBQ0U7QUFBMUc7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRDRCO0FBQUEsK0JBQTlCO0FBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBREQsR0FhQyxJQXJKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLDhCQXBMRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFWRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBUEYsZUFrWEUscUVBQUMsY0FBRDtBQUFnQixpQkFBTyxFQUFFM0YsT0FBTyxDQUFDc0Q7QUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFsWEY7QUFBQTtBQUpKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUE4WEQ7O0lBcGdCdUJ2RCxPO1VBa0lQbUQscUQ7OztNQWxJT25ELE87QUFvZ0J2QiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9bc2x1Z10uYWMwMjc3MzE3Y2UzNTkwZDcyZDIuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgRXJyb3JQYWdlIGZyb20gXCJuZXh0L2Vycm9yXCI7XHJcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcclxuaW1wb3J0IGR5bmFtaWMgZnJvbSAnbmV4dC9keW5hbWljJ1xyXG5pbXBvcnQgeyB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQgUmVhY3RJbWFnZU1hZ25pZnkgZnJvbSAncmVhY3QtaW1hZ2UtbWFnbmlmeSdcclxuaW1wb3J0IFNsaWRlciBmcm9tIFwicmVhY3Qtc2xpY2tcIjtcclxuXHJcbmltcG9ydCBDb250YWluZXIgZnJvbSBcIkAvY29tcG9uZW50cy9jb250YWluZXJcIjtcclxuaW1wb3J0IExheW91dCBmcm9tIFwiQC9jb21wb25lbnRzL2xheW91dFwiO1xyXG5pbXBvcnQgeyBnZXRDb25maWdzLCBnZXRNYWluTWVudSwgZ2V0UHJvZHVjdEJ5U2x1ZywgZ2V0UHJvZHVjdHNCeUNhdGVnb3J5IH0gZnJvbSBcIkAvbGliL2FwaVwiO1xyXG5cclxudmFyIGlkU3RvcmVBcHAgPSAnbjQ5c2hvcHYyX3RyaWpvaWEnO1xyXG52YXIgVGVtcGxhdGVIZWFkZXIgPSBkeW5hbWljKCgpID0+IGltcG9ydCgnQC9jb21wb25lbnRzL3RlbXBsYXRlcy8nK2lkU3RvcmVBcHArJy9jb21wb25lbnRzL2hlYWRlcicpKVxyXG52YXIgVGVtcGxhdGVGb290ZXIgPSBkeW5hbWljKCgpID0+IGltcG9ydCgnQC9jb21wb25lbnRzL3RlbXBsYXRlcy8nK2lkU3RvcmVBcHArJy9jb21wb25lbnRzL2Zvb3RlcicpKVxyXG5cclxubGV0IHNsaWRlclRodW1icyA9IHtcclxuICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgdmVydGljYWw6IHRydWUsXHJcbiAgc2xpZGVzVG9TaG93OiA0LFxyXG4gIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gIHNwZWVkOiA1MDAsXHJcbiAgcmVzcG9uc2l2ZTogW1xyXG4gICAge1xyXG4gICAgICBicmVha3BvaW50OiA0MTUsXHJcbiAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgdmVydGljYWw6IGZhbHNlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzogM1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgXVxyXG59XHJcblxyXG52YXIgc2V0dGluZ3NNb2JpbGVTbGlkZSA9IHtcclxuICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgc3BlZWQ6IDUwMCxcclxuICBkb3RzOiB0cnVlXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFByb2R1Y3QoeyBjb25maWdzLCBtYWluTWVudSwgZGFkb3NQcm9kdXRvIH0pIHtcclxuICB2YXIgaW5pdGlhbEltYWdlcyA9IFtdO1xyXG4gIGZ1bmN0aW9uIGdldEluaXRpYWxJbWFnZShkYWRvc1Byb2R1dG8pIHtcclxuICAgIFxyXG4gICAgdmFyIGltZ0luaWNpYWwgPSBbXTtcclxuICAgIHZhciB0ZW1wSW1nSW5pY2lhbCA9IFtdO1xyXG4gICAgXHJcbiAgICBmb3IodmFyIGkgaW4gZGFkb3NQcm9kdXRvLmltYWdlcyl7XHJcbiAgICAgIHRlbXBJbWdJbmljaWFsLnB1c2goW2ksIGRhZG9zUHJvZHV0by5pbWFnZXMgW2ldXSk7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIGluaXRpYWxLZXkgPSBkYWRvc1Byb2R1dG8uaW1hZ2Uuc3Vic3RyaW5nKGRhZG9zUHJvZHV0by5pbWFnZS5sYXN0SW5kZXhPZignLycpKzEpXHJcbiAgICBcclxuICAgIGlmKGRhZG9zUHJvZHV0by5pbWFnZXMgIT0gdW5kZWZpbmVkICYmIGRhZG9zUHJvZHV0by5pbWFnZXNbaW5pdGlhbEtleV0gIT0gbnVsbCl7XHJcbiAgICAgIGltZ0luaWNpYWxbJ3Nob3cnXSA9IGRhZG9zUHJvZHV0by5pbWFnZXNbaW5pdGlhbEtleV0uc2hvdyBcclxuICAgICAgaW1nSW5pY2lhbFsncG9wdXAnXSA9IGRhZG9zUHJvZHV0by5pbWFnZXNbaW5pdGlhbEtleV0ucG9wdXBcclxuICAgICAgaW1nSW5pY2lhbFsndGh1bWInXSA9IGRhZG9zUHJvZHV0by5pbWFnZXNbaW5pdGlhbEtleV0udGh1bWJcclxuICAgIH1lbHNlIGlmKHRlbXBJbWdJbmljaWFsWzBdICE9IG51bGwpe1xyXG4gICAgICBpbWdJbmljaWFsID0gdGVtcEltZ0luaWNpYWxbMF1bMV1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gaW1nSW5pY2lhbDtcclxuICB9XHJcbiAgaWYoZGFkb3NQcm9kdXRvKXtcclxuICAgIGluaXRpYWxJbWFnZXMgPSBnZXRJbml0aWFsSW1hZ2UoZGFkb3NQcm9kdXRvKTtcclxuICB9XHJcblxyXG4gIHZhciBzZWxlY3RlZFF0ZFBhcmNlbCA9IDE7XHJcbiAgdmFyIHNlbGVjdGVkVmFsUGFyY2VsID0gMTtcclxuICB2YXIgcXVhbnRpdHlCdXkgPSAxO1xyXG4gIHZhciBzaGlwcGluZ01ldGhvZHMgPSBudWxsO1xyXG4gIHZhciBzZWxlY3RlZE9wdGlvbkJ1eSA9IDE7XHJcbiAgXHJcbiAgZnVuY3Rpb24gZ2V0U3BlY2lhbFZhbHVlKGRhZG9zUHJvZHV0bykge1xyXG4gICAgdmFyIGluaXRpYWxTcGVjaWFsVmFsdWUgPSBudWxsO1xyXG4gICAgaWYoZGFkb3NQcm9kdXRvICE9IHVuZGVmaW5lZCl7XHJcbiAgICAgIGlmKHBhcnNlSW50KGRhZG9zUHJvZHV0by5oYXNfb3B0aW9uKSA9PSAwKXsgLy8gUFJPRFVUTyBTRU0gT1BDT0VTXHJcbiAgICAgICAgaWYoZGFkb3NQcm9kdXRvLnNwZWNpYWwpe1xyXG4gICAgICAgICAgaW5pdGlhbFNwZWNpYWxWYWx1ZSA9IGRhZG9zUHJvZHV0by5zcGVjaWFsO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgaW5pdGlhbFNwZWNpYWxWYWx1ZSA9IGRhZG9zUHJvZHV0by5wcmljZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIGlmKGluaXRpYWxTcGVjaWFsVmFsdWUgPT0gbnVsbCAmJiBkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgICBpbml0aWFsU3BlY2lhbFZhbHVlID0gZGFkb3NQcm9kdXRvLnNwZWNpYWxcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgY29uc3Qgc3BlY2lhbFZhbHVlID0gdXNlU3RhdGUoaW5pdGlhbFNwZWNpYWxWYWx1ZSk7XHJcbiAgICByZXR1cm4gc3BlY2lhbFZhbHVlO1xyXG4gIH1cclxuICBjb25zdCBbc3BlY2lhbFZhbHVlLCBzZXRTcGVjaWFsVmFsdWVdID0gZ2V0U3BlY2lhbFZhbHVlKGRhZG9zUHJvZHV0byk7XHJcblxyXG4gIGZ1bmN0aW9uIGdldE1haW5WYWx1ZShkYWRvc1Byb2R1dG8pIHtcclxuICAgIHZhciBpbml0aWFsTWFpblZhbHVlID0gbnVsbDtcclxuICAgIGlmKGRhZG9zUHJvZHV0byAhPSB1bmRlZmluZWQpe1xyXG4gICAgICBpZihwYXJzZUludChkYWRvc1Byb2R1dG8uaGFzX29wdGlvbikgPT0gMCl7IC8vIFBST0RVVE8gU0VNIE9QQ09FU1xyXG4gICAgICAgIGlmKGRhZG9zUHJvZHV0by5zcGVjaWFsKXtcclxuICAgICAgICAgIGluaXRpYWxNYWluVmFsdWUgPSBkYWRvc1Byb2R1dG8ucHJpY2U7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICBpbml0aWFsTWFpblZhbHVlID0gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIGlmKGluaXRpYWxNYWluVmFsdWUgPT0gbnVsbCAmJiBkYWRvc1Byb2R1dG8ucHJpY2Upe1xyXG4gICAgICAgICAgaW5pdGlhbE1haW5WYWx1ZSA9IGRhZG9zUHJvZHV0by5wcmljZVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICB2YXIgbWFpblZhbHVlU3RhdGUgPSB1c2VTdGF0ZShpbml0aWFsTWFpblZhbHVlKTtcclxuICAgIHJldHVybiBtYWluVmFsdWVTdGF0ZTtcclxuICB9XHJcbiAgdmFyIFttYWluVmFsdWUsIHNldE1haW5WYWx1ZV0gPSBnZXRNYWluVmFsdWUoZGFkb3NQcm9kdXRvKTtcclxuXHJcbiAgY29uc3QgW3NvbGRPdXRTdG9jaywgc2V0U29sZE91dFN0b2NrXSA9IHVzZVN0YXRlKCgpID0+IHtcclxuICAgIGNvbnN0IGluaXRpYWxTdGF0ZSA9IDA7XHJcbiAgICByZXR1cm4gaW5pdGlhbFN0YXRlO1xyXG4gIH0pO1xyXG4gIFxyXG4gIFxyXG4gIC8qaWYocGFyc2VJbnQoZGFkb3NQcm9kdXRvLmhhc19vcHRpb24pID09IDApeyAvLyBQUk9EVVRPIFNFTSBPUENPRVNcclxuICAgICAgXHJcbiAgICBjb25zdCB2YWxvckF0dWFsID0gZGFkb3NQcm9kdXRvLnByaWNlXHJcbiAgICBpZihkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgIHNldFNlbGVjdGVkTWFpblZhbHVlKHZhbG9yQXR1YWwpO1xyXG4gICAgICBzZXRTZWxlY3RlZFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8uc3BlY2lhbCk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgc2V0U2VsZWN0ZWRNYWluVmFsdWUobnVsbCk7XHJcbiAgICAgIHNldFNlbGVjdGVkU3BlY2lhbFZhbHVlKHZhbG9yQXR1YWwpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmKCFkYWRvc1Byb2R1dG8uc29sZF9vdXQpe1xyXG4gICAgICBzZXRTb2xkT3V0U3RvY2soMCk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgc2V0U29sZE91dFN0b2NrKDEpO1xyXG4gICAgfVxyXG4gIH1lbHNleyAvLyBQUk9EVVRPIENPTSBPUENPRVNcclxuICAgIHNldFNvbGRPdXRTdG9jaygwKTtcclxuXHJcbiAgICAgIGlmKHNlbGVjdGVkTWFpblZhbHVlID09IG51bGwpe1xyXG4gICAgICAgIHNldFNlbGVjdGVkTWFpblZhbHVlKGRhZG9zUHJvZHV0by5wcmljZSlcclxuICAgICAgfVxyXG4gICAgICBpZihzZWxlY3RlZFNwZWNpYWxWYWx1ZSA9PSBudWxsICYmIGRhZG9zUHJvZHV0by5zcGVjaWFsKXtcclxuICAgICAgICBzZXRTZWxlY3RlZFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8uc3BlY2lhbClcclxuICAgICAgfSovXHJcblxyXG4gICAgICAvKmZvcih2YXIgaSBpbiBkYWRvc1Byb2R1dG8ub3B0aW9ucyl7XHJcbiAgICAgICAgZ3J1cG9BdHVhbCA9IGRhZG9zUHJvZHV0by5vcHRpb25zW2ldXHJcbiAgICAgICAgZm9yKHZhciBvcHQgaW4gZ3J1cG9BdHVhbC5vcHRpb25fdmFsdWUpe1xyXG4gICAgICAgICAgaWYoZ3J1cG9BdHVhbC5vcHRpb25fdmFsdWVbb3B0XS5vcHRpb25fdmFsdWVfaWQ9PWRhZG9zUHJvZHV0by5vcGNhb19zZWxlY2lvbmFkYSl7XHJcbiAgICAgICAgICAgIGluaXRpYWxPcHRpb24gPSBncnVwb0F0dWFsLnByb2R1Y3Rfb3B0aW9uX2lkKydfJytncnVwb0F0dWFsLm9wdGlvbl92YWx1ZVtvcHRdLnByb2R1Y3Rfb3B0aW9uX3ZhbHVlX2lkXHJcbiAgICAgICAgICAgIHRoaXMuU2VsZWN0T3B0aW9uKGluaXRpYWxPcHRpb24sIG51bGwpXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gIH0qL1xyXG4gXHJcbiAgdmFyIGNoYW5nZVpvb20gPSAoZXYpID0+IHtcclxuICAgIGNvbnN0IGltZ1Nob3cgPSBldi5jdXJyZW50VGFyZ2V0LmRhdGFzZXQuc3Jjc2hvd1xyXG4gICAgY29uc3QgaW1nUG9wdXAgPSBldi5jdXJyZW50VGFyZ2V0LmRhdGFzZXQuc3JjcG9wdXBcclxuICAgIHZhciB0ZW1wSW1hZ2UgPSB7fTtcclxuICAgIHRlbXBJbWFnZVsnc2hvdyddID0gaW1nU2hvd1xyXG4gICAgdGVtcEltYWdlWydwb3B1cCddID0gaW1nUG9wdXBcclxuICAgIC8vc2V0SW5pdGlhbEltYWdlKHRlbXBJbWFnZSk7XHJcbiAgICBcclxuICB9XHJcbiAgXHJcblxyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG4gIGlmICghcm91dGVyLmlzRmFsbGJhY2sgJiYgIWRhZG9zUHJvZHV0bz8ucHJvZHVjdF9pZCkge1xyXG4gICAgcmV0dXJuIDxFcnJvclBhZ2Ugc3RhdHVzQ29kZT17NDA0fSAvPjtcclxuICB9XHJcbiAgcmV0dXJuIChcclxuICAgIDxMYXlvdXQ+XHJcbiAgICAgIDxDb250YWluZXI+XHJcbiAgICAgICAge3JvdXRlci5pc0ZhbGxiYWNrID8gKFxyXG4gICAgICAgICAgPGRpdj5Mb2FkaW5n4oCmPC9kaXY+XHJcbiAgICAgICAgKSA6IChcclxuICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgIDxIZWFkPlxyXG4gICAgICAgICAgICAgIDx0aXRsZT57ZGFkb3NQcm9kdXRvLm5hbWV9PC90aXRsZT5cclxuICAgICAgICAgICAgICA8bWV0YSBuYW1lPVwiZGVzY3JpcHRpb25cIiBjb250ZW50PVwibGVsZWxlXCIgLz5cclxuICAgICAgICAgICAgICA8bWV0YSBuYW1lPVwib2c6aW1hZ2VcIiBjb250ZW50PVwibGlsaWxpXCIgLz5cclxuICAgICAgICAgICAgPC9IZWFkPlxyXG4gICAgICAgICAgICA8VGVtcGxhdGVIZWFkZXIgY29uZmlncz17Y29uZmlncy5yZXNwb3N0YX0gbWFpbk1lbnU9e21haW5NZW51LnJlc3Bvc3RhfT48L1RlbXBsYXRlSGVhZGVyPiBcclxuICAgICAgICAgICAgPGRpdiBjbGFzPVwibWFpbi1jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwYWdlLXByb2R1Y3RzXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1wYWRkaW5nIGxpZ2h0LWJhY2tncm91bmQgbnByb2R1Y3QtYnJlYWRjcnVtYlwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxvbCBjbGFzc05hbWU9XCJicmVhZGNydW1iXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT1cImJyZWFkY3J1bWItaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIvXCIgdGl0bGU9XCJQw6FnaW5hIGluaWNpYWxcIj5Ib21lPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT1cImJyZWFkY3J1bWItaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHtkYWRvc1Byb2R1dG8ubmFtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvb2w+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY3AtcHJldmlldzNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZHVjdC1wYWdlXCI+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZHVjdC1nYWxsZXJ5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC10aHVtYm5haWxzIFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LWltYWdlcy1jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by5pbWFnZXMpID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLmxlbmd0aCA+IDQpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTbGlkZXIgY2xhc3NOYW1lPVwic2xpZGVyVGh1bWJzXCIgey4uLnNsaWRlclRodW1ic30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLm1hcCgoaXRlbSwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2UtdGh1bWIganMtY2Fyb3VzZWwtY29udHJvbC1pdGVtIHBvaW50ZXIganMtcHJvZHVjdC1pbWFnZS10aHVtYlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWx0PXtkYWRvc1Byb2R1dG8ubmFtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEtc3Jjc2hvdz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5zaG93fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1zcmNwb3B1cD17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPVwiMTBcIiBoZWlnaHQ9XCIxMFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPVwiXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLnZpZGVvcy5sZW5ndGgpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by52aWRlb3MubWFwKChpdGVtVmlkZW8sIGtleVZpZGVvKSA9PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIml0ZW0tdmlkZW9cIiBrZXk9e2tleVZpZGVvfT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGRhdGEtdmlkZW9pZD17aXRlbVZpZGVvLmlkX3ZpZGVvfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17YGh0dHBzOi8vaW1nLnlvdXR1YmUuY29tL3ZpLyR7aXRlbVZpZGVvLmlkX3ZpZGVvfS9tcWRlZmF1bHQuanBnYH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS15b3V0dWJlLXBsYXlcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1NsaWRlcj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLm1hcCgoaXRlbSwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwcm9kdWN0LWltYWdlLXRodW1iIGpzLWNhcm91c2VsLWNvbnRyb2wtaXRlbSBwb2ludGVyIGpzLXByb2R1Y3QtaW1hZ2UtdGh1bWJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD17ZGFkb3NQcm9kdXRvLm5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEtc3Jjc2hvdz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5zaG93fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLXNyY3BvcHVwPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjEwXCIgaGVpZ2h0PVwiMTBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPVwiXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLnZpZGVvcy5sZW5ndGgpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8udmlkZW9zLm1hcCgoaXRlbVZpZGVvLCBrZXlWaWRlbykgPT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaXRlbS12aWRlb1wiIGtleT17a2V5VmlkZW99PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGRhdGEtdmlkZW9pZD17aXRlbVZpZGVvLmlkX3ZpZGVvfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2BodHRwczovL2ltZy55b3V0dWJlLmNvbS92aS8ke2l0ZW1WaWRlby5pZF92aWRlb30vbXFkZWZhdWx0LmpwZ2B9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXlvdXR1YmUtcGxheVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZXMgbWluLXdpZHRoLTQxNXB4XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb190b3BfbGVmdC5pbWFnZSAhPSBudWxsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGFiZWxJdGVtIGxhYmVsVG9wTGVmdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb190b3BfbGVmdC5pbWFnZX0gLz4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb190b3BfcmlnaHQuaW1hZ2UgIT0gbnVsbCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxhYmVsSXRlbSBsYWJlbFRvcFJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX3RvcF9yaWdodC5pbWFnZX0gLz4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb19ib3R0b21fbGVmdC5pbWFnZSAhPSBudWxsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGFiZWxJdGVtIGxhYmVsQm90dG9tTGVmdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb19ib3R0b21fbGVmdC5pbWFnZX0gLz4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb19ib3R0b21fcmlnaHQuaW1hZ2UgIT0gbnVsbCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxhYmVsSXRlbSBsYWJlbEJvdHRvbVJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX2JvdHRvbV9yaWdodC5pbWFnZX0gLz4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSZWFjdEltYWdlTWFnbmlmeSB7Li4ue1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNtYWxsSW1hZ2U6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ6IGRhZG9zUHJvZHV0by5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzRmx1aWRXaWR0aDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcmM6IGluaXRpYWxJbWFnZXMuc2hvd1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhcmdlSW1hZ2U6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcmM6IGluaXRpYWxJbWFnZXMucG9wdXAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IGNvbmZpZ3Mud2lkdGhab29tLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogY29uZmlncy53aWR0aFpvb21cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX0gLz4gICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1heC13aWR0aC00MTRweFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykgJiYgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubGVuZ3RoID4gMSkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LWltYWdlc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U2xpZGVyIGNsYXNzTmFtZT1cInNsaWRlVml0cmluZVwiIHsuLi5zZXR0aW5nc01vYmlsZVNsaWRlfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhcmVhWm9vbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH0gYWx0PXtkYWRvc1Byb2R1dG8ubmFtZX0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8udmlkZW9zLmxlbmd0aCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by52aWRlb3MubWFwKChpdGVtVmlkZW8sIGtleVZpZGVvKSA9PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhcmVhWm9vbSBpdGVtLXZpZGVvXCIga2V5PXtrZXlWaWRlb30+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgZGF0YS12aWRlb2lkPXtpdGVtVmlkZW8uaWRfdmlkZW99PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17YGh0dHBzOi8vaW1nLnlvdXR1YmUuY29tL3ZpLyR7aXRlbVZpZGVvLmlkX3ZpZGVvfS9tcWRlZmF1bHQuanBnYH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEteW91dHViZS1wbGF5XCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU2xpZGVyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiAoT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykgJiYgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubGVuZ3RoID4gMCkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLm1hcCgoaXRlbSwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhcmVhWm9vbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfSBhbHQ9e2RhZG9zUHJvZHV0by5uYW1lfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by52aWRlb3MubGVuZ3RoKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLnZpZGVvcy5tYXAoKGl0ZW1WaWRlbywga2V5VmlkZW8pID0+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tIGl0ZW0tdmlkZW9cIiBrZXk9e2tleVZpZGVvfT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBkYXRhLXZpZGVvaWQ9e2l0ZW1WaWRlby5pZF92aWRlb30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvJHtpdGVtVmlkZW8uaWRfdmlkZW99L21xZGVmYXVsdC5qcGdgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS15b3V0dWJlLXBsYXlcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2R1Y3QtaW5mb1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1hY3Rpb25zIGNvbnRhaW5lci1wYWRkaW5nIGNvbnRhaW5lci1wYWRkaW5nLXRvcFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZHVjdC1oZWFkZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoMSBjbGFzc05hbWU9XCJucHJvZHVjdC10aXRsZVwiPntkYWRvc1Byb2R1dG8ubmFtZX08L2gxPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGRhZG9zUHJvZHV0by5tb2RlbCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwiaW5mb3NQcm9kdWN0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxzcGFuIGNsYXNzTmFtZT1cInRpdGxlSW5mb1wiPlJFRjo8L3NwYW4+IHtkYWRvc1Byb2R1dG8ubW9kZWx9PC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJhdGVCb3hcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cImxrLWF2YWxpYXJcIiBvbkNsaWNrPVwiXCI+QXZhbGlhciBhZ29yYTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhkYWRvc1Byb2R1dG8uc2hvcnQgIT0gJycpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpbmZvc0FyZWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJlc3VtZVByb2R1Y3RcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBkYW5nZXJvdXNseVNldElubmVySFRNTD17eyBfX2h0bWw6IGRhZG9zUHJvZHV0by5zaG9ydCB9fSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXlBcmVhXCI+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbFNlY3Rpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kY3QtcHJpY2VcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhzcGVjaWFsVmFsdWUpID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm9sZFByaWNlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBEZSA8c3BhbiBjbGFzc05hbWU9XCJucHJvZHVjdC1wcmljZS1tYXhcIj57Z2V0TWFpblZhbHVlKGRhZG9zUHJvZHV0byl9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LXByaWNlLXZhbHVlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBvciA8c3BhbiBjbGFzc05hbWU9XCJzcGVjaWFsVmFsdWVcIj57Z2V0U3BlY2lhbFZhbHVlKGRhZG9zUHJvZHV0byl9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIml0ZW0tZGlzY291bnRcIj57ZGFkb3NQcm9kdXRvLmRpc2NvdW50X3BlcmNlbnR9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoc2VsZWN0ZWRRdGRQYXJjZWwgIT0gJycgJiYgc2VsZWN0ZWRWYWxQYXJjZWwgIT0gJycpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInNlbGVjdGVkUGFyY2VsXCI+T3UgPHNwYW4gY2xhc3NOYW1lPVwibnVtUGFyY1wiPntzZWxlY3RlZFF0ZFBhcmNlbH14PC9zcGFuPiBkZSA8c3BhbiBjbGFzc05hbWU9XCJ2YWxQYXJjXCI+e3NlbGVjdGVkVmFsUGFyY2VsfTwvc3Bhbj48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8gIT0gJycpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAocGFyc2VJbnQoZGFkb3NQcm9kdXRvLmhhc19vcHRpb24pICE9IDApID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwib3B0aW9uc0FyZWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by5vcHRpb25zLm1hcCgoaXRlbSwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJveC1vcHRpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbD57aXRlbS5uYW1lfTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoaXRlbS50eXBlID09ICd0ZXh0Jyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidHh0T3B0aW9uXCI+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgaWQ9XCJmaWVsZE9wdGlvblwiIG1heGxlbmd0aD1cIjNcIiB0eXBlPVwidGV4dFwiIGRhdGEtZ3JvdXA9e2l0ZW0ucHJvZHVjdF9vcHRpb25faWR9IGNsYXNzTmFtZT1cImZpZWxkXCIgbmFtZT1cInR4dC1vcHRpb25cIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVscFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1xdWVzdGlvbi1jaXJjbGUgY29sb3IyXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpbmZvSGVscFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgSW5zaXJhIGF0w6kgMyBsZXRyYXMgcGFyYSBwZXJzb25hbGl6YXIgYSBjYW1pc2EgY29tIHVtIGJvcmRhZG8gZXhjbHVzaXZvLiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJsaXN0T3B0aW9uc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0ub3B0aW9uX3ZhbHVlLm1hcCgoaXRlbU9wdGlvbiwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9e3NlbGVjdGVkT3B0aW9uQnV5ICE9PSBpdGVtLnByb2R1Y3Rfb3B0aW9uX2lkKydfJytpdGVtT3B0aW9uLnByb2R1Y3Rfb3B0aW9uX3ZhbHVlX2lkID8gJ29wdGlvbicgOiAnb3B0aW9uIHNlbGVjdGVkJ30gb25DbGljaz1cIlwiIGhyZWY9XCJqYXZhc2NyaXB0OjtcIj57aXRlbU9wdGlvbi5uYW1lfTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogICcnICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sU2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJxdWFudGl0eUFyZWFcIj4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbD5RdWFudGlkYWRlPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV0dG9uc1F1YW50aXR5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGNsYXNzTmFtZT1cImJ0bkxlc3NcIj48aSBjbGFzc05hbWU9XCJmYSBmYS1taW51c1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT48L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgaWQ9XCJ0eHRRdWFudGl0eVwiIHR5cGU9XCJ0ZXh0XCIgbmFtZT1cInR4dC1xdWFudGl0eVwiIHZhbHVlPXsgcXVhbnRpdHlCdXkgfSBjbGFzc05hbWU9XCJ0eHRRdWFudGl0eVwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGNsYXNzTmFtZT1cImJ0bk1vcmVcIj48aSBjbGFzc05hbWU9XCJmYSBmYS1wbHVzXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhzb2xkT3V0U3RvY2sgIT0gbnVsbCAmJiAhc29sZE91dFN0b2NrKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXlCdXR0b25BcmVhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBjbGFzc05hbWU9XCJidXlCdXR0b24gYnRuX2J1eVwiIG9uQ2xpY2s9XCJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhIGZhLXNob3BwaW5nLWNhcnRcIj48L2k+IHsnQ29tcHJhcid9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IChzb2xkT3V0U3RvY2sgIT0gbnVsbCAmJiBzb2xkT3V0U3RvY2spP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJ1eUJ1dHRvbkFyZWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIGNsYXNzTmFtZT1cImJ1eUJ1dHRvbiBub3RpZnlCdXR0b25cIiBkYXRhLXByb2R1Y3RpZD17ZGFkb3NQcm9kdXRvLnByb2R1Y3RfaWR9IG9uQ2xpY2s9XCJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtZW52ZWxvcGVcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+IEF2aXNlLW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGRhZG9zUHJvZHV0by50ZXh0X3ByZXZlbmRhIT0gbnVsbCAmJiBkYWRvc1Byb2R1dG8udGV4dF9wcmV2ZW5kYSAhPSAnJyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImNoZWNrUHJldmVuZGFcIj48aW5wdXQgaWQ9XCJja3ByZXZlbmRhXCIgdHlwZT1cImNoZWNrYm94XCIgb25DaGFuZ2U9XCJcIiAvPiBDb25jb3JkbyBjb20gbyBwcmF6byBkZSBlbnRyZWdhIGRlc2NyaXRvIGFiYWl4by48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImluZm9QcmV2ZW5kYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwidGl0XCI+VEVSTU8gREUgQUNFSVRBw4fDg088L2g0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ZGFkb3NQcm9kdXRvLnRleHRfcHJldmVuZGF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhkYWRvc1Byb2R1dG8uZ3VpYV9tZWRpZGFzKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJndWlhc01lZGlkYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8uZ3VpYV9tZWRpZGFzLm1hcCgoaXRlbUd1aWEsIGtleUd1aWEpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwiaXRlbUd1aWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cImNvbG9yMlwiIHRpdGxlPXtpdGVtR3VpYS50aXRsZX0gZGF0YS10aXR1bG9ndWlhPVwiXCIgZGF0YS1jb250ZXVkb2d1aWE9XCJcIiBvbkNsaWNrPVwiXCI+e2l0ZW1HdWlhLnRpdGxlfTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxUZW1wbGF0ZUZvb3RlciBjb25maWdzPXtjb25maWdzLnJlc3Bvc3RhfSAvPlxyXG4gICAgICAgICAgPC8+XHJcbiAgICAgICAgKX1cclxuICAgICAgPC9Db250YWluZXI+XHJcbiAgICA8L0xheW91dD5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFN0YXRpY1Byb3BzKHsgcGFyYW1zIH0pIHtcclxuICB2YXIgY29uZmlncyA9IGF3YWl0IGdldENvbmZpZ3MoKTtcclxuICB2YXIgbWFpbk1lbnUgPSBhd2FpdCBnZXRNYWluTWVudSgpO1xyXG4gIHZhciByZXNQcm9kdWN0ID0gYXdhaXQgZ2V0UHJvZHVjdEJ5U2x1ZyhwYXJhbXMuc2x1Zyk7XHJcblxyXG4gIHZhciBkYWRvc1Byb2R1dG8gPSBudWxsO1xyXG4gIGlmKHJlc1Byb2R1Y3Quc3VjY2Vzcyl7XHJcbiAgICBkYWRvc1Byb2R1dG8gPSByZXNQcm9kdWN0LnJlc3Bvc3RhLnByb2R1dG87XHJcbiAgfVxyXG4gIHJldHVybiB7XHJcbiAgICBwcm9wczoge1xyXG4gICAgICBjb25maWdzLFxyXG4gICAgICBkYWRvc1Byb2R1dG8sXHJcbiAgICAgIG1haW5NZW51XHJcbiAgICB9LFxyXG4gICAgcmV2YWxpZGF0ZTogNjBcclxuICB9O1xyXG59XHJcblxyXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0U3RhdGljUGF0aHMoKSB7XHJcbiAgY29uc3QgcHJvZHV0b3MgPSBhd2FpdCBnZXRQcm9kdWN0c0J5Q2F0ZWdvcnkoKTtcclxuXHJcbiAgcmV0dXJuIHtcclxuICAgIHBhdGhzOlxyXG4gICAgICBbXSxcclxuICAgIGZhbGxiYWNrOiB0cnVlLFxyXG4gIH07XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==