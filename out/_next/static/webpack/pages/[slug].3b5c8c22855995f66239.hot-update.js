webpackHotUpdate_N_E("pages/[slug]",{

/***/ "./pages/[slug].js":
/*!*************************!*\
  !*** ./pages/[slug].js ***!
  \*************************/
/*! exports provided: __N_SSG, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__N_SSG", function() { return __N_SSG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Product; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/error */ "./node_modules/next/error.js");
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_error__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_image_magnify__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-image-magnify */ "./node_modules/react-image-magnify/dist/es/ReactImageMagnify.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_container__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/components/container */ "./components/container.js");
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @/components/layout */ "./components/layout.js");





var _jsxFileName = "C:\\Users\\sauln49\\Desktop\\nextjs\\pages\\[slug].js",
    _s3 = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }










var idStoreApp = 'n49shopv2_trijoia';
var TemplateHeader = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c = function _c() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/header$")("./" + idStoreApp + "/components/header");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/header$").resolve("./" + idStoreApp + "/components/header"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/header']
  }
});
_c2 = TemplateHeader;
var TemplateFooter = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c3 = function _c3() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/footer$")("./" + idStoreApp + "/components/footer");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/footer$").resolve("./" + idStoreApp + "/components/footer"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/footer']
  }
});
_c4 = TemplateFooter;
var sliderThumbs = {
  infinite: false,
  vertical: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  speed: 500,
  responsive: [{
    breakpoint: 415,
    settings: {
      vertical: false,
      slidesToShow: 3
    }
  }]
};
var settingsMobileSlide = {
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  dots: true
};
var __N_SSG = true;
function Product(_ref) {
  _s3();

  var _s = $RefreshSig$(),
      _s2 = $RefreshSig$(),
      _this = this;

  var configs = _ref.configs,
      mainMenu = _ref.mainMenu,
      dadosProduto = _ref.dadosProduto;
  var initialImages = [];

  function getInitialImage(dadosProduto) {
    var imgInicial = [];
    var tempImgInicial = [];

    for (var i in dadosProduto.images) {
      tempImgInicial.push([i, dadosProduto.images[i]]);
    }

    var initialKey = dadosProduto.image.substring(dadosProduto.image.lastIndexOf('/') + 1);

    if (dadosProduto.images != undefined && dadosProduto.images[initialKey] != null) {
      imgInicial['show'] = dadosProduto.images[initialKey].show;
      imgInicial['popup'] = dadosProduto.images[initialKey].popup;
      imgInicial['thumb'] = dadosProduto.images[initialKey].thumb;
    } else if (tempImgInicial[0] != null) {
      imgInicial = tempImgInicial[0][1];
    }

    return imgInicial;
  }

  if (dadosProduto) {
    initialImages = getInitialImage(dadosProduto);
  }

  var selectedQtdParcel = 1;
  var selectedValParcel = 1;
  var quantityBuy = 1;
  var shippingMethods = null;
  var selectedOptionBuy = 1;

  function getSpecialValue(dadosProduto) {
    _s();

    var initialSpecialValue = null;

    if (dadosProduto != undefined) {
      if (parseInt(dadosProduto.has_option) == 0) {
        // PRODUTO SEM OPCOES
        if (dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        } else {
          initialSpecialValue = dadosProduto.price;
        }
      } else {
        if (initialSpecialValue == null && dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        }
      }
    }

    var specialValue = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(initialSpecialValue);
    return specialValue;
  }

  _s(getSpecialValue, "uOCI/u7Rhc0OEPJW7t3Rh1DIVCE=");

  var _getSpecialValue = getSpecialValue(dadosProduto),
      _getSpecialValue2 = Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_getSpecialValue, 2),
      specialValue = _getSpecialValue2[0],
      setSpecialValue = _getSpecialValue2[1];

  function getMainValue(dadosProduto) {
    _s2();

    var initialMainValue = null;

    if (dadosProduto != undefined) {
      if (parseInt(dadosProduto.has_option) == 0) {
        // PRODUTO SEM OPCOES
        if (dadosProduto.special) {
          initialMainValue = dadosProduto.price;
        } else {
          initialMainValue = null;
        }
      } else {
        if (initialMainValue == null && dadosProduto.price) {
          initialMainValue = dadosProduto.price;
        }
      }
    }

    var mainValueState = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(initialMainValue);
    return mainValueState;
  }

  _s2(getMainValue, "loXbXvMqcS2N0dBaSjwn2V8iaXI=");

  var _getMainValue = getMainValue(dadosProduto),
      _getMainValue2 = Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_getMainValue, 2),
      mainValue = _getMainValue2[0],
      setMainValue = _getMainValue2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(function () {
    var initialState = 0;
    return initialState;
  }),
      soldOutStock = _useState[0],
      setSoldOutStock = _useState[1];
  /*if(parseInt(dadosProduto.has_option) == 0){ // PRODUTO SEM OPCOES
      
    const valorAtual = dadosProduto.price
    if(dadosProduto.special){
      setSelectedMainValue(valorAtual);
      setSelectedSpecialValue(dadosProduto.special);
    }else{
      setSelectedMainValue(null);
      setSelectedSpecialValue(valorAtual);
    }
      if(!dadosProduto.sold_out){
      setSoldOutStock(0);
    }else{
      setSoldOutStock(1);
    }
  }else{ // PRODUTO COM OPCOES
    setSoldOutStock(0);
        if(selectedMainValue == null){
        setSelectedMainValue(dadosProduto.price)
      }
      if(selectedSpecialValue == null && dadosProduto.special){
        setSelectedSpecialValue(dadosProduto.special)
      }*/

  /*for(var i in dadosProduto.options){
    grupoAtual = dadosProduto.options[i]
    for(var opt in grupoAtual.option_value){
      if(grupoAtual.option_value[opt].option_value_id==dadosProduto.opcao_selecionada){
        initialOption = grupoAtual.product_option_id+'_'+grupoAtual.option_value[opt].product_option_value_id
        this.SelectOption(initialOption, null)
      }
    }
  }
        
  }*/


  var changeZoom = function changeZoom(ev) {
    var imgShow = ev.currentTarget.dataset.srcshow;
    var imgPopup = ev.currentTarget.dataset.srcpopup;
    var tempImage = {};
    tempImage['show'] = imgShow;
    tempImage['popup'] = imgPopup; //setInitialImage(tempImage);
  };

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])();

  if (!router.isFallback && !(dadosProduto !== null && dadosProduto !== void 0 && dadosProduto.product_id)) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_error__WEBPACK_IMPORTED_MODULE_4___default.a, {
      statusCode: 404
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 173,
      columnNumber: 12
    }, this);
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_layout__WEBPACK_IMPORTED_MODULE_11__["default"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_container__WEBPACK_IMPORTED_MODULE_10__["default"], {
      children: router.isFallback ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        children: "Loading\u2026"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 179,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_5___default.a, {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
            children: dadosProduto.name
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 183,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "description",
            content: "lelele"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 184,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "og:image",
            content: "lilili"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 185,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 182,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateHeader, {
          configs: configs.resposta,
          mainMenu: mainMenu.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 187,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          clas: "main-content",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "page-products",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "container-padding light-background nproduct-breadcrumb",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "container",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ol", {
                  className: "breadcrumb",
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                      href: "/",
                      title: "P\xE1gina inicial",
                      children: "Home"
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 194,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 193,
                    columnNumber: 24
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: dadosProduto.name
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 196,
                    columnNumber: 24
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 192,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                  className: "cp-preview3",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                    className: "container",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                      className: "nproduct-page",
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-gallery",
                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-thumbnails ",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images-container",
                            children: dadosProduto.images ? Object.keys(dadosProduto.images).length > 4 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "sliderThumbs"
                            }, sliderThumbs), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 218,
                                    columnNumber: 57
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 215,
                                  columnNumber: 53
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 232,
                                      columnNumber: 51
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 233,
                                      columnNumber: 51
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 231,
                                    columnNumber: 49
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 230,
                                  columnNumber: 47
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 211,
                              columnNumber: 39
                            }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 249,
                                    columnNumber: 53
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 246,
                                  columnNumber: 49
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 264,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 265,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 263,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 262,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }, void 0, true) : null
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 207,
                            columnNumber: 32
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 206,
                          columnNumber: 31
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-images min-width-415px",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "areaZoom",
                            children: [dadosProduto.labels.promo_top_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 283,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 282,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_top_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 291,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 290,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 299,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 298,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 307,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 306,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_image_magnify__WEBPACK_IMPORTED_MODULE_8__["default"], _objectSpread({}, {
                              smallImage: {
                                alt: dadosProduto.name,
                                isFluidWidth: true,
                                src: initialImages.show
                              },
                              largeImage: {
                                src: initialImages.popup,
                                width: configs.widthZoom,
                                height: configs.widthZoom
                              }
                            }), void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 313,
                              columnNumber: 37
                            }, this)]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 279,
                            columnNumber: 35
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 278,
                          columnNumber: 33
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "max-width-414px",
                          children: Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 1 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "slideVitrine"
                            }, settingsMobileSlide), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 336,
                                    columnNumber: 49
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 335,
                                  columnNumber: 47
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 345,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 346,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 344,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 343,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 332,
                              columnNumber: 37
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 331,
                            columnNumber: 35
                          }, this) : Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                            children: [Object.keys(dadosProduto.images).map(function (item, key) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                  src: dadosProduto.images[item].popup,
                                  alt: dadosProduto.name
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 360,
                                  columnNumber: 45
                                }, _this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 359,
                                columnNumber: 43
                              }, _this);
                            }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom item-video",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  onClick: "",
                                  "data-videoid": itemVideo.id_video,
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 369,
                                    columnNumber: 49
                                  }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-youtube-play",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 370,
                                    columnNumber: 49
                                  }, _this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 368,
                                  columnNumber: 47
                                }, _this)
                              }, keyVideo, false, {
                                fileName: _jsxFileName,
                                lineNumber: 367,
                                columnNumber: 45
                              }, _this);
                            }) : null]
                          }, void 0, true) : null
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 329,
                          columnNumber: 31
                        }, this)]
                      }, void 0, true, {
                        fileName: _jsxFileName,
                        lineNumber: 205,
                        columnNumber: 29
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-info",
                        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-actions container-padding container-padding-top",
                          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "nproduct-header",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
                              className: "nproduct-title",
                              children: dadosProduto.name
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 386,
                              columnNumber: 38
                            }, this), dadosProduto.model ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              className: "infosProduct",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "titleInfo",
                                  children: "REF:"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 390,
                                  columnNumber: 49
                                }, this), " ", dadosProduto.model]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 390,
                                columnNumber: 45
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 389,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "rateBox",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                className: "lk-avaliar",
                                onClick: "",
                                children: "Avaliar agora"
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 397,
                                columnNumber: 41
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 396,
                              columnNumber: 38
                            }, this), dadosProduto["short"] != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "infosArea",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "resumeProduct",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  dangerouslySetInnerHTML: {
                                    __html: dadosProduto["short"]
                                  }
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 404,
                                  columnNumber: 43
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 403,
                                columnNumber: 42
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 402,
                              columnNumber: 41
                            }, this) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 385,
                            columnNumber: 35
                          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "buyArea",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "nprodct-price",
                                children: [specialValue ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "oldPrice",
                                  children: ["De ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "nproduct-price-max",
                                    children: getMainValue(dadosProduto)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 419,
                                    columnNumber: 50
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 418,
                                  columnNumber: 45
                                }, this) : '', /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "nproduct-price-value",
                                  children: specialValue ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                                    children: ["Por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "specialValue",
                                      children: getSpecialValue(dadosProduto)
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 427,
                                      columnNumber: 53
                                    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "item-discount",
                                      children: dadosProduto.discount_percent
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 428,
                                      columnNumber: 48
                                    }, this)]
                                  }, void 0, true) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                    children: ["por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      children: getMainValue(dadosProduto)
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 431,
                                      columnNumber: 54
                                    }, this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 431,
                                    columnNumber: 47
                                  }, this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 424,
                                  columnNumber: 43
                                }, this), selectedQtdParcel != '' && selectedValParcel != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                  className: "selectedParcel",
                                  children: ["Ou ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "numParc",
                                    children: [selectedQtdParcel, "x"]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 436,
                                    columnNumber: 78
                                  }, this), " de ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "valParc",
                                    children: selectedValParcel
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 436,
                                    columnNumber: 135
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 436,
                                  columnNumber: 45
                                }, this) : null]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 416,
                                columnNumber: 39
                              }, this), dadosProduto != '' ? parseInt(dadosProduto.has_option) != 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "optionsArea",
                                children: dadosProduto.options.map(function (item, key) {
                                  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                    className: "box-option",
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                      children: item.name
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 449,
                                      columnNumber: 53
                                    }, _this), item.type == 'text' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                      className: "txtOption",
                                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                        id: "fieldOption",
                                        maxlength: "3",
                                        type: "text",
                                        "data-group": item.product_option_id,
                                        className: "field",
                                        name: "txt-option"
                                      }, void 0, false, {
                                        fileName: _jsxFileName,
                                        lineNumber: 453,
                                        columnNumber: 57
                                      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                        className: "help",
                                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                          className: "fa fa-question-circle color2",
                                          "aria-hidden": "true"
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 455,
                                          columnNumber: 61
                                        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                          className: "infoHelp",
                                          children: "Insira at\xE9 3 letras para personalizar a camisa com um bordado exclusivo."
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 457,
                                          columnNumber: 61
                                        }, _this)]
                                      }, void 0, true, {
                                        fileName: _jsxFileName,
                                        lineNumber: 454,
                                        columnNumber: 57
                                      }, _this)]
                                    }, void 0, true, {
                                      fileName: _jsxFileName,
                                      lineNumber: 452,
                                      columnNumber: 55
                                    }, _this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                                      className: "listOptions",
                                      children: item.option_value.map(function (itemOption, key) {
                                        return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                            className: selectedOptionBuy !== item.product_option_id + '_' + itemOption.product_option_value_id ? 'option' : 'option selected',
                                            onClick: "",
                                            href: "javascript:;",
                                            children: itemOption.name
                                          }, void 0, false, {
                                            fileName: _jsxFileName,
                                            lineNumber: 467,
                                            columnNumber: 63
                                          }, _this)
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 466,
                                          columnNumber: 61
                                        }, _this);
                                      })
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 463,
                                      columnNumber: 55
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 448,
                                    columnNumber: 51
                                  }, _this);
                                })
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 445,
                                columnNumber: 44
                              }, this) : '' : '']
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 415,
                              columnNumber: 37
                            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "quantityArea",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                  children: "Quantidade"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 485,
                                  columnNumber: 44
                                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "buttonsQuantity",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnLess",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-minus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 487,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 487,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                    id: "txtQuantity",
                                    type: "text",
                                    name: "txt-quantity",
                                    value: quantityBuy,
                                    className: "txtQuantity"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 488,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnMore",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-plus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 489,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 489,
                                    columnNumber: 47
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 486,
                                  columnNumber: 44
                                }, this)]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 484,
                                columnNumber: 41
                              }, this), soldOutStock != null && !soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton btn_buy",
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fas fa fa-shopping-cart"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 496,
                                    columnNumber: 52
                                  }, this), " ", 'Comprar']
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 495,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 494,
                                columnNumber: 43
                              }, this) : soldOutStock != null && soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton notifyButton",
                                  "data-productid": dadosProduto.product_id,
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-envelope",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 502,
                                    columnNumber: 52
                                  }, this), " Avise-me"]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 501,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 500,
                                columnNumber: 43
                              }, this) : null]
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 483,
                              columnNumber: 39
                            }, this), dadosProduto.text_prevenda != null && dadosProduto.text_prevenda != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                className: "checkPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                  id: "ckprevenda",
                                  type: "checkbox",
                                  onChange: ""
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 512,
                                  columnNumber: 70
                                }, this), " Concordo com o prazo de entrega descrito abaixo."]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 512,
                                columnNumber: 41
                              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "infoPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
                                  className: "tit",
                                  children: "TERMO DE ACEITA\xC7\xC3O"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 514,
                                  columnNumber: 43
                                }, this), dadosProduto.text_prevenda]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 513,
                                columnNumber: 41
                              }, this)]
                            }, void 0, true) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 413,
                            columnNumber: 35
                          }, this), dadosProduto.guia_medidas ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "guiasMedida",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              children: dadosProduto.guia_medidas.map(function (itemGuia, keyGuia) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                  className: "itemGuia",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    className: "color2",
                                    title: itemGuia.title,
                                    "data-tituloguia": "",
                                    "data-conteudoguia": "",
                                    onClick: "",
                                    children: itemGuia.title
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 530,
                                    columnNumber: 47
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 529,
                                  columnNumber: 43
                                }, _this);
                              })
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 526,
                              columnNumber: 39
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 525,
                            columnNumber: 37
                          }, this) : null]
                        }, void 0, true, {
                          fileName: _jsxFileName,
                          lineNumber: 384,
                          columnNumber: 33
                        }, this)
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 383,
                        columnNumber: 29
                      }, this)]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 203,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 202,
                    columnNumber: 23
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 201,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 191,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 190,
              columnNumber: 17
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 189,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 188,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateFooter, {
          configs: configs.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 555,
          columnNumber: 13
        }, this)]
      }, void 0, true)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 177,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 176,
    columnNumber: 5
  }, this);
}

_s3(Product, "9ivUjH6RR1GoW/oyO3lSZCyjhfw=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"]];
});

_c5 = Product;
;

var _c, _c2, _c3, _c4, _c5;

$RefreshReg$(_c, "TemplateHeader$dynamic");
$RefreshReg$(_c2, "TemplateHeader");
$RefreshReg$(_c3, "TemplateFooter$dynamic");
$RefreshReg$(_c4, "TemplateFooter");
$RefreshReg$(_c5, "Product");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvW3NsdWddLmpzIl0sIm5hbWVzIjpbImlkU3RvcmVBcHAiLCJUZW1wbGF0ZUhlYWRlciIsImR5bmFtaWMiLCJUZW1wbGF0ZUZvb3RlciIsInNsaWRlclRodW1icyIsImluZmluaXRlIiwidmVydGljYWwiLCJzbGlkZXNUb1Nob3ciLCJzbGlkZXNUb1Njcm9sbCIsInNwZWVkIiwicmVzcG9uc2l2ZSIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsInNldHRpbmdzTW9iaWxlU2xpZGUiLCJkb3RzIiwiUHJvZHVjdCIsImNvbmZpZ3MiLCJtYWluTWVudSIsImRhZG9zUHJvZHV0byIsImluaXRpYWxJbWFnZXMiLCJnZXRJbml0aWFsSW1hZ2UiLCJpbWdJbmljaWFsIiwidGVtcEltZ0luaWNpYWwiLCJpIiwiaW1hZ2VzIiwicHVzaCIsImluaXRpYWxLZXkiLCJpbWFnZSIsInN1YnN0cmluZyIsImxhc3RJbmRleE9mIiwidW5kZWZpbmVkIiwic2hvdyIsInBvcHVwIiwidGh1bWIiLCJzZWxlY3RlZFF0ZFBhcmNlbCIsInNlbGVjdGVkVmFsUGFyY2VsIiwicXVhbnRpdHlCdXkiLCJzaGlwcGluZ01ldGhvZHMiLCJzZWxlY3RlZE9wdGlvbkJ1eSIsImdldFNwZWNpYWxWYWx1ZSIsImluaXRpYWxTcGVjaWFsVmFsdWUiLCJwYXJzZUludCIsImhhc19vcHRpb24iLCJzcGVjaWFsIiwicHJpY2UiLCJzcGVjaWFsVmFsdWUiLCJ1c2VTdGF0ZSIsInNldFNwZWNpYWxWYWx1ZSIsImdldE1haW5WYWx1ZSIsImluaXRpYWxNYWluVmFsdWUiLCJtYWluVmFsdWVTdGF0ZSIsIm1haW5WYWx1ZSIsInNldE1haW5WYWx1ZSIsImluaXRpYWxTdGF0ZSIsInNvbGRPdXRTdG9jayIsInNldFNvbGRPdXRTdG9jayIsImNoYW5nZVpvb20iLCJldiIsImltZ1Nob3ciLCJjdXJyZW50VGFyZ2V0IiwiZGF0YXNldCIsInNyY3Nob3ciLCJpbWdQb3B1cCIsInNyY3BvcHVwIiwidGVtcEltYWdlIiwicm91dGVyIiwidXNlUm91dGVyIiwiaXNGYWxsYmFjayIsInByb2R1Y3RfaWQiLCJuYW1lIiwicmVzcG9zdGEiLCJPYmplY3QiLCJrZXlzIiwibGVuZ3RoIiwibWFwIiwiaXRlbSIsImtleSIsInZpZGVvcyIsIml0ZW1WaWRlbyIsImtleVZpZGVvIiwiaWRfdmlkZW8iLCJsYWJlbHMiLCJwcm9tb190b3BfbGVmdCIsInByb21vX3RvcF9yaWdodCIsInByb21vX2JvdHRvbV9sZWZ0IiwicHJvbW9fYm90dG9tX3JpZ2h0Iiwic21hbGxJbWFnZSIsImFsdCIsImlzRmx1aWRXaWR0aCIsInNyYyIsImxhcmdlSW1hZ2UiLCJ3aWR0aCIsIndpZHRoWm9vbSIsImhlaWdodCIsIm1vZGVsIiwiX19odG1sIiwiZGlzY291bnRfcGVyY2VudCIsIm9wdGlvbnMiLCJ0eXBlIiwicHJvZHVjdF9vcHRpb25faWQiLCJvcHRpb25fdmFsdWUiLCJpdGVtT3B0aW9uIiwicHJvZHVjdF9vcHRpb25fdmFsdWVfaWQiLCJ0ZXh0X3ByZXZlbmRhIiwiZ3VpYV9tZWRpZGFzIiwiaXRlbUd1aWEiLCJrZXlHdWlhIiwidGl0bGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBR0EsSUFBSUEsVUFBVSxHQUFHLG1CQUFqQjtBQUNBLElBQUlDLGNBQWMsR0FBR0MsbURBQU8sTUFBQztBQUFBLFNBQU0sOEZBQU8sSUFBeUIsR0FBQ0YsVUFBMUIsR0FBcUMsb0JBQTVDLENBQU47QUFBQSxDQUFEO0FBQUE7QUFBQTtBQUFBLGtDQUFjLDBHQUF5QixHQUFDQSxVQUExQixHQUFxQyxvQkFBbkQ7QUFBQTtBQUFBLGNBQWMsNEJBQTBCQSxVQUExQixHQUFxQyxvQkFBbkQ7QUFBQTtBQUFBLEVBQTVCO01BQUlDLGM7QUFDSixJQUFJRSxjQUFjLEdBQUdELG1EQUFPLE9BQUM7QUFBQSxTQUFNLDhGQUFPLElBQXlCLEdBQUNGLFVBQTFCLEdBQXFDLG9CQUE1QyxDQUFOO0FBQUEsQ0FBRDtBQUFBO0FBQUE7QUFBQSxrQ0FBYywwR0FBeUIsR0FBQ0EsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxjQUFjLDRCQUEwQkEsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxFQUE1QjtNQUFJRyxjO0FBRUosSUFBSUMsWUFBWSxHQUFHO0FBQ2pCQyxVQUFRLEVBQUUsS0FETztBQUVqQkMsVUFBUSxFQUFFLElBRk87QUFHakJDLGNBQVksRUFBRSxDQUhHO0FBSWpCQyxnQkFBYyxFQUFFLENBSkM7QUFLakJDLE9BQUssRUFBRSxHQUxVO0FBTWpCQyxZQUFVLEVBQUUsQ0FDVjtBQUNFQyxjQUFVLEVBQUUsR0FEZDtBQUVFQyxZQUFRLEVBQUU7QUFDUk4sY0FBUSxFQUFFLEtBREY7QUFFUkMsa0JBQVksRUFBRTtBQUZOO0FBRlosR0FEVTtBQU5LLENBQW5CO0FBaUJBLElBQUlNLG1CQUFtQixHQUFHO0FBQ3hCTixjQUFZLEVBQUUsQ0FEVTtBQUV4QkMsZ0JBQWMsRUFBRSxDQUZRO0FBR3hCQyxPQUFLLEVBQUUsR0FIaUI7QUFJeEJLLE1BQUksRUFBRTtBQUprQixDQUExQjs7QUFPZSxTQUFTQyxPQUFULE9BQXNEO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBLE1BQW5DQyxPQUFtQyxRQUFuQ0EsT0FBbUM7QUFBQSxNQUExQkMsUUFBMEIsUUFBMUJBLFFBQTBCO0FBQUEsTUFBaEJDLFlBQWdCLFFBQWhCQSxZQUFnQjtBQUNuRSxNQUFJQyxhQUFhLEdBQUcsRUFBcEI7O0FBQ0EsV0FBU0MsZUFBVCxDQUF5QkYsWUFBekIsRUFBdUM7QUFFckMsUUFBSUcsVUFBVSxHQUFHLEVBQWpCO0FBQ0EsUUFBSUMsY0FBYyxHQUFHLEVBQXJCOztBQUVBLFNBQUksSUFBSUMsQ0FBUixJQUFhTCxZQUFZLENBQUNNLE1BQTFCLEVBQWlDO0FBQy9CRixvQkFBYyxDQUFDRyxJQUFmLENBQW9CLENBQUNGLENBQUQsRUFBSUwsWUFBWSxDQUFDTSxNQUFiLENBQXFCRCxDQUFyQixDQUFKLENBQXBCO0FBQ0Q7O0FBRUQsUUFBSUcsVUFBVSxHQUFHUixZQUFZLENBQUNTLEtBQWIsQ0FBbUJDLFNBQW5CLENBQTZCVixZQUFZLENBQUNTLEtBQWIsQ0FBbUJFLFdBQW5CLENBQStCLEdBQS9CLElBQW9DLENBQWpFLENBQWpCOztBQUVBLFFBQUdYLFlBQVksQ0FBQ00sTUFBYixJQUF1Qk0sU0FBdkIsSUFBb0NaLFlBQVksQ0FBQ00sTUFBYixDQUFvQkUsVUFBcEIsS0FBbUMsSUFBMUUsRUFBK0U7QUFDN0VMLGdCQUFVLENBQUMsTUFBRCxDQUFWLEdBQXFCSCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JFLFVBQXBCLEVBQWdDSyxJQUFyRDtBQUNBVixnQkFBVSxDQUFDLE9BQUQsQ0FBVixHQUFzQkgsWUFBWSxDQUFDTSxNQUFiLENBQW9CRSxVQUFwQixFQUFnQ00sS0FBdEQ7QUFDQVgsZ0JBQVUsQ0FBQyxPQUFELENBQVYsR0FBc0JILFlBQVksQ0FBQ00sTUFBYixDQUFvQkUsVUFBcEIsRUFBZ0NPLEtBQXREO0FBQ0QsS0FKRCxNQUlNLElBQUdYLGNBQWMsQ0FBQyxDQUFELENBQWQsSUFBcUIsSUFBeEIsRUFBNkI7QUFDakNELGdCQUFVLEdBQUdDLGNBQWMsQ0FBQyxDQUFELENBQWQsQ0FBa0IsQ0FBbEIsQ0FBYjtBQUNEOztBQUVELFdBQU9ELFVBQVA7QUFDRDs7QUFDRCxNQUFHSCxZQUFILEVBQWdCO0FBQ2RDLGlCQUFhLEdBQUdDLGVBQWUsQ0FBQ0YsWUFBRCxDQUEvQjtBQUNEOztBQUVELE1BQUlnQixpQkFBaUIsR0FBRyxDQUF4QjtBQUNBLE1BQUlDLGlCQUFpQixHQUFHLENBQXhCO0FBQ0EsTUFBSUMsV0FBVyxHQUFHLENBQWxCO0FBQ0EsTUFBSUMsZUFBZSxHQUFHLElBQXRCO0FBQ0EsTUFBSUMsaUJBQWlCLEdBQUcsQ0FBeEI7O0FBRUEsV0FBU0MsZUFBVCxDQUF5QnJCLFlBQXpCLEVBQXVDO0FBQUE7O0FBQ3JDLFFBQUlzQixtQkFBbUIsR0FBRyxJQUExQjs7QUFDQSxRQUFHdEIsWUFBWSxJQUFJWSxTQUFuQixFQUE2QjtBQUMzQixVQUFHVyxRQUFRLENBQUN2QixZQUFZLENBQUN3QixVQUFkLENBQVIsSUFBcUMsQ0FBeEMsRUFBMEM7QUFBRTtBQUMxQyxZQUFHeEIsWUFBWSxDQUFDeUIsT0FBaEIsRUFBd0I7QUFDdEJILDZCQUFtQixHQUFHdEIsWUFBWSxDQUFDeUIsT0FBbkM7QUFDRCxTQUZELE1BRUs7QUFDSEgsNkJBQW1CLEdBQUd0QixZQUFZLENBQUMwQixLQUFuQztBQUNEO0FBQ0YsT0FORCxNQU1LO0FBQ0gsWUFBR0osbUJBQW1CLElBQUksSUFBdkIsSUFBK0J0QixZQUFZLENBQUN5QixPQUEvQyxFQUF1RDtBQUNyREgsNkJBQW1CLEdBQUd0QixZQUFZLENBQUN5QixPQUFuQztBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxRQUFNRSxZQUFZLEdBQUdDLHNEQUFRLENBQUNOLG1CQUFELENBQTdCO0FBQ0EsV0FBT0ssWUFBUDtBQUNEOztBQW5Ea0UsS0FpQzFETixlQWpDMEQ7O0FBQUEseUJBb0QzQkEsZUFBZSxDQUFDckIsWUFBRCxDQXBEWTtBQUFBO0FBQUEsTUFvRDVEMkIsWUFwRDREO0FBQUEsTUFvRDlDRSxlQXBEOEM7O0FBc0RuRSxXQUFTQyxZQUFULENBQXNCOUIsWUFBdEIsRUFBb0M7QUFBQTs7QUFDbEMsUUFBSStCLGdCQUFnQixHQUFHLElBQXZCOztBQUNBLFFBQUcvQixZQUFZLElBQUlZLFNBQW5CLEVBQTZCO0FBQzNCLFVBQUdXLFFBQVEsQ0FBQ3ZCLFlBQVksQ0FBQ3dCLFVBQWQsQ0FBUixJQUFxQyxDQUF4QyxFQUEwQztBQUFFO0FBQzFDLFlBQUd4QixZQUFZLENBQUN5QixPQUFoQixFQUF3QjtBQUN0Qk0sMEJBQWdCLEdBQUcvQixZQUFZLENBQUMwQixLQUFoQztBQUNELFNBRkQsTUFFSztBQUNISywwQkFBZ0IsR0FBRyxJQUFuQjtBQUNEO0FBQ0YsT0FORCxNQU1LO0FBQ0gsWUFBR0EsZ0JBQWdCLElBQUksSUFBcEIsSUFBNEIvQixZQUFZLENBQUMwQixLQUE1QyxFQUFrRDtBQUNoREssMEJBQWdCLEdBQUcvQixZQUFZLENBQUMwQixLQUFoQztBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxRQUFJTSxjQUFjLEdBQUdKLHNEQUFRLENBQUNHLGdCQUFELENBQTdCO0FBQ0EsV0FBT0MsY0FBUDtBQUNEOztBQXhFa0UsTUFzRDFERixZQXREMEQ7O0FBQUEsc0JBeUVuQ0EsWUFBWSxDQUFDOUIsWUFBRCxDQXpFdUI7QUFBQTtBQUFBLE1BeUU5RGlDLFNBekU4RDtBQUFBLE1BeUVuREMsWUF6RW1EOztBQUFBLGtCQTJFM0JOLHNEQUFRLENBQUMsWUFBTTtBQUNyRCxRQUFNTyxZQUFZLEdBQUcsQ0FBckI7QUFDQSxXQUFPQSxZQUFQO0FBQ0QsR0FIK0MsQ0EzRW1CO0FBQUEsTUEyRTVEQyxZQTNFNEQ7QUFBQSxNQTJFOUNDLGVBM0U4QztBQWlGbkU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFJTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFRSxNQUFJQyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDQyxFQUFELEVBQVE7QUFDdkIsUUFBTUMsT0FBTyxHQUFHRCxFQUFFLENBQUNFLGFBQUgsQ0FBaUJDLE9BQWpCLENBQXlCQyxPQUF6QztBQUNBLFFBQU1DLFFBQVEsR0FBR0wsRUFBRSxDQUFDRSxhQUFILENBQWlCQyxPQUFqQixDQUF5QkcsUUFBMUM7QUFDQSxRQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFDQUEsYUFBUyxDQUFDLE1BQUQsQ0FBVCxHQUFvQk4sT0FBcEI7QUFDQU0sYUFBUyxDQUFDLE9BQUQsQ0FBVCxHQUFxQkYsUUFBckIsQ0FMdUIsQ0FNdkI7QUFFRCxHQVJEOztBQVdBLE1BQU1HLE1BQU0sR0FBR0MsNkRBQVMsRUFBeEI7O0FBQ0EsTUFBSSxDQUFDRCxNQUFNLENBQUNFLFVBQVIsSUFBc0IsRUFBQ2pELFlBQUQsYUFBQ0EsWUFBRCxlQUFDQSxZQUFZLENBQUVrRCxVQUFmLENBQTFCLEVBQXFEO0FBQ25ELHdCQUFPLHFFQUFDLGlEQUFEO0FBQVcsZ0JBQVUsRUFBRTtBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBQVA7QUFDRDs7QUFDRCxzQkFDRSxxRUFBQywyREFBRDtBQUFBLDJCQUNFLHFFQUFDLDhEQUFEO0FBQUEsZ0JBQ0dILE1BQU0sQ0FBQ0UsVUFBUCxnQkFDQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURELGdCQUdDO0FBQUEsZ0NBQ0UscUVBQUMsZ0RBQUQ7QUFBQSxrQ0FDRTtBQUFBLHNCQUFRakQsWUFBWSxDQUFDbUQ7QUFBckI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQUVFO0FBQU0sZ0JBQUksRUFBQyxhQUFYO0FBQXlCLG1CQUFPLEVBQUM7QUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFGRixlQUdFO0FBQU0sZ0JBQUksRUFBQyxVQUFYO0FBQXNCLG1CQUFPLEVBQUM7QUFBOUI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFNRSxxRUFBQyxjQUFEO0FBQWdCLGlCQUFPLEVBQUVyRCxPQUFPLENBQUNzRCxRQUFqQztBQUEyQyxrQkFBUSxFQUFFckQsUUFBUSxDQUFDcUQ7QUFBOUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFORixlQU9FO0FBQUssY0FBSSxFQUFDLGNBQVY7QUFBQSxpQ0FDRTtBQUFLLHFCQUFTLEVBQUMsZUFBZjtBQUFBLG1DQUNFO0FBQUssdUJBQVMsRUFBQyx3REFBZjtBQUFBLHFDQUNFO0FBQUsseUJBQVMsRUFBQyxXQUFmO0FBQUEsd0NBQ0U7QUFBSSwyQkFBUyxFQUFDLFlBQWQ7QUFBQSwwQ0FDRztBQUFJLDZCQUFTLEVBQUMsaUJBQWQ7QUFBQSwyQ0FDRztBQUFHLDBCQUFJLEVBQUMsR0FBUjtBQUFZLDJCQUFLLEVBQUMsbUJBQWxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREg7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFESCxlQUlHO0FBQUksNkJBQVMsRUFBQyxpQkFBZDtBQUFBLDhCQUNJcEQsWUFBWSxDQUFDbUQ7QUFEakI7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFKSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREYsZUFVRTtBQUFLLDJCQUFTLEVBQUMsYUFBZjtBQUFBLHlDQUNFO0FBQUssNkJBQVMsRUFBQyxXQUFmO0FBQUEsMkNBQ0k7QUFBSywrQkFBUyxFQUFDLGVBQWY7QUFBQSw4Q0FFRTtBQUFLLGlDQUFTLEVBQUMsa0JBQWY7QUFBQSxnREFDRTtBQUFLLG1DQUFTLEVBQUMscUJBQWY7QUFBQSxpREFDQztBQUFLLHFDQUFTLEVBQUMsMEJBQWY7QUFBQSxzQ0FFSW5ELFlBQVksQ0FBQ00sTUFBZCxHQUNHK0MsTUFBTSxDQUFDQyxJQUFQLENBQVl0RCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDaUQsTUFBakMsR0FBMEMsQ0FBM0MsZ0JBQ0UscUVBQUMsa0RBQUQ7QUFBUSx1Q0FBUyxFQUFDO0FBQWxCLCtCQUFxQ3JFLFlBQXJDO0FBQUEseUNBR01tRSxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNrRCxHQUFqQyxDQUFxQyxVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxvREFDN0I7QUFDRSwyQ0FBUyxFQUFDLDZFQURaO0FBQUEseURBR0k7QUFBSyx1Q0FBRyxFQUFFMUQsWUFBWSxDQUFDTSxNQUFiLENBQW9CbUQsSUFBcEIsRUFBMEIzQyxLQUFwQztBQUNFLHVDQUFHLEVBQUVkLFlBQVksQ0FBQ21ELElBRHBCO0FBRUUsb0RBQWNuRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjVDLElBRjFDO0FBR0UscURBQWViLFlBQVksQ0FBQ00sTUFBYixDQUFvQm1ELElBQXBCLEVBQTBCM0MsS0FIM0M7QUFJRSx5Q0FBSyxFQUFDLElBSlI7QUFJYSwwQ0FBTSxFQUFDLElBSnBCO0FBS0UsMkNBQU8sRUFBQztBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFISjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQUQ2QjtBQUFBLCtCQUFyQyxDQUhOLEVBaUJLZCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSixNQUFyQixHQUNFdkQsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkgsR0FBcEIsQ0FBd0IsVUFBQ0ksU0FBRCxFQUFZQyxRQUFaO0FBQUEsb0RBQ3RCO0FBQUssMkNBQVMsRUFBQyxZQUFmO0FBQUEseURBQ0U7QUFBRyx3Q0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQU8sRUFBQyxFQUEvQjtBQUFrQyxvREFBY0QsU0FBUyxDQUFDRSxRQUExRDtBQUFBLDREQUNFO0FBQUsseUNBQUcsdUNBQWdDRixTQUFTLENBQUNFLFFBQTFDO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FERixlQUVFO0FBQUcsK0NBQVMsRUFBQyxvQkFBYjtBQUFrQyxxREFBWTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLG1DQUFpQ0QsUUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FEc0I7QUFBQSwrQkFBeEIsQ0FERixHQVVFLElBM0JOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixnQkFpQ0U7QUFBQSx5Q0FFRVIsTUFBTSxDQUFDQyxJQUFQLENBQVl0RCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDa0QsR0FBakMsQ0FBcUMsVUFBQ0MsSUFBRCxFQUFPQyxHQUFQO0FBQUEsb0RBQzdCO0FBQ0UsMkNBQVMsRUFBQyw2RUFEWjtBQUFBLHlEQUdJO0FBQUssdUNBQUcsRUFBRTFELFlBQVksQ0FBQ00sTUFBYixDQUFvQm1ELElBQXBCLEVBQTBCM0MsS0FBcEM7QUFDRSx1Q0FBRyxFQUFFZCxZQUFZLENBQUNtRCxJQURwQjtBQUVFLG9EQUFjbkQsWUFBWSxDQUFDTSxNQUFiLENBQW9CbUQsSUFBcEIsRUFBMEI1QyxJQUYxQztBQUdFLHFEQUFlYixZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjNDLEtBSDNDO0FBSUUseUNBQUssRUFBQyxJQUpSO0FBSWEsMENBQU0sRUFBQyxJQUpwQjtBQUtFLDJDQUFPLEVBQUM7QUFMVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSEo7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FENkI7QUFBQSwrQkFBckMsQ0FGRixFQWlCR2QsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkosTUFBckIsR0FDRXZELFlBQVksQ0FBQzJELE1BQWIsQ0FBb0JILEdBQXBCLENBQXdCLFVBQUNJLFNBQUQsRUFBWUMsUUFBWjtBQUFBLG9EQUN0QjtBQUFLLDJDQUFTLEVBQUMsWUFBZjtBQUFBLHlEQUNFO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0Msb0RBQWNELFNBQVMsQ0FBQ0UsUUFBMUQ7QUFBQSw0REFDRTtBQUFLLHlDQUFHLHVDQUFnQ0YsU0FBUyxDQUFDRSxRQUExQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREYsZUFFRTtBQUFHLCtDQUFTLEVBQUMsb0JBQWI7QUFBa0MscURBQVk7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixtQ0FBaUNELFFBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRHNCO0FBQUEsK0JBQXhCLENBREYsR0FVRSxJQTNCSjtBQUFBLDRDQWxDSixHQWdFRTtBQWxFTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0FERixlQXlFSTtBQUFLLG1DQUFTLEVBQUMsZ0NBQWY7QUFBQSxpREFDRTtBQUFLLHFDQUFTLEVBQUMsVUFBZjtBQUFBLHVDQUVLN0QsWUFBWSxDQUFDK0QsTUFBYixDQUFvQkMsY0FBcEIsQ0FBbUN2RCxLQUFuQyxJQUE0QyxJQUE3QyxnQkFDRTtBQUFLLHVDQUFTLEVBQUMsd0JBQWY7QUFBQSxxREFDRTtBQUFLLG1DQUFHLEVBQUVULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JDLGNBQXBCLENBQW1DdkQ7QUFBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREYsR0FLRSxJQVBOLEVBVUtULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JFLGVBQXBCLENBQW9DeEQsS0FBcEMsSUFBNkMsSUFBOUMsZ0JBQ0U7QUFBSyx1Q0FBUyxFQUFDLHlCQUFmO0FBQUEscURBQ0U7QUFBSyxtQ0FBRyxFQUFFVCxZQUFZLENBQUMrRCxNQUFiLENBQW9CRSxlQUFwQixDQUFvQ3hEO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLEdBS0UsSUFmTixFQWtCS1QsWUFBWSxDQUFDK0QsTUFBYixDQUFvQkcsaUJBQXBCLENBQXNDekQsS0FBdEMsSUFBK0MsSUFBaEQsZ0JBQ0U7QUFBSyx1Q0FBUyxFQUFDLDJCQUFmO0FBQUEscURBQ0U7QUFBSyxtQ0FBRyxFQUFFVCxZQUFZLENBQUMrRCxNQUFiLENBQW9CRyxpQkFBcEIsQ0FBc0N6RDtBQUFoRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixHQUtFLElBdkJOLEVBMEJLVCxZQUFZLENBQUMrRCxNQUFiLENBQW9CSSxrQkFBcEIsQ0FBdUMxRCxLQUF2QyxJQUFnRCxJQUFqRCxnQkFDRTtBQUFLLHVDQUFTLEVBQUMsNEJBQWY7QUFBQSxxREFDRTtBQUFLLG1DQUFHLEVBQUVULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JJLGtCQUFwQixDQUF1QzFEO0FBQWpEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLEdBS0UsSUEvQk4sZUFrQ0UscUVBQUMsMkRBQUQsb0JBQXVCO0FBQ2YyRCx3Q0FBVSxFQUFFO0FBQ1pDLG1DQUFHLEVBQUVyRSxZQUFZLENBQUNtRCxJQUROO0FBRVptQiw0Q0FBWSxFQUFFLElBRkY7QUFHWkMsbUNBQUcsRUFBRXRFLGFBQWEsQ0FBQ1k7QUFIUCwrQkFERztBQU1uQjJELHdDQUFVLEVBQUU7QUFDUkQsbUNBQUcsRUFBRXRFLGFBQWEsQ0FBQ2EsS0FEWDtBQUVSMkQscUNBQUssRUFBRTNFLE9BQU8sQ0FBQzRFLFNBRlA7QUFHUkMsc0NBQU0sRUFBRTdFLE9BQU8sQ0FBQzRFO0FBSFI7QUFOTyw2QkFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FsQ0Y7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0F6RUosZUE0SEU7QUFBSyxtQ0FBUyxFQUFDLGlCQUFmO0FBQUEsb0NBQ0lyQixNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsS0FBb0MrQyxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNpRCxNQUFqQyxHQUEwQyxDQUEvRSxnQkFDQztBQUFLLHFDQUFTLEVBQUMsZ0JBQWY7QUFBQSxtREFDRSxxRUFBQyxrREFBRDtBQUFRLHVDQUFTLEVBQUM7QUFBbEIsK0JBQXFDNUQsbUJBQXJDO0FBQUEseUNBRU0wRCxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNrRCxHQUFqQyxDQUFxQyxVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxvREFDakM7QUFBSywyQ0FBUyxFQUFDLFVBQWY7QUFBQSx5REFDRTtBQUFLLHVDQUFHLEVBQUUxRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjNDLEtBQXBDO0FBQTJDLHVDQUFHLEVBQUVkLFlBQVksQ0FBQ21EO0FBQTdEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQURpQztBQUFBLCtCQUFyQyxDQUZOLEVBU0tuRCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSixNQUFyQixHQUNFdkQsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkgsR0FBcEIsQ0FBd0IsVUFBQ0ksU0FBRCxFQUFZQyxRQUFaO0FBQUEsb0RBQ3RCO0FBQUssMkNBQVMsRUFBQyxxQkFBZjtBQUFBLHlEQUNFO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0Msb0RBQWNELFNBQVMsQ0FBQ0UsUUFBMUQ7QUFBQSw0REFDRTtBQUFLLHlDQUFHLHVDQUFnQ0YsU0FBUyxDQUFDRSxRQUExQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREYsZUFFRTtBQUFHLCtDQUFTLEVBQUMsb0JBQWI7QUFBa0MscURBQVk7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixtQ0FBMENELFFBQTFDO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRHNCO0FBQUEsK0JBQXhCLENBREYsR0FVRSxJQW5CTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQURELEdBeUJFUixNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsS0FBb0MrQyxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNpRCxNQUFqQyxHQUEwQyxDQUEvRSxnQkFDRTtBQUFBLHVDQUVJRixNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNrRCxHQUFqQyxDQUFxQyxVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxrREFDbkM7QUFBSyx5Q0FBUyxFQUFDLFVBQWY7QUFBQSx1REFDRTtBQUFLLHFDQUFHLEVBQUUxRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjNDLEtBQXBDO0FBQTJDLHFDQUFHLEVBQUVkLFlBQVksQ0FBQ21EO0FBQTdEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLHVDQURtQztBQUFBLDZCQUFyQyxDQUZKLEVBU0tuRCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSixNQUFyQixHQUNFdkQsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkgsR0FBcEIsQ0FBd0IsVUFBQ0ksU0FBRCxFQUFZQyxRQUFaO0FBQUEsa0RBQ3RCO0FBQUsseUNBQVMsRUFBQyxxQkFBZjtBQUFBLHVEQUNFO0FBQUcsc0NBQUksRUFBQyxjQUFSO0FBQXVCLHlDQUFPLEVBQUMsRUFBL0I7QUFBa0Msa0RBQWNELFNBQVMsQ0FBQ0UsUUFBMUQ7QUFBQSwwREFDRTtBQUFLLHVDQUFHLHVDQUFnQ0YsU0FBUyxDQUFDRSxRQUExQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkNBREYsZUFFRTtBQUFHLDZDQUFTLEVBQUMsb0JBQWI7QUFBa0MsbURBQVk7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQ0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixpQ0FBMENELFFBQTFDO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUNBRHNCO0FBQUEsNkJBQXhCLENBREYsR0FVRSxJQW5CTjtBQUFBLDBDQURGLEdBd0JBO0FBbERKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0NBNUhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw4QkFGRixlQW9MRTtBQUFLLGlDQUFTLEVBQUMsZUFBZjtBQUFBLCtDQUNJO0FBQUssbUNBQVMsRUFBQyx5REFBZjtBQUFBLGtEQUNFO0FBQUsscUNBQVMsRUFBQyxpQkFBZjtBQUFBLG9EQUNHO0FBQUksdUNBQVMsRUFBQyxnQkFBZDtBQUFBLHdDQUFnQzdELFlBQVksQ0FBQ21EO0FBQTdDO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREgsRUFHTW5ELFlBQVksQ0FBQzRFLEtBQWQsZ0JBQ0M7QUFBSSx1Q0FBUyxFQUFDLGNBQWQ7QUFBQSxxREFDSTtBQUFBLHdEQUFJO0FBQU0sMkNBQVMsRUFBQyxXQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FBSixPQUE2QzVFLFlBQVksQ0FBQzRFLEtBQTFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREQsR0FLQyxJQVJOLGVBV0c7QUFBSyx1Q0FBUyxFQUFDLFNBQWY7QUFBQSxxREFDRztBQUFHLHlDQUFTLEVBQUMsWUFBYjtBQUEwQix1Q0FBTyxFQUFDLEVBQWxDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREg7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FYSCxFQWdCTTVFLFlBQVksU0FBWixJQUFzQixFQUF2QixnQkFDQztBQUFLLHVDQUFTLEVBQUMsV0FBZjtBQUFBLHFEQUNDO0FBQUsseUNBQVMsRUFBQyxlQUFmO0FBQUEsdURBQ0M7QUFBSyx5REFBdUIsRUFBRTtBQUFFNkUsMENBQU0sRUFBRTdFLFlBQVk7QUFBdEI7QUFBOUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERDtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURELEdBT0MsSUF2Qk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQURGLGVBNkJFO0FBQUsscUNBQVMsRUFBQyxTQUFmO0FBQUEsb0RBRUU7QUFBSyx1Q0FBUyxFQUFDLFlBQWY7QUFBQSxzREFDRTtBQUFLLHlDQUFTLEVBQUMsZUFBZjtBQUFBLDJDQUNNMkIsWUFBRCxnQkFDQztBQUFLLDJDQUFTLEVBQUMsVUFBZjtBQUFBLGlFQUNLO0FBQU0sNkNBQVMsRUFBQyxvQkFBaEI7QUFBQSw4Q0FBc0NHLFlBQVksQ0FBQzlCLFlBQUQ7QUFBbEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FETDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBREQsR0FLQyxFQU5OLGVBUUk7QUFBSywyQ0FBUyxFQUFDLHNCQUFmO0FBQUEsNENBQ0kyQixZQUFELGdCQUNDO0FBQUEsb0VBQ007QUFBTSwrQ0FBUyxFQUFDLGNBQWhCO0FBQUEsZ0RBQWdDTixlQUFlLENBQUNyQixZQUFEO0FBQS9DO0FBQUE7QUFBQTtBQUFBO0FBQUEsNENBRE4sZUFFQztBQUFNLCtDQUFTLEVBQUMsZUFBaEI7QUFBQSxnREFBaUNBLFlBQVksQ0FBQzhFO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsNENBRkQ7QUFBQSxrREFERCxnQkFNQztBQUFBLG9FQUFPO0FBQUEsZ0RBQU9oRCxZQUFZLENBQUM5QixZQUFEO0FBQW5CO0FBQUE7QUFBQTtBQUFBO0FBQUEsNENBQVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEo7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FSSixFQW1CTWdCLGlCQUFpQixJQUFJLEVBQXJCLElBQTJCQyxpQkFBaUIsSUFBSSxFQUFqRCxnQkFDQztBQUFHLDJDQUFTLEVBQUMsZ0JBQWI7QUFBQSxpRUFBaUM7QUFBTSw2Q0FBUyxFQUFDLFNBQWhCO0FBQUEsK0NBQTJCRCxpQkFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUFqQyx1QkFBMEY7QUFBTSw2Q0FBUyxFQUFDLFNBQWhCO0FBQUEsOENBQTJCQztBQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUExRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBREQsR0FHQyxJQXRCTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREYsRUE0QktqQixZQUFZLElBQUksRUFBakIsR0FDR3VCLFFBQVEsQ0FBQ3ZCLFlBQVksQ0FBQ3dCLFVBQWQsQ0FBUixJQUFxQyxDQUF0QyxnQkFDQztBQUFLLHlDQUFTLEVBQUMsYUFBZjtBQUFBLDBDQUVLeEIsWUFBWSxDQUFDK0UsT0FBYixDQUFxQnZCLEdBQXJCLENBQXlCLFVBQUNDLElBQUQsRUFBT0MsR0FBUDtBQUFBLHNEQUN2QjtBQUFLLDZDQUFTLEVBQUMsWUFBZjtBQUFBLDREQUNFO0FBQUEsZ0RBQVFELElBQUksQ0FBQ047QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQURGLEVBR0lNLElBQUksQ0FBQ3VCLElBQUwsSUFBYSxNQUFkLGdCQUNDO0FBQUssK0NBQVMsRUFBQyxXQUFmO0FBQUEsOERBQ0U7QUFBTywwQ0FBRSxFQUFDLGFBQVY7QUFBd0IsaURBQVMsRUFBQyxHQUFsQztBQUFzQyw0Q0FBSSxFQUFDLE1BQTNDO0FBQWtELHNEQUFZdkIsSUFBSSxDQUFDd0IsaUJBQW5FO0FBQXNGLGlEQUFTLEVBQUMsT0FBaEc7QUFBd0csNENBQUksRUFBQztBQUE3RztBQUFBO0FBQUE7QUFBQTtBQUFBLCtDQURGLGVBRUU7QUFBSyxpREFBUyxFQUFDLE1BQWY7QUFBQSxnRUFDSTtBQUFHLG1EQUFTLEVBQUMsOEJBQWI7QUFBNEMseURBQVk7QUFBeEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxpREFESixlQUdJO0FBQUssbURBQVMsRUFBQyxVQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlEQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwrQ0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREQsZ0JBWUM7QUFBSSwrQ0FBUyxFQUFDLGFBQWQ7QUFBQSxnREFFSXhCLElBQUksQ0FBQ3lCLFlBQUwsQ0FBa0IxQixHQUFsQixDQUFzQixVQUFDMkIsVUFBRCxFQUFhekIsR0FBYjtBQUFBLDREQUNwQjtBQUFBLGlFQUNFO0FBQU8scURBQVMsRUFBRXRDLGlCQUFpQixLQUFLcUMsSUFBSSxDQUFDd0IsaUJBQUwsR0FBdUIsR0FBdkIsR0FBMkJFLFVBQVUsQ0FBQ0MsdUJBQTVELEdBQXNGLFFBQXRGLEdBQWlHLGlCQUFuSDtBQUFzSSxtREFBTyxFQUFDLEVBQTlJO0FBQWlKLGdEQUFJLEVBQUMsY0FBdEo7QUFBQSxzREFBc0tELFVBQVUsQ0FBQ2hDO0FBQWpMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlEQURvQjtBQUFBLHVDQUF0QjtBQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBZko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJDQUR1QjtBQUFBLGlDQUF6QjtBQUZMO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREQsR0FrQ0EsRUFuQ0YsR0FvQ0QsRUFoRUg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQUZGLGVBc0VJO0FBQUssdUNBQVMsRUFBQyxZQUFmO0FBQUEsc0RBQ0U7QUFBSyx5Q0FBUyxFQUFDLGNBQWY7QUFBQSx3REFDRztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FESCxlQUVHO0FBQUssMkNBQVMsRUFBQyxpQkFBZjtBQUFBLDBEQUNHO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0MsNkNBQVMsRUFBQyxTQUE1QztBQUFBLDJEQUFzRDtBQUFHLCtDQUFTLEVBQUMsYUFBYjtBQUEyQixxREFBWTtBQUF2QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXREO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBREgsZUFFRztBQUFPLHNDQUFFLEVBQUMsYUFBVjtBQUF3Qix3Q0FBSSxFQUFDLE1BQTdCO0FBQW9DLHdDQUFJLEVBQUMsY0FBekM7QUFBd0QseUNBQUssRUFBR2pDLFdBQWhFO0FBQThFLDZDQUFTLEVBQUM7QUFBeEY7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FGSCxlQUdHO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0MsNkNBQVMsRUFBQyxTQUE1QztBQUFBLDJEQUFzRDtBQUFHLCtDQUFTLEVBQUMsWUFBYjtBQUEwQixxREFBWTtBQUF0QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXREO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBSEg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQUZIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERixFQVVJa0IsWUFBWSxJQUFJLElBQWhCLElBQXdCLENBQUNBLFlBQTFCLGdCQUNDO0FBQUsseUNBQVMsRUFBQyxlQUFmO0FBQUEsdURBQ007QUFBRyxzQ0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQVMsRUFBQyxtQkFBakM7QUFBcUQseUNBQU8sRUFBQyxFQUE3RDtBQUFBLDBEQUNHO0FBQUcsNkNBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBREgsT0FDZ0QsU0FEaEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRE47QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERCxHQU1FQSxZQUFZLElBQUksSUFBaEIsSUFBd0JBLFlBQXpCLGdCQUNBO0FBQUsseUNBQVMsRUFBQyxlQUFmO0FBQUEsdURBQ007QUFBRyxzQ0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQVMsRUFBQyx3QkFBakM7QUFBMEQsb0RBQWdCcEMsWUFBWSxDQUFDa0QsVUFBdkY7QUFBbUcseUNBQU8sRUFBQyxFQUEzRztBQUFBLDBEQUNHO0FBQUcsNkNBQVMsRUFBQyxnQkFBYjtBQUE4QixtREFBWTtBQUExQztBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQURIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUROO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREEsR0FPQSxJQXZCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBdEVKLEVBaUdJbEQsWUFBWSxDQUFDcUYsYUFBYixJQUE2QixJQUE3QixJQUFxQ3JGLFlBQVksQ0FBQ3FGLGFBQWIsSUFBOEIsRUFBcEUsZ0JBQ0M7QUFBQSxzREFDRTtBQUFHLHlDQUFTLEVBQUMsZUFBYjtBQUFBLHdEQUE2QjtBQUFPLG9DQUFFLEVBQUMsWUFBVjtBQUF1QixzQ0FBSSxFQUFDLFVBQTVCO0FBQXVDLDBDQUFRLEVBQUM7QUFBaEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FBN0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURGLGVBRUU7QUFBSyx5Q0FBUyxFQUFDLGNBQWY7QUFBQSx3REFDRTtBQUFJLDJDQUFTLEVBQUMsS0FBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FERixFQUVHckYsWUFBWSxDQUFDcUYsYUFGaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQUZGO0FBQUEsNENBREQsR0FTQyxJQTFHSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBN0JGLEVBNElJckYsWUFBWSxDQUFDc0YsWUFBZCxnQkFDQztBQUFLLHFDQUFTLEVBQUMsYUFBZjtBQUFBLG1EQUNFO0FBQUEsd0NBRUV0RixZQUFZLENBQUNzRixZQUFiLENBQTBCOUIsR0FBMUIsQ0FBOEIsVUFBQytCLFFBQUQsRUFBV0MsT0FBWDtBQUFBLG9EQUM1QjtBQUFJLDJDQUFTLEVBQUMsVUFBZDtBQUFBLHlEQUNJO0FBQUcsNkNBQVMsRUFBQyxRQUFiO0FBQXNCLHlDQUFLLEVBQUVELFFBQVEsQ0FBQ0UsS0FBdEM7QUFBNkMsdURBQWdCLEVBQTdEO0FBQWdFLHlEQUFrQixFQUFsRjtBQUFxRiwyQ0FBTyxFQUFDLEVBQTdGO0FBQUEsOENBQWlHRixRQUFRLENBQUNFO0FBQTFHO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQUQ0QjtBQUFBLCtCQUE5QjtBQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQURELEdBYUMsSUF6Sko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSw4QkFwTEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBVkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQVBGLGVBc1hFLHFFQUFDLGNBQUQ7QUFBZ0IsaUJBQU8sRUFBRTNGLE9BQU8sQ0FBQ3NEO0FBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBdFhGO0FBQUE7QUFKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBa1lEOztJQXhnQnVCdkQsTztVQWtJUG1ELHFEOzs7TUFsSU9uRCxPO0FBd2dCdkIiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvW3NsdWddLjNiNWM4YzIyODU1OTk1ZjY2MjM5LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuaW1wb3J0IEVycm9yUGFnZSBmcm9tIFwibmV4dC9lcnJvclwiO1xyXG5pbXBvcnQgSGVhZCBmcm9tIFwibmV4dC9oZWFkXCI7XHJcbmltcG9ydCBkeW5hbWljIGZyb20gJ25leHQvZHluYW1pYydcclxuaW1wb3J0IHsgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCdcclxuaW1wb3J0IFJlYWN0SW1hZ2VNYWduaWZ5IGZyb20gJ3JlYWN0LWltYWdlLW1hZ25pZnknXHJcbmltcG9ydCBTbGlkZXIgZnJvbSBcInJlYWN0LXNsaWNrXCI7XHJcblxyXG5pbXBvcnQgQ29udGFpbmVyIGZyb20gXCJAL2NvbXBvbmVudHMvY29udGFpbmVyXCI7XHJcbmltcG9ydCBMYXlvdXQgZnJvbSBcIkAvY29tcG9uZW50cy9sYXlvdXRcIjtcclxuaW1wb3J0IHsgZ2V0Q29uZmlncywgZ2V0TWFpbk1lbnUsIGdldFByb2R1Y3RCeVNsdWcsIGdldFByb2R1Y3RzQnlDYXRlZ29yeSB9IGZyb20gXCJAL2xpYi9hcGlcIjtcclxuXHJcbnZhciBpZFN0b3JlQXBwID0gJ240OXNob3B2Ml90cmlqb2lhJztcclxudmFyIFRlbXBsYXRlSGVhZGVyID0gZHluYW1pYygoKSA9PiBpbXBvcnQoJ0AvY29tcG9uZW50cy90ZW1wbGF0ZXMvJytpZFN0b3JlQXBwKycvY29tcG9uZW50cy9oZWFkZXInKSlcclxudmFyIFRlbXBsYXRlRm9vdGVyID0gZHluYW1pYygoKSA9PiBpbXBvcnQoJ0AvY29tcG9uZW50cy90ZW1wbGF0ZXMvJytpZFN0b3JlQXBwKycvY29tcG9uZW50cy9mb290ZXInKSlcclxuXHJcbmxldCBzbGlkZXJUaHVtYnMgPSB7XHJcbiAgaW5maW5pdGU6IGZhbHNlLFxyXG4gIHZlcnRpY2FsOiB0cnVlLFxyXG4gIHNsaWRlc1RvU2hvdzogNCxcclxuICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICBzcGVlZDogNTAwLFxyXG4gIHJlc3BvbnNpdmU6IFtcclxuICAgIHtcclxuICAgICAgYnJlYWtwb2ludDogNDE1LFxyXG4gICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgIHZlcnRpY2FsOiBmYWxzZSxcclxuICAgICAgICBzbGlkZXNUb1Nob3c6IDNcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIF1cclxufVxyXG5cclxudmFyIHNldHRpbmdzTW9iaWxlU2xpZGUgPSB7XHJcbiAgc2xpZGVzVG9TaG93OiAxLFxyXG4gIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gIHNwZWVkOiA1MDAsXHJcbiAgZG90czogdHJ1ZVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBQcm9kdWN0KHsgY29uZmlncywgbWFpbk1lbnUsIGRhZG9zUHJvZHV0byB9KSB7XHJcbiAgdmFyIGluaXRpYWxJbWFnZXMgPSBbXTtcclxuICBmdW5jdGlvbiBnZXRJbml0aWFsSW1hZ2UoZGFkb3NQcm9kdXRvKSB7XHJcbiAgICBcclxuICAgIHZhciBpbWdJbmljaWFsID0gW107XHJcbiAgICB2YXIgdGVtcEltZ0luaWNpYWwgPSBbXTtcclxuICAgIFxyXG4gICAgZm9yKHZhciBpIGluIGRhZG9zUHJvZHV0by5pbWFnZXMpe1xyXG4gICAgICB0ZW1wSW1nSW5pY2lhbC5wdXNoKFtpLCBkYWRvc1Byb2R1dG8uaW1hZ2VzIFtpXV0pO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciBpbml0aWFsS2V5ID0gZGFkb3NQcm9kdXRvLmltYWdlLnN1YnN0cmluZyhkYWRvc1Byb2R1dG8uaW1hZ2UubGFzdEluZGV4T2YoJy8nKSsxKVxyXG4gICAgXHJcbiAgICBpZihkYWRvc1Byb2R1dG8uaW1hZ2VzICE9IHVuZGVmaW5lZCAmJiBkYWRvc1Byb2R1dG8uaW1hZ2VzW2luaXRpYWxLZXldICE9IG51bGwpe1xyXG4gICAgICBpbWdJbmljaWFsWydzaG93J10gPSBkYWRvc1Byb2R1dG8uaW1hZ2VzW2luaXRpYWxLZXldLnNob3cgXHJcbiAgICAgIGltZ0luaWNpYWxbJ3BvcHVwJ10gPSBkYWRvc1Byb2R1dG8uaW1hZ2VzW2luaXRpYWxLZXldLnBvcHVwXHJcbiAgICAgIGltZ0luaWNpYWxbJ3RodW1iJ10gPSBkYWRvc1Byb2R1dG8uaW1hZ2VzW2luaXRpYWxLZXldLnRodW1iXHJcbiAgICB9ZWxzZSBpZih0ZW1wSW1nSW5pY2lhbFswXSAhPSBudWxsKXtcclxuICAgICAgaW1nSW5pY2lhbCA9IHRlbXBJbWdJbmljaWFsWzBdWzFdXHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGltZ0luaWNpYWw7XHJcbiAgfVxyXG4gIGlmKGRhZG9zUHJvZHV0byl7XHJcbiAgICBpbml0aWFsSW1hZ2VzID0gZ2V0SW5pdGlhbEltYWdlKGRhZG9zUHJvZHV0byk7XHJcbiAgfVxyXG5cclxuICB2YXIgc2VsZWN0ZWRRdGRQYXJjZWwgPSAxO1xyXG4gIHZhciBzZWxlY3RlZFZhbFBhcmNlbCA9IDE7XHJcbiAgdmFyIHF1YW50aXR5QnV5ID0gMTtcclxuICB2YXIgc2hpcHBpbmdNZXRob2RzID0gbnVsbDtcclxuICB2YXIgc2VsZWN0ZWRPcHRpb25CdXkgPSAxO1xyXG4gIFxyXG4gIGZ1bmN0aW9uIGdldFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8pIHtcclxuICAgIHZhciBpbml0aWFsU3BlY2lhbFZhbHVlID0gbnVsbDtcclxuICAgIGlmKGRhZG9zUHJvZHV0byAhPSB1bmRlZmluZWQpe1xyXG4gICAgICBpZihwYXJzZUludChkYWRvc1Byb2R1dG8uaGFzX29wdGlvbikgPT0gMCl7IC8vIFBST0RVVE8gU0VNIE9QQ09FU1xyXG4gICAgICAgIGlmKGRhZG9zUHJvZHV0by5zcGVjaWFsKXtcclxuICAgICAgICAgIGluaXRpYWxTcGVjaWFsVmFsdWUgPSBkYWRvc1Byb2R1dG8uc3BlY2lhbDtcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgIGluaXRpYWxTcGVjaWFsVmFsdWUgPSBkYWRvc1Byb2R1dG8ucHJpY2U7XHJcbiAgICAgICAgfVxyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICBpZihpbml0aWFsU3BlY2lhbFZhbHVlID09IG51bGwgJiYgZGFkb3NQcm9kdXRvLnNwZWNpYWwpe1xyXG4gICAgICAgICAgaW5pdGlhbFNwZWNpYWxWYWx1ZSA9IGRhZG9zUHJvZHV0by5zcGVjaWFsXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGNvbnN0IHNwZWNpYWxWYWx1ZSA9IHVzZVN0YXRlKGluaXRpYWxTcGVjaWFsVmFsdWUpO1xyXG4gICAgcmV0dXJuIHNwZWNpYWxWYWx1ZTtcclxuICB9XHJcbiAgY29uc3QgW3NwZWNpYWxWYWx1ZSwgc2V0U3BlY2lhbFZhbHVlXSA9IGdldFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8pO1xyXG5cclxuICBmdW5jdGlvbiBnZXRNYWluVmFsdWUoZGFkb3NQcm9kdXRvKSB7XHJcbiAgICB2YXIgaW5pdGlhbE1haW5WYWx1ZSA9IG51bGw7XHJcbiAgICBpZihkYWRvc1Byb2R1dG8gIT0gdW5kZWZpbmVkKXtcclxuICAgICAgaWYocGFyc2VJbnQoZGFkb3NQcm9kdXRvLmhhc19vcHRpb24pID09IDApeyAvLyBQUk9EVVRPIFNFTSBPUENPRVNcclxuICAgICAgICBpZihkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgICBpbml0aWFsTWFpblZhbHVlID0gZGFkb3NQcm9kdXRvLnByaWNlO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgaW5pdGlhbE1haW5WYWx1ZSA9IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICBpZihpbml0aWFsTWFpblZhbHVlID09IG51bGwgJiYgZGFkb3NQcm9kdXRvLnByaWNlKXtcclxuICAgICAgICAgIGluaXRpYWxNYWluVmFsdWUgPSBkYWRvc1Byb2R1dG8ucHJpY2VcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgdmFyIG1haW5WYWx1ZVN0YXRlID0gdXNlU3RhdGUoaW5pdGlhbE1haW5WYWx1ZSk7XHJcbiAgICByZXR1cm4gbWFpblZhbHVlU3RhdGU7XHJcbiAgfVxyXG4gIHZhciBbbWFpblZhbHVlLCBzZXRNYWluVmFsdWVdID0gZ2V0TWFpblZhbHVlKGRhZG9zUHJvZHV0byk7XHJcblxyXG4gIGNvbnN0IFtzb2xkT3V0U3RvY2ssIHNldFNvbGRPdXRTdG9ja10gPSB1c2VTdGF0ZSgoKSA9PiB7XHJcbiAgICBjb25zdCBpbml0aWFsU3RhdGUgPSAwO1xyXG4gICAgcmV0dXJuIGluaXRpYWxTdGF0ZTtcclxuICB9KTtcclxuICBcclxuICBcclxuICAvKmlmKHBhcnNlSW50KGRhZG9zUHJvZHV0by5oYXNfb3B0aW9uKSA9PSAwKXsgLy8gUFJPRFVUTyBTRU0gT1BDT0VTXHJcbiAgICAgIFxyXG4gICAgY29uc3QgdmFsb3JBdHVhbCA9IGRhZG9zUHJvZHV0by5wcmljZVxyXG4gICAgaWYoZGFkb3NQcm9kdXRvLnNwZWNpYWwpe1xyXG4gICAgICBzZXRTZWxlY3RlZE1haW5WYWx1ZSh2YWxvckF0dWFsKTtcclxuICAgICAgc2V0U2VsZWN0ZWRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvLnNwZWNpYWwpO1xyXG4gICAgfWVsc2V7XHJcbiAgICAgIHNldFNlbGVjdGVkTWFpblZhbHVlKG51bGwpO1xyXG4gICAgICBzZXRTZWxlY3RlZFNwZWNpYWxWYWx1ZSh2YWxvckF0dWFsKTtcclxuICAgIH1cclxuXHJcbiAgICBpZighZGFkb3NQcm9kdXRvLnNvbGRfb3V0KXtcclxuICAgICAgc2V0U29sZE91dFN0b2NrKDApO1xyXG4gICAgfWVsc2V7XHJcbiAgICAgIHNldFNvbGRPdXRTdG9jaygxKTtcclxuICAgIH1cclxuICB9ZWxzZXsgLy8gUFJPRFVUTyBDT00gT1BDT0VTXHJcbiAgICBzZXRTb2xkT3V0U3RvY2soMCk7XHJcblxyXG4gICAgICBpZihzZWxlY3RlZE1haW5WYWx1ZSA9PSBudWxsKXtcclxuICAgICAgICBzZXRTZWxlY3RlZE1haW5WYWx1ZShkYWRvc1Byb2R1dG8ucHJpY2UpXHJcbiAgICAgIH1cclxuICAgICAgaWYoc2VsZWN0ZWRTcGVjaWFsVmFsdWUgPT0gbnVsbCAmJiBkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgc2V0U2VsZWN0ZWRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvLnNwZWNpYWwpXHJcbiAgICAgIH0qL1xyXG5cclxuICAgICAgLypmb3IodmFyIGkgaW4gZGFkb3NQcm9kdXRvLm9wdGlvbnMpe1xyXG4gICAgICAgIGdydXBvQXR1YWwgPSBkYWRvc1Byb2R1dG8ub3B0aW9uc1tpXVxyXG4gICAgICAgIGZvcih2YXIgb3B0IGluIGdydXBvQXR1YWwub3B0aW9uX3ZhbHVlKXtcclxuICAgICAgICAgIGlmKGdydXBvQXR1YWwub3B0aW9uX3ZhbHVlW29wdF0ub3B0aW9uX3ZhbHVlX2lkPT1kYWRvc1Byb2R1dG8ub3BjYW9fc2VsZWNpb25hZGEpe1xyXG4gICAgICAgICAgICBpbml0aWFsT3B0aW9uID0gZ3J1cG9BdHVhbC5wcm9kdWN0X29wdGlvbl9pZCsnXycrZ3J1cG9BdHVhbC5vcHRpb25fdmFsdWVbb3B0XS5wcm9kdWN0X29wdGlvbl92YWx1ZV9pZFxyXG4gICAgICAgICAgICB0aGlzLlNlbGVjdE9wdGlvbihpbml0aWFsT3B0aW9uLCBudWxsKVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAgICAgICBcclxuICB9Ki9cclxuIFxyXG4gIHZhciBjaGFuZ2Vab29tID0gKGV2KSA9PiB7XHJcbiAgICBjb25zdCBpbWdTaG93ID0gZXYuY3VycmVudFRhcmdldC5kYXRhc2V0LnNyY3Nob3dcclxuICAgIGNvbnN0IGltZ1BvcHVwID0gZXYuY3VycmVudFRhcmdldC5kYXRhc2V0LnNyY3BvcHVwXHJcbiAgICB2YXIgdGVtcEltYWdlID0ge307XHJcbiAgICB0ZW1wSW1hZ2VbJ3Nob3cnXSA9IGltZ1Nob3dcclxuICAgIHRlbXBJbWFnZVsncG9wdXAnXSA9IGltZ1BvcHVwXHJcbiAgICAvL3NldEluaXRpYWxJbWFnZSh0ZW1wSW1hZ2UpO1xyXG4gICAgXHJcbiAgfVxyXG4gIFxyXG5cclxuICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKTtcclxuICBpZiAoIXJvdXRlci5pc0ZhbGxiYWNrICYmICFkYWRvc1Byb2R1dG8/LnByb2R1Y3RfaWQpIHtcclxuICAgIHJldHVybiA8RXJyb3JQYWdlIHN0YXR1c0NvZGU9ezQwNH0gLz47XHJcbiAgfVxyXG4gIHJldHVybiAoXHJcbiAgICA8TGF5b3V0PlxyXG4gICAgICA8Q29udGFpbmVyPlxyXG4gICAgICAgIHtyb3V0ZXIuaXNGYWxsYmFjayA/IChcclxuICAgICAgICAgIDxkaXY+TG9hZGluZ+KApjwvZGl2PlxyXG4gICAgICAgICkgOiAoXHJcbiAgICAgICAgICA8PlxyXG4gICAgICAgICAgICA8SGVhZD5cclxuICAgICAgICAgICAgICA8dGl0bGU+e2RhZG9zUHJvZHV0by5uYW1lfTwvdGl0bGU+XHJcbiAgICAgICAgICAgICAgPG1ldGEgbmFtZT1cImRlc2NyaXB0aW9uXCIgY29udGVudD1cImxlbGVsZVwiIC8+XHJcbiAgICAgICAgICAgICAgPG1ldGEgbmFtZT1cIm9nOmltYWdlXCIgY29udGVudD1cImxpbGlsaVwiIC8+XHJcbiAgICAgICAgICAgIDwvSGVhZD5cclxuICAgICAgICAgICAgPFRlbXBsYXRlSGVhZGVyIGNvbmZpZ3M9e2NvbmZpZ3MucmVzcG9zdGF9IG1haW5NZW51PXttYWluTWVudS5yZXNwb3N0YX0+PC9UZW1wbGF0ZUhlYWRlcj4gXHJcbiAgICAgICAgICAgIDxkaXYgY2xhcz1cIm1haW4tY29udGVudFwiPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicGFnZS1wcm9kdWN0c1wiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItcGFkZGluZyBsaWdodC1iYWNrZ3JvdW5kIG5wcm9kdWN0LWJyZWFkY3J1bWJcIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8b2wgY2xhc3NOYW1lPVwiYnJlYWRjcnVtYlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJicmVhZGNydW1iLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiL1wiIHRpdGxlPVwiUMOhZ2luYSBpbmljaWFsXCI+SG9tZTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJicmVhZGNydW1iLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICB7ZGFkb3NQcm9kdXRvLm5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8L29sPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNwLXByZXZpZXczXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2R1Y3QtcGFnZVwiPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2R1Y3QtZ2FsbGVyeVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtdGh1bWJuYWlscyBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZXMtY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8uaW1hZ2VzKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5sZW5ndGggPiA0KT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U2xpZGVyIGNsYXNzTmFtZT1cInNsaWRlclRodW1ic1wiIHsuLi5zbGlkZXJUaHVtYnN9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwcm9kdWN0LWltYWdlLXRodW1iIGpzLWNhcm91c2VsLWNvbnRyb2wtaXRlbSBwb2ludGVyIGpzLXByb2R1Y3QtaW1hZ2UtdGh1bWJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD17ZGFkb3NQcm9kdXRvLm5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLXNyY3Nob3c9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0uc2hvd31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEtc3JjcG9wdXA9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjEwXCIgaGVpZ2h0PVwiMTBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz1cIlwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by52aWRlb3MubGVuZ3RoKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8udmlkZW9zLm1hcCgoaXRlbVZpZGVvLCBrZXlWaWRlbykgPT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpdGVtLXZpZGVvXCIga2V5PXtrZXlWaWRlb30+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBkYXRhLXZpZGVvaWQ9e2l0ZW1WaWRlby5pZF92aWRlb30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2BodHRwczovL2ltZy55b3V0dWJlLmNvbS92aS8ke2l0ZW1WaWRlby5pZF92aWRlb30vbXFkZWZhdWx0LmpwZ2B9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEteW91dHViZS1wbGF5XCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TbGlkZXI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZS10aHVtYiBqcy1jYXJvdXNlbC1jb250cm9sLWl0ZW0gcG9pbnRlciBqcy1wcm9kdWN0LWltYWdlLXRodW1iXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ9e2RhZG9zUHJvZHV0by5uYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLXNyY3Nob3c9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0uc2hvd31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1zcmNwb3B1cD17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCIxMFwiIGhlaWdodD1cIjEwXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz1cIlwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by52aWRlb3MubGVuZ3RoKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLnZpZGVvcy5tYXAoKGl0ZW1WaWRlbywga2V5VmlkZW8pID0+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIml0ZW0tdmlkZW9cIiBrZXk9e2tleVZpZGVvfT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBkYXRhLXZpZGVvaWQ9e2l0ZW1WaWRlby5pZF92aWRlb30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvJHtpdGVtVmlkZW8uaWRfdmlkZW99L21xZGVmYXVsdC5qcGdgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS15b3V0dWJlLXBsYXlcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2VzIG1pbi13aWR0aC00MTVweFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhcmVhWm9vbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fdG9wX2xlZnQuaW1hZ2UgIT0gbnVsbCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxhYmVsSXRlbSBsYWJlbFRvcExlZnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fdG9wX2xlZnQuaW1hZ2V9IC8+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fdG9wX3JpZ2h0LmltYWdlICE9IG51bGwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsYWJlbEl0ZW0gbGFiZWxUb3BSaWdodFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb190b3BfcmlnaHQuaW1hZ2V9IC8+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fYm90dG9tX2xlZnQuaW1hZ2UgIT0gbnVsbCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxhYmVsSXRlbSBsYWJlbEJvdHRvbUxlZnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fYm90dG9tX2xlZnQuaW1hZ2V9IC8+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fYm90dG9tX3JpZ2h0LmltYWdlICE9IG51bGwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsYWJlbEl0ZW0gbGFiZWxCb3R0b21SaWdodFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb19ib3R0b21fcmlnaHQuaW1hZ2V9IC8+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8UmVhY3RJbWFnZU1hZ25pZnkgey4uLntcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzbWFsbEltYWdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWx0OiBkYWRvc1Byb2R1dG8ubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0ZsdWlkV2lkdGg6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjOiBpbml0aWFsSW1hZ2VzLnNob3dcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXJnZUltYWdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjOiBpbml0aWFsSW1hZ2VzLnBvcHVwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBjb25maWdzLndpZHRoWm9vbSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IGNvbmZpZ3Mud2lkdGhab29tXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19IC8+ICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtYXgtd2lkdGgtNDE0cHhcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpICYmIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLmxlbmd0aCA+IDEpID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZXNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNsaWRlciBjbGFzc05hbWU9XCJzbGlkZVZpdHJpbmVcIiB7Li4uc2V0dGluZ3NNb2JpbGVTbGlkZX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLm1hcCgoaXRlbSwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb21cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9IGFsdD17ZGFkb3NQcm9kdXRvLm5hbWV9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLnZpZGVvcy5sZW5ndGgpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8udmlkZW9zLm1hcCgoaXRlbVZpZGVvLCBrZXlWaWRlbykgPT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb20gaXRlbS12aWRlb1wiIGtleT17a2V5VmlkZW99PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGRhdGEtdmlkZW9pZD17aXRlbVZpZGVvLmlkX3ZpZGVvfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2BodHRwczovL2ltZy55b3V0dWJlLmNvbS92aS8ke2l0ZW1WaWRlby5pZF92aWRlb30vbXFkZWZhdWx0LmpwZ2B9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXlvdXR1YmUtcGxheVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1NsaWRlcj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogKE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpICYmIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLmxlbmd0aCA+IDApID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb21cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH0gYWx0PXtkYWRvc1Byb2R1dG8ubmFtZX0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8udmlkZW9zLmxlbmd0aCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by52aWRlb3MubWFwKChpdGVtVmlkZW8sIGtleVZpZGVvKSA9PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhcmVhWm9vbSBpdGVtLXZpZGVvXCIga2V5PXtrZXlWaWRlb30+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgZGF0YS12aWRlb2lkPXtpdGVtVmlkZW8uaWRfdmlkZW99PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17YGh0dHBzOi8vaW1nLnlvdXR1YmUuY29tL3ZpLyR7aXRlbVZpZGVvLmlkX3ZpZGVvfS9tcWRlZmF1bHQuanBnYH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEteW91dHViZS1wbGF5XCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC8+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LWluZm9cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtYWN0aW9ucyBjb250YWluZXItcGFkZGluZyBjb250YWluZXItcGFkZGluZy10b3BcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2R1Y3QtaGVhZGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDEgY2xhc3NOYW1lPVwibnByb2R1Y3QtdGl0bGVcIj57ZGFkb3NQcm9kdXRvLm5hbWV9PC9oMT5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhkYWRvc1Byb2R1dG8ubW9kZWwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzTmFtZT1cImluZm9zUHJvZHVjdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48c3BhbiBjbGFzc05hbWU9XCJ0aXRsZUluZm9cIj5SRUY6PC9zcGFuPiB7ZGFkb3NQcm9kdXRvLm1vZGVsfTwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyYXRlQm94XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJsay1hdmFsaWFyXCIgb25DbGljaz1cIlwiPkF2YWxpYXIgYWdvcmE8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoZGFkb3NQcm9kdXRvLnNob3J0ICE9ICcnKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaW5mb3NBcmVhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyZXN1bWVQcm9kdWN0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9e3sgX19odG1sOiBkYWRvc1Byb2R1dG8uc2hvcnQgfX0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV5QXJlYVwiPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2xTZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZGN0LXByaWNlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoc3BlY2lhbFZhbHVlKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJvbGRQcmljZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgRGUgPHNwYW4gY2xhc3NOYW1lPVwibnByb2R1Y3QtcHJpY2UtbWF4XCI+e2dldE1haW5WYWx1ZShkYWRvc1Byb2R1dG8pfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZHVjdC1wcmljZS12YWx1ZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoc3BlY2lhbFZhbHVlKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBQb3IgPHNwYW4gY2xhc3NOYW1lPVwic3BlY2lhbFZhbHVlXCI+e2dldFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8pfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJpdGVtLWRpc2NvdW50XCI+e2RhZG9zUHJvZHV0by5kaXNjb3VudF9wZXJjZW50fTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPnBvciA8c3Bhbj57Z2V0TWFpblZhbHVlKGRhZG9zUHJvZHV0byl9PC9zcGFuPjwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhzZWxlY3RlZFF0ZFBhcmNlbCAhPSAnJyAmJiBzZWxlY3RlZFZhbFBhcmNlbCAhPSAnJyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwic2VsZWN0ZWRQYXJjZWxcIj5PdSA8c3BhbiBjbGFzc05hbWU9XCJudW1QYXJjXCI+e3NlbGVjdGVkUXRkUGFyY2VsfXg8L3NwYW4+IGRlIDxzcGFuIGNsYXNzTmFtZT1cInZhbFBhcmNcIj57c2VsZWN0ZWRWYWxQYXJjZWx9PC9zcGFuPjwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0byAhPSAnJyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChwYXJzZUludChkYWRvc1Byb2R1dG8uaGFzX29wdGlvbikgIT0gMCkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJvcHRpb25zQXJlYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLm9wdGlvbnMubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYm94LW9wdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPntpdGVtLm5hbWV9PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhpdGVtLnR5cGUgPT0gJ3RleHQnKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0eHRPcHRpb25cIj4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCBpZD1cImZpZWxkT3B0aW9uXCIgbWF4bGVuZ3RoPVwiM1wiIHR5cGU9XCJ0ZXh0XCIgZGF0YS1ncm91cD17aXRlbS5wcm9kdWN0X29wdGlvbl9pZH0gY2xhc3NOYW1lPVwiZmllbGRcIiBuYW1lPVwidHh0LW9wdGlvblwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJoZWxwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXF1ZXN0aW9uLWNpcmNsZSBjb2xvcjJcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImluZm9IZWxwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBJbnNpcmEgYXTDqSAzIGxldHJhcyBwYXJhIHBlcnNvbmFsaXphciBhIGNhbWlzYSBjb20gdW0gYm9yZGFkbyBleGNsdXNpdm8uICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzTmFtZT1cImxpc3RPcHRpb25zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlbS5vcHRpb25fdmFsdWUubWFwKChpdGVtT3B0aW9uLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzTmFtZT17c2VsZWN0ZWRPcHRpb25CdXkgIT09IGl0ZW0ucHJvZHVjdF9vcHRpb25faWQrJ18nK2l0ZW1PcHRpb24ucHJvZHVjdF9vcHRpb25fdmFsdWVfaWQgPyAnb3B0aW9uJyA6ICdvcHRpb24gc2VsZWN0ZWQnfSBvbkNsaWNrPVwiXCIgaHJlZj1cImphdmFzY3JpcHQ6O1wiPntpdGVtT3B0aW9uLm5hbWV9PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiAgJycgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2xTZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInF1YW50aXR5QXJlYVwiPiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPlF1YW50aWRhZGU8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXR0b25zUXVhbnRpdHlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgY2xhc3NOYW1lPVwiYnRuTGVzc1wiPjxpIGNsYXNzTmFtZT1cImZhIGZhLW1pbnVzXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCBpZD1cInR4dFF1YW50aXR5XCIgdHlwZT1cInRleHRcIiBuYW1lPVwidHh0LXF1YW50aXR5XCIgdmFsdWU9eyBxdWFudGl0eUJ1eSB9IGNsYXNzTmFtZT1cInR4dFF1YW50aXR5XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgY2xhc3NOYW1lPVwiYnRuTW9yZVwiPjxpIGNsYXNzTmFtZT1cImZhIGZhLXBsdXNcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KHNvbGRPdXRTdG9jayAhPSBudWxsICYmICFzb2xkT3V0U3RvY2spP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJ1eUJ1dHRvbkFyZWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIGNsYXNzTmFtZT1cImJ1eUJ1dHRvbiBidG5fYnV5XCIgb25DbGljaz1cIlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEgZmEtc2hvcHBpbmctY2FydFwiPjwvaT4geydDb21wcmFyJ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogKHNvbGRPdXRTdG9jayAhPSBudWxsICYmIHNvbGRPdXRTdG9jayk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV5QnV0dG9uQXJlYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgY2xhc3NOYW1lPVwiYnV5QnV0dG9uIG5vdGlmeUJ1dHRvblwiIGRhdGEtcHJvZHVjdGlkPXtkYWRvc1Byb2R1dG8ucHJvZHVjdF9pZH0gb25DbGljaz1cIlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1lbnZlbG9wZVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT4gQXZpc2UtbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoZGFkb3NQcm9kdXRvLnRleHRfcHJldmVuZGEhPSBudWxsICYmIGRhZG9zUHJvZHV0by50ZXh0X3ByZXZlbmRhICE9ICcnKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwiY2hlY2tQcmV2ZW5kYVwiPjxpbnB1dCBpZD1cImNrcHJldmVuZGFcIiB0eXBlPVwiY2hlY2tib3hcIiBvbkNoYW5nZT1cIlwiIC8+IENvbmNvcmRvIGNvbSBvIHByYXpvIGRlIGVudHJlZ2EgZGVzY3JpdG8gYWJhaXhvLjwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaW5mb1ByZXZlbmRhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJ0aXRcIj5URVJNTyBERSBBQ0VJVEHDh8ODTzwvaDQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtkYWRvc1Byb2R1dG8udGV4dF9wcmV2ZW5kYX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGRhZG9zUHJvZHV0by5ndWlhX21lZGlkYXMpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImd1aWFzTWVkaWRhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by5ndWlhX21lZGlkYXMubWFwKChpdGVtR3VpYSwga2V5R3VpYSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJpdGVtR3VpYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiY29sb3IyXCIgdGl0bGU9e2l0ZW1HdWlhLnRpdGxlfSBkYXRhLXRpdHVsb2d1aWE9XCJcIiBkYXRhLWNvbnRldWRvZ3VpYT1cIlwiIG9uQ2xpY2s9XCJcIj57aXRlbUd1aWEudGl0bGV9PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPFRlbXBsYXRlRm9vdGVyIGNvbmZpZ3M9e2NvbmZpZ3MucmVzcG9zdGF9IC8+XHJcbiAgICAgICAgICA8Lz5cclxuICAgICAgICApfVxyXG4gICAgICA8L0NvbnRhaW5lcj5cclxuICAgIDwvTGF5b3V0PlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0U3RhdGljUHJvcHMoeyBwYXJhbXMgfSkge1xyXG4gIHZhciBjb25maWdzID0gYXdhaXQgZ2V0Q29uZmlncygpO1xyXG4gIHZhciBtYWluTWVudSA9IGF3YWl0IGdldE1haW5NZW51KCk7XHJcbiAgdmFyIHJlc1Byb2R1Y3QgPSBhd2FpdCBnZXRQcm9kdWN0QnlTbHVnKHBhcmFtcy5zbHVnKTtcclxuXHJcbiAgdmFyIGRhZG9zUHJvZHV0byA9IG51bGw7XHJcbiAgaWYocmVzUHJvZHVjdC5zdWNjZXNzKXtcclxuICAgIGRhZG9zUHJvZHV0byA9IHJlc1Byb2R1Y3QucmVzcG9zdGEucHJvZHV0bztcclxuICB9XHJcbiAgcmV0dXJuIHtcclxuICAgIHByb3BzOiB7XHJcbiAgICAgIGNvbmZpZ3MsXHJcbiAgICAgIGRhZG9zUHJvZHV0byxcclxuICAgICAgbWFpbk1lbnVcclxuICAgIH0sXHJcbiAgICByZXZhbGlkYXRlOiA2MFxyXG4gIH07XHJcbn1cclxuXHJcbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBnZXRTdGF0aWNQYXRocygpIHtcclxuICBjb25zdCBwcm9kdXRvcyA9IGF3YWl0IGdldFByb2R1Y3RzQnlDYXRlZ29yeSgpO1xyXG5cclxuICByZXR1cm4ge1xyXG4gICAgcGF0aHM6XHJcbiAgICAgIFtdLFxyXG4gICAgZmFsbGJhY2s6IHRydWUsXHJcbiAgfTtcclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9