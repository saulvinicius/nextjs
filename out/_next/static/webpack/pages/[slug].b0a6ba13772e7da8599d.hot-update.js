webpackHotUpdate_N_E("pages/[slug]",{

/***/ "./node_modules/@babel/runtime/helpers/esm/arrayLikeToArray.js":
false,

/***/ "./node_modules/@babel/runtime/helpers/esm/arrayWithHoles.js":
false,

/***/ "./node_modules/@babel/runtime/helpers/esm/iterableToArrayLimit.js":
false,

/***/ "./node_modules/@babel/runtime/helpers/esm/nonIterableRest.js":
false,

/***/ "./node_modules/@babel/runtime/helpers/esm/slicedToArray.js":
false,

/***/ "./node_modules/@babel/runtime/helpers/esm/unsupportedIterableToArray.js":
false,

/***/ "./pages/[slug].js":
/*!*************************!*\
  !*** ./pages/[slug].js ***!
  \*************************/
/*! exports provided: __N_SSG, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__N_SSG", function() { return __N_SSG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Product; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/error */ "./node_modules/next/error.js");
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_error__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_image_magnify__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-image-magnify */ "./node_modules/react-image-magnify/dist/es/ReactImageMagnify.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _components_container__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @/components/container */ "./components/container.js");
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/components/layout */ "./components/layout.js");




var _jsxFileName = "C:\\Users\\sauln49\\Desktop\\nextjs\\pages\\[slug].js",
    _s = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }










var idStoreApp = 'n49shopv2_trijoia';
var TemplateHeader = next_dynamic__WEBPACK_IMPORTED_MODULE_5___default()(_c = function _c() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/header$")("./" + idStoreApp + "/components/header");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/header$").resolve("./" + idStoreApp + "/components/header"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/header']
  }
});
_c2 = TemplateHeader;
var TemplateFooter = next_dynamic__WEBPACK_IMPORTED_MODULE_5___default()(_c3 = function _c3() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/footer$")("./" + idStoreApp + "/components/footer");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/footer$").resolve("./" + idStoreApp + "/components/footer"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/footer']
  }
});
_c4 = TemplateFooter;
var sliderThumbs = {
  infinite: false,
  vertical: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  speed: 500,
  responsive: [{
    breakpoint: 415,
    settings: {
      vertical: false,
      slidesToShow: 3
    }
  }]
};
var settingsMobileSlide = {
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  dots: true
};
var __N_SSG = true;
function Product(_ref) {
  _s();

  var _this = this;

  var configs = _ref.configs,
      mainMenu = _ref.mainMenu,
      dadosProduto = _ref.dadosProduto;
  var initialImages = [];

  function getInitialImage(dadosProduto) {
    var imgInicial = [];
    var tempImgInicial = [];

    for (var i in dadosProduto.images) {
      tempImgInicial.push([i, dadosProduto.images[i]]);
    }

    var initialKey = dadosProduto.image.substring(dadosProduto.image.lastIndexOf('/') + 1);

    if (dadosProduto.images != undefined && dadosProduto.images[initialKey] != null) {
      imgInicial['show'] = dadosProduto.images[initialKey].show;
      imgInicial['popup'] = dadosProduto.images[initialKey].popup;
      imgInicial['thumb'] = dadosProduto.images[initialKey].thumb;
    } else if (tempImgInicial[0] != null) {
      imgInicial = tempImgInicial[0][1];
    }

    return imgInicial;
  }

  if (dadosProduto) {
    initialImages = getInitialImage(dadosProduto);
  }

  var selectedQtdParcel = 1;
  var selectedValParcel = 1;
  var quantityBuy = 1;
  var shippingMethods = null;
  var selectedOptionBuy = 1;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_6__["useState"])(null),
      specialValue = _useState[0],
      setSpecialValue = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_6__["useState"])(null),
      mainValue = _useState2[0],
      setMainValue = _useState2[1];

  Object(react__WEBPACK_IMPORTED_MODULE_6__["useEffect"])(function () {
    function getMainValue(dadosProduto) {
      var initialMainValue = null;

      if (dadosProduto != undefined) {
        if (parseInt(dadosProduto.has_option) == 0) {
          // PRODUTO SEM OPCOES
          if (dadosProduto.special) {
            initialMainValue = dadosProduto.price;
          } else {
            initialMainValue = dadosProduto.price;
          }
        } else {
          if (initialMainValue == null && dadosProduto.price) {
            initialMainValue = dadosProduto.price;
          }
        }

        setMainValue(initialMainValue);
      }
    }

    function getSpecialValue(dadosProduto) {
      var initialSpecialValue = null;

      if (dadosProduto != undefined) {
        if (parseInt(dadosProduto.has_option) == 0) {
          // PRODUTO SEM OPCOES
          if (dadosProduto.special) {
            initialSpecialValue = dadosProduto.special;
          } else {
            initialSpecialValue = dadosProduto.price;
          }
        } else {
          if (initialSpecialValue == null && dadosProduto.special) {
            initialSpecialValue = dadosProduto.special;
          }
        }
      }

      setSpecialValue(initialSpecialValue);
    }

    getSpecialValue(dadosProduto);
    getMainValue(dadosProduto);
  });

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_6__["useState"])(function () {
    var initialState = 0;
    return initialState;
  }),
      soldOutStock = _useState3[0],
      setSoldOutStock = _useState3[1];
  /*if(parseInt(dadosProduto.has_option) == 0){ // PRODUTO SEM OPCOES
      
    const valorAtual = dadosProduto.price
    if(dadosProduto.special){
      setSelectedMainValue(valorAtual);
      setSelectedSpecialValue(dadosProduto.special);
    }else{
      setSelectedMainValue(null);
      setSelectedSpecialValue(valorAtual);
    }
      if(!dadosProduto.sold_out){
      setSoldOutStock(0);
    }else{
      setSoldOutStock(1);
    }
  }else{ // PRODUTO COM OPCOES
    setSoldOutStock(0);
        if(selectedMainValue == null){
        setSelectedMainValue(dadosProduto.price)
      }
      if(selectedSpecialValue == null && dadosProduto.special){
        setSelectedSpecialValue(dadosProduto.special)
      }*/

  /*for(var i in dadosProduto.options){
    grupoAtual = dadosProduto.options[i]
    for(var opt in grupoAtual.option_value){
      if(grupoAtual.option_value[opt].option_value_id==dadosProduto.opcao_selecionada){
        initialOption = grupoAtual.product_option_id+'_'+grupoAtual.option_value[opt].product_option_value_id
        this.SelectOption(initialOption, null)
      }
    }
  }
        
  }*/


  var changeZoom = function changeZoom(ev) {
    var imgShow = ev.currentTarget.dataset.srcshow;
    var imgPopup = ev.currentTarget.dataset.srcpopup;
    var tempImage = {};
    tempImage['show'] = imgShow;
    tempImage['popup'] = imgPopup; //setInitialImage(tempImage);
  };

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"])();

  if (!router.isFallback && !(dadosProduto !== null && dadosProduto !== void 0 && dadosProduto.product_id)) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_error__WEBPACK_IMPORTED_MODULE_3___default.a, {
      statusCode: 404
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 173,
      columnNumber: 12
    }, this);
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_layout__WEBPACK_IMPORTED_MODULE_10__["default"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_container__WEBPACK_IMPORTED_MODULE_9__["default"], {
      children: router.isFallback ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        children: "Loading\u2026"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 181,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_4___default.a, {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
            children: dadosProduto.name
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 185,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "description",
            content: "lelele"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 186,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "og:image",
            content: "lilili"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 187,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 184,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateHeader, {
          configs: configs.resposta,
          mainMenu: mainMenu.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 189,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "main-content",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "page-products",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "container-padding light-background nproduct-breadcrumb",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "container",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ol", {
                  className: "breadcrumb",
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                      href: "/",
                      title: "P\xE1gina inicial",
                      children: "Home"
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 196,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 195,
                    columnNumber: 24
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: dadosProduto.name
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 198,
                    columnNumber: 24
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 194,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                  className: "cp-preview3",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                    className: "container",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                      className: "nproduct-page",
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-gallery",
                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-thumbnails ",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images-container",
                            children: dadosProduto.images ? Object.keys(dadosProduto.images).length > 4 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_8___default.a, _objectSpread(_objectSpread({
                              className: "sliderThumbs"
                            }, sliderThumbs), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 219,
                                    columnNumber: 57
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 216,
                                  columnNumber: 53
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 233,
                                      columnNumber: 51
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 234,
                                      columnNumber: 51
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 232,
                                    columnNumber: 49
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 231,
                                  columnNumber: 47
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 212,
                              columnNumber: 39
                            }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 250,
                                    columnNumber: 53
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 247,
                                  columnNumber: 49
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 265,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 266,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 264,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 263,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }, void 0, true) : null
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 208,
                            columnNumber: 32
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 207,
                          columnNumber: 31
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-images min-width-415px",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "areaZoom",
                            children: [dadosProduto.labels.promo_top_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 284,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 283,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_top_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 292,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 291,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 300,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 299,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 308,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 307,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_image_magnify__WEBPACK_IMPORTED_MODULE_7__["default"], _objectSpread({}, {
                              smallImage: {
                                alt: dadosProduto.name,
                                isFluidWidth: true,
                                src: initialImages.show
                              },
                              largeImage: {
                                src: initialImages.popup,
                                width: configs.widthZoom,
                                height: configs.widthZoom
                              }
                            }), void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 314,
                              columnNumber: 37
                            }, this)]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 280,
                            columnNumber: 35
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 279,
                          columnNumber: 33
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "max-width-414px",
                          children: Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 1 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_8___default.a, _objectSpread(_objectSpread({
                              className: "slideVitrine"
                            }, settingsMobileSlide), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 337,
                                    columnNumber: 49
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 336,
                                  columnNumber: 47
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 346,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 347,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 345,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 344,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 333,
                              columnNumber: 37
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 332,
                            columnNumber: 35
                          }, this) : Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                            children: [Object.keys(dadosProduto.images).map(function (item, key) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                  src: dadosProduto.images[item].popup,
                                  alt: dadosProduto.name
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 361,
                                  columnNumber: 45
                                }, _this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 360,
                                columnNumber: 43
                              }, _this);
                            }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom item-video",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  onClick: "",
                                  "data-videoid": itemVideo.id_video,
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 370,
                                    columnNumber: 49
                                  }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-youtube-play",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 371,
                                    columnNumber: 49
                                  }, _this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 369,
                                  columnNumber: 47
                                }, _this)
                              }, keyVideo, false, {
                                fileName: _jsxFileName,
                                lineNumber: 368,
                                columnNumber: 45
                              }, _this);
                            }) : null]
                          }, void 0, true) : null
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 330,
                          columnNumber: 31
                        }, this)]
                      }, void 0, true, {
                        fileName: _jsxFileName,
                        lineNumber: 206,
                        columnNumber: 29
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-info",
                        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-actions container-padding container-padding-top",
                          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "nproduct-header",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
                              className: "nproduct-title",
                              children: dadosProduto.name
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 387,
                              columnNumber: 38
                            }, this), dadosProduto.model ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              className: "infosProduct",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "titleInfo",
                                  children: "REF:"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 391,
                                  columnNumber: 49
                                }, this), " ", dadosProduto.model]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 391,
                                columnNumber: 45
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 390,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "rateBox",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                className: "lk-avaliar",
                                onClick: "",
                                children: "Avaliar agora"
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 398,
                                columnNumber: 41
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 397,
                              columnNumber: 38
                            }, this), dadosProduto["short"] != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "infosArea",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "resumeProduct",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  dangerouslySetInnerHTML: {
                                    __html: dadosProduto["short"]
                                  }
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 405,
                                  columnNumber: 43
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 404,
                                columnNumber: 42
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 403,
                              columnNumber: 41
                            }, this) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 386,
                            columnNumber: 35
                          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "buyArea",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "nprodct-price",
                                children: [lalala ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "oldPrice",
                                  children: ["De ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "nproduct-price-max",
                                    children: mainValue
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 420,
                                    columnNumber: 50
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 419,
                                  columnNumber: 45
                                }, this) : '', /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "nproduct-price-value",
                                  children: lalala ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                                    children: ["Por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "specialValue",
                                      children: lalala
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 428,
                                      columnNumber: 53
                                    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "item-discount",
                                      children: dadosProduto.discount_percent
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 429,
                                      columnNumber: 48
                                    }, this)]
                                  }, void 0, true) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                    children: ["por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      children: mainValue
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 432,
                                      columnNumber: 54
                                    }, this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 432,
                                    columnNumber: 47
                                  }, this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 425,
                                  columnNumber: 43
                                }, this), selectedQtdParcel != '' && selectedValParcel != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                  className: "selectedParcel",
                                  children: ["Ou ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "numParc",
                                    children: [selectedQtdParcel, "x"]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 437,
                                    columnNumber: 78
                                  }, this), " de ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "valParc",
                                    children: selectedValParcel
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 437,
                                    columnNumber: 135
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 437,
                                  columnNumber: 45
                                }, this) : null]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 417,
                                columnNumber: 39
                              }, this), dadosProduto != '' ? parseInt(dadosProduto.has_option) != 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "optionsArea",
                                children: dadosProduto.options.map(function (item, key) {
                                  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                    className: "box-option",
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                      children: item.name
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 450,
                                      columnNumber: 53
                                    }, _this), item.type == 'text' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                      className: "txtOption",
                                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                        id: "fieldOption",
                                        maxlength: "3",
                                        type: "text",
                                        "data-group": item.product_option_id,
                                        className: "field",
                                        name: "txt-option"
                                      }, void 0, false, {
                                        fileName: _jsxFileName,
                                        lineNumber: 454,
                                        columnNumber: 57
                                      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                        className: "help",
                                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                          className: "fa fa-question-circle color2",
                                          "aria-hidden": "true"
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 456,
                                          columnNumber: 61
                                        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                          className: "infoHelp",
                                          children: "Insira at\xE9 3 letras para personalizar a camisa com um bordado exclusivo."
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 458,
                                          columnNumber: 61
                                        }, _this)]
                                      }, void 0, true, {
                                        fileName: _jsxFileName,
                                        lineNumber: 455,
                                        columnNumber: 57
                                      }, _this)]
                                    }, void 0, true, {
                                      fileName: _jsxFileName,
                                      lineNumber: 453,
                                      columnNumber: 55
                                    }, _this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                                      className: "listOptions",
                                      children: item.option_value.map(function (itemOption, key) {
                                        return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                            className: selectedOptionBuy !== item.product_option_id + '_' + itemOption.product_option_value_id ? 'option' : 'option selected',
                                            onClick: "",
                                            href: "javascript:;",
                                            children: itemOption.name
                                          }, void 0, false, {
                                            fileName: _jsxFileName,
                                            lineNumber: 468,
                                            columnNumber: 63
                                          }, _this)
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 467,
                                          columnNumber: 61
                                        }, _this);
                                      })
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 464,
                                      columnNumber: 55
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 449,
                                    columnNumber: 51
                                  }, _this);
                                })
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 446,
                                columnNumber: 44
                              }, this) : '' : '']
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 416,
                              columnNumber: 37
                            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "quantityArea",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                  children: "Quantidade"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 486,
                                  columnNumber: 44
                                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "buttonsQuantity",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnLess",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-minus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 488,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 488,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                    id: "txtQuantity",
                                    type: "text",
                                    name: "txt-quantity",
                                    value: quantityBuy,
                                    className: "txtQuantity"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 489,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnMore",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-plus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 490,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 490,
                                    columnNumber: 47
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 487,
                                  columnNumber: 44
                                }, this)]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 485,
                                columnNumber: 41
                              }, this), soldOutStock != null && !soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton btn_buy",
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fas fa fa-shopping-cart"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 497,
                                    columnNumber: 52
                                  }, this), " ", 'Comprar']
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 496,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 495,
                                columnNumber: 43
                              }, this) : soldOutStock != null && soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton notifyButton",
                                  "data-productid": dadosProduto.product_id,
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-envelope",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 503,
                                    columnNumber: 52
                                  }, this), " Avise-me"]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 502,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 501,
                                columnNumber: 43
                              }, this) : null]
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 484,
                              columnNumber: 39
                            }, this), dadosProduto.text_prevenda != null && dadosProduto.text_prevenda != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                className: "checkPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                  id: "ckprevenda",
                                  type: "checkbox",
                                  onChange: ""
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 513,
                                  columnNumber: 70
                                }, this), " Concordo com o prazo de entrega descrito abaixo."]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 513,
                                columnNumber: 41
                              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "infoPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
                                  className: "tit",
                                  children: "TERMO DE ACEITA\xC7\xC3O"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 515,
                                  columnNumber: 43
                                }, this), dadosProduto.text_prevenda]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 514,
                                columnNumber: 41
                              }, this)]
                            }, void 0, true) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 414,
                            columnNumber: 35
                          }, this), dadosProduto.guia_medidas ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "guiasMedida",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              children: dadosProduto.guia_medidas.map(function (itemGuia, keyGuia) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                  className: "itemGuia",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    className: "color2",
                                    title: itemGuia.title,
                                    "data-tituloguia": "",
                                    "data-conteudoguia": "",
                                    onClick: "",
                                    children: itemGuia.title
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 531,
                                    columnNumber: 47
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 530,
                                  columnNumber: 43
                                }, _this);
                              })
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 527,
                              columnNumber: 39
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 526,
                            columnNumber: 37
                          }, this) : null]
                        }, void 0, true, {
                          fileName: _jsxFileName,
                          lineNumber: 385,
                          columnNumber: 33
                        }, this)
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 384,
                        columnNumber: 29
                      }, this)]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 204,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 203,
                    columnNumber: 23
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 202,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 193,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 192,
              columnNumber: 17
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 191,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 190,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateFooter, {
          configs: configs.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 556,
          columnNumber: 13
        }, this)]
      }, void 0, true)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 177,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 176,
    columnNumber: 5
  }, this);
}

_s(Product, "/0s/ODYuaimeRDvt5WaYiIqqOgw=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"]];
});

_c5 = Product;
;

var _c, _c2, _c3, _c4, _c5;

$RefreshReg$(_c, "TemplateHeader$dynamic");
$RefreshReg$(_c2, "TemplateHeader");
$RefreshReg$(_c3, "TemplateFooter$dynamic");
$RefreshReg$(_c4, "TemplateFooter");
$RefreshReg$(_c5, "Product");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvW3NsdWddLmpzIl0sIm5hbWVzIjpbImlkU3RvcmVBcHAiLCJUZW1wbGF0ZUhlYWRlciIsImR5bmFtaWMiLCJUZW1wbGF0ZUZvb3RlciIsInNsaWRlclRodW1icyIsImluZmluaXRlIiwidmVydGljYWwiLCJzbGlkZXNUb1Nob3ciLCJzbGlkZXNUb1Njcm9sbCIsInNwZWVkIiwicmVzcG9uc2l2ZSIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsInNldHRpbmdzTW9iaWxlU2xpZGUiLCJkb3RzIiwiUHJvZHVjdCIsImNvbmZpZ3MiLCJtYWluTWVudSIsImRhZG9zUHJvZHV0byIsImluaXRpYWxJbWFnZXMiLCJnZXRJbml0aWFsSW1hZ2UiLCJpbWdJbmljaWFsIiwidGVtcEltZ0luaWNpYWwiLCJpIiwiaW1hZ2VzIiwicHVzaCIsImluaXRpYWxLZXkiLCJpbWFnZSIsInN1YnN0cmluZyIsImxhc3RJbmRleE9mIiwidW5kZWZpbmVkIiwic2hvdyIsInBvcHVwIiwidGh1bWIiLCJzZWxlY3RlZFF0ZFBhcmNlbCIsInNlbGVjdGVkVmFsUGFyY2VsIiwicXVhbnRpdHlCdXkiLCJzaGlwcGluZ01ldGhvZHMiLCJzZWxlY3RlZE9wdGlvbkJ1eSIsInVzZVN0YXRlIiwic3BlY2lhbFZhbHVlIiwic2V0U3BlY2lhbFZhbHVlIiwibWFpblZhbHVlIiwic2V0TWFpblZhbHVlIiwidXNlRWZmZWN0IiwiZ2V0TWFpblZhbHVlIiwiaW5pdGlhbE1haW5WYWx1ZSIsInBhcnNlSW50IiwiaGFzX29wdGlvbiIsInNwZWNpYWwiLCJwcmljZSIsImdldFNwZWNpYWxWYWx1ZSIsImluaXRpYWxTcGVjaWFsVmFsdWUiLCJpbml0aWFsU3RhdGUiLCJzb2xkT3V0U3RvY2siLCJzZXRTb2xkT3V0U3RvY2siLCJjaGFuZ2Vab29tIiwiZXYiLCJpbWdTaG93IiwiY3VycmVudFRhcmdldCIsImRhdGFzZXQiLCJzcmNzaG93IiwiaW1nUG9wdXAiLCJzcmNwb3B1cCIsInRlbXBJbWFnZSIsInJvdXRlciIsInVzZVJvdXRlciIsImlzRmFsbGJhY2siLCJwcm9kdWN0X2lkIiwibmFtZSIsInJlc3Bvc3RhIiwiT2JqZWN0Iiwia2V5cyIsImxlbmd0aCIsIm1hcCIsIml0ZW0iLCJrZXkiLCJ2aWRlb3MiLCJpdGVtVmlkZW8iLCJrZXlWaWRlbyIsImlkX3ZpZGVvIiwibGFiZWxzIiwicHJvbW9fdG9wX2xlZnQiLCJwcm9tb190b3BfcmlnaHQiLCJwcm9tb19ib3R0b21fbGVmdCIsInByb21vX2JvdHRvbV9yaWdodCIsInNtYWxsSW1hZ2UiLCJhbHQiLCJpc0ZsdWlkV2lkdGgiLCJzcmMiLCJsYXJnZUltYWdlIiwid2lkdGgiLCJ3aWR0aFpvb20iLCJoZWlnaHQiLCJtb2RlbCIsIl9faHRtbCIsImxhbGFsYSIsImRpc2NvdW50X3BlcmNlbnQiLCJvcHRpb25zIiwidHlwZSIsInByb2R1Y3Rfb3B0aW9uX2lkIiwib3B0aW9uX3ZhbHVlIiwiaXRlbU9wdGlvbiIsInByb2R1Y3Rfb3B0aW9uX3ZhbHVlX2lkIiwidGV4dF9wcmV2ZW5kYSIsImd1aWFfbWVkaWRhcyIsIml0ZW1HdWlhIiwia2V5R3VpYSIsInRpdGxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFHQSxJQUFJQSxVQUFVLEdBQUcsbUJBQWpCO0FBQ0EsSUFBSUMsY0FBYyxHQUFHQyxtREFBTyxNQUFDO0FBQUEsU0FBTSw4RkFBTyxJQUF5QixHQUFDRixVQUExQixHQUFxQyxvQkFBNUMsQ0FBTjtBQUFBLENBQUQ7QUFBQTtBQUFBO0FBQUEsa0NBQWMsMEdBQXlCLEdBQUNBLFVBQTFCLEdBQXFDLG9CQUFuRDtBQUFBO0FBQUEsY0FBYyw0QkFBMEJBLFVBQTFCLEdBQXFDLG9CQUFuRDtBQUFBO0FBQUEsRUFBNUI7TUFBSUMsYztBQUNKLElBQUlFLGNBQWMsR0FBR0QsbURBQU8sT0FBQztBQUFBLFNBQU0sOEZBQU8sSUFBeUIsR0FBQ0YsVUFBMUIsR0FBcUMsb0JBQTVDLENBQU47QUFBQSxDQUFEO0FBQUE7QUFBQTtBQUFBLGtDQUFjLDBHQUF5QixHQUFDQSxVQUExQixHQUFxQyxvQkFBbkQ7QUFBQTtBQUFBLGNBQWMsNEJBQTBCQSxVQUExQixHQUFxQyxvQkFBbkQ7QUFBQTtBQUFBLEVBQTVCO01BQUlHLGM7QUFFSixJQUFJQyxZQUFZLEdBQUc7QUFDakJDLFVBQVEsRUFBRSxLQURPO0FBRWpCQyxVQUFRLEVBQUUsSUFGTztBQUdqQkMsY0FBWSxFQUFFLENBSEc7QUFJakJDLGdCQUFjLEVBQUUsQ0FKQztBQUtqQkMsT0FBSyxFQUFFLEdBTFU7QUFNakJDLFlBQVUsRUFBRSxDQUNWO0FBQ0VDLGNBQVUsRUFBRSxHQURkO0FBRUVDLFlBQVEsRUFBRTtBQUNSTixjQUFRLEVBQUUsS0FERjtBQUVSQyxrQkFBWSxFQUFFO0FBRk47QUFGWixHQURVO0FBTkssQ0FBbkI7QUFpQkEsSUFBSU0sbUJBQW1CLEdBQUc7QUFDeEJOLGNBQVksRUFBRSxDQURVO0FBRXhCQyxnQkFBYyxFQUFFLENBRlE7QUFHeEJDLE9BQUssRUFBRSxHQUhpQjtBQUl4QkssTUFBSSxFQUFFO0FBSmtCLENBQTFCOztBQU9lLFNBQVNDLE9BQVQsT0FBc0Q7QUFBQTs7QUFBQTs7QUFBQSxNQUFuQ0MsT0FBbUMsUUFBbkNBLE9BQW1DO0FBQUEsTUFBMUJDLFFBQTBCLFFBQTFCQSxRQUEwQjtBQUFBLE1BQWhCQyxZQUFnQixRQUFoQkEsWUFBZ0I7QUFDbkUsTUFBSUMsYUFBYSxHQUFHLEVBQXBCOztBQUNBLFdBQVNDLGVBQVQsQ0FBeUJGLFlBQXpCLEVBQXVDO0FBRXJDLFFBQUlHLFVBQVUsR0FBRyxFQUFqQjtBQUNBLFFBQUlDLGNBQWMsR0FBRyxFQUFyQjs7QUFFQSxTQUFJLElBQUlDLENBQVIsSUFBYUwsWUFBWSxDQUFDTSxNQUExQixFQUFpQztBQUMvQkYsb0JBQWMsQ0FBQ0csSUFBZixDQUFvQixDQUFDRixDQUFELEVBQUlMLFlBQVksQ0FBQ00sTUFBYixDQUFxQkQsQ0FBckIsQ0FBSixDQUFwQjtBQUNEOztBQUVELFFBQUlHLFVBQVUsR0FBR1IsWUFBWSxDQUFDUyxLQUFiLENBQW1CQyxTQUFuQixDQUE2QlYsWUFBWSxDQUFDUyxLQUFiLENBQW1CRSxXQUFuQixDQUErQixHQUEvQixJQUFvQyxDQUFqRSxDQUFqQjs7QUFFQSxRQUFHWCxZQUFZLENBQUNNLE1BQWIsSUFBdUJNLFNBQXZCLElBQW9DWixZQUFZLENBQUNNLE1BQWIsQ0FBb0JFLFVBQXBCLEtBQW1DLElBQTFFLEVBQStFO0FBQzdFTCxnQkFBVSxDQUFDLE1BQUQsQ0FBVixHQUFxQkgsWUFBWSxDQUFDTSxNQUFiLENBQW9CRSxVQUFwQixFQUFnQ0ssSUFBckQ7QUFDQVYsZ0JBQVUsQ0FBQyxPQUFELENBQVYsR0FBc0JILFlBQVksQ0FBQ00sTUFBYixDQUFvQkUsVUFBcEIsRUFBZ0NNLEtBQXREO0FBQ0FYLGdCQUFVLENBQUMsT0FBRCxDQUFWLEdBQXNCSCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JFLFVBQXBCLEVBQWdDTyxLQUF0RDtBQUNELEtBSkQsTUFJTSxJQUFHWCxjQUFjLENBQUMsQ0FBRCxDQUFkLElBQXFCLElBQXhCLEVBQTZCO0FBQ2pDRCxnQkFBVSxHQUFHQyxjQUFjLENBQUMsQ0FBRCxDQUFkLENBQWtCLENBQWxCLENBQWI7QUFDRDs7QUFFRCxXQUFPRCxVQUFQO0FBQ0Q7O0FBQ0QsTUFBR0gsWUFBSCxFQUFnQjtBQUNkQyxpQkFBYSxHQUFHQyxlQUFlLENBQUNGLFlBQUQsQ0FBL0I7QUFDRDs7QUFFRCxNQUFJZ0IsaUJBQWlCLEdBQUcsQ0FBeEI7QUFDQSxNQUFJQyxpQkFBaUIsR0FBRyxDQUF4QjtBQUNBLE1BQUlDLFdBQVcsR0FBRyxDQUFsQjtBQUNBLE1BQUlDLGVBQWUsR0FBRyxJQUF0QjtBQUNBLE1BQUlDLGlCQUFpQixHQUFHLENBQXhCOztBQS9CbUUsa0JBaUMzQkMsc0RBQVEsQ0FBQyxJQUFELENBakNtQjtBQUFBLE1BaUM1REMsWUFqQzREO0FBQUEsTUFpQzlDQyxlQWpDOEM7O0FBQUEsbUJBa0NuQ0Ysc0RBQVEsQ0FBQyxJQUFELENBbEMyQjtBQUFBLE1Ba0M5REcsU0FsQzhEO0FBQUEsTUFrQ25EQyxZQWxDbUQ7O0FBbUNuRUMseURBQVMsQ0FBQyxZQUFNO0FBQ2QsYUFBU0MsWUFBVCxDQUFzQjNCLFlBQXRCLEVBQW9DO0FBQ2xDLFVBQUk0QixnQkFBZ0IsR0FBRyxJQUF2Qjs7QUFDQSxVQUFHNUIsWUFBWSxJQUFJWSxTQUFuQixFQUE2QjtBQUMzQixZQUFHaUIsUUFBUSxDQUFDN0IsWUFBWSxDQUFDOEIsVUFBZCxDQUFSLElBQXFDLENBQXhDLEVBQTBDO0FBQUU7QUFDMUMsY0FBRzlCLFlBQVksQ0FBQytCLE9BQWhCLEVBQXdCO0FBQ3RCSCw0QkFBZ0IsR0FBRzVCLFlBQVksQ0FBQ2dDLEtBQWhDO0FBQ0QsV0FGRCxNQUVLO0FBQ0hKLDRCQUFnQixHQUFHNUIsWUFBWSxDQUFDZ0MsS0FBaEM7QUFDRDtBQUNGLFNBTkQsTUFNSztBQUNILGNBQUdKLGdCQUFnQixJQUFJLElBQXBCLElBQTRCNUIsWUFBWSxDQUFDZ0MsS0FBNUMsRUFBa0Q7QUFDaERKLDRCQUFnQixHQUFHNUIsWUFBWSxDQUFDZ0MsS0FBaEM7QUFDRDtBQUNGOztBQUNEUCxvQkFBWSxDQUFDRyxnQkFBRCxDQUFaO0FBQ0Q7QUFDRjs7QUFFRCxhQUFTSyxlQUFULENBQXlCakMsWUFBekIsRUFBdUM7QUFDckMsVUFBSWtDLG1CQUFtQixHQUFHLElBQTFCOztBQUNBLFVBQUdsQyxZQUFZLElBQUlZLFNBQW5CLEVBQTZCO0FBQzNCLFlBQUdpQixRQUFRLENBQUM3QixZQUFZLENBQUM4QixVQUFkLENBQVIsSUFBcUMsQ0FBeEMsRUFBMEM7QUFBRTtBQUMxQyxjQUFHOUIsWUFBWSxDQUFDK0IsT0FBaEIsRUFBd0I7QUFDdEJHLCtCQUFtQixHQUFHbEMsWUFBWSxDQUFDK0IsT0FBbkM7QUFDRCxXQUZELE1BRUs7QUFDSEcsK0JBQW1CLEdBQUdsQyxZQUFZLENBQUNnQyxLQUFuQztBQUNEO0FBQ0YsU0FORCxNQU1LO0FBQ0gsY0FBR0UsbUJBQW1CLElBQUksSUFBdkIsSUFBK0JsQyxZQUFZLENBQUMrQixPQUEvQyxFQUF1RDtBQUNyREcsK0JBQW1CLEdBQUdsQyxZQUFZLENBQUMrQixPQUFuQztBQUNEO0FBQ0Y7QUFDRjs7QUFDRFIscUJBQWUsQ0FBQ1csbUJBQUQsQ0FBZjtBQUNEOztBQUVERCxtQkFBZSxDQUFDakMsWUFBRCxDQUFmO0FBQ0EyQixnQkFBWSxDQUFDM0IsWUFBRCxDQUFaO0FBQ0QsR0F2Q1EsQ0FBVDs7QUFuQ21FLG1CQTRFM0JxQixzREFBUSxDQUFDLFlBQU07QUFDckQsUUFBTWMsWUFBWSxHQUFHLENBQXJCO0FBQ0EsV0FBT0EsWUFBUDtBQUNELEdBSCtDLENBNUVtQjtBQUFBLE1BNEU1REMsWUE1RTREO0FBQUEsTUE0RTlDQyxlQTVFOEM7QUFrRm5FO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBSU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUUsTUFBSUMsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ0MsRUFBRCxFQUFRO0FBQ3ZCLFFBQU1DLE9BQU8sR0FBR0QsRUFBRSxDQUFDRSxhQUFILENBQWlCQyxPQUFqQixDQUF5QkMsT0FBekM7QUFDQSxRQUFNQyxRQUFRLEdBQUdMLEVBQUUsQ0FBQ0UsYUFBSCxDQUFpQkMsT0FBakIsQ0FBeUJHLFFBQTFDO0FBQ0EsUUFBSUMsU0FBUyxHQUFHLEVBQWhCO0FBQ0FBLGFBQVMsQ0FBQyxNQUFELENBQVQsR0FBb0JOLE9BQXBCO0FBQ0FNLGFBQVMsQ0FBQyxPQUFELENBQVQsR0FBcUJGLFFBQXJCLENBTHVCLENBTXZCO0FBRUQsR0FSRDs7QUFVQSxNQUFNRyxNQUFNLEdBQUdDLDZEQUFTLEVBQXhCOztBQUNBLE1BQUksQ0FBQ0QsTUFBTSxDQUFDRSxVQUFSLElBQXNCLEVBQUNqRCxZQUFELGFBQUNBLFlBQUQsZUFBQ0EsWUFBWSxDQUFFa0QsVUFBZixDQUExQixFQUFxRDtBQUNuRCx3QkFBTyxxRUFBQyxpREFBRDtBQUFXLGdCQUFVLEVBQUU7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUFQO0FBQ0Q7O0FBQ0Qsc0JBQ0UscUVBQUMsMkRBQUQ7QUFBQSwyQkFDRSxxRUFBQyw2REFBRDtBQUFBLGdCQUdFSCxNQUFNLENBQUNFLFVBQVAsZ0JBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixnQkFHRTtBQUFBLGdDQUNFLHFFQUFDLGdEQUFEO0FBQUEsa0NBQ0U7QUFBQSxzQkFBUWpELFlBQVksQ0FBQ21EO0FBQXJCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREYsZUFFRTtBQUFNLGdCQUFJLEVBQUMsYUFBWDtBQUF5QixtQkFBTyxFQUFDO0FBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRkYsZUFHRTtBQUFNLGdCQUFJLEVBQUMsVUFBWDtBQUFzQixtQkFBTyxFQUFDO0FBQTlCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBSEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBTUUscUVBQUMsY0FBRDtBQUFnQixpQkFBTyxFQUFFckQsT0FBTyxDQUFDc0QsUUFBakM7QUFBMkMsa0JBQVEsRUFBRXJELFFBQVEsQ0FBQ3FEO0FBQTlEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBTkYsZUFPRTtBQUFLLG1CQUFTLEVBQUMsY0FBZjtBQUFBLGlDQUNFO0FBQUsscUJBQVMsRUFBQyxlQUFmO0FBQUEsbUNBQ0U7QUFBSyx1QkFBUyxFQUFDLHdEQUFmO0FBQUEscUNBQ0U7QUFBSyx5QkFBUyxFQUFDLFdBQWY7QUFBQSx3Q0FDRTtBQUFJLDJCQUFTLEVBQUMsWUFBZDtBQUFBLDBDQUNHO0FBQUksNkJBQVMsRUFBQyxpQkFBZDtBQUFBLDJDQUNHO0FBQUcsMEJBQUksRUFBQyxHQUFSO0FBQVksMkJBQUssRUFBQyxtQkFBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESDtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQURILGVBSUc7QUFBSSw2QkFBUyxFQUFDLGlCQUFkO0FBQUEsOEJBQ0lwRCxZQUFZLENBQUNtRDtBQURqQjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQUpIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFERixlQVNFO0FBQUssMkJBQVMsRUFBQyxhQUFmO0FBQUEseUNBQ0U7QUFBSyw2QkFBUyxFQUFDLFdBQWY7QUFBQSwyQ0FDSTtBQUFLLCtCQUFTLEVBQUMsZUFBZjtBQUFBLDhDQUVFO0FBQUssaUNBQVMsRUFBQyxrQkFBZjtBQUFBLGdEQUNFO0FBQUssbUNBQVMsRUFBQyxxQkFBZjtBQUFBLGlEQUNDO0FBQUsscUNBQVMsRUFBQywwQkFBZjtBQUFBLHNDQUVJbkQsWUFBWSxDQUFDTSxNQUFkLEdBQ0crQyxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNpRCxNQUFqQyxHQUEwQyxDQUEzQyxnQkFDRSxxRUFBQyxrREFBRDtBQUFRLHVDQUFTLEVBQUM7QUFBbEIsK0JBQXFDckUsWUFBckM7QUFBQSx5Q0FHTW1FLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdEQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ2tELEdBQWpDLENBQXFDLFVBQUNDLElBQUQsRUFBT0MsR0FBUDtBQUFBLG9EQUM3QjtBQUNFLDJDQUFTLEVBQUMsNkVBRFo7QUFBQSx5REFHSTtBQUFLLHVDQUFHLEVBQUUxRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjNDLEtBQXBDO0FBQ0UsdUNBQUcsRUFBRWQsWUFBWSxDQUFDbUQsSUFEcEI7QUFFRSxvREFBY25ELFlBQVksQ0FBQ00sTUFBYixDQUFvQm1ELElBQXBCLEVBQTBCNUMsSUFGMUM7QUFHRSxxREFBZWIsWUFBWSxDQUFDTSxNQUFiLENBQW9CbUQsSUFBcEIsRUFBMEIzQyxLQUgzQztBQUlFLHlDQUFLLEVBQUMsSUFKUjtBQUlhLDBDQUFNLEVBQUMsSUFKcEI7QUFLRSwyQ0FBTyxFQUFDO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRDZCO0FBQUEsK0JBQXJDLENBSE4sRUFpQktkLFlBQVksQ0FBQzJELE1BQWIsQ0FBb0JKLE1BQXJCLEdBQ0V2RCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSCxHQUFwQixDQUF3QixVQUFDSSxTQUFELEVBQVlDLFFBQVo7QUFBQSxvREFDdEI7QUFBSywyQ0FBUyxFQUFDLFlBQWY7QUFBQSx5REFDRTtBQUFHLHdDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBTyxFQUFDLEVBQS9CO0FBQWtDLG9EQUFjRCxTQUFTLENBQUNFLFFBQTFEO0FBQUEsNERBQ0U7QUFBSyx5Q0FBRyx1Q0FBZ0NGLFNBQVMsQ0FBQ0UsUUFBMUM7QUFBUjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQURGLGVBRUU7QUFBRywrQ0FBUyxFQUFDLG9CQUFiO0FBQWtDLHFEQUFZO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREYsbUNBQWlDRCxRQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQURzQjtBQUFBLCtCQUF4QixDQURGLEdBVUUsSUEzQk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLGdCQWlDRTtBQUFBLHlDQUVFUixNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNrRCxHQUFqQyxDQUFxQyxVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxvREFDN0I7QUFDRSwyQ0FBUyxFQUFDLDZFQURaO0FBQUEseURBR0k7QUFBSyx1Q0FBRyxFQUFFMUQsWUFBWSxDQUFDTSxNQUFiLENBQW9CbUQsSUFBcEIsRUFBMEIzQyxLQUFwQztBQUNFLHVDQUFHLEVBQUVkLFlBQVksQ0FBQ21ELElBRHBCO0FBRUUsb0RBQWNuRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjVDLElBRjFDO0FBR0UscURBQWViLFlBQVksQ0FBQ00sTUFBYixDQUFvQm1ELElBQXBCLEVBQTBCM0MsS0FIM0M7QUFJRSx5Q0FBSyxFQUFDLElBSlI7QUFJYSwwQ0FBTSxFQUFDLElBSnBCO0FBS0UsMkNBQU8sRUFBQztBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFISjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQUQ2QjtBQUFBLCtCQUFyQyxDQUZGLEVBaUJHZCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSixNQUFyQixHQUNFdkQsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkgsR0FBcEIsQ0FBd0IsVUFBQ0ksU0FBRCxFQUFZQyxRQUFaO0FBQUEsb0RBQ3RCO0FBQUssMkNBQVMsRUFBQyxZQUFmO0FBQUEseURBQ0U7QUFBRyx3Q0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQU8sRUFBQyxFQUEvQjtBQUFrQyxvREFBY0QsU0FBUyxDQUFDRSxRQUExRDtBQUFBLDREQUNFO0FBQUsseUNBQUcsdUNBQWdDRixTQUFTLENBQUNFLFFBQTFDO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FERixlQUVFO0FBQUcsK0NBQVMsRUFBQyxvQkFBYjtBQUFrQyxxREFBWTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLG1DQUFpQ0QsUUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FEc0I7QUFBQSwrQkFBeEIsQ0FERixHQVVFLElBM0JKO0FBQUEsNENBbENKLEdBZ0VFO0FBbEVMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdDQURGLGVBeUVJO0FBQUssbUNBQVMsRUFBQyxnQ0FBZjtBQUFBLGlEQUNFO0FBQUsscUNBQVMsRUFBQyxVQUFmO0FBQUEsdUNBRUs3RCxZQUFZLENBQUMrRCxNQUFiLENBQW9CQyxjQUFwQixDQUFtQ3ZELEtBQW5DLElBQTRDLElBQTdDLGdCQUNFO0FBQUssdUNBQVMsRUFBQyx3QkFBZjtBQUFBLHFEQUNFO0FBQUssbUNBQUcsRUFBRVQsWUFBWSxDQUFDK0QsTUFBYixDQUFvQkMsY0FBcEIsQ0FBbUN2RDtBQUE3QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixHQUtFLElBUE4sRUFVS1QsWUFBWSxDQUFDK0QsTUFBYixDQUFvQkUsZUFBcEIsQ0FBb0N4RCxLQUFwQyxJQUE2QyxJQUE5QyxnQkFDRTtBQUFLLHVDQUFTLEVBQUMseUJBQWY7QUFBQSxxREFDRTtBQUFLLG1DQUFHLEVBQUVULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JFLGVBQXBCLENBQW9DeEQ7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREYsR0FLRSxJQWZOLEVBa0JLVCxZQUFZLENBQUMrRCxNQUFiLENBQW9CRyxpQkFBcEIsQ0FBc0N6RCxLQUF0QyxJQUErQyxJQUFoRCxnQkFDRTtBQUFLLHVDQUFTLEVBQUMsMkJBQWY7QUFBQSxxREFDRTtBQUFLLG1DQUFHLEVBQUVULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JHLGlCQUFwQixDQUFzQ3pEO0FBQWhEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLEdBS0UsSUF2Qk4sRUEwQktULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JJLGtCQUFwQixDQUF1QzFELEtBQXZDLElBQWdELElBQWpELGdCQUNFO0FBQUssdUNBQVMsRUFBQyw0QkFBZjtBQUFBLHFEQUNFO0FBQUssbUNBQUcsRUFBRVQsWUFBWSxDQUFDK0QsTUFBYixDQUFvQkksa0JBQXBCLENBQXVDMUQ7QUFBakQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREYsR0FLRSxJQS9CTixlQWtDRSxxRUFBQywyREFBRCxvQkFBdUI7QUFDZjJELHdDQUFVLEVBQUU7QUFDWkMsbUNBQUcsRUFBRXJFLFlBQVksQ0FBQ21ELElBRE47QUFFWm1CLDRDQUFZLEVBQUUsSUFGRjtBQUdaQyxtQ0FBRyxFQUFFdEUsYUFBYSxDQUFDWTtBQUhQLCtCQURHO0FBTW5CMkQsd0NBQVUsRUFBRTtBQUNSRCxtQ0FBRyxFQUFFdEUsYUFBYSxDQUFDYSxLQURYO0FBRVIyRCxxQ0FBSyxFQUFFM0UsT0FBTyxDQUFDNEUsU0FGUDtBQUdSQyxzQ0FBTSxFQUFFN0UsT0FBTyxDQUFDNEU7QUFIUjtBQU5PLDZCQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQWxDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdDQXpFSixlQTRIRTtBQUFLLG1DQUFTLEVBQUMsaUJBQWY7QUFBQSxvQ0FDSXJCLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdEQsWUFBWSxDQUFDTSxNQUF6QixLQUFvQytDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdEQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ2lELE1BQWpDLEdBQTBDLENBQS9FLGdCQUNDO0FBQUsscUNBQVMsRUFBQyxnQkFBZjtBQUFBLG1EQUNFLHFFQUFDLGtEQUFEO0FBQVEsdUNBQVMsRUFBQztBQUFsQiwrQkFBcUM1RCxtQkFBckM7QUFBQSx5Q0FFTTBELE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdEQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ2tELEdBQWpDLENBQXFDLFVBQUNDLElBQUQsRUFBT0MsR0FBUDtBQUFBLG9EQUNqQztBQUFLLDJDQUFTLEVBQUMsVUFBZjtBQUFBLHlEQUNFO0FBQUssdUNBQUcsRUFBRTFELFlBQVksQ0FBQ00sTUFBYixDQUFvQm1ELElBQXBCLEVBQTBCM0MsS0FBcEM7QUFBMkMsdUNBQUcsRUFBRWQsWUFBWSxDQUFDbUQ7QUFBN0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRGlDO0FBQUEsK0JBQXJDLENBRk4sRUFTS25ELFlBQVksQ0FBQzJELE1BQWIsQ0FBb0JKLE1BQXJCLEdBQ0V2RCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSCxHQUFwQixDQUF3QixVQUFDSSxTQUFELEVBQVlDLFFBQVo7QUFBQSxvREFDdEI7QUFBSywyQ0FBUyxFQUFDLHFCQUFmO0FBQUEseURBQ0U7QUFBRyx3Q0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQU8sRUFBQyxFQUEvQjtBQUFrQyxvREFBY0QsU0FBUyxDQUFDRSxRQUExRDtBQUFBLDREQUNFO0FBQUsseUNBQUcsdUNBQWdDRixTQUFTLENBQUNFLFFBQTFDO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FERixlQUVFO0FBQUcsK0NBQVMsRUFBQyxvQkFBYjtBQUFrQyxxREFBWTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLG1DQUEwQ0QsUUFBMUM7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FEc0I7QUFBQSwrQkFBeEIsQ0FERixHQVVFLElBbkJOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBREQsR0F5QkVSLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdEQsWUFBWSxDQUFDTSxNQUF6QixLQUFvQytDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdEQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ2lELE1BQWpDLEdBQTBDLENBQS9FLGdCQUNFO0FBQUEsdUNBRUlGLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdEQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ2tELEdBQWpDLENBQXFDLFVBQUNDLElBQUQsRUFBT0MsR0FBUDtBQUFBLGtEQUNuQztBQUFLLHlDQUFTLEVBQUMsVUFBZjtBQUFBLHVEQUNFO0FBQUsscUNBQUcsRUFBRTFELFlBQVksQ0FBQ00sTUFBYixDQUFvQm1ELElBQXBCLEVBQTBCM0MsS0FBcEM7QUFBMkMscUNBQUcsRUFBRWQsWUFBWSxDQUFDbUQ7QUFBN0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUNBRG1DO0FBQUEsNkJBQXJDLENBRkosRUFTS25ELFlBQVksQ0FBQzJELE1BQWIsQ0FBb0JKLE1BQXJCLEdBQ0V2RCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSCxHQUFwQixDQUF3QixVQUFDSSxTQUFELEVBQVlDLFFBQVo7QUFBQSxrREFDdEI7QUFBSyx5Q0FBUyxFQUFDLHFCQUFmO0FBQUEsdURBQ0U7QUFBRyxzQ0FBSSxFQUFDLGNBQVI7QUFBdUIseUNBQU8sRUFBQyxFQUEvQjtBQUFrQyxrREFBY0QsU0FBUyxDQUFDRSxRQUExRDtBQUFBLDBEQUNFO0FBQUssdUNBQUcsdUNBQWdDRixTQUFTLENBQUNFLFFBQTFDO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQ0FERixlQUVFO0FBQUcsNkNBQVMsRUFBQyxvQkFBYjtBQUFrQyxtREFBWTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDJDQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLGlDQUEwQ0QsUUFBMUM7QUFBQTtBQUFBO0FBQUE7QUFBQSx1Q0FEc0I7QUFBQSw2QkFBeEIsQ0FERixHQVVFLElBbkJOO0FBQUEsMENBREYsR0F3QkE7QUFsREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0E1SEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDhCQUZGLGVBb0xFO0FBQUssaUNBQVMsRUFBQyxlQUFmO0FBQUEsK0NBQ0k7QUFBSyxtQ0FBUyxFQUFDLHlEQUFmO0FBQUEsa0RBQ0U7QUFBSyxxQ0FBUyxFQUFDLGlCQUFmO0FBQUEsb0RBQ0c7QUFBSSx1Q0FBUyxFQUFDLGdCQUFkO0FBQUEsd0NBQWdDN0QsWUFBWSxDQUFDbUQ7QUFBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FESCxFQUdNbkQsWUFBWSxDQUFDNEUsS0FBZCxnQkFDQztBQUFJLHVDQUFTLEVBQUMsY0FBZDtBQUFBLHFEQUNJO0FBQUEsd0RBQUk7QUFBTSwyQ0FBUyxFQUFDLFdBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQUFKLE9BQTZDNUUsWUFBWSxDQUFDNEUsS0FBMUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERCxHQUtDLElBUk4sZUFXRztBQUFLLHVDQUFTLEVBQUMsU0FBZjtBQUFBLHFEQUNHO0FBQUcseUNBQVMsRUFBQyxZQUFiO0FBQTBCLHVDQUFPLEVBQUMsRUFBbEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESDtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQVhILEVBZ0JNNUUsWUFBWSxTQUFaLElBQXNCLEVBQXZCLGdCQUNDO0FBQUssdUNBQVMsRUFBQyxXQUFmO0FBQUEscURBQ0M7QUFBSyx5Q0FBUyxFQUFDLGVBQWY7QUFBQSx1REFDQztBQUFLLHlEQUF1QixFQUFFO0FBQUU2RSwwQ0FBTSxFQUFFN0UsWUFBWTtBQUF0QjtBQUE5QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREQsR0FPQyxJQXZCTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBREYsZUE2QkU7QUFBSyxxQ0FBUyxFQUFDLFNBQWY7QUFBQSxvREFFRTtBQUFLLHVDQUFTLEVBQUMsWUFBZjtBQUFBLHNEQUNFO0FBQUsseUNBQVMsRUFBQyxlQUFmO0FBQUEsMkNBQ004RSxNQUFELGdCQUNDO0FBQUssMkNBQVMsRUFBQyxVQUFmO0FBQUEsaUVBQ0s7QUFBTSw2Q0FBUyxFQUFDLG9CQUFoQjtBQUFBLDhDQUFzQ3REO0FBQXRDO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBREw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQURELEdBS0MsRUFOTixlQVFJO0FBQUssMkNBQVMsRUFBQyxzQkFBZjtBQUFBLDRDQUNJc0QsTUFBRCxnQkFDQztBQUFBLG9FQUNNO0FBQU0sK0NBQVMsRUFBQyxjQUFoQjtBQUFBLGdEQUFnQ0E7QUFBaEM7QUFBQTtBQUFBO0FBQUE7QUFBQSw0Q0FETixlQUVDO0FBQU0sK0NBQVMsRUFBQyxlQUFoQjtBQUFBLGdEQUFpQzlFLFlBQVksQ0FBQytFO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsNENBRkQ7QUFBQSxrREFERCxnQkFNQztBQUFBLG9FQUFPO0FBQUEsZ0RBQU92RDtBQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUEsNENBQVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEo7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FSSixFQW1CTVIsaUJBQWlCLElBQUksRUFBckIsSUFBMkJDLGlCQUFpQixJQUFJLEVBQWpELGdCQUNDO0FBQUcsMkNBQVMsRUFBQyxnQkFBYjtBQUFBLGlFQUFpQztBQUFNLDZDQUFTLEVBQUMsU0FBaEI7QUFBQSwrQ0FBMkJELGlCQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBQWpDLHVCQUEwRjtBQUFNLDZDQUFTLEVBQUMsU0FBaEI7QUFBQSw4Q0FBMkJDO0FBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBQTFGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FERCxHQUdDLElBdEJOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERixFQTRCS2pCLFlBQVksSUFBSSxFQUFqQixHQUNHNkIsUUFBUSxDQUFDN0IsWUFBWSxDQUFDOEIsVUFBZCxDQUFSLElBQXFDLENBQXRDLGdCQUNDO0FBQUsseUNBQVMsRUFBQyxhQUFmO0FBQUEsMENBRUs5QixZQUFZLENBQUNnRixPQUFiLENBQXFCeEIsR0FBckIsQ0FBeUIsVUFBQ0MsSUFBRCxFQUFPQyxHQUFQO0FBQUEsc0RBQ3ZCO0FBQUssNkNBQVMsRUFBQyxZQUFmO0FBQUEsNERBQ0U7QUFBQSxnREFBUUQsSUFBSSxDQUFDTjtBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREYsRUFHSU0sSUFBSSxDQUFDd0IsSUFBTCxJQUFhLE1BQWQsZ0JBQ0M7QUFBSywrQ0FBUyxFQUFDLFdBQWY7QUFBQSw4REFDRTtBQUFPLDBDQUFFLEVBQUMsYUFBVjtBQUF3QixpREFBUyxFQUFDLEdBQWxDO0FBQXNDLDRDQUFJLEVBQUMsTUFBM0M7QUFBa0Qsc0RBQVl4QixJQUFJLENBQUN5QixpQkFBbkU7QUFBc0YsaURBQVMsRUFBQyxPQUFoRztBQUF3Ryw0Q0FBSSxFQUFDO0FBQTdHO0FBQUE7QUFBQTtBQUFBO0FBQUEsK0NBREYsZUFFRTtBQUFLLGlEQUFTLEVBQUMsTUFBZjtBQUFBLGdFQUNJO0FBQUcsbURBQVMsRUFBQyw4QkFBYjtBQUE0Qyx5REFBWTtBQUF4RDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlEQURKLGVBR0k7QUFBSyxtREFBUyxFQUFDLFVBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaURBSEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLCtDQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FERCxnQkFZQztBQUFJLCtDQUFTLEVBQUMsYUFBZDtBQUFBLGdEQUVJekIsSUFBSSxDQUFDMEIsWUFBTCxDQUFrQjNCLEdBQWxCLENBQXNCLFVBQUM0QixVQUFELEVBQWExQixHQUFiO0FBQUEsNERBQ3BCO0FBQUEsaUVBQ0U7QUFBTyxxREFBUyxFQUFFdEMsaUJBQWlCLEtBQUtxQyxJQUFJLENBQUN5QixpQkFBTCxHQUF1QixHQUF2QixHQUEyQkUsVUFBVSxDQUFDQyx1QkFBNUQsR0FBc0YsUUFBdEYsR0FBaUcsaUJBQW5IO0FBQXNJLG1EQUFPLEVBQUMsRUFBOUk7QUFBaUosZ0RBQUksRUFBQyxjQUF0SjtBQUFBLHNEQUFzS0QsVUFBVSxDQUFDakM7QUFBakw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsaURBRG9CO0FBQUEsdUNBQXRCO0FBRko7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FmSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkNBRHVCO0FBQUEsaUNBQXpCO0FBRkw7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERCxHQWtDQSxFQW5DRixHQW9DRCxFQWhFSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBRkYsZUFzRUk7QUFBSyx1Q0FBUyxFQUFDLFlBQWY7QUFBQSxzREFDRTtBQUFLLHlDQUFTLEVBQUMsY0FBZjtBQUFBLHdEQUNHO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQURILGVBRUc7QUFBSywyQ0FBUyxFQUFDLGlCQUFmO0FBQUEsMERBQ0c7QUFBRyx3Q0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQU8sRUFBQyxFQUEvQjtBQUFrQyw2Q0FBUyxFQUFDLFNBQTVDO0FBQUEsMkRBQXNEO0FBQUcsK0NBQVMsRUFBQyxhQUFiO0FBQTJCLHFEQUFZO0FBQXZDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBdEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FESCxlQUVHO0FBQU8sc0NBQUUsRUFBQyxhQUFWO0FBQXdCLHdDQUFJLEVBQUMsTUFBN0I7QUFBb0Msd0NBQUksRUFBQyxjQUF6QztBQUF3RCx5Q0FBSyxFQUFHakMsV0FBaEU7QUFBOEUsNkNBQVMsRUFBQztBQUF4RjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUZILGVBR0c7QUFBRyx3Q0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQU8sRUFBQyxFQUEvQjtBQUFrQyw2Q0FBUyxFQUFDLFNBQTVDO0FBQUEsMkRBQXNEO0FBQUcsK0NBQVMsRUFBQyxZQUFiO0FBQTBCLHFEQUFZO0FBQXRDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBdEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FISDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBRkg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURGLEVBVUlrQixZQUFZLElBQUksSUFBaEIsSUFBd0IsQ0FBQ0EsWUFBMUIsZ0JBQ0M7QUFBSyx5Q0FBUyxFQUFDLGVBQWY7QUFBQSx1REFDTTtBQUFHLHNDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBUyxFQUFDLG1CQUFqQztBQUFxRCx5Q0FBTyxFQUFDLEVBQTdEO0FBQUEsMERBQ0c7QUFBRyw2Q0FBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FESCxPQUNnRCxTQURoRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFETjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURELEdBTUVBLFlBQVksSUFBSSxJQUFoQixJQUF3QkEsWUFBekIsZ0JBQ0E7QUFBSyx5Q0FBUyxFQUFDLGVBQWY7QUFBQSx1REFDTTtBQUFHLHNDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBUyxFQUFDLHdCQUFqQztBQUEwRCxvREFBZ0JwQyxZQUFZLENBQUNrRCxVQUF2RjtBQUFtRyx5Q0FBTyxFQUFDLEVBQTNHO0FBQUEsMERBQ0c7QUFBRyw2Q0FBUyxFQUFDLGdCQUFiO0FBQThCLG1EQUFZO0FBQTFDO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBREg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRE47QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FEQSxHQU9BLElBdkJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0F0RUosRUFpR0lsRCxZQUFZLENBQUNzRixhQUFiLElBQTZCLElBQTdCLElBQXFDdEYsWUFBWSxDQUFDc0YsYUFBYixJQUE4QixFQUFwRSxnQkFDQztBQUFBLHNEQUNFO0FBQUcseUNBQVMsRUFBQyxlQUFiO0FBQUEsd0RBQTZCO0FBQU8sb0NBQUUsRUFBQyxZQUFWO0FBQXVCLHNDQUFJLEVBQUMsVUFBNUI7QUFBdUMsMENBQVEsRUFBQztBQUFoRDtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQUE3QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREYsZUFFRTtBQUFLLHlDQUFTLEVBQUMsY0FBZjtBQUFBLHdEQUNFO0FBQUksMkNBQVMsRUFBQyxLQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQURGLEVBRUd0RixZQUFZLENBQUNzRixhQUZoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBRkY7QUFBQSw0Q0FERCxHQVNDLElBMUdKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQ0E3QkYsRUE0SUl0RixZQUFZLENBQUN1RixZQUFkLGdCQUNDO0FBQUsscUNBQVMsRUFBQyxhQUFmO0FBQUEsbURBQ0U7QUFBQSx3Q0FFRXZGLFlBQVksQ0FBQ3VGLFlBQWIsQ0FBMEIvQixHQUExQixDQUE4QixVQUFDZ0MsUUFBRCxFQUFXQyxPQUFYO0FBQUEsb0RBQzVCO0FBQUksMkNBQVMsRUFBQyxVQUFkO0FBQUEseURBQ0k7QUFBRyw2Q0FBUyxFQUFDLFFBQWI7QUFBc0IseUNBQUssRUFBRUQsUUFBUSxDQUFDRSxLQUF0QztBQUE2Qyx1REFBZ0IsRUFBN0Q7QUFBZ0UseURBQWtCLEVBQWxGO0FBQXFGLDJDQUFPLEVBQUMsRUFBN0Y7QUFBQSw4Q0FBaUdGLFFBQVEsQ0FBQ0U7QUFBMUc7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRDRCO0FBQUEsK0JBQTlCO0FBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBREQsR0FhQyxJQXpKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLDhCQXBMRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFURjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBUEYsZUFxWEUscUVBQUMsY0FBRDtBQUFnQixpQkFBTyxFQUFFNUYsT0FBTyxDQUFDc0Q7QUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFyWEY7QUFBQTtBQU5KO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFtWUQ7O0dBemdCdUJ2RCxPO1VBa0lQbUQscUQ7OztNQWxJT25ELE87QUF5Z0J2QiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9bc2x1Z10uYjBhNmJhMTM3NzJlN2RhODU5OWQuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgRXJyb3JQYWdlIGZyb20gXCJuZXh0L2Vycm9yXCI7XHJcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcclxuaW1wb3J0IGR5bmFtaWMgZnJvbSAnbmV4dC9keW5hbWljJ1xyXG5pbXBvcnQgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnXHJcbmltcG9ydCBSZWFjdEltYWdlTWFnbmlmeSBmcm9tICdyZWFjdC1pbWFnZS1tYWduaWZ5J1xyXG5pbXBvcnQgU2xpZGVyIGZyb20gXCJyZWFjdC1zbGlja1wiO1xyXG5cclxuaW1wb3J0IENvbnRhaW5lciBmcm9tIFwiQC9jb21wb25lbnRzL2NvbnRhaW5lclwiO1xyXG5pbXBvcnQgTGF5b3V0IGZyb20gXCJAL2NvbXBvbmVudHMvbGF5b3V0XCI7XHJcbmltcG9ydCB7IGdldENvbmZpZ3MsIGdldE1haW5NZW51LCBnZXRQcm9kdWN0QnlTbHVnLCBnZXRQcm9kdWN0c0J5Q2F0ZWdvcnkgfSBmcm9tIFwiQC9saWIvYXBpXCI7XHJcblxyXG52YXIgaWRTdG9yZUFwcCA9ICduNDlzaG9wdjJfdHJpam9pYSc7XHJcbnZhciBUZW1wbGF0ZUhlYWRlciA9IGR5bmFtaWMoKCkgPT4gaW1wb3J0KCdAL2NvbXBvbmVudHMvdGVtcGxhdGVzLycraWRTdG9yZUFwcCsnL2NvbXBvbmVudHMvaGVhZGVyJykpXHJcbnZhciBUZW1wbGF0ZUZvb3RlciA9IGR5bmFtaWMoKCkgPT4gaW1wb3J0KCdAL2NvbXBvbmVudHMvdGVtcGxhdGVzLycraWRTdG9yZUFwcCsnL2NvbXBvbmVudHMvZm9vdGVyJykpXHJcblxyXG5sZXQgc2xpZGVyVGh1bWJzID0ge1xyXG4gIGluZmluaXRlOiBmYWxzZSxcclxuICB2ZXJ0aWNhbDogdHJ1ZSxcclxuICBzbGlkZXNUb1Nob3c6IDQsXHJcbiAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgc3BlZWQ6IDUwMCxcclxuICByZXNwb25zaXZlOiBbXHJcbiAgICB7XHJcbiAgICAgIGJyZWFrcG9pbnQ6IDQxNSxcclxuICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICB2ZXJ0aWNhbDogZmFsc2UsXHJcbiAgICAgICAgc2xpZGVzVG9TaG93OiAzXHJcbiAgICAgIH1cclxuICAgIH1cclxuICBdXHJcbn1cclxuXHJcbnZhciBzZXR0aW5nc01vYmlsZVNsaWRlID0ge1xyXG4gIHNsaWRlc1RvU2hvdzogMSxcclxuICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICBzcGVlZDogNTAwLFxyXG4gIGRvdHM6IHRydWVcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gUHJvZHVjdCh7IGNvbmZpZ3MsIG1haW5NZW51LCBkYWRvc1Byb2R1dG8gfSkge1xyXG4gIHZhciBpbml0aWFsSW1hZ2VzID0gW107XHJcbiAgZnVuY3Rpb24gZ2V0SW5pdGlhbEltYWdlKGRhZG9zUHJvZHV0bykge1xyXG4gICAgXHJcbiAgICB2YXIgaW1nSW5pY2lhbCA9IFtdO1xyXG4gICAgdmFyIHRlbXBJbWdJbmljaWFsID0gW107XHJcbiAgICBcclxuICAgIGZvcih2YXIgaSBpbiBkYWRvc1Byb2R1dG8uaW1hZ2VzKXtcclxuICAgICAgdGVtcEltZ0luaWNpYWwucHVzaChbaSwgZGFkb3NQcm9kdXRvLmltYWdlcyBbaV1dKTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgaW5pdGlhbEtleSA9IGRhZG9zUHJvZHV0by5pbWFnZS5zdWJzdHJpbmcoZGFkb3NQcm9kdXRvLmltYWdlLmxhc3RJbmRleE9mKCcvJykrMSlcclxuICAgIFxyXG4gICAgaWYoZGFkb3NQcm9kdXRvLmltYWdlcyAhPSB1bmRlZmluZWQgJiYgZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XSAhPSBudWxsKXtcclxuICAgICAgaW1nSW5pY2lhbFsnc2hvdyddID0gZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XS5zaG93IFxyXG4gICAgICBpbWdJbmljaWFsWydwb3B1cCddID0gZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XS5wb3B1cFxyXG4gICAgICBpbWdJbmljaWFsWyd0aHVtYiddID0gZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XS50aHVtYlxyXG4gICAgfWVsc2UgaWYodGVtcEltZ0luaWNpYWxbMF0gIT0gbnVsbCl7XHJcbiAgICAgIGltZ0luaWNpYWwgPSB0ZW1wSW1nSW5pY2lhbFswXVsxXVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBpbWdJbmljaWFsO1xyXG4gIH1cclxuICBpZihkYWRvc1Byb2R1dG8pe1xyXG4gICAgaW5pdGlhbEltYWdlcyA9IGdldEluaXRpYWxJbWFnZShkYWRvc1Byb2R1dG8pO1xyXG4gIH1cclxuXHJcbiAgdmFyIHNlbGVjdGVkUXRkUGFyY2VsID0gMTtcclxuICB2YXIgc2VsZWN0ZWRWYWxQYXJjZWwgPSAxO1xyXG4gIHZhciBxdWFudGl0eUJ1eSA9IDE7XHJcbiAgdmFyIHNoaXBwaW5nTWV0aG9kcyA9IG51bGw7XHJcbiAgdmFyIHNlbGVjdGVkT3B0aW9uQnV5ID0gMTtcclxuICBcclxuICBjb25zdCBbc3BlY2lhbFZhbHVlLCBzZXRTcGVjaWFsVmFsdWVdID0gdXNlU3RhdGUobnVsbCk7XHJcbiAgdmFyIFttYWluVmFsdWUsIHNldE1haW5WYWx1ZV0gPSB1c2VTdGF0ZShudWxsKTtcclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgZnVuY3Rpb24gZ2V0TWFpblZhbHVlKGRhZG9zUHJvZHV0bykge1xyXG4gICAgICB2YXIgaW5pdGlhbE1haW5WYWx1ZSA9IG51bGw7XHJcbiAgICAgIGlmKGRhZG9zUHJvZHV0byAhPSB1bmRlZmluZWQpe1xyXG4gICAgICAgIGlmKHBhcnNlSW50KGRhZG9zUHJvZHV0by5oYXNfb3B0aW9uKSA9PSAwKXsgLy8gUFJPRFVUTyBTRU0gT1BDT0VTXHJcbiAgICAgICAgICBpZihkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgICAgIGluaXRpYWxNYWluVmFsdWUgPSBkYWRvc1Byb2R1dG8ucHJpY2U7XHJcbiAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgaW5pdGlhbE1haW5WYWx1ZSA9IGRhZG9zUHJvZHV0by5wcmljZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgIGlmKGluaXRpYWxNYWluVmFsdWUgPT0gbnVsbCAmJiBkYWRvc1Byb2R1dG8ucHJpY2Upe1xyXG4gICAgICAgICAgICBpbml0aWFsTWFpblZhbHVlID0gZGFkb3NQcm9kdXRvLnByaWNlXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNldE1haW5WYWx1ZShpbml0aWFsTWFpblZhbHVlKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIGdldFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8pIHtcclxuICAgICAgdmFyIGluaXRpYWxTcGVjaWFsVmFsdWUgPSBudWxsO1xyXG4gICAgICBpZihkYWRvc1Byb2R1dG8gIT0gdW5kZWZpbmVkKXtcclxuICAgICAgICBpZihwYXJzZUludChkYWRvc1Byb2R1dG8uaGFzX29wdGlvbikgPT0gMCl7IC8vIFBST0RVVE8gU0VNIE9QQ09FU1xyXG4gICAgICAgICAgaWYoZGFkb3NQcm9kdXRvLnNwZWNpYWwpe1xyXG4gICAgICAgICAgICBpbml0aWFsU3BlY2lhbFZhbHVlID0gZGFkb3NQcm9kdXRvLnNwZWNpYWw7XHJcbiAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgaW5pdGlhbFNwZWNpYWxWYWx1ZSA9IGRhZG9zUHJvZHV0by5wcmljZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgIGlmKGluaXRpYWxTcGVjaWFsVmFsdWUgPT0gbnVsbCAmJiBkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgICAgIGluaXRpYWxTcGVjaWFsVmFsdWUgPSBkYWRvc1Byb2R1dG8uc3BlY2lhbFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBzZXRTcGVjaWFsVmFsdWUoaW5pdGlhbFNwZWNpYWxWYWx1ZSk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGdldFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8pO1xyXG4gICAgZ2V0TWFpblZhbHVlKGRhZG9zUHJvZHV0byk7XHJcbiAgfSlcclxuXHJcbiAgY29uc3QgW3NvbGRPdXRTdG9jaywgc2V0U29sZE91dFN0b2NrXSA9IHVzZVN0YXRlKCgpID0+IHtcclxuICAgIGNvbnN0IGluaXRpYWxTdGF0ZSA9IDA7XHJcbiAgICByZXR1cm4gaW5pdGlhbFN0YXRlO1xyXG4gIH0pO1xyXG4gIFxyXG4gIFxyXG4gIC8qaWYocGFyc2VJbnQoZGFkb3NQcm9kdXRvLmhhc19vcHRpb24pID09IDApeyAvLyBQUk9EVVRPIFNFTSBPUENPRVNcclxuICAgICAgXHJcbiAgICBjb25zdCB2YWxvckF0dWFsID0gZGFkb3NQcm9kdXRvLnByaWNlXHJcbiAgICBpZihkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgIHNldFNlbGVjdGVkTWFpblZhbHVlKHZhbG9yQXR1YWwpO1xyXG4gICAgICBzZXRTZWxlY3RlZFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8uc3BlY2lhbCk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgc2V0U2VsZWN0ZWRNYWluVmFsdWUobnVsbCk7XHJcbiAgICAgIHNldFNlbGVjdGVkU3BlY2lhbFZhbHVlKHZhbG9yQXR1YWwpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmKCFkYWRvc1Byb2R1dG8uc29sZF9vdXQpe1xyXG4gICAgICBzZXRTb2xkT3V0U3RvY2soMCk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgc2V0U29sZE91dFN0b2NrKDEpO1xyXG4gICAgfVxyXG4gIH1lbHNleyAvLyBQUk9EVVRPIENPTSBPUENPRVNcclxuICAgIHNldFNvbGRPdXRTdG9jaygwKTtcclxuXHJcbiAgICAgIGlmKHNlbGVjdGVkTWFpblZhbHVlID09IG51bGwpe1xyXG4gICAgICAgIHNldFNlbGVjdGVkTWFpblZhbHVlKGRhZG9zUHJvZHV0by5wcmljZSlcclxuICAgICAgfVxyXG4gICAgICBpZihzZWxlY3RlZFNwZWNpYWxWYWx1ZSA9PSBudWxsICYmIGRhZG9zUHJvZHV0by5zcGVjaWFsKXtcclxuICAgICAgICBzZXRTZWxlY3RlZFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8uc3BlY2lhbClcclxuICAgICAgfSovXHJcblxyXG4gICAgICAvKmZvcih2YXIgaSBpbiBkYWRvc1Byb2R1dG8ub3B0aW9ucyl7XHJcbiAgICAgICAgZ3J1cG9BdHVhbCA9IGRhZG9zUHJvZHV0by5vcHRpb25zW2ldXHJcbiAgICAgICAgZm9yKHZhciBvcHQgaW4gZ3J1cG9BdHVhbC5vcHRpb25fdmFsdWUpe1xyXG4gICAgICAgICAgaWYoZ3J1cG9BdHVhbC5vcHRpb25fdmFsdWVbb3B0XS5vcHRpb25fdmFsdWVfaWQ9PWRhZG9zUHJvZHV0by5vcGNhb19zZWxlY2lvbmFkYSl7XHJcbiAgICAgICAgICAgIGluaXRpYWxPcHRpb24gPSBncnVwb0F0dWFsLnByb2R1Y3Rfb3B0aW9uX2lkKydfJytncnVwb0F0dWFsLm9wdGlvbl92YWx1ZVtvcHRdLnByb2R1Y3Rfb3B0aW9uX3ZhbHVlX2lkXHJcbiAgICAgICAgICAgIHRoaXMuU2VsZWN0T3B0aW9uKGluaXRpYWxPcHRpb24sIG51bGwpXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gIH0qL1xyXG4gXHJcbiAgdmFyIGNoYW5nZVpvb20gPSAoZXYpID0+IHtcclxuICAgIGNvbnN0IGltZ1Nob3cgPSBldi5jdXJyZW50VGFyZ2V0LmRhdGFzZXQuc3Jjc2hvd1xyXG4gICAgY29uc3QgaW1nUG9wdXAgPSBldi5jdXJyZW50VGFyZ2V0LmRhdGFzZXQuc3JjcG9wdXBcclxuICAgIHZhciB0ZW1wSW1hZ2UgPSB7fTtcclxuICAgIHRlbXBJbWFnZVsnc2hvdyddID0gaW1nU2hvd1xyXG4gICAgdGVtcEltYWdlWydwb3B1cCddID0gaW1nUG9wdXBcclxuICAgIC8vc2V0SW5pdGlhbEltYWdlKHRlbXBJbWFnZSk7XHJcbiAgICBcclxuICB9XHJcbiAgXHJcbiAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcbiAgaWYgKCFyb3V0ZXIuaXNGYWxsYmFjayAmJiAhZGFkb3NQcm9kdXRvPy5wcm9kdWN0X2lkKSB7XHJcbiAgICByZXR1cm4gPEVycm9yUGFnZSBzdGF0dXNDb2RlPXs0MDR9IC8+O1xyXG4gIH1cclxuICByZXR1cm4gKFxyXG4gICAgPExheW91dD5cclxuICAgICAgPENvbnRhaW5lcj5cclxuICAgICAgXHJcbiAgICAgICAge1xyXG4gICAgICAgIHJvdXRlci5pc0ZhbGxiYWNrID8gKFxyXG4gICAgICAgICAgPGRpdj5Mb2FkaW5n4oCmPC9kaXY+XHJcbiAgICAgICAgKSA6IChcclxuICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgIDxIZWFkPlxyXG4gICAgICAgICAgICAgIDx0aXRsZT57ZGFkb3NQcm9kdXRvLm5hbWV9PC90aXRsZT5cclxuICAgICAgICAgICAgICA8bWV0YSBuYW1lPVwiZGVzY3JpcHRpb25cIiBjb250ZW50PVwibGVsZWxlXCIgLz5cclxuICAgICAgICAgICAgICA8bWV0YSBuYW1lPVwib2c6aW1hZ2VcIiBjb250ZW50PVwibGlsaWxpXCIgLz5cclxuICAgICAgICAgICAgPC9IZWFkPlxyXG4gICAgICAgICAgICA8VGVtcGxhdGVIZWFkZXIgY29uZmlncz17Y29uZmlncy5yZXNwb3N0YX0gbWFpbk1lbnU9e21haW5NZW51LnJlc3Bvc3RhfT48L1RlbXBsYXRlSGVhZGVyPiBcclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtYWluLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBhZ2UtcHJvZHVjdHNcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyLXBhZGRpbmcgbGlnaHQtYmFja2dyb3VuZCBucHJvZHVjdC1icmVhZGNydW1iXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPG9sIGNsYXNzTmFtZT1cImJyZWFkY3J1bWJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwiYnJlYWRjcnVtYi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cIi9cIiB0aXRsZT1cIlDDoWdpbmEgaW5pY2lhbFwiPkhvbWU8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwiYnJlYWRjcnVtYi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAge2RhZG9zUHJvZHV0by5uYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9vbD5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNwLXByZXZpZXczXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2R1Y3QtcGFnZVwiPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2R1Y3QtZ2FsbGVyeVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtdGh1bWJuYWlscyBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZXMtY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8uaW1hZ2VzKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5sZW5ndGggPiA0KT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U2xpZGVyIGNsYXNzTmFtZT1cInNsaWRlclRodW1ic1wiIHsuLi5zbGlkZXJUaHVtYnN9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwcm9kdWN0LWltYWdlLXRodW1iIGpzLWNhcm91c2VsLWNvbnRyb2wtaXRlbSBwb2ludGVyIGpzLXByb2R1Y3QtaW1hZ2UtdGh1bWJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD17ZGFkb3NQcm9kdXRvLm5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLXNyY3Nob3c9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0uc2hvd31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEtc3JjcG9wdXA9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjEwXCIgaGVpZ2h0PVwiMTBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz1cIlwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by52aWRlb3MubGVuZ3RoKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8udmlkZW9zLm1hcCgoaXRlbVZpZGVvLCBrZXlWaWRlbykgPT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpdGVtLXZpZGVvXCIga2V5PXtrZXlWaWRlb30+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBkYXRhLXZpZGVvaWQ9e2l0ZW1WaWRlby5pZF92aWRlb30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2BodHRwczovL2ltZy55b3V0dWJlLmNvbS92aS8ke2l0ZW1WaWRlby5pZF92aWRlb30vbXFkZWZhdWx0LmpwZ2B9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEteW91dHViZS1wbGF5XCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TbGlkZXI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZS10aHVtYiBqcy1jYXJvdXNlbC1jb250cm9sLWl0ZW0gcG9pbnRlciBqcy1wcm9kdWN0LWltYWdlLXRodW1iXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ9e2RhZG9zUHJvZHV0by5uYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLXNyY3Nob3c9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0uc2hvd31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1zcmNwb3B1cD17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCIxMFwiIGhlaWdodD1cIjEwXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz1cIlwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by52aWRlb3MubGVuZ3RoKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLnZpZGVvcy5tYXAoKGl0ZW1WaWRlbywga2V5VmlkZW8pID0+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIml0ZW0tdmlkZW9cIiBrZXk9e2tleVZpZGVvfT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBkYXRhLXZpZGVvaWQ9e2l0ZW1WaWRlby5pZF92aWRlb30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvJHtpdGVtVmlkZW8uaWRfdmlkZW99L21xZGVmYXVsdC5qcGdgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS15b3V0dWJlLXBsYXlcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2VzIG1pbi13aWR0aC00MTVweFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhcmVhWm9vbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fdG9wX2xlZnQuaW1hZ2UgIT0gbnVsbCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxhYmVsSXRlbSBsYWJlbFRvcExlZnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fdG9wX2xlZnQuaW1hZ2V9IC8+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fdG9wX3JpZ2h0LmltYWdlICE9IG51bGwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsYWJlbEl0ZW0gbGFiZWxUb3BSaWdodFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb190b3BfcmlnaHQuaW1hZ2V9IC8+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fYm90dG9tX2xlZnQuaW1hZ2UgIT0gbnVsbCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxhYmVsSXRlbSBsYWJlbEJvdHRvbUxlZnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fYm90dG9tX2xlZnQuaW1hZ2V9IC8+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fYm90dG9tX3JpZ2h0LmltYWdlICE9IG51bGwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsYWJlbEl0ZW0gbGFiZWxCb3R0b21SaWdodFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb19ib3R0b21fcmlnaHQuaW1hZ2V9IC8+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8UmVhY3RJbWFnZU1hZ25pZnkgey4uLntcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzbWFsbEltYWdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWx0OiBkYWRvc1Byb2R1dG8ubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0ZsdWlkV2lkdGg6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjOiBpbml0aWFsSW1hZ2VzLnNob3dcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXJnZUltYWdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjOiBpbml0aWFsSW1hZ2VzLnBvcHVwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBjb25maWdzLndpZHRoWm9vbSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IGNvbmZpZ3Mud2lkdGhab29tXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19IC8+ICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtYXgtd2lkdGgtNDE0cHhcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpICYmIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLmxlbmd0aCA+IDEpID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZXNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNsaWRlciBjbGFzc05hbWU9XCJzbGlkZVZpdHJpbmVcIiB7Li4uc2V0dGluZ3NNb2JpbGVTbGlkZX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLm1hcCgoaXRlbSwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb21cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9IGFsdD17ZGFkb3NQcm9kdXRvLm5hbWV9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLnZpZGVvcy5sZW5ndGgpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8udmlkZW9zLm1hcCgoaXRlbVZpZGVvLCBrZXlWaWRlbykgPT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb20gaXRlbS12aWRlb1wiIGtleT17a2V5VmlkZW99PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGRhdGEtdmlkZW9pZD17aXRlbVZpZGVvLmlkX3ZpZGVvfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2BodHRwczovL2ltZy55b3V0dWJlLmNvbS92aS8ke2l0ZW1WaWRlby5pZF92aWRlb30vbXFkZWZhdWx0LmpwZ2B9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXlvdXR1YmUtcGxheVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1NsaWRlcj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogKE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpICYmIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLmxlbmd0aCA+IDApID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb21cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH0gYWx0PXtkYWRvc1Byb2R1dG8ubmFtZX0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8udmlkZW9zLmxlbmd0aCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by52aWRlb3MubWFwKChpdGVtVmlkZW8sIGtleVZpZGVvKSA9PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhcmVhWm9vbSBpdGVtLXZpZGVvXCIga2V5PXtrZXlWaWRlb30+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgZGF0YS12aWRlb2lkPXtpdGVtVmlkZW8uaWRfdmlkZW99PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17YGh0dHBzOi8vaW1nLnlvdXR1YmUuY29tL3ZpLyR7aXRlbVZpZGVvLmlkX3ZpZGVvfS9tcWRlZmF1bHQuanBnYH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEteW91dHViZS1wbGF5XCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC8+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LWluZm9cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtYWN0aW9ucyBjb250YWluZXItcGFkZGluZyBjb250YWluZXItcGFkZGluZy10b3BcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2R1Y3QtaGVhZGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDEgY2xhc3NOYW1lPVwibnByb2R1Y3QtdGl0bGVcIj57ZGFkb3NQcm9kdXRvLm5hbWV9PC9oMT5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhkYWRvc1Byb2R1dG8ubW9kZWwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzTmFtZT1cImluZm9zUHJvZHVjdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48c3BhbiBjbGFzc05hbWU9XCJ0aXRsZUluZm9cIj5SRUY6PC9zcGFuPiB7ZGFkb3NQcm9kdXRvLm1vZGVsfTwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyYXRlQm94XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJsay1hdmFsaWFyXCIgb25DbGljaz1cIlwiPkF2YWxpYXIgYWdvcmE8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoZGFkb3NQcm9kdXRvLnNob3J0ICE9ICcnKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaW5mb3NBcmVhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyZXN1bWVQcm9kdWN0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9e3sgX19odG1sOiBkYWRvc1Byb2R1dG8uc2hvcnQgfX0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV5QXJlYVwiPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2xTZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZGN0LXByaWNlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsobGFsYWxhKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJvbGRQcmljZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgRGUgPHNwYW4gY2xhc3NOYW1lPVwibnByb2R1Y3QtcHJpY2UtbWF4XCI+e21haW5WYWx1ZX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2R1Y3QtcHJpY2UtdmFsdWVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGxhbGFsYSkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUG9yIDxzcGFuIGNsYXNzTmFtZT1cInNwZWNpYWxWYWx1ZVwiPntsYWxhbGF9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIml0ZW0tZGlzY291bnRcIj57ZGFkb3NQcm9kdXRvLmRpc2NvdW50X3BlcmNlbnR9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHA+cG9yIDxzcGFuPnttYWluVmFsdWV9PC9zcGFuPjwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhzZWxlY3RlZFF0ZFBhcmNlbCAhPSAnJyAmJiBzZWxlY3RlZFZhbFBhcmNlbCAhPSAnJyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwic2VsZWN0ZWRQYXJjZWxcIj5PdSA8c3BhbiBjbGFzc05hbWU9XCJudW1QYXJjXCI+e3NlbGVjdGVkUXRkUGFyY2VsfXg8L3NwYW4+IGRlIDxzcGFuIGNsYXNzTmFtZT1cInZhbFBhcmNcIj57c2VsZWN0ZWRWYWxQYXJjZWx9PC9zcGFuPjwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0byAhPSAnJyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChwYXJzZUludChkYWRvc1Byb2R1dG8uaGFzX29wdGlvbikgIT0gMCkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJvcHRpb25zQXJlYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLm9wdGlvbnMubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYm94LW9wdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPntpdGVtLm5hbWV9PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhpdGVtLnR5cGUgPT0gJ3RleHQnKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0eHRPcHRpb25cIj4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCBpZD1cImZpZWxkT3B0aW9uXCIgbWF4bGVuZ3RoPVwiM1wiIHR5cGU9XCJ0ZXh0XCIgZGF0YS1ncm91cD17aXRlbS5wcm9kdWN0X29wdGlvbl9pZH0gY2xhc3NOYW1lPVwiZmllbGRcIiBuYW1lPVwidHh0LW9wdGlvblwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJoZWxwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXF1ZXN0aW9uLWNpcmNsZSBjb2xvcjJcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImluZm9IZWxwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBJbnNpcmEgYXTDqSAzIGxldHJhcyBwYXJhIHBlcnNvbmFsaXphciBhIGNhbWlzYSBjb20gdW0gYm9yZGFkbyBleGNsdXNpdm8uICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzTmFtZT1cImxpc3RPcHRpb25zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlbS5vcHRpb25fdmFsdWUubWFwKChpdGVtT3B0aW9uLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzTmFtZT17c2VsZWN0ZWRPcHRpb25CdXkgIT09IGl0ZW0ucHJvZHVjdF9vcHRpb25faWQrJ18nK2l0ZW1PcHRpb24ucHJvZHVjdF9vcHRpb25fdmFsdWVfaWQgPyAnb3B0aW9uJyA6ICdvcHRpb24gc2VsZWN0ZWQnfSBvbkNsaWNrPVwiXCIgaHJlZj1cImphdmFzY3JpcHQ6O1wiPntpdGVtT3B0aW9uLm5hbWV9PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiAgJycgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2xTZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInF1YW50aXR5QXJlYVwiPiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPlF1YW50aWRhZGU8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXR0b25zUXVhbnRpdHlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgY2xhc3NOYW1lPVwiYnRuTGVzc1wiPjxpIGNsYXNzTmFtZT1cImZhIGZhLW1pbnVzXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCBpZD1cInR4dFF1YW50aXR5XCIgdHlwZT1cInRleHRcIiBuYW1lPVwidHh0LXF1YW50aXR5XCIgdmFsdWU9eyBxdWFudGl0eUJ1eSB9IGNsYXNzTmFtZT1cInR4dFF1YW50aXR5XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgY2xhc3NOYW1lPVwiYnRuTW9yZVwiPjxpIGNsYXNzTmFtZT1cImZhIGZhLXBsdXNcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KHNvbGRPdXRTdG9jayAhPSBudWxsICYmICFzb2xkT3V0U3RvY2spP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJ1eUJ1dHRvbkFyZWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIGNsYXNzTmFtZT1cImJ1eUJ1dHRvbiBidG5fYnV5XCIgb25DbGljaz1cIlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEgZmEtc2hvcHBpbmctY2FydFwiPjwvaT4geydDb21wcmFyJ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogKHNvbGRPdXRTdG9jayAhPSBudWxsICYmIHNvbGRPdXRTdG9jayk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV5QnV0dG9uQXJlYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgY2xhc3NOYW1lPVwiYnV5QnV0dG9uIG5vdGlmeUJ1dHRvblwiIGRhdGEtcHJvZHVjdGlkPXtkYWRvc1Byb2R1dG8ucHJvZHVjdF9pZH0gb25DbGljaz1cIlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1lbnZlbG9wZVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT4gQXZpc2UtbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoZGFkb3NQcm9kdXRvLnRleHRfcHJldmVuZGEhPSBudWxsICYmIGRhZG9zUHJvZHV0by50ZXh0X3ByZXZlbmRhICE9ICcnKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwiY2hlY2tQcmV2ZW5kYVwiPjxpbnB1dCBpZD1cImNrcHJldmVuZGFcIiB0eXBlPVwiY2hlY2tib3hcIiBvbkNoYW5nZT1cIlwiIC8+IENvbmNvcmRvIGNvbSBvIHByYXpvIGRlIGVudHJlZ2EgZGVzY3JpdG8gYWJhaXhvLjwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaW5mb1ByZXZlbmRhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJ0aXRcIj5URVJNTyBERSBBQ0VJVEHDh8ODTzwvaDQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtkYWRvc1Byb2R1dG8udGV4dF9wcmV2ZW5kYX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGRhZG9zUHJvZHV0by5ndWlhX21lZGlkYXMpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImd1aWFzTWVkaWRhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by5ndWlhX21lZGlkYXMubWFwKChpdGVtR3VpYSwga2V5R3VpYSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJpdGVtR3VpYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiY29sb3IyXCIgdGl0bGU9e2l0ZW1HdWlhLnRpdGxlfSBkYXRhLXRpdHVsb2d1aWE9XCJcIiBkYXRhLWNvbnRldWRvZ3VpYT1cIlwiIG9uQ2xpY2s9XCJcIj57aXRlbUd1aWEudGl0bGV9PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPFRlbXBsYXRlRm9vdGVyIGNvbmZpZ3M9e2NvbmZpZ3MucmVzcG9zdGF9IC8+XHJcbiAgICAgICAgICA8Lz5cclxuICAgICAgICApfVxyXG4gICAgICA8L0NvbnRhaW5lcj5cclxuICAgIDwvTGF5b3V0PlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0U3RhdGljUHJvcHMoeyBwYXJhbXMgfSkge1xyXG4gIHZhciBjb25maWdzID0gYXdhaXQgZ2V0Q29uZmlncygpO1xyXG4gIHZhciBtYWluTWVudSA9IGF3YWl0IGdldE1haW5NZW51KCk7XHJcbiAgdmFyIHJlc1Byb2R1Y3QgPSBhd2FpdCBnZXRQcm9kdWN0QnlTbHVnKHBhcmFtcy5zbHVnKTtcclxuXHJcbiAgdmFyIGRhZG9zUHJvZHV0byA9IG51bGw7XHJcbiAgaWYocmVzUHJvZHVjdC5zdWNjZXNzKXtcclxuICAgIGRhZG9zUHJvZHV0byA9IHJlc1Byb2R1Y3QucmVzcG9zdGEucHJvZHV0bztcclxuICB9XHJcbiAgcmV0dXJuIHtcclxuICAgIHByb3BzOiB7XHJcbiAgICAgIGNvbmZpZ3MsXHJcbiAgICAgIGRhZG9zUHJvZHV0byxcclxuICAgICAgbWFpbk1lbnVcclxuICAgIH0sXHJcbiAgICByZXZhbGlkYXRlOiA2MFxyXG4gIH07XHJcbn1cclxuXHJcbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBnZXRTdGF0aWNQYXRocygpIHtcclxuICBjb25zdCBwcm9kdXRvcyA9IGF3YWl0IGdldFByb2R1Y3RzQnlDYXRlZ29yeSgpO1xyXG5cclxuICByZXR1cm4ge1xyXG4gICAgcGF0aHM6XHJcbiAgICAgIFtdLFxyXG4gICAgZmFsbGJhY2s6IHRydWUsXHJcbiAgfTtcclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9