webpackHotUpdate_N_E("pages/[slug]",{

/***/ "./pages/[slug].js":
/*!*************************!*\
  !*** ./pages/[slug].js ***!
  \*************************/
/*! exports provided: __N_SSG, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__N_SSG", function() { return __N_SSG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Product; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/error */ "./node_modules/next/error.js");
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_error__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_image_magnify__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-image-magnify */ "./node_modules/react-image-magnify/dist/es/ReactImageMagnify.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_container__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/components/container */ "./components/container.js");
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @/components/layout */ "./components/layout.js");





var _jsxFileName = "C:\\Users\\sauln49\\Desktop\\nextjs\\pages\\[slug].js",
    _s2 = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }










var idStoreApp = 'n49shopv2_trijoia';
var TemplateHeader = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c = function _c() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/header$")("./" + idStoreApp + "/components/header");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/header$").resolve("./" + idStoreApp + "/components/header"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/header']
  }
});
_c2 = TemplateHeader;
var TemplateFooter = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c3 = function _c3() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/footer$")("./" + idStoreApp + "/components/footer");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/footer$").resolve("./" + idStoreApp + "/components/footer"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/footer']
  }
});
_c4 = TemplateFooter;
var sliderThumbs = {
  infinite: false,
  vertical: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  speed: 500,
  responsive: [{
    breakpoint: 415,
    settings: {
      vertical: false,
      slidesToShow: 3
    }
  }]
};
var settingsMobileSlide = {
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  dots: true
};
var __N_SSG = true;
function Product(_ref) {
  _s2();

  var _s = $RefreshSig$(),
      _this = this;

  var configs = _ref.configs,
      mainMenu = _ref.mainMenu,
      dadosProduto = _ref.dadosProduto;
  var initialImages = [];

  function getInitialImage(dadosProduto) {
    var imgInicial = [];
    var tempImgInicial = [];

    for (var i in dadosProduto.images) {
      tempImgInicial.push([i, dadosProduto.images[i]]);
    }

    var initialKey = dadosProduto.image.substring(dadosProduto.image.lastIndexOf('/') + 1);

    if (dadosProduto.images != undefined && dadosProduto.images[initialKey] != null) {
      imgInicial['show'] = dadosProduto.images[initialKey].show;
      imgInicial['popup'] = dadosProduto.images[initialKey].popup;
      imgInicial['thumb'] = dadosProduto.images[initialKey].thumb;
    } else if (tempImgInicial[0] != null) {
      imgInicial = tempImgInicial[0][1];
    }

    return imgInicial;
  }

  if (dadosProduto) {
    initialImages = getInitialImage(dadosProduto);
  }

  var selectedQtdParcel = 1;
  var selectedValParcel = 1;
  var quantityBuy = 1;
  var shippingMethods = null;
  var selectedOptionBuy = 1;

  function getSpecialValue(dadosProduto) {
    _s();

    var initialSpecialValue = null;

    if (dadosProduto != undefined) {
      if (parseInt(dadosProduto.has_option) == 0) {
        // PRODUTO SEM OPCOES
        if (dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        } else {
          initialSpecialValue = dadosProduto.price;
        }
      } else {
        if (initialSpecialValue == null && dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        }
      }
    }

    var specialValue = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(initialSpecialValue);
    return specialValue;
  }

  _s(getSpecialValue, "uOCI/u7Rhc0OEPJW7t3Rh1DIVCE=");

  var _getSpecialValue = getSpecialValue(dadosProduto),
      _getSpecialValue2 = Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_getSpecialValue, 2),
      specialValue = _getSpecialValue2[0],
      setSpecialValue = _getSpecialValue2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(null),
      mainValue = _useState[0],
      setMainValue = _useState[1];

  useEffect(function () {
    function getMainValue(dadosProduto) {
      var initialMainValue = null;

      if (dadosProduto != undefined) {
        if (parseInt(dadosProduto.has_option) == 0) {
          // PRODUTO SEM OPCOES
          if (dadosProduto.special) {
            initialMainValue = dadosProduto.price;
          } else {
            initialMainValue = null;
          }
        } else {
          if (initialMainValue == null && dadosProduto.price) {
            initialMainValue = dadosProduto.price;
          }
        }
      }

      setMainValue(initialMainValue);
    }

    getMainValue(dadosProduto);
  });

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(function () {
    var initialState = 0;
    return initialState;
  }),
      soldOutStock = _useState2[0],
      setSoldOutStock = _useState2[1];
  /*if(parseInt(dadosProduto.has_option) == 0){ // PRODUTO SEM OPCOES
      
    const valorAtual = dadosProduto.price
    if(dadosProduto.special){
      setSelectedMainValue(valorAtual);
      setSelectedSpecialValue(dadosProduto.special);
    }else{
      setSelectedMainValue(null);
      setSelectedSpecialValue(valorAtual);
    }
      if(!dadosProduto.sold_out){
      setSoldOutStock(0);
    }else{
      setSoldOutStock(1);
    }
  }else{ // PRODUTO COM OPCOES
    setSoldOutStock(0);
        if(selectedMainValue == null){
        setSelectedMainValue(dadosProduto.price)
      }
      if(selectedSpecialValue == null && dadosProduto.special){
        setSelectedSpecialValue(dadosProduto.special)
      }*/

  /*for(var i in dadosProduto.options){
    grupoAtual = dadosProduto.options[i]
    for(var opt in grupoAtual.option_value){
      if(grupoAtual.option_value[opt].option_value_id==dadosProduto.opcao_selecionada){
        initialOption = grupoAtual.product_option_id+'_'+grupoAtual.option_value[opt].product_option_value_id
        this.SelectOption(initialOption, null)
      }
    }
  }
        
  }*/


  var changeZoom = function changeZoom(ev) {
    var imgShow = ev.currentTarget.dataset.srcshow;
    var imgPopup = ev.currentTarget.dataset.srcpopup;
    var tempImage = {};
    tempImage['show'] = imgShow;
    tempImage['popup'] = imgPopup; //setInitialImage(tempImage);
  };

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])();

  if (!router.isFallback && !(dadosProduto !== null && dadosProduto !== void 0 && dadosProduto.product_id)) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_error__WEBPACK_IMPORTED_MODULE_4___default.a, {
      statusCode: 404
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 174,
      columnNumber: 12
    }, this);
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_layout__WEBPACK_IMPORTED_MODULE_11__["default"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_container__WEBPACK_IMPORTED_MODULE_10__["default"], {
      children: router.isFallback ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        children: "Loading\u2026"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 182,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_5___default.a, {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
            children: dadosProduto.name
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 186,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "description",
            content: "lelele"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 187,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "og:image",
            content: "lilili"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 188,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 185,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateHeader, {
          configs: configs.resposta,
          mainMenu: mainMenu.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 190,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          clas: "main-content",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "page-products",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "container-padding light-background nproduct-breadcrumb",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "container",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ol", {
                  className: "breadcrumb",
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                      href: "/",
                      title: "P\xE1gina inicial",
                      children: "Home"
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 197,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 196,
                    columnNumber: 24
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: dadosProduto.name
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 199,
                    columnNumber: 24
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 195,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                  className: "cp-preview3",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                    className: "container",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                      className: "nproduct-page",
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-gallery",
                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-thumbnails ",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images-container",
                            children: dadosProduto.images ? Object.keys(dadosProduto.images).length > 4 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "sliderThumbs"
                            }, sliderThumbs), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 220,
                                    columnNumber: 57
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 217,
                                  columnNumber: 53
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 234,
                                      columnNumber: 51
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 235,
                                      columnNumber: 51
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 233,
                                    columnNumber: 49
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 232,
                                  columnNumber: 47
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 213,
                              columnNumber: 39
                            }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 251,
                                    columnNumber: 53
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 248,
                                  columnNumber: 49
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 266,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 267,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 265,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 264,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }, void 0, true) : null
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 209,
                            columnNumber: 32
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 208,
                          columnNumber: 31
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-images min-width-415px",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "areaZoom",
                            children: [dadosProduto.labels.promo_top_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 285,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 284,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_top_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 293,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 292,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 301,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 300,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 309,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 308,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_image_magnify__WEBPACK_IMPORTED_MODULE_8__["default"], _objectSpread({}, {
                              smallImage: {
                                alt: dadosProduto.name,
                                isFluidWidth: true,
                                src: initialImages.show
                              },
                              largeImage: {
                                src: initialImages.popup,
                                width: configs.widthZoom,
                                height: configs.widthZoom
                              }
                            }), void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 315,
                              columnNumber: 37
                            }, this)]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 281,
                            columnNumber: 35
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 280,
                          columnNumber: 33
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "max-width-414px",
                          children: Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 1 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "slideVitrine"
                            }, settingsMobileSlide), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 338,
                                    columnNumber: 49
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 337,
                                  columnNumber: 47
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 347,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 348,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 346,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 345,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 334,
                              columnNumber: 37
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 333,
                            columnNumber: 35
                          }, this) : Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                            children: [Object.keys(dadosProduto.images).map(function (item, key) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                  src: dadosProduto.images[item].popup,
                                  alt: dadosProduto.name
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 362,
                                  columnNumber: 45
                                }, _this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 361,
                                columnNumber: 43
                              }, _this);
                            }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom item-video",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  onClick: "",
                                  "data-videoid": itemVideo.id_video,
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 371,
                                    columnNumber: 49
                                  }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-youtube-play",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 372,
                                    columnNumber: 49
                                  }, _this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 370,
                                  columnNumber: 47
                                }, _this)
                              }, keyVideo, false, {
                                fileName: _jsxFileName,
                                lineNumber: 369,
                                columnNumber: 45
                              }, _this);
                            }) : null]
                          }, void 0, true) : null
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 331,
                          columnNumber: 31
                        }, this)]
                      }, void 0, true, {
                        fileName: _jsxFileName,
                        lineNumber: 207,
                        columnNumber: 29
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-info",
                        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-actions container-padding container-padding-top",
                          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "nproduct-header",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
                              className: "nproduct-title",
                              children: dadosProduto.name
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 388,
                              columnNumber: 38
                            }, this), dadosProduto.model ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              className: "infosProduct",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "titleInfo",
                                  children: "REF:"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 392,
                                  columnNumber: 49
                                }, this), " ", dadosProduto.model]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 392,
                                columnNumber: 45
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 391,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "rateBox",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                className: "lk-avaliar",
                                onClick: "",
                                children: "Avaliar agora"
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 399,
                                columnNumber: 41
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 398,
                              columnNumber: 38
                            }, this), dadosProduto["short"] != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "infosArea",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "resumeProduct",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  dangerouslySetInnerHTML: {
                                    __html: dadosProduto["short"]
                                  }
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 406,
                                  columnNumber: 43
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 405,
                                columnNumber: 42
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 404,
                              columnNumber: 41
                            }, this) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 387,
                            columnNumber: 35
                          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "buyArea",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "nprodct-price",
                                children: [lalala ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "oldPrice",
                                  children: ["De ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "nproduct-price-max",
                                    children: lelele
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 421,
                                    columnNumber: 50
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 420,
                                  columnNumber: 45
                                }, this) : '', /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "nproduct-price-value",
                                  children: lalala ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                                    children: ["Por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "specialValue",
                                      children: lalala
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 429,
                                      columnNumber: 53
                                    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "item-discount",
                                      children: dadosProduto.discount_percent
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 430,
                                      columnNumber: 48
                                    }, this)]
                                  }, void 0, true) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                    children: ["por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      children: lelele
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 433,
                                      columnNumber: 54
                                    }, this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 433,
                                    columnNumber: 47
                                  }, this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 426,
                                  columnNumber: 43
                                }, this), selectedQtdParcel != '' && selectedValParcel != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                  className: "selectedParcel",
                                  children: ["Ou ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "numParc",
                                    children: [selectedQtdParcel, "x"]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 438,
                                    columnNumber: 78
                                  }, this), " de ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "valParc",
                                    children: selectedValParcel
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 438,
                                    columnNumber: 135
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 438,
                                  columnNumber: 45
                                }, this) : null]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 418,
                                columnNumber: 39
                              }, this), dadosProduto != '' ? parseInt(dadosProduto.has_option) != 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "optionsArea",
                                children: dadosProduto.options.map(function (item, key) {
                                  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                    className: "box-option",
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                      children: item.name
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 451,
                                      columnNumber: 53
                                    }, _this), item.type == 'text' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                      className: "txtOption",
                                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                        id: "fieldOption",
                                        maxlength: "3",
                                        type: "text",
                                        "data-group": item.product_option_id,
                                        className: "field",
                                        name: "txt-option"
                                      }, void 0, false, {
                                        fileName: _jsxFileName,
                                        lineNumber: 455,
                                        columnNumber: 57
                                      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                        className: "help",
                                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                          className: "fa fa-question-circle color2",
                                          "aria-hidden": "true"
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 457,
                                          columnNumber: 61
                                        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                          className: "infoHelp",
                                          children: "Insira at\xE9 3 letras para personalizar a camisa com um bordado exclusivo."
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 459,
                                          columnNumber: 61
                                        }, _this)]
                                      }, void 0, true, {
                                        fileName: _jsxFileName,
                                        lineNumber: 456,
                                        columnNumber: 57
                                      }, _this)]
                                    }, void 0, true, {
                                      fileName: _jsxFileName,
                                      lineNumber: 454,
                                      columnNumber: 55
                                    }, _this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                                      className: "listOptions",
                                      children: item.option_value.map(function (itemOption, key) {
                                        return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                            className: selectedOptionBuy !== item.product_option_id + '_' + itemOption.product_option_value_id ? 'option' : 'option selected',
                                            onClick: "",
                                            href: "javascript:;",
                                            children: itemOption.name
                                          }, void 0, false, {
                                            fileName: _jsxFileName,
                                            lineNumber: 469,
                                            columnNumber: 63
                                          }, _this)
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 468,
                                          columnNumber: 61
                                        }, _this);
                                      })
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 465,
                                      columnNumber: 55
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 450,
                                    columnNumber: 51
                                  }, _this);
                                })
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 447,
                                columnNumber: 44
                              }, this) : '' : '']
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 417,
                              columnNumber: 37
                            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "quantityArea",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                  children: "Quantidade"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 487,
                                  columnNumber: 44
                                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "buttonsQuantity",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnLess",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-minus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 489,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 489,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                    id: "txtQuantity",
                                    type: "text",
                                    name: "txt-quantity",
                                    value: quantityBuy,
                                    className: "txtQuantity"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 490,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnMore",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-plus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 491,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 491,
                                    columnNumber: 47
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 488,
                                  columnNumber: 44
                                }, this)]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 486,
                                columnNumber: 41
                              }, this), soldOutStock != null && !soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton btn_buy",
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fas fa fa-shopping-cart"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 498,
                                    columnNumber: 52
                                  }, this), " ", 'Comprar']
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 497,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 496,
                                columnNumber: 43
                              }, this) : soldOutStock != null && soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton notifyButton",
                                  "data-productid": dadosProduto.product_id,
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-envelope",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 504,
                                    columnNumber: 52
                                  }, this), " Avise-me"]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 503,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 502,
                                columnNumber: 43
                              }, this) : null]
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 485,
                              columnNumber: 39
                            }, this), dadosProduto.text_prevenda != null && dadosProduto.text_prevenda != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                className: "checkPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                  id: "ckprevenda",
                                  type: "checkbox",
                                  onChange: ""
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 514,
                                  columnNumber: 70
                                }, this), " Concordo com o prazo de entrega descrito abaixo."]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 514,
                                columnNumber: 41
                              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "infoPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
                                  className: "tit",
                                  children: "TERMO DE ACEITA\xC7\xC3O"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 516,
                                  columnNumber: 43
                                }, this), dadosProduto.text_prevenda]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 515,
                                columnNumber: 41
                              }, this)]
                            }, void 0, true) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 415,
                            columnNumber: 35
                          }, this), dadosProduto.guia_medidas ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "guiasMedida",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              children: dadosProduto.guia_medidas.map(function (itemGuia, keyGuia) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                  className: "itemGuia",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    className: "color2",
                                    title: itemGuia.title,
                                    "data-tituloguia": "",
                                    "data-conteudoguia": "",
                                    onClick: "",
                                    children: itemGuia.title
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 532,
                                    columnNumber: 47
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 531,
                                  columnNumber: 43
                                }, _this);
                              })
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 528,
                              columnNumber: 39
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 527,
                            columnNumber: 37
                          }, this) : null]
                        }, void 0, true, {
                          fileName: _jsxFileName,
                          lineNumber: 386,
                          columnNumber: 33
                        }, this)
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 385,
                        columnNumber: 29
                      }, this)]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 205,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 204,
                    columnNumber: 23
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 203,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 194,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 193,
              columnNumber: 17
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 192,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 191,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateFooter, {
          configs: configs.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 557,
          columnNumber: 13
        }, this)]
      }, void 0, true)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 178,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 177,
    columnNumber: 5
  }, this);
}

_s2(Product, "4ziBSee6/0ZHo0Uq+ig8AtGtzxc=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"]];
});

_c5 = Product;
;

var _c, _c2, _c3, _c4, _c5;

$RefreshReg$(_c, "TemplateHeader$dynamic");
$RefreshReg$(_c2, "TemplateHeader");
$RefreshReg$(_c3, "TemplateFooter$dynamic");
$RefreshReg$(_c4, "TemplateFooter");
$RefreshReg$(_c5, "Product");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvW3NsdWddLmpzIl0sIm5hbWVzIjpbImlkU3RvcmVBcHAiLCJUZW1wbGF0ZUhlYWRlciIsImR5bmFtaWMiLCJUZW1wbGF0ZUZvb3RlciIsInNsaWRlclRodW1icyIsImluZmluaXRlIiwidmVydGljYWwiLCJzbGlkZXNUb1Nob3ciLCJzbGlkZXNUb1Njcm9sbCIsInNwZWVkIiwicmVzcG9uc2l2ZSIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsInNldHRpbmdzTW9iaWxlU2xpZGUiLCJkb3RzIiwiUHJvZHVjdCIsImNvbmZpZ3MiLCJtYWluTWVudSIsImRhZG9zUHJvZHV0byIsImluaXRpYWxJbWFnZXMiLCJnZXRJbml0aWFsSW1hZ2UiLCJpbWdJbmljaWFsIiwidGVtcEltZ0luaWNpYWwiLCJpIiwiaW1hZ2VzIiwicHVzaCIsImluaXRpYWxLZXkiLCJpbWFnZSIsInN1YnN0cmluZyIsImxhc3RJbmRleE9mIiwidW5kZWZpbmVkIiwic2hvdyIsInBvcHVwIiwidGh1bWIiLCJzZWxlY3RlZFF0ZFBhcmNlbCIsInNlbGVjdGVkVmFsUGFyY2VsIiwicXVhbnRpdHlCdXkiLCJzaGlwcGluZ01ldGhvZHMiLCJzZWxlY3RlZE9wdGlvbkJ1eSIsImdldFNwZWNpYWxWYWx1ZSIsImluaXRpYWxTcGVjaWFsVmFsdWUiLCJwYXJzZUludCIsImhhc19vcHRpb24iLCJzcGVjaWFsIiwicHJpY2UiLCJzcGVjaWFsVmFsdWUiLCJ1c2VTdGF0ZSIsInNldFNwZWNpYWxWYWx1ZSIsIm1haW5WYWx1ZSIsInNldE1haW5WYWx1ZSIsInVzZUVmZmVjdCIsImdldE1haW5WYWx1ZSIsImluaXRpYWxNYWluVmFsdWUiLCJpbml0aWFsU3RhdGUiLCJzb2xkT3V0U3RvY2siLCJzZXRTb2xkT3V0U3RvY2siLCJjaGFuZ2Vab29tIiwiZXYiLCJpbWdTaG93IiwiY3VycmVudFRhcmdldCIsImRhdGFzZXQiLCJzcmNzaG93IiwiaW1nUG9wdXAiLCJzcmNwb3B1cCIsInRlbXBJbWFnZSIsInJvdXRlciIsInVzZVJvdXRlciIsImlzRmFsbGJhY2siLCJwcm9kdWN0X2lkIiwibmFtZSIsInJlc3Bvc3RhIiwiT2JqZWN0Iiwia2V5cyIsImxlbmd0aCIsIm1hcCIsIml0ZW0iLCJrZXkiLCJ2aWRlb3MiLCJpdGVtVmlkZW8iLCJrZXlWaWRlbyIsImlkX3ZpZGVvIiwibGFiZWxzIiwicHJvbW9fdG9wX2xlZnQiLCJwcm9tb190b3BfcmlnaHQiLCJwcm9tb19ib3R0b21fbGVmdCIsInByb21vX2JvdHRvbV9yaWdodCIsInNtYWxsSW1hZ2UiLCJhbHQiLCJpc0ZsdWlkV2lkdGgiLCJzcmMiLCJsYXJnZUltYWdlIiwid2lkdGgiLCJ3aWR0aFpvb20iLCJoZWlnaHQiLCJtb2RlbCIsIl9faHRtbCIsImxhbGFsYSIsImxlbGVsZSIsImRpc2NvdW50X3BlcmNlbnQiLCJvcHRpb25zIiwidHlwZSIsInByb2R1Y3Rfb3B0aW9uX2lkIiwib3B0aW9uX3ZhbHVlIiwiaXRlbU9wdGlvbiIsInByb2R1Y3Rfb3B0aW9uX3ZhbHVlX2lkIiwidGV4dF9wcmV2ZW5kYSIsImd1aWFfbWVkaWRhcyIsIml0ZW1HdWlhIiwia2V5R3VpYSIsInRpdGxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUdBLElBQUlBLFVBQVUsR0FBRyxtQkFBakI7QUFDQSxJQUFJQyxjQUFjLEdBQUdDLG1EQUFPLE1BQUM7QUFBQSxTQUFNLDhGQUFPLElBQXlCLEdBQUNGLFVBQTFCLEdBQXFDLG9CQUE1QyxDQUFOO0FBQUEsQ0FBRDtBQUFBO0FBQUE7QUFBQSxrQ0FBYywwR0FBeUIsR0FBQ0EsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxjQUFjLDRCQUEwQkEsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxFQUE1QjtNQUFJQyxjO0FBQ0osSUFBSUUsY0FBYyxHQUFHRCxtREFBTyxPQUFDO0FBQUEsU0FBTSw4RkFBTyxJQUF5QixHQUFDRixVQUExQixHQUFxQyxvQkFBNUMsQ0FBTjtBQUFBLENBQUQ7QUFBQTtBQUFBO0FBQUEsa0NBQWMsMEdBQXlCLEdBQUNBLFVBQTFCLEdBQXFDLG9CQUFuRDtBQUFBO0FBQUEsY0FBYyw0QkFBMEJBLFVBQTFCLEdBQXFDLG9CQUFuRDtBQUFBO0FBQUEsRUFBNUI7TUFBSUcsYztBQUVKLElBQUlDLFlBQVksR0FBRztBQUNqQkMsVUFBUSxFQUFFLEtBRE87QUFFakJDLFVBQVEsRUFBRSxJQUZPO0FBR2pCQyxjQUFZLEVBQUUsQ0FIRztBQUlqQkMsZ0JBQWMsRUFBRSxDQUpDO0FBS2pCQyxPQUFLLEVBQUUsR0FMVTtBQU1qQkMsWUFBVSxFQUFFLENBQ1Y7QUFDRUMsY0FBVSxFQUFFLEdBRGQ7QUFFRUMsWUFBUSxFQUFFO0FBQ1JOLGNBQVEsRUFBRSxLQURGO0FBRVJDLGtCQUFZLEVBQUU7QUFGTjtBQUZaLEdBRFU7QUFOSyxDQUFuQjtBQWlCQSxJQUFJTSxtQkFBbUIsR0FBRztBQUN4Qk4sY0FBWSxFQUFFLENBRFU7QUFFeEJDLGdCQUFjLEVBQUUsQ0FGUTtBQUd4QkMsT0FBSyxFQUFFLEdBSGlCO0FBSXhCSyxNQUFJLEVBQUU7QUFKa0IsQ0FBMUI7O0FBT2UsU0FBU0MsT0FBVCxPQUFzRDtBQUFBOztBQUFBO0FBQUE7O0FBQUEsTUFBbkNDLE9BQW1DLFFBQW5DQSxPQUFtQztBQUFBLE1BQTFCQyxRQUEwQixRQUExQkEsUUFBMEI7QUFBQSxNQUFoQkMsWUFBZ0IsUUFBaEJBLFlBQWdCO0FBQ25FLE1BQUlDLGFBQWEsR0FBRyxFQUFwQjs7QUFDQSxXQUFTQyxlQUFULENBQXlCRixZQUF6QixFQUF1QztBQUVyQyxRQUFJRyxVQUFVLEdBQUcsRUFBakI7QUFDQSxRQUFJQyxjQUFjLEdBQUcsRUFBckI7O0FBRUEsU0FBSSxJQUFJQyxDQUFSLElBQWFMLFlBQVksQ0FBQ00sTUFBMUIsRUFBaUM7QUFDL0JGLG9CQUFjLENBQUNHLElBQWYsQ0FBb0IsQ0FBQ0YsQ0FBRCxFQUFJTCxZQUFZLENBQUNNLE1BQWIsQ0FBcUJELENBQXJCLENBQUosQ0FBcEI7QUFDRDs7QUFFRCxRQUFJRyxVQUFVLEdBQUdSLFlBQVksQ0FBQ1MsS0FBYixDQUFtQkMsU0FBbkIsQ0FBNkJWLFlBQVksQ0FBQ1MsS0FBYixDQUFtQkUsV0FBbkIsQ0FBK0IsR0FBL0IsSUFBb0MsQ0FBakUsQ0FBakI7O0FBRUEsUUFBR1gsWUFBWSxDQUFDTSxNQUFiLElBQXVCTSxTQUF2QixJQUFvQ1osWUFBWSxDQUFDTSxNQUFiLENBQW9CRSxVQUFwQixLQUFtQyxJQUExRSxFQUErRTtBQUM3RUwsZ0JBQVUsQ0FBQyxNQUFELENBQVYsR0FBcUJILFlBQVksQ0FBQ00sTUFBYixDQUFvQkUsVUFBcEIsRUFBZ0NLLElBQXJEO0FBQ0FWLGdCQUFVLENBQUMsT0FBRCxDQUFWLEdBQXNCSCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JFLFVBQXBCLEVBQWdDTSxLQUF0RDtBQUNBWCxnQkFBVSxDQUFDLE9BQUQsQ0FBVixHQUFzQkgsWUFBWSxDQUFDTSxNQUFiLENBQW9CRSxVQUFwQixFQUFnQ08sS0FBdEQ7QUFDRCxLQUpELE1BSU0sSUFBR1gsY0FBYyxDQUFDLENBQUQsQ0FBZCxJQUFxQixJQUF4QixFQUE2QjtBQUNqQ0QsZ0JBQVUsR0FBR0MsY0FBYyxDQUFDLENBQUQsQ0FBZCxDQUFrQixDQUFsQixDQUFiO0FBQ0Q7O0FBRUQsV0FBT0QsVUFBUDtBQUNEOztBQUNELE1BQUdILFlBQUgsRUFBZ0I7QUFDZEMsaUJBQWEsR0FBR0MsZUFBZSxDQUFDRixZQUFELENBQS9CO0FBQ0Q7O0FBRUQsTUFBSWdCLGlCQUFpQixHQUFHLENBQXhCO0FBQ0EsTUFBSUMsaUJBQWlCLEdBQUcsQ0FBeEI7QUFDQSxNQUFJQyxXQUFXLEdBQUcsQ0FBbEI7QUFDQSxNQUFJQyxlQUFlLEdBQUcsSUFBdEI7QUFDQSxNQUFJQyxpQkFBaUIsR0FBRyxDQUF4Qjs7QUFFQSxXQUFTQyxlQUFULENBQXlCckIsWUFBekIsRUFBdUM7QUFBQTs7QUFDckMsUUFBSXNCLG1CQUFtQixHQUFHLElBQTFCOztBQUNBLFFBQUd0QixZQUFZLElBQUlZLFNBQW5CLEVBQTZCO0FBQzNCLFVBQUdXLFFBQVEsQ0FBQ3ZCLFlBQVksQ0FBQ3dCLFVBQWQsQ0FBUixJQUFxQyxDQUF4QyxFQUEwQztBQUFFO0FBQzFDLFlBQUd4QixZQUFZLENBQUN5QixPQUFoQixFQUF3QjtBQUN0QkgsNkJBQW1CLEdBQUd0QixZQUFZLENBQUN5QixPQUFuQztBQUNELFNBRkQsTUFFSztBQUNISCw2QkFBbUIsR0FBR3RCLFlBQVksQ0FBQzBCLEtBQW5DO0FBQ0Q7QUFDRixPQU5ELE1BTUs7QUFDSCxZQUFHSixtQkFBbUIsSUFBSSxJQUF2QixJQUErQnRCLFlBQVksQ0FBQ3lCLE9BQS9DLEVBQXVEO0FBQ3JESCw2QkFBbUIsR0FBR3RCLFlBQVksQ0FBQ3lCLE9BQW5DO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFFBQU1FLFlBQVksR0FBR0Msc0RBQVEsQ0FBQ04sbUJBQUQsQ0FBN0I7QUFDQSxXQUFPSyxZQUFQO0FBQ0Q7O0FBbkRrRSxLQWlDMUROLGVBakMwRDs7QUFBQSx5QkFvRDNCQSxlQUFlLENBQUNyQixZQUFELENBcERZO0FBQUE7QUFBQSxNQW9ENUQyQixZQXBENEQ7QUFBQSxNQW9EOUNFLGVBcEQ4Qzs7QUFBQSxrQkFzRG5DRCxzREFBUSxDQUFDLElBQUQsQ0F0RDJCO0FBQUEsTUFzRDlERSxTQXREOEQ7QUFBQSxNQXNEbkRDLFlBdERtRDs7QUF1RG5FQyxXQUFTLENBQUMsWUFBTTtBQUNkLGFBQVNDLFlBQVQsQ0FBc0JqQyxZQUF0QixFQUFvQztBQUNsQyxVQUFJa0MsZ0JBQWdCLEdBQUcsSUFBdkI7O0FBQ0EsVUFBR2xDLFlBQVksSUFBSVksU0FBbkIsRUFBNkI7QUFDM0IsWUFBR1csUUFBUSxDQUFDdkIsWUFBWSxDQUFDd0IsVUFBZCxDQUFSLElBQXFDLENBQXhDLEVBQTBDO0FBQUU7QUFDMUMsY0FBR3hCLFlBQVksQ0FBQ3lCLE9BQWhCLEVBQXdCO0FBQ3RCUyw0QkFBZ0IsR0FBR2xDLFlBQVksQ0FBQzBCLEtBQWhDO0FBQ0QsV0FGRCxNQUVLO0FBQ0hRLDRCQUFnQixHQUFHLElBQW5CO0FBQ0Q7QUFDRixTQU5ELE1BTUs7QUFDSCxjQUFHQSxnQkFBZ0IsSUFBSSxJQUFwQixJQUE0QmxDLFlBQVksQ0FBQzBCLEtBQTVDLEVBQWtEO0FBQ2hEUSw0QkFBZ0IsR0FBR2xDLFlBQVksQ0FBQzBCLEtBQWhDO0FBQ0Q7QUFDRjtBQUNGOztBQUVESyxrQkFBWSxDQUFDRyxnQkFBRCxDQUFaO0FBQ0Q7O0FBQ0RELGdCQUFZLENBQUNqQyxZQUFELENBQVo7QUFDRCxHQXBCUSxDQUFUOztBQXZEbUUsbUJBNkUzQjRCLHNEQUFRLENBQUMsWUFBTTtBQUNyRCxRQUFNTyxZQUFZLEdBQUcsQ0FBckI7QUFDQSxXQUFPQSxZQUFQO0FBQ0QsR0FIK0MsQ0E3RW1CO0FBQUEsTUE2RTVEQyxZQTdFNEQ7QUFBQSxNQTZFOUNDLGVBN0U4QztBQW1GbkU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFJTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFRSxNQUFJQyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDQyxFQUFELEVBQVE7QUFDdkIsUUFBTUMsT0FBTyxHQUFHRCxFQUFFLENBQUNFLGFBQUgsQ0FBaUJDLE9BQWpCLENBQXlCQyxPQUF6QztBQUNBLFFBQU1DLFFBQVEsR0FBR0wsRUFBRSxDQUFDRSxhQUFILENBQWlCQyxPQUFqQixDQUF5QkcsUUFBMUM7QUFDQSxRQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFDQUEsYUFBUyxDQUFDLE1BQUQsQ0FBVCxHQUFvQk4sT0FBcEI7QUFDQU0sYUFBUyxDQUFDLE9BQUQsQ0FBVCxHQUFxQkYsUUFBckIsQ0FMdUIsQ0FNdkI7QUFFRCxHQVJEOztBQVVBLE1BQU1HLE1BQU0sR0FBR0MsNkRBQVMsRUFBeEI7O0FBQ0EsTUFBSSxDQUFDRCxNQUFNLENBQUNFLFVBQVIsSUFBc0IsRUFBQ2pELFlBQUQsYUFBQ0EsWUFBRCxlQUFDQSxZQUFZLENBQUVrRCxVQUFmLENBQTFCLEVBQXFEO0FBQ25ELHdCQUFPLHFFQUFDLGlEQUFEO0FBQVcsZ0JBQVUsRUFBRTtBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBQVA7QUFDRDs7QUFDRCxzQkFDRSxxRUFBQywyREFBRDtBQUFBLDJCQUNFLHFFQUFDLDhEQUFEO0FBQUEsZ0JBR0VILE1BQU0sQ0FBQ0UsVUFBUCxnQkFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGdCQUdFO0FBQUEsZ0NBQ0UscUVBQUMsZ0RBQUQ7QUFBQSxrQ0FDRTtBQUFBLHNCQUFRakQsWUFBWSxDQUFDbUQ7QUFBckI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQUVFO0FBQU0sZ0JBQUksRUFBQyxhQUFYO0FBQXlCLG1CQUFPLEVBQUM7QUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFGRixlQUdFO0FBQU0sZ0JBQUksRUFBQyxVQUFYO0FBQXNCLG1CQUFPLEVBQUM7QUFBOUI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFNRSxxRUFBQyxjQUFEO0FBQWdCLGlCQUFPLEVBQUVyRCxPQUFPLENBQUNzRCxRQUFqQztBQUEyQyxrQkFBUSxFQUFFckQsUUFBUSxDQUFDcUQ7QUFBOUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFORixlQU9FO0FBQUssY0FBSSxFQUFDLGNBQVY7QUFBQSxpQ0FDRTtBQUFLLHFCQUFTLEVBQUMsZUFBZjtBQUFBLG1DQUNFO0FBQUssdUJBQVMsRUFBQyx3REFBZjtBQUFBLHFDQUNFO0FBQUsseUJBQVMsRUFBQyxXQUFmO0FBQUEsd0NBQ0U7QUFBSSwyQkFBUyxFQUFDLFlBQWQ7QUFBQSwwQ0FDRztBQUFJLDZCQUFTLEVBQUMsaUJBQWQ7QUFBQSwyQ0FDRztBQUFHLDBCQUFJLEVBQUMsR0FBUjtBQUFZLDJCQUFLLEVBQUMsbUJBQWxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREg7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFESCxlQUlHO0FBQUksNkJBQVMsRUFBQyxpQkFBZDtBQUFBLDhCQUNJcEQsWUFBWSxDQUFDbUQ7QUFEakI7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFKSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREYsZUFTRTtBQUFLLDJCQUFTLEVBQUMsYUFBZjtBQUFBLHlDQUNFO0FBQUssNkJBQVMsRUFBQyxXQUFmO0FBQUEsMkNBQ0k7QUFBSywrQkFBUyxFQUFDLGVBQWY7QUFBQSw4Q0FFRTtBQUFLLGlDQUFTLEVBQUMsa0JBQWY7QUFBQSxnREFDRTtBQUFLLG1DQUFTLEVBQUMscUJBQWY7QUFBQSxpREFDQztBQUFLLHFDQUFTLEVBQUMsMEJBQWY7QUFBQSxzQ0FFSW5ELFlBQVksQ0FBQ00sTUFBZCxHQUNHK0MsTUFBTSxDQUFDQyxJQUFQLENBQVl0RCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDaUQsTUFBakMsR0FBMEMsQ0FBM0MsZ0JBQ0UscUVBQUMsa0RBQUQ7QUFBUSx1Q0FBUyxFQUFDO0FBQWxCLCtCQUFxQ3JFLFlBQXJDO0FBQUEseUNBR01tRSxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNrRCxHQUFqQyxDQUFxQyxVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxvREFDN0I7QUFDRSwyQ0FBUyxFQUFDLDZFQURaO0FBQUEseURBR0k7QUFBSyx1Q0FBRyxFQUFFMUQsWUFBWSxDQUFDTSxNQUFiLENBQW9CbUQsSUFBcEIsRUFBMEIzQyxLQUFwQztBQUNFLHVDQUFHLEVBQUVkLFlBQVksQ0FBQ21ELElBRHBCO0FBRUUsb0RBQWNuRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjVDLElBRjFDO0FBR0UscURBQWViLFlBQVksQ0FBQ00sTUFBYixDQUFvQm1ELElBQXBCLEVBQTBCM0MsS0FIM0M7QUFJRSx5Q0FBSyxFQUFDLElBSlI7QUFJYSwwQ0FBTSxFQUFDLElBSnBCO0FBS0UsMkNBQU8sRUFBQztBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFISjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQUQ2QjtBQUFBLCtCQUFyQyxDQUhOLEVBaUJLZCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSixNQUFyQixHQUNFdkQsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkgsR0FBcEIsQ0FBd0IsVUFBQ0ksU0FBRCxFQUFZQyxRQUFaO0FBQUEsb0RBQ3RCO0FBQUssMkNBQVMsRUFBQyxZQUFmO0FBQUEseURBQ0U7QUFBRyx3Q0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQU8sRUFBQyxFQUEvQjtBQUFrQyxvREFBY0QsU0FBUyxDQUFDRSxRQUExRDtBQUFBLDREQUNFO0FBQUsseUNBQUcsdUNBQWdDRixTQUFTLENBQUNFLFFBQTFDO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FERixlQUVFO0FBQUcsK0NBQVMsRUFBQyxvQkFBYjtBQUFrQyxxREFBWTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLG1DQUFpQ0QsUUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FEc0I7QUFBQSwrQkFBeEIsQ0FERixHQVVFLElBM0JOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixnQkFpQ0U7QUFBQSx5Q0FFRVIsTUFBTSxDQUFDQyxJQUFQLENBQVl0RCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDa0QsR0FBakMsQ0FBcUMsVUFBQ0MsSUFBRCxFQUFPQyxHQUFQO0FBQUEsb0RBQzdCO0FBQ0UsMkNBQVMsRUFBQyw2RUFEWjtBQUFBLHlEQUdJO0FBQUssdUNBQUcsRUFBRTFELFlBQVksQ0FBQ00sTUFBYixDQUFvQm1ELElBQXBCLEVBQTBCM0MsS0FBcEM7QUFDRSx1Q0FBRyxFQUFFZCxZQUFZLENBQUNtRCxJQURwQjtBQUVFLG9EQUFjbkQsWUFBWSxDQUFDTSxNQUFiLENBQW9CbUQsSUFBcEIsRUFBMEI1QyxJQUYxQztBQUdFLHFEQUFlYixZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjNDLEtBSDNDO0FBSUUseUNBQUssRUFBQyxJQUpSO0FBSWEsMENBQU0sRUFBQyxJQUpwQjtBQUtFLDJDQUFPLEVBQUM7QUFMVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSEo7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FENkI7QUFBQSwrQkFBckMsQ0FGRixFQWlCR2QsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkosTUFBckIsR0FDRXZELFlBQVksQ0FBQzJELE1BQWIsQ0FBb0JILEdBQXBCLENBQXdCLFVBQUNJLFNBQUQsRUFBWUMsUUFBWjtBQUFBLG9EQUN0QjtBQUFLLDJDQUFTLEVBQUMsWUFBZjtBQUFBLHlEQUNFO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0Msb0RBQWNELFNBQVMsQ0FBQ0UsUUFBMUQ7QUFBQSw0REFDRTtBQUFLLHlDQUFHLHVDQUFnQ0YsU0FBUyxDQUFDRSxRQUExQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREYsZUFFRTtBQUFHLCtDQUFTLEVBQUMsb0JBQWI7QUFBa0MscURBQVk7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixtQ0FBaUNELFFBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRHNCO0FBQUEsK0JBQXhCLENBREYsR0FVRSxJQTNCSjtBQUFBLDRDQWxDSixHQWdFRTtBQWxFTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0FERixlQXlFSTtBQUFLLG1DQUFTLEVBQUMsZ0NBQWY7QUFBQSxpREFDRTtBQUFLLHFDQUFTLEVBQUMsVUFBZjtBQUFBLHVDQUVLN0QsWUFBWSxDQUFDK0QsTUFBYixDQUFvQkMsY0FBcEIsQ0FBbUN2RCxLQUFuQyxJQUE0QyxJQUE3QyxnQkFDRTtBQUFLLHVDQUFTLEVBQUMsd0JBQWY7QUFBQSxxREFDRTtBQUFLLG1DQUFHLEVBQUVULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JDLGNBQXBCLENBQW1DdkQ7QUFBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREYsR0FLRSxJQVBOLEVBVUtULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JFLGVBQXBCLENBQW9DeEQsS0FBcEMsSUFBNkMsSUFBOUMsZ0JBQ0U7QUFBSyx1Q0FBUyxFQUFDLHlCQUFmO0FBQUEscURBQ0U7QUFBSyxtQ0FBRyxFQUFFVCxZQUFZLENBQUMrRCxNQUFiLENBQW9CRSxlQUFwQixDQUFvQ3hEO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLEdBS0UsSUFmTixFQWtCS1QsWUFBWSxDQUFDK0QsTUFBYixDQUFvQkcsaUJBQXBCLENBQXNDekQsS0FBdEMsSUFBK0MsSUFBaEQsZ0JBQ0U7QUFBSyx1Q0FBUyxFQUFDLDJCQUFmO0FBQUEscURBQ0U7QUFBSyxtQ0FBRyxFQUFFVCxZQUFZLENBQUMrRCxNQUFiLENBQW9CRyxpQkFBcEIsQ0FBc0N6RDtBQUFoRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixHQUtFLElBdkJOLEVBMEJLVCxZQUFZLENBQUMrRCxNQUFiLENBQW9CSSxrQkFBcEIsQ0FBdUMxRCxLQUF2QyxJQUFnRCxJQUFqRCxnQkFDRTtBQUFLLHVDQUFTLEVBQUMsNEJBQWY7QUFBQSxxREFDRTtBQUFLLG1DQUFHLEVBQUVULFlBQVksQ0FBQytELE1BQWIsQ0FBb0JJLGtCQUFwQixDQUF1QzFEO0FBQWpEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLEdBS0UsSUEvQk4sZUFrQ0UscUVBQUMsMkRBQUQsb0JBQXVCO0FBQ2YyRCx3Q0FBVSxFQUFFO0FBQ1pDLG1DQUFHLEVBQUVyRSxZQUFZLENBQUNtRCxJQUROO0FBRVptQiw0Q0FBWSxFQUFFLElBRkY7QUFHWkMsbUNBQUcsRUFBRXRFLGFBQWEsQ0FBQ1k7QUFIUCwrQkFERztBQU1uQjJELHdDQUFVLEVBQUU7QUFDUkQsbUNBQUcsRUFBRXRFLGFBQWEsQ0FBQ2EsS0FEWDtBQUVSMkQscUNBQUssRUFBRTNFLE9BQU8sQ0FBQzRFLFNBRlA7QUFHUkMsc0NBQU0sRUFBRTdFLE9BQU8sQ0FBQzRFO0FBSFI7QUFOTyw2QkFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FsQ0Y7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0F6RUosZUE0SEU7QUFBSyxtQ0FBUyxFQUFDLGlCQUFmO0FBQUEsb0NBQ0lyQixNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsS0FBb0MrQyxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNpRCxNQUFqQyxHQUEwQyxDQUEvRSxnQkFDQztBQUFLLHFDQUFTLEVBQUMsZ0JBQWY7QUFBQSxtREFDRSxxRUFBQyxrREFBRDtBQUFRLHVDQUFTLEVBQUM7QUFBbEIsK0JBQXFDNUQsbUJBQXJDO0FBQUEseUNBRU0wRCxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNrRCxHQUFqQyxDQUFxQyxVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxvREFDakM7QUFBSywyQ0FBUyxFQUFDLFVBQWY7QUFBQSx5REFDRTtBQUFLLHVDQUFHLEVBQUUxRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjNDLEtBQXBDO0FBQTJDLHVDQUFHLEVBQUVkLFlBQVksQ0FBQ21EO0FBQTdEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQURpQztBQUFBLCtCQUFyQyxDQUZOLEVBU0tuRCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSixNQUFyQixHQUNFdkQsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkgsR0FBcEIsQ0FBd0IsVUFBQ0ksU0FBRCxFQUFZQyxRQUFaO0FBQUEsb0RBQ3RCO0FBQUssMkNBQVMsRUFBQyxxQkFBZjtBQUFBLHlEQUNFO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0Msb0RBQWNELFNBQVMsQ0FBQ0UsUUFBMUQ7QUFBQSw0REFDRTtBQUFLLHlDQUFHLHVDQUFnQ0YsU0FBUyxDQUFDRSxRQUExQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREYsZUFFRTtBQUFHLCtDQUFTLEVBQUMsb0JBQWI7QUFBa0MscURBQVk7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixtQ0FBMENELFFBQTFDO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRHNCO0FBQUEsK0JBQXhCLENBREYsR0FVRSxJQW5CTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQURELEdBeUJFUixNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsS0FBb0MrQyxNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNpRCxNQUFqQyxHQUEwQyxDQUEvRSxnQkFDRTtBQUFBLHVDQUVJRixNQUFNLENBQUNDLElBQVAsQ0FBWXRELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNrRCxHQUFqQyxDQUFxQyxVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxrREFDbkM7QUFBSyx5Q0FBUyxFQUFDLFVBQWY7QUFBQSx1REFDRTtBQUFLLHFDQUFHLEVBQUUxRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JtRCxJQUFwQixFQUEwQjNDLEtBQXBDO0FBQTJDLHFDQUFHLEVBQUVkLFlBQVksQ0FBQ21EO0FBQTdEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLHVDQURtQztBQUFBLDZCQUFyQyxDQUZKLEVBU0tuRCxZQUFZLENBQUMyRCxNQUFiLENBQW9CSixNQUFyQixHQUNFdkQsWUFBWSxDQUFDMkQsTUFBYixDQUFvQkgsR0FBcEIsQ0FBd0IsVUFBQ0ksU0FBRCxFQUFZQyxRQUFaO0FBQUEsa0RBQ3RCO0FBQUsseUNBQVMsRUFBQyxxQkFBZjtBQUFBLHVEQUNFO0FBQUcsc0NBQUksRUFBQyxjQUFSO0FBQXVCLHlDQUFPLEVBQUMsRUFBL0I7QUFBa0Msa0RBQWNELFNBQVMsQ0FBQ0UsUUFBMUQ7QUFBQSwwREFDRTtBQUFLLHVDQUFHLHVDQUFnQ0YsU0FBUyxDQUFDRSxRQUExQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkNBREYsZUFFRTtBQUFHLDZDQUFTLEVBQUMsb0JBQWI7QUFBa0MsbURBQVk7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQ0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixpQ0FBMENELFFBQTFDO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUNBRHNCO0FBQUEsNkJBQXhCLENBREYsR0FVRSxJQW5CTjtBQUFBLDBDQURGLEdBd0JBO0FBbERKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0NBNUhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw4QkFGRixlQW9MRTtBQUFLLGlDQUFTLEVBQUMsZUFBZjtBQUFBLCtDQUNJO0FBQUssbUNBQVMsRUFBQyx5REFBZjtBQUFBLGtEQUNFO0FBQUsscUNBQVMsRUFBQyxpQkFBZjtBQUFBLG9EQUNHO0FBQUksdUNBQVMsRUFBQyxnQkFBZDtBQUFBLHdDQUFnQzdELFlBQVksQ0FBQ21EO0FBQTdDO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREgsRUFHTW5ELFlBQVksQ0FBQzRFLEtBQWQsZ0JBQ0M7QUFBSSx1Q0FBUyxFQUFDLGNBQWQ7QUFBQSxxREFDSTtBQUFBLHdEQUFJO0FBQU0sMkNBQVMsRUFBQyxXQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FBSixPQUE2QzVFLFlBQVksQ0FBQzRFLEtBQTFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREQsR0FLQyxJQVJOLGVBV0c7QUFBSyx1Q0FBUyxFQUFDLFNBQWY7QUFBQSxxREFDRztBQUFHLHlDQUFTLEVBQUMsWUFBYjtBQUEwQix1Q0FBTyxFQUFDLEVBQWxDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREg7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FYSCxFQWdCTTVFLFlBQVksU0FBWixJQUFzQixFQUF2QixnQkFDQztBQUFLLHVDQUFTLEVBQUMsV0FBZjtBQUFBLHFEQUNDO0FBQUsseUNBQVMsRUFBQyxlQUFmO0FBQUEsdURBQ0M7QUFBSyx5REFBdUIsRUFBRTtBQUFFNkUsMENBQU0sRUFBRTdFLFlBQVk7QUFBdEI7QUFBOUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERDtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURELEdBT0MsSUF2Qk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQURGLGVBNkJFO0FBQUsscUNBQVMsRUFBQyxTQUFmO0FBQUEsb0RBRUU7QUFBSyx1Q0FBUyxFQUFDLFlBQWY7QUFBQSxzREFDRTtBQUFLLHlDQUFTLEVBQUMsZUFBZjtBQUFBLDJDQUNNOEUsTUFBRCxnQkFDQztBQUFLLDJDQUFTLEVBQUMsVUFBZjtBQUFBLGlFQUNLO0FBQU0sNkNBQVMsRUFBQyxvQkFBaEI7QUFBQSw4Q0FBc0NDO0FBQXRDO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBREw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQURELEdBS0MsRUFOTixlQVFJO0FBQUssMkNBQVMsRUFBQyxzQkFBZjtBQUFBLDRDQUNJRCxNQUFELGdCQUNDO0FBQUEsb0VBQ007QUFBTSwrQ0FBUyxFQUFDLGNBQWhCO0FBQUEsZ0RBQWdDQTtBQUFoQztBQUFBO0FBQUE7QUFBQTtBQUFBLDRDQUROLGVBRUM7QUFBTSwrQ0FBUyxFQUFDLGVBQWhCO0FBQUEsZ0RBQWlDOUUsWUFBWSxDQUFDZ0Y7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSw0Q0FGRDtBQUFBLGtEQURELGdCQU1DO0FBQUEsb0VBQU87QUFBQSxnREFBT0Q7QUFBUDtBQUFBO0FBQUE7QUFBQTtBQUFBLDRDQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBUkosRUFtQk0vRCxpQkFBaUIsSUFBSSxFQUFyQixJQUEyQkMsaUJBQWlCLElBQUksRUFBakQsZ0JBQ0M7QUFBRywyQ0FBUyxFQUFDLGdCQUFiO0FBQUEsaUVBQWlDO0FBQU0sNkNBQVMsRUFBQyxTQUFoQjtBQUFBLCtDQUEyQkQsaUJBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FBakMsdUJBQTBGO0FBQU0sNkNBQVMsRUFBQyxTQUFoQjtBQUFBLDhDQUEyQkM7QUFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FBMUY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQURELEdBR0MsSUF0Qk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURGLEVBNEJLakIsWUFBWSxJQUFJLEVBQWpCLEdBQ0d1QixRQUFRLENBQUN2QixZQUFZLENBQUN3QixVQUFkLENBQVIsSUFBcUMsQ0FBdEMsZ0JBQ0M7QUFBSyx5Q0FBUyxFQUFDLGFBQWY7QUFBQSwwQ0FFS3hCLFlBQVksQ0FBQ2lGLE9BQWIsQ0FBcUJ6QixHQUFyQixDQUF5QixVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxzREFDdkI7QUFBSyw2Q0FBUyxFQUFDLFlBQWY7QUFBQSw0REFDRTtBQUFBLGdEQUFRRCxJQUFJLENBQUNOO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FERixFQUdJTSxJQUFJLENBQUN5QixJQUFMLElBQWEsTUFBZCxnQkFDQztBQUFLLCtDQUFTLEVBQUMsV0FBZjtBQUFBLDhEQUNFO0FBQU8sMENBQUUsRUFBQyxhQUFWO0FBQXdCLGlEQUFTLEVBQUMsR0FBbEM7QUFBc0MsNENBQUksRUFBQyxNQUEzQztBQUFrRCxzREFBWXpCLElBQUksQ0FBQzBCLGlCQUFuRTtBQUFzRixpREFBUyxFQUFDLE9BQWhHO0FBQXdHLDRDQUFJLEVBQUM7QUFBN0c7QUFBQTtBQUFBO0FBQUE7QUFBQSwrQ0FERixlQUVFO0FBQUssaURBQVMsRUFBQyxNQUFmO0FBQUEsZ0VBQ0k7QUFBRyxtREFBUyxFQUFDLDhCQUFiO0FBQTRDLHlEQUFZO0FBQXhEO0FBQUE7QUFBQTtBQUFBO0FBQUEsaURBREosZUFHSTtBQUFLLG1EQUFTLEVBQUMsVUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpREFISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsK0NBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQURELGdCQVlDO0FBQUksK0NBQVMsRUFBQyxhQUFkO0FBQUEsZ0RBRUkxQixJQUFJLENBQUMyQixZQUFMLENBQWtCNUIsR0FBbEIsQ0FBc0IsVUFBQzZCLFVBQUQsRUFBYTNCLEdBQWI7QUFBQSw0REFDcEI7QUFBQSxpRUFDRTtBQUFPLHFEQUFTLEVBQUV0QyxpQkFBaUIsS0FBS3FDLElBQUksQ0FBQzBCLGlCQUFMLEdBQXVCLEdBQXZCLEdBQTJCRSxVQUFVLENBQUNDLHVCQUE1RCxHQUFzRixRQUF0RixHQUFpRyxpQkFBbkg7QUFBc0ksbURBQU8sRUFBQyxFQUE5STtBQUFpSixnREFBSSxFQUFDLGNBQXRKO0FBQUEsc0RBQXNLRCxVQUFVLENBQUNsQztBQUFqTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpREFEb0I7QUFBQSx1Q0FBdEI7QUFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQWZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQ0FEdUI7QUFBQSxpQ0FBekI7QUFGTDtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURELEdBa0NBLEVBbkNGLEdBb0NELEVBaEVIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FGRixlQXNFSTtBQUFLLHVDQUFTLEVBQUMsWUFBZjtBQUFBLHNEQUNFO0FBQUsseUNBQVMsRUFBQyxjQUFmO0FBQUEsd0RBQ0c7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBREgsZUFFRztBQUFLLDJDQUFTLEVBQUMsaUJBQWY7QUFBQSwwREFDRztBQUFHLHdDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBTyxFQUFDLEVBQS9CO0FBQWtDLDZDQUFTLEVBQUMsU0FBNUM7QUFBQSwyREFBc0Q7QUFBRywrQ0FBUyxFQUFDLGFBQWI7QUFBMkIscURBQVk7QUFBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF0RDtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQURILGVBRUc7QUFBTyxzQ0FBRSxFQUFDLGFBQVY7QUFBd0Isd0NBQUksRUFBQyxNQUE3QjtBQUFvQyx3Q0FBSSxFQUFDLGNBQXpDO0FBQXdELHlDQUFLLEVBQUdqQyxXQUFoRTtBQUE4RSw2Q0FBUyxFQUFDO0FBQXhGO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBRkgsZUFHRztBQUFHLHdDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBTyxFQUFDLEVBQS9CO0FBQWtDLDZDQUFTLEVBQUMsU0FBNUM7QUFBQSwyREFBc0Q7QUFBRywrQ0FBUyxFQUFDLFlBQWI7QUFBMEIscURBQVk7QUFBdEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF0RDtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUhIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FGSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREYsRUFVSWtCLFlBQVksSUFBSSxJQUFoQixJQUF3QixDQUFDQSxZQUExQixnQkFDQztBQUFLLHlDQUFTLEVBQUMsZUFBZjtBQUFBLHVEQUNNO0FBQUcsc0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFTLEVBQUMsbUJBQWpDO0FBQXFELHlDQUFPLEVBQUMsRUFBN0Q7QUFBQSwwREFDRztBQUFHLDZDQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQURILE9BQ2dELFNBRGhEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUROO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREQsR0FNRUEsWUFBWSxJQUFJLElBQWhCLElBQXdCQSxZQUF6QixnQkFDQTtBQUFLLHlDQUFTLEVBQUMsZUFBZjtBQUFBLHVEQUNNO0FBQUcsc0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFTLEVBQUMsd0JBQWpDO0FBQTBELG9EQUFnQnBDLFlBQVksQ0FBQ2tELFVBQXZGO0FBQW1HLHlDQUFPLEVBQUMsRUFBM0c7QUFBQSwwREFDRztBQUFHLDZDQUFTLEVBQUMsZ0JBQWI7QUFBOEIsbURBQVk7QUFBMUM7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FESDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFETjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURBLEdBT0EsSUF2Qko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQXRFSixFQWlHSWxELFlBQVksQ0FBQ3VGLGFBQWIsSUFBNkIsSUFBN0IsSUFBcUN2RixZQUFZLENBQUN1RixhQUFiLElBQThCLEVBQXBFLGdCQUNDO0FBQUEsc0RBQ0U7QUFBRyx5Q0FBUyxFQUFDLGVBQWI7QUFBQSx3REFBNkI7QUFBTyxvQ0FBRSxFQUFDLFlBQVY7QUFBdUIsc0NBQUksRUFBQyxVQUE1QjtBQUF1QywwQ0FBUSxFQUFDO0FBQWhEO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBQTdCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERixlQUVFO0FBQUsseUNBQVMsRUFBQyxjQUFmO0FBQUEsd0RBQ0U7QUFBSSwyQ0FBUyxFQUFDLEtBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBREYsRUFFR3ZGLFlBQVksQ0FBQ3VGLGFBRmhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FGRjtBQUFBLDRDQURELEdBU0MsSUExR0o7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQTdCRixFQTRJSXZGLFlBQVksQ0FBQ3dGLFlBQWQsZ0JBQ0M7QUFBSyxxQ0FBUyxFQUFDLGFBQWY7QUFBQSxtREFDRTtBQUFBLHdDQUVFeEYsWUFBWSxDQUFDd0YsWUFBYixDQUEwQmhDLEdBQTFCLENBQThCLFVBQUNpQyxRQUFELEVBQVdDLE9BQVg7QUFBQSxvREFDNUI7QUFBSSwyQ0FBUyxFQUFDLFVBQWQ7QUFBQSx5REFDSTtBQUFHLDZDQUFTLEVBQUMsUUFBYjtBQUFzQix5Q0FBSyxFQUFFRCxRQUFRLENBQUNFLEtBQXRDO0FBQTZDLHVEQUFnQixFQUE3RDtBQUFnRSx5REFBa0IsRUFBbEY7QUFBcUYsMkNBQU8sRUFBQyxFQUE3RjtBQUFBLDhDQUFpR0YsUUFBUSxDQUFDRTtBQUExRztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FENEI7QUFBQSwrQkFBOUI7QUFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQ0FERCxHQWFDLElBekpKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsOEJBcExGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQVRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFQRixlQXFYRSxxRUFBQyxjQUFEO0FBQWdCLGlCQUFPLEVBQUU3RixPQUFPLENBQUNzRDtBQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQXJYRjtBQUFBO0FBTko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQW1ZRDs7SUExZ0J1QnZELE87VUFtSVBtRCxxRDs7O01BbklPbkQsTztBQTBnQnZCIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL1tzbHVnXS5kMzgxYjk1MGEyYTY1YThiM2QwMC5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSBcIm5leHQvcm91dGVyXCI7XHJcbmltcG9ydCBFcnJvclBhZ2UgZnJvbSBcIm5leHQvZXJyb3JcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgZHluYW1pYyBmcm9tICduZXh0L2R5bmFtaWMnXHJcbmltcG9ydCB7IHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnXHJcbmltcG9ydCBSZWFjdEltYWdlTWFnbmlmeSBmcm9tICdyZWFjdC1pbWFnZS1tYWduaWZ5J1xyXG5pbXBvcnQgU2xpZGVyIGZyb20gXCJyZWFjdC1zbGlja1wiO1xyXG5cclxuaW1wb3J0IENvbnRhaW5lciBmcm9tIFwiQC9jb21wb25lbnRzL2NvbnRhaW5lclwiO1xyXG5pbXBvcnQgTGF5b3V0IGZyb20gXCJAL2NvbXBvbmVudHMvbGF5b3V0XCI7XHJcbmltcG9ydCB7IGdldENvbmZpZ3MsIGdldE1haW5NZW51LCBnZXRQcm9kdWN0QnlTbHVnLCBnZXRQcm9kdWN0c0J5Q2F0ZWdvcnkgfSBmcm9tIFwiQC9saWIvYXBpXCI7XHJcblxyXG52YXIgaWRTdG9yZUFwcCA9ICduNDlzaG9wdjJfdHJpam9pYSc7XHJcbnZhciBUZW1wbGF0ZUhlYWRlciA9IGR5bmFtaWMoKCkgPT4gaW1wb3J0KCdAL2NvbXBvbmVudHMvdGVtcGxhdGVzLycraWRTdG9yZUFwcCsnL2NvbXBvbmVudHMvaGVhZGVyJykpXHJcbnZhciBUZW1wbGF0ZUZvb3RlciA9IGR5bmFtaWMoKCkgPT4gaW1wb3J0KCdAL2NvbXBvbmVudHMvdGVtcGxhdGVzLycraWRTdG9yZUFwcCsnL2NvbXBvbmVudHMvZm9vdGVyJykpXHJcblxyXG5sZXQgc2xpZGVyVGh1bWJzID0ge1xyXG4gIGluZmluaXRlOiBmYWxzZSxcclxuICB2ZXJ0aWNhbDogdHJ1ZSxcclxuICBzbGlkZXNUb1Nob3c6IDQsXHJcbiAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgc3BlZWQ6IDUwMCxcclxuICByZXNwb25zaXZlOiBbXHJcbiAgICB7XHJcbiAgICAgIGJyZWFrcG9pbnQ6IDQxNSxcclxuICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICB2ZXJ0aWNhbDogZmFsc2UsXHJcbiAgICAgICAgc2xpZGVzVG9TaG93OiAzXHJcbiAgICAgIH1cclxuICAgIH1cclxuICBdXHJcbn1cclxuXHJcbnZhciBzZXR0aW5nc01vYmlsZVNsaWRlID0ge1xyXG4gIHNsaWRlc1RvU2hvdzogMSxcclxuICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICBzcGVlZDogNTAwLFxyXG4gIGRvdHM6IHRydWVcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gUHJvZHVjdCh7IGNvbmZpZ3MsIG1haW5NZW51LCBkYWRvc1Byb2R1dG8gfSkge1xyXG4gIHZhciBpbml0aWFsSW1hZ2VzID0gW107XHJcbiAgZnVuY3Rpb24gZ2V0SW5pdGlhbEltYWdlKGRhZG9zUHJvZHV0bykge1xyXG4gICAgXHJcbiAgICB2YXIgaW1nSW5pY2lhbCA9IFtdO1xyXG4gICAgdmFyIHRlbXBJbWdJbmljaWFsID0gW107XHJcbiAgICBcclxuICAgIGZvcih2YXIgaSBpbiBkYWRvc1Byb2R1dG8uaW1hZ2VzKXtcclxuICAgICAgdGVtcEltZ0luaWNpYWwucHVzaChbaSwgZGFkb3NQcm9kdXRvLmltYWdlcyBbaV1dKTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgaW5pdGlhbEtleSA9IGRhZG9zUHJvZHV0by5pbWFnZS5zdWJzdHJpbmcoZGFkb3NQcm9kdXRvLmltYWdlLmxhc3RJbmRleE9mKCcvJykrMSlcclxuICAgIFxyXG4gICAgaWYoZGFkb3NQcm9kdXRvLmltYWdlcyAhPSB1bmRlZmluZWQgJiYgZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XSAhPSBudWxsKXtcclxuICAgICAgaW1nSW5pY2lhbFsnc2hvdyddID0gZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XS5zaG93IFxyXG4gICAgICBpbWdJbmljaWFsWydwb3B1cCddID0gZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XS5wb3B1cFxyXG4gICAgICBpbWdJbmljaWFsWyd0aHVtYiddID0gZGFkb3NQcm9kdXRvLmltYWdlc1tpbml0aWFsS2V5XS50aHVtYlxyXG4gICAgfWVsc2UgaWYodGVtcEltZ0luaWNpYWxbMF0gIT0gbnVsbCl7XHJcbiAgICAgIGltZ0luaWNpYWwgPSB0ZW1wSW1nSW5pY2lhbFswXVsxXVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBpbWdJbmljaWFsO1xyXG4gIH1cclxuICBpZihkYWRvc1Byb2R1dG8pe1xyXG4gICAgaW5pdGlhbEltYWdlcyA9IGdldEluaXRpYWxJbWFnZShkYWRvc1Byb2R1dG8pO1xyXG4gIH1cclxuXHJcbiAgdmFyIHNlbGVjdGVkUXRkUGFyY2VsID0gMTtcclxuICB2YXIgc2VsZWN0ZWRWYWxQYXJjZWwgPSAxO1xyXG4gIHZhciBxdWFudGl0eUJ1eSA9IDE7XHJcbiAgdmFyIHNoaXBwaW5nTWV0aG9kcyA9IG51bGw7XHJcbiAgdmFyIHNlbGVjdGVkT3B0aW9uQnV5ID0gMTtcclxuICBcclxuICBmdW5jdGlvbiBnZXRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvKSB7XHJcbiAgICB2YXIgaW5pdGlhbFNwZWNpYWxWYWx1ZSA9IG51bGw7XHJcbiAgICBpZihkYWRvc1Byb2R1dG8gIT0gdW5kZWZpbmVkKXtcclxuICAgICAgaWYocGFyc2VJbnQoZGFkb3NQcm9kdXRvLmhhc19vcHRpb24pID09IDApeyAvLyBQUk9EVVRPIFNFTSBPUENPRVNcclxuICAgICAgICBpZihkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgICBpbml0aWFsU3BlY2lhbFZhbHVlID0gZGFkb3NQcm9kdXRvLnNwZWNpYWw7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICBpbml0aWFsU3BlY2lhbFZhbHVlID0gZGFkb3NQcm9kdXRvLnByaWNlO1xyXG4gICAgICAgIH1cclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgaWYoaW5pdGlhbFNwZWNpYWxWYWx1ZSA9PSBudWxsICYmIGRhZG9zUHJvZHV0by5zcGVjaWFsKXtcclxuICAgICAgICAgIGluaXRpYWxTcGVjaWFsVmFsdWUgPSBkYWRvc1Byb2R1dG8uc3BlY2lhbFxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBjb25zdCBzcGVjaWFsVmFsdWUgPSB1c2VTdGF0ZShpbml0aWFsU3BlY2lhbFZhbHVlKTtcclxuICAgIHJldHVybiBzcGVjaWFsVmFsdWU7XHJcbiAgfVxyXG4gIGNvbnN0IFtzcGVjaWFsVmFsdWUsIHNldFNwZWNpYWxWYWx1ZV0gPSBnZXRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvKTtcclxuXHJcbiAgdmFyIFttYWluVmFsdWUsIHNldE1haW5WYWx1ZV0gPSB1c2VTdGF0ZShudWxsKTtcclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgZnVuY3Rpb24gZ2V0TWFpblZhbHVlKGRhZG9zUHJvZHV0bykge1xyXG4gICAgICB2YXIgaW5pdGlhbE1haW5WYWx1ZSA9IG51bGw7XHJcbiAgICAgIGlmKGRhZG9zUHJvZHV0byAhPSB1bmRlZmluZWQpe1xyXG4gICAgICAgIGlmKHBhcnNlSW50KGRhZG9zUHJvZHV0by5oYXNfb3B0aW9uKSA9PSAwKXsgLy8gUFJPRFVUTyBTRU0gT1BDT0VTXHJcbiAgICAgICAgICBpZihkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgICAgIGluaXRpYWxNYWluVmFsdWUgPSBkYWRvc1Byb2R1dG8ucHJpY2U7XHJcbiAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgaW5pdGlhbE1haW5WYWx1ZSA9IG51bGw7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICBpZihpbml0aWFsTWFpblZhbHVlID09IG51bGwgJiYgZGFkb3NQcm9kdXRvLnByaWNlKXtcclxuICAgICAgICAgICAgaW5pdGlhbE1haW5WYWx1ZSA9IGRhZG9zUHJvZHV0by5wcmljZVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBcclxuICAgICAgc2V0TWFpblZhbHVlKGluaXRpYWxNYWluVmFsdWUpO1xyXG4gICAgfVxyXG4gICAgZ2V0TWFpblZhbHVlKGRhZG9zUHJvZHV0byk7XHJcbiAgfSlcclxuXHJcbiAgY29uc3QgW3NvbGRPdXRTdG9jaywgc2V0U29sZE91dFN0b2NrXSA9IHVzZVN0YXRlKCgpID0+IHtcclxuICAgIGNvbnN0IGluaXRpYWxTdGF0ZSA9IDA7XHJcbiAgICByZXR1cm4gaW5pdGlhbFN0YXRlO1xyXG4gIH0pO1xyXG4gIFxyXG4gIFxyXG4gIC8qaWYocGFyc2VJbnQoZGFkb3NQcm9kdXRvLmhhc19vcHRpb24pID09IDApeyAvLyBQUk9EVVRPIFNFTSBPUENPRVNcclxuICAgICAgXHJcbiAgICBjb25zdCB2YWxvckF0dWFsID0gZGFkb3NQcm9kdXRvLnByaWNlXHJcbiAgICBpZihkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgIHNldFNlbGVjdGVkTWFpblZhbHVlKHZhbG9yQXR1YWwpO1xyXG4gICAgICBzZXRTZWxlY3RlZFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8uc3BlY2lhbCk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgc2V0U2VsZWN0ZWRNYWluVmFsdWUobnVsbCk7XHJcbiAgICAgIHNldFNlbGVjdGVkU3BlY2lhbFZhbHVlKHZhbG9yQXR1YWwpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmKCFkYWRvc1Byb2R1dG8uc29sZF9vdXQpe1xyXG4gICAgICBzZXRTb2xkT3V0U3RvY2soMCk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgc2V0U29sZE91dFN0b2NrKDEpO1xyXG4gICAgfVxyXG4gIH1lbHNleyAvLyBQUk9EVVRPIENPTSBPUENPRVNcclxuICAgIHNldFNvbGRPdXRTdG9jaygwKTtcclxuXHJcbiAgICAgIGlmKHNlbGVjdGVkTWFpblZhbHVlID09IG51bGwpe1xyXG4gICAgICAgIHNldFNlbGVjdGVkTWFpblZhbHVlKGRhZG9zUHJvZHV0by5wcmljZSlcclxuICAgICAgfVxyXG4gICAgICBpZihzZWxlY3RlZFNwZWNpYWxWYWx1ZSA9PSBudWxsICYmIGRhZG9zUHJvZHV0by5zcGVjaWFsKXtcclxuICAgICAgICBzZXRTZWxlY3RlZFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8uc3BlY2lhbClcclxuICAgICAgfSovXHJcblxyXG4gICAgICAvKmZvcih2YXIgaSBpbiBkYWRvc1Byb2R1dG8ub3B0aW9ucyl7XHJcbiAgICAgICAgZ3J1cG9BdHVhbCA9IGRhZG9zUHJvZHV0by5vcHRpb25zW2ldXHJcbiAgICAgICAgZm9yKHZhciBvcHQgaW4gZ3J1cG9BdHVhbC5vcHRpb25fdmFsdWUpe1xyXG4gICAgICAgICAgaWYoZ3J1cG9BdHVhbC5vcHRpb25fdmFsdWVbb3B0XS5vcHRpb25fdmFsdWVfaWQ9PWRhZG9zUHJvZHV0by5vcGNhb19zZWxlY2lvbmFkYSl7XHJcbiAgICAgICAgICAgIGluaXRpYWxPcHRpb24gPSBncnVwb0F0dWFsLnByb2R1Y3Rfb3B0aW9uX2lkKydfJytncnVwb0F0dWFsLm9wdGlvbl92YWx1ZVtvcHRdLnByb2R1Y3Rfb3B0aW9uX3ZhbHVlX2lkXHJcbiAgICAgICAgICAgIHRoaXMuU2VsZWN0T3B0aW9uKGluaXRpYWxPcHRpb24sIG51bGwpXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gIH0qL1xyXG4gXHJcbiAgdmFyIGNoYW5nZVpvb20gPSAoZXYpID0+IHtcclxuICAgIGNvbnN0IGltZ1Nob3cgPSBldi5jdXJyZW50VGFyZ2V0LmRhdGFzZXQuc3Jjc2hvd1xyXG4gICAgY29uc3QgaW1nUG9wdXAgPSBldi5jdXJyZW50VGFyZ2V0LmRhdGFzZXQuc3JjcG9wdXBcclxuICAgIHZhciB0ZW1wSW1hZ2UgPSB7fTtcclxuICAgIHRlbXBJbWFnZVsnc2hvdyddID0gaW1nU2hvd1xyXG4gICAgdGVtcEltYWdlWydwb3B1cCddID0gaW1nUG9wdXBcclxuICAgIC8vc2V0SW5pdGlhbEltYWdlKHRlbXBJbWFnZSk7XHJcbiAgICBcclxuICB9XHJcbiAgXHJcbiAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcbiAgaWYgKCFyb3V0ZXIuaXNGYWxsYmFjayAmJiAhZGFkb3NQcm9kdXRvPy5wcm9kdWN0X2lkKSB7XHJcbiAgICByZXR1cm4gPEVycm9yUGFnZSBzdGF0dXNDb2RlPXs0MDR9IC8+O1xyXG4gIH1cclxuICByZXR1cm4gKFxyXG4gICAgPExheW91dD5cclxuICAgICAgPENvbnRhaW5lcj5cclxuICAgICAgXHJcbiAgICAgICAge1xyXG4gICAgICAgIHJvdXRlci5pc0ZhbGxiYWNrID8gKFxyXG4gICAgICAgICAgPGRpdj5Mb2FkaW5n4oCmPC9kaXY+XHJcbiAgICAgICAgKSA6IChcclxuICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgIDxIZWFkPlxyXG4gICAgICAgICAgICAgIDx0aXRsZT57ZGFkb3NQcm9kdXRvLm5hbWV9PC90aXRsZT5cclxuICAgICAgICAgICAgICA8bWV0YSBuYW1lPVwiZGVzY3JpcHRpb25cIiBjb250ZW50PVwibGVsZWxlXCIgLz5cclxuICAgICAgICAgICAgICA8bWV0YSBuYW1lPVwib2c6aW1hZ2VcIiBjb250ZW50PVwibGlsaWxpXCIgLz5cclxuICAgICAgICAgICAgPC9IZWFkPlxyXG4gICAgICAgICAgICA8VGVtcGxhdGVIZWFkZXIgY29uZmlncz17Y29uZmlncy5yZXNwb3N0YX0gbWFpbk1lbnU9e21haW5NZW51LnJlc3Bvc3RhfT48L1RlbXBsYXRlSGVhZGVyPiBcclxuICAgICAgICAgICAgPGRpdiBjbGFzPVwibWFpbi1jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwYWdlLXByb2R1Y3RzXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1wYWRkaW5nIGxpZ2h0LWJhY2tncm91bmQgbnByb2R1Y3QtYnJlYWRjcnVtYlwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxvbCBjbGFzc05hbWU9XCJicmVhZGNydW1iXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT1cImJyZWFkY3J1bWItaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIvXCIgdGl0bGU9XCJQw6FnaW5hIGluaWNpYWxcIj5Ib21lPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT1cImJyZWFkY3J1bWItaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHtkYWRvc1Byb2R1dG8ubmFtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvb2w+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjcC1wcmV2aWV3M1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LXBhZ2VcIj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LWdhbGxlcnlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LXRodW1ibmFpbHMgXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2VzLWNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLmltYWdlcykgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubGVuZ3RoID4gNCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNsaWRlciBjbGFzc05hbWU9XCJzbGlkZXJUaHVtYnNcIiB7Li4uc2xpZGVyVGh1bWJzfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZS10aHVtYiBqcy1jYXJvdXNlbC1jb250cm9sLWl0ZW0gcG9pbnRlciBqcy1wcm9kdWN0LWltYWdlLXRodW1iXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ9e2RhZG9zUHJvZHV0by5uYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1zcmNzaG93PXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnNob3d9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLXNyY3BvcHVwPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCIxMFwiIGhlaWdodD1cIjEwXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9XCJcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8udmlkZW9zLmxlbmd0aCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLnZpZGVvcy5tYXAoKGl0ZW1WaWRlbywga2V5VmlkZW8pID0+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaXRlbS12aWRlb1wiIGtleT17a2V5VmlkZW99PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgZGF0YS12aWRlb2lkPXtpdGVtVmlkZW8uaWRfdmlkZW99PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvJHtpdGVtVmlkZW8uaWRfdmlkZW99L21xZGVmYXVsdC5qcGdgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXlvdXR1YmUtcGxheVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU2xpZGVyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2UtdGh1bWIganMtY2Fyb3VzZWwtY29udHJvbC1pdGVtIHBvaW50ZXIganMtcHJvZHVjdC1pbWFnZS10aHVtYlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWx0PXtkYWRvc1Byb2R1dG8ubmFtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1zcmNzaG93PXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnNob3d9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEtc3JjcG9wdXA9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPVwiMTBcIiBoZWlnaHQ9XCIxMFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9XCJcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8udmlkZW9zLmxlbmd0aCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by52aWRlb3MubWFwKChpdGVtVmlkZW8sIGtleVZpZGVvKSA9PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpdGVtLXZpZGVvXCIga2V5PXtrZXlWaWRlb30+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgZGF0YS12aWRlb2lkPXtpdGVtVmlkZW8uaWRfdmlkZW99PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17YGh0dHBzOi8vaW1nLnlvdXR1YmUuY29tL3ZpLyR7aXRlbVZpZGVvLmlkX3ZpZGVvfS9tcWRlZmF1bHQuanBnYH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEteW91dHViZS1wbGF5XCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LWltYWdlcyBtaW4td2lkdGgtNDE1cHhcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb21cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX3RvcF9sZWZ0LmltYWdlICE9IG51bGwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsYWJlbEl0ZW0gbGFiZWxUb3BMZWZ0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX3RvcF9sZWZ0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX3RvcF9yaWdodC5pbWFnZSAhPSBudWxsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGFiZWxJdGVtIGxhYmVsVG9wUmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fdG9wX3JpZ2h0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX2JvdHRvbV9sZWZ0LmltYWdlICE9IG51bGwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsYWJlbEl0ZW0gbGFiZWxCb3R0b21MZWZ0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX2JvdHRvbV9sZWZ0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX2JvdHRvbV9yaWdodC5pbWFnZSAhPSBudWxsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGFiZWxJdGVtIGxhYmVsQm90dG9tUmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fYm90dG9tX3JpZ2h0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFJlYWN0SW1hZ2VNYWduaWZ5IHsuLi57XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc21hbGxJbWFnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdDogZGFkb3NQcm9kdXRvLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNGbHVpZFdpZHRoOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYzogaW5pdGlhbEltYWdlcy5zaG93XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFyZ2VJbWFnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYzogaW5pdGlhbEltYWdlcy5wb3B1cCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogY29uZmlncy53aWR0aFpvb20sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBjb25maWdzLndpZHRoWm9vbVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fSAvPiAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWF4LXdpZHRoLTQxNHB4XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKSAmJiBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5sZW5ndGggPiAxKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2VzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTbGlkZXIgY2xhc3NOYW1lPVwic2xpZGVWaXRyaW5lXCIgey4uLnNldHRpbmdzTW9iaWxlU2xpZGV9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfSBhbHQ9e2RhZG9zUHJvZHV0by5uYW1lfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by52aWRlb3MubGVuZ3RoKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLnZpZGVvcy5tYXAoKGl0ZW1WaWRlbywga2V5VmlkZW8pID0+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tIGl0ZW0tdmlkZW9cIiBrZXk9e2tleVZpZGVvfT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBkYXRhLXZpZGVvaWQ9e2l0ZW1WaWRlby5pZF92aWRlb30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvJHtpdGVtVmlkZW8uaWRfdmlkZW99L21xZGVmYXVsdC5qcGdgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS15b3V0dWJlLXBsYXlcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TbGlkZXI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IChPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKSAmJiBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5sZW5ndGggPiAwKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9IGFsdD17ZGFkb3NQcm9kdXRvLm5hbWV9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLnZpZGVvcy5sZW5ndGgpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8udmlkZW9zLm1hcCgoaXRlbVZpZGVvLCBrZXlWaWRlbykgPT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb20gaXRlbS12aWRlb1wiIGtleT17a2V5VmlkZW99PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGRhdGEtdmlkZW9pZD17aXRlbVZpZGVvLmlkX3ZpZGVvfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2BodHRwczovL2ltZy55b3V0dWJlLmNvbS92aS8ke2l0ZW1WaWRlby5pZF92aWRlb30vbXFkZWZhdWx0LmpwZ2B9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXlvdXR1YmUtcGxheVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZHVjdC1pbmZvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LWFjdGlvbnMgY29udGFpbmVyLXBhZGRpbmcgY29udGFpbmVyLXBhZGRpbmctdG9wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LWhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGgxIGNsYXNzTmFtZT1cIm5wcm9kdWN0LXRpdGxlXCI+e2RhZG9zUHJvZHV0by5uYW1lfTwvaDE+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoZGFkb3NQcm9kdXRvLm1vZGVsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJpbmZvc1Byb2R1Y3RcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+PHNwYW4gY2xhc3NOYW1lPVwidGl0bGVJbmZvXCI+UkVGOjwvc3Bhbj4ge2RhZG9zUHJvZHV0by5tb2RlbH08L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmF0ZUJveFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwibGstYXZhbGlhclwiIG9uQ2xpY2s9XCJcIj5BdmFsaWFyIGFnb3JhPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGRhZG9zUHJvZHV0by5zaG9ydCAhPSAnJyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImluZm9zQXJlYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmVzdW1lUHJvZHVjdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXt7IF9faHRtbDogZGFkb3NQcm9kdXRvLnNob3J0IH19IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJ1eUFyZWFcIj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sU2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2RjdC1wcmljZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGxhbGFsYSkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwib2xkUHJpY2VcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIERlIDxzcGFuIGNsYXNzTmFtZT1cIm5wcm9kdWN0LXByaWNlLW1heFwiPntsZWxlbGV9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LXByaWNlLXZhbHVlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhsYWxhbGEpID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBvciA8c3BhbiBjbGFzc05hbWU9XCJzcGVjaWFsVmFsdWVcIj57bGFsYWxhfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJpdGVtLWRpc2NvdW50XCI+e2RhZG9zUHJvZHV0by5kaXNjb3VudF9wZXJjZW50fTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPnBvciA8c3Bhbj57bGVsZWxlfTwvc3Bhbj48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoc2VsZWN0ZWRRdGRQYXJjZWwgIT0gJycgJiYgc2VsZWN0ZWRWYWxQYXJjZWwgIT0gJycpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInNlbGVjdGVkUGFyY2VsXCI+T3UgPHNwYW4gY2xhc3NOYW1lPVwibnVtUGFyY1wiPntzZWxlY3RlZFF0ZFBhcmNlbH14PC9zcGFuPiBkZSA8c3BhbiBjbGFzc05hbWU9XCJ2YWxQYXJjXCI+e3NlbGVjdGVkVmFsUGFyY2VsfTwvc3Bhbj48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8gIT0gJycpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAocGFyc2VJbnQoZGFkb3NQcm9kdXRvLmhhc19vcHRpb24pICE9IDApID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwib3B0aW9uc0FyZWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by5vcHRpb25zLm1hcCgoaXRlbSwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJveC1vcHRpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbD57aXRlbS5uYW1lfTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoaXRlbS50eXBlID09ICd0ZXh0Jyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidHh0T3B0aW9uXCI+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgaWQ9XCJmaWVsZE9wdGlvblwiIG1heGxlbmd0aD1cIjNcIiB0eXBlPVwidGV4dFwiIGRhdGEtZ3JvdXA9e2l0ZW0ucHJvZHVjdF9vcHRpb25faWR9IGNsYXNzTmFtZT1cImZpZWxkXCIgbmFtZT1cInR4dC1vcHRpb25cIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVscFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1xdWVzdGlvbi1jaXJjbGUgY29sb3IyXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpbmZvSGVscFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgSW5zaXJhIGF0w6kgMyBsZXRyYXMgcGFyYSBwZXJzb25hbGl6YXIgYSBjYW1pc2EgY29tIHVtIGJvcmRhZG8gZXhjbHVzaXZvLiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJsaXN0T3B0aW9uc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0ub3B0aW9uX3ZhbHVlLm1hcCgoaXRlbU9wdGlvbiwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9e3NlbGVjdGVkT3B0aW9uQnV5ICE9PSBpdGVtLnByb2R1Y3Rfb3B0aW9uX2lkKydfJytpdGVtT3B0aW9uLnByb2R1Y3Rfb3B0aW9uX3ZhbHVlX2lkID8gJ29wdGlvbicgOiAnb3B0aW9uIHNlbGVjdGVkJ30gb25DbGljaz1cIlwiIGhyZWY9XCJqYXZhc2NyaXB0OjtcIj57aXRlbU9wdGlvbi5uYW1lfTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogICcnICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sU2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJxdWFudGl0eUFyZWFcIj4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbD5RdWFudGlkYWRlPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV0dG9uc1F1YW50aXR5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGNsYXNzTmFtZT1cImJ0bkxlc3NcIj48aSBjbGFzc05hbWU9XCJmYSBmYS1taW51c1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT48L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgaWQ9XCJ0eHRRdWFudGl0eVwiIHR5cGU9XCJ0ZXh0XCIgbmFtZT1cInR4dC1xdWFudGl0eVwiIHZhbHVlPXsgcXVhbnRpdHlCdXkgfSBjbGFzc05hbWU9XCJ0eHRRdWFudGl0eVwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGNsYXNzTmFtZT1cImJ0bk1vcmVcIj48aSBjbGFzc05hbWU9XCJmYSBmYS1wbHVzXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhzb2xkT3V0U3RvY2sgIT0gbnVsbCAmJiAhc29sZE91dFN0b2NrKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXlCdXR0b25BcmVhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBjbGFzc05hbWU9XCJidXlCdXR0b24gYnRuX2J1eVwiIG9uQ2xpY2s9XCJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhIGZhLXNob3BwaW5nLWNhcnRcIj48L2k+IHsnQ29tcHJhcid9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IChzb2xkT3V0U3RvY2sgIT0gbnVsbCAmJiBzb2xkT3V0U3RvY2spP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJ1eUJ1dHRvbkFyZWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIGNsYXNzTmFtZT1cImJ1eUJ1dHRvbiBub3RpZnlCdXR0b25cIiBkYXRhLXByb2R1Y3RpZD17ZGFkb3NQcm9kdXRvLnByb2R1Y3RfaWR9IG9uQ2xpY2s9XCJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtZW52ZWxvcGVcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+IEF2aXNlLW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGRhZG9zUHJvZHV0by50ZXh0X3ByZXZlbmRhIT0gbnVsbCAmJiBkYWRvc1Byb2R1dG8udGV4dF9wcmV2ZW5kYSAhPSAnJyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImNoZWNrUHJldmVuZGFcIj48aW5wdXQgaWQ9XCJja3ByZXZlbmRhXCIgdHlwZT1cImNoZWNrYm94XCIgb25DaGFuZ2U9XCJcIiAvPiBDb25jb3JkbyBjb20gbyBwcmF6byBkZSBlbnRyZWdhIGRlc2NyaXRvIGFiYWl4by48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImluZm9QcmV2ZW5kYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwidGl0XCI+VEVSTU8gREUgQUNFSVRBw4fDg088L2g0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ZGFkb3NQcm9kdXRvLnRleHRfcHJldmVuZGF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhkYWRvc1Byb2R1dG8uZ3VpYV9tZWRpZGFzKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJndWlhc01lZGlkYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8uZ3VpYV9tZWRpZGFzLm1hcCgoaXRlbUd1aWEsIGtleUd1aWEpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwiaXRlbUd1aWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cImNvbG9yMlwiIHRpdGxlPXtpdGVtR3VpYS50aXRsZX0gZGF0YS10aXR1bG9ndWlhPVwiXCIgZGF0YS1jb250ZXVkb2d1aWE9XCJcIiBvbkNsaWNrPVwiXCI+e2l0ZW1HdWlhLnRpdGxlfTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxUZW1wbGF0ZUZvb3RlciBjb25maWdzPXtjb25maWdzLnJlc3Bvc3RhfSAvPlxyXG4gICAgICAgICAgPC8+XHJcbiAgICAgICAgKX1cclxuICAgICAgPC9Db250YWluZXI+XHJcbiAgICA8L0xheW91dD5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFN0YXRpY1Byb3BzKHsgcGFyYW1zIH0pIHtcclxuICB2YXIgY29uZmlncyA9IGF3YWl0IGdldENvbmZpZ3MoKTtcclxuICB2YXIgbWFpbk1lbnUgPSBhd2FpdCBnZXRNYWluTWVudSgpO1xyXG4gIHZhciByZXNQcm9kdWN0ID0gYXdhaXQgZ2V0UHJvZHVjdEJ5U2x1ZyhwYXJhbXMuc2x1Zyk7XHJcblxyXG4gIHZhciBkYWRvc1Byb2R1dG8gPSBudWxsO1xyXG4gIGlmKHJlc1Byb2R1Y3Quc3VjY2Vzcyl7XHJcbiAgICBkYWRvc1Byb2R1dG8gPSByZXNQcm9kdWN0LnJlc3Bvc3RhLnByb2R1dG87XHJcbiAgfVxyXG4gIHJldHVybiB7XHJcbiAgICBwcm9wczoge1xyXG4gICAgICBjb25maWdzLFxyXG4gICAgICBkYWRvc1Byb2R1dG8sXHJcbiAgICAgIG1haW5NZW51XHJcbiAgICB9LFxyXG4gICAgcmV2YWxpZGF0ZTogNjBcclxuICB9O1xyXG59XHJcblxyXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0U3RhdGljUGF0aHMoKSB7XHJcbiAgY29uc3QgcHJvZHV0b3MgPSBhd2FpdCBnZXRQcm9kdWN0c0J5Q2F0ZWdvcnkoKTtcclxuXHJcbiAgcmV0dXJuIHtcclxuICAgIHBhdGhzOlxyXG4gICAgICBbXSxcclxuICAgIGZhbGxiYWNrOiB0cnVlLFxyXG4gIH07XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==