webpackHotUpdate_N_E("pages/[slug]",{

/***/ "./pages/[slug].js":
/*!*************************!*\
  !*** ./pages/[slug].js ***!
  \*************************/
/*! exports provided: __N_SSG, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__N_SSG", function() { return __N_SSG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Product; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/error */ "./node_modules/next/error.js");
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_error__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_image_magnify__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-image-magnify */ "./node_modules/react-image-magnify/dist/es/ReactImageMagnify.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_container__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/components/container */ "./components/container.js");
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @/components/layout */ "./components/layout.js");





var _jsxFileName = "C:\\Users\\sauln49\\Desktop\\nextjs\\pages\\[slug].js",
    _s2 = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }










var idStoreApp = 'n49shopv2_trijoia';
var TemplateHeader = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c = function _c() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/header$")("./" + idStoreApp + "/components/header");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/header$").resolve("./" + idStoreApp + "/components/header"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/header']
  }
});
_c2 = TemplateHeader;
var TemplateFooter = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c3 = function _c3() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/footer$")("./" + idStoreApp + "/components/footer");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/footer$").resolve("./" + idStoreApp + "/components/footer"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/footer']
  }
});
_c4 = TemplateFooter;
var sliderThumbs = {
  infinite: false,
  vertical: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  speed: 500,
  responsive: [{
    breakpoint: 415,
    settings: {
      vertical: false,
      slidesToShow: 3
    }
  }]
};
var settingsMobileSlide = {
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  dots: true
};
var __N_SSG = true;
function Product(_ref) {
  _s2();

  var _s = $RefreshSig$(),
      _this = this;

  var configs = _ref.configs,
      mainMenu = _ref.mainMenu,
      dadosProduto = _ref.dadosProduto;
  var initialImages = [];

  function getInitialImage(dadosProduto) {
    var imgInicial = [];
    var tempImgInicial = [];

    for (var i in dadosProduto.images) {
      tempImgInicial.push([i, dadosProduto.images[i]]);
    }

    var initialKey = dadosProduto.image.substring(dadosProduto.image.lastIndexOf('/') + 1);

    if (dadosProduto.images != undefined && dadosProduto.images[initialKey] != null) {
      imgInicial['show'] = dadosProduto.images[initialKey].show;
      imgInicial['popup'] = dadosProduto.images[initialKey].popup;
      imgInicial['thumb'] = dadosProduto.images[initialKey].thumb;
    } else if (tempImgInicial[0] != null) {
      imgInicial = tempImgInicial[0][1];
    }

    return imgInicial;
  }

  if (dadosProduto) {
    initialImages = getInitialImage(dadosProduto);
  }

  var selectedQtdParcel = 1;
  var selectedValParcel = 1;
  var quantityBuy = 1;
  var shippingMethods = null;
  var selectedOptionBuy = 1;

  function getSpecialValue(dadosProduto) {
    _s();

    var initialSpecialValue = null;

    if (dadosProduto != undefined) {
      if (parseInt(dadosProduto.has_option) == 0) {
        // PRODUTO SEM OPCOES
        if (dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        } else {
          initialSpecialValue = dadosProduto.price;
        }
      } else {
        if (initialSpecialValue == null && dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        }
      }
    }

    var specialValue = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(initialSpecialValue);
    return specialValue;
  }

  _s(getSpecialValue, "uOCI/u7Rhc0OEPJW7t3Rh1DIVCE=");

  var _getSpecialValue = getSpecialValue(dadosProduto),
      _getSpecialValue2 = Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_getSpecialValue, 2),
      specialValue = _getSpecialValue2[0],
      setSpecialValue = _getSpecialValue2[1];

  var lalala = 0;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(null),
      mainValue = _useState[0],
      setMainValue = _useState[1];

  Object(react__WEBPACK_IMPORTED_MODULE_7__["useEffect"])(function () {
    function getMainValue(dadosProduto) {
      var initialMainValue = null;

      if (dadosProduto != undefined) {
        if (parseInt(dadosProduto.has_option) == 0) {
          // PRODUTO SEM OPCOES
          if (dadosProduto.special) {
            initialMainValue = dadosProduto.price;
          } else {
            initialMainValue = null;
          }
        } else {
          if (initialMainValue == null && dadosProduto.price) {
            initialMainValue = dadosProduto.price;
          }
        }

        setMainValue(initialMainValue);
      }
    }

    getMainValue(dadosProduto);
  });

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(function () {
    var initialState = 0;
    return initialState;
  }),
      soldOutStock = _useState2[0],
      setSoldOutStock = _useState2[1];
  /*if(parseInt(dadosProduto.has_option) == 0){ // PRODUTO SEM OPCOES
      
    const valorAtual = dadosProduto.price
    if(dadosProduto.special){
      setSelectedMainValue(valorAtual);
      setSelectedSpecialValue(dadosProduto.special);
    }else{
      setSelectedMainValue(null);
      setSelectedSpecialValue(valorAtual);
    }
      if(!dadosProduto.sold_out){
      setSoldOutStock(0);
    }else{
      setSoldOutStock(1);
    }
  }else{ // PRODUTO COM OPCOES
    setSoldOutStock(0);
        if(selectedMainValue == null){
        setSelectedMainValue(dadosProduto.price)
      }
      if(selectedSpecialValue == null && dadosProduto.special){
        setSelectedSpecialValue(dadosProduto.special)
      }*/

  /*for(var i in dadosProduto.options){
    grupoAtual = dadosProduto.options[i]
    for(var opt in grupoAtual.option_value){
      if(grupoAtual.option_value[opt].option_value_id==dadosProduto.opcao_selecionada){
        initialOption = grupoAtual.product_option_id+'_'+grupoAtual.option_value[opt].product_option_value_id
        this.SelectOption(initialOption, null)
      }
    }
  }
        
  }*/


  var changeZoom = function changeZoom(ev) {
    var imgShow = ev.currentTarget.dataset.srcshow;
    var imgPopup = ev.currentTarget.dataset.srcpopup;
    var tempImage = {};
    tempImage['show'] = imgShow;
    tempImage['popup'] = imgPopup; //setInitialImage(tempImage);
  };

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])();

  if (!router.isFallback && !(dadosProduto !== null && dadosProduto !== void 0 && dadosProduto.product_id)) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_error__WEBPACK_IMPORTED_MODULE_4___default.a, {
      statusCode: 404
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 176,
      columnNumber: 12
    }, this);
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_layout__WEBPACK_IMPORTED_MODULE_11__["default"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_container__WEBPACK_IMPORTED_MODULE_10__["default"], {
      children: router.isFallback ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        children: "Loading\u2026"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 184,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_5___default.a, {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
            children: dadosProduto.name
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 188,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "description",
            content: "lelele"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 189,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "og:image",
            content: "lilili"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 190,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 187,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateHeader, {
          configs: configs.resposta,
          mainMenu: mainMenu.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 192,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          clas: "main-content",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "page-products",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "container-padding light-background nproduct-breadcrumb",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "container",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ol", {
                  className: "breadcrumb",
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                      href: "/",
                      title: "P\xE1gina inicial",
                      children: "Home"
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 199,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 198,
                    columnNumber: 24
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: dadosProduto.name
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 201,
                    columnNumber: 24
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 197,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                  className: "cp-preview3",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                    className: "container",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                      className: "nproduct-page",
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-gallery",
                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-thumbnails ",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images-container",
                            children: dadosProduto.images ? Object.keys(dadosProduto.images).length > 4 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "sliderThumbs"
                            }, sliderThumbs), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 222,
                                    columnNumber: 57
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 219,
                                  columnNumber: 53
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 236,
                                      columnNumber: 51
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 237,
                                      columnNumber: 51
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 235,
                                    columnNumber: 49
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 234,
                                  columnNumber: 47
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 215,
                              columnNumber: 39
                            }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 253,
                                    columnNumber: 53
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 250,
                                  columnNumber: 49
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 268,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 269,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 267,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 266,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }, void 0, true) : null
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 211,
                            columnNumber: 32
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 210,
                          columnNumber: 31
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-images min-width-415px",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "areaZoom",
                            children: [dadosProduto.labels.promo_top_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 287,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 286,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_top_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 295,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 294,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 303,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 302,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 311,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 310,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_image_magnify__WEBPACK_IMPORTED_MODULE_8__["default"], _objectSpread({}, {
                              smallImage: {
                                alt: dadosProduto.name,
                                isFluidWidth: true,
                                src: initialImages.show
                              },
                              largeImage: {
                                src: initialImages.popup,
                                width: configs.widthZoom,
                                height: configs.widthZoom
                              }
                            }), void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 317,
                              columnNumber: 37
                            }, this)]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 283,
                            columnNumber: 35
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 282,
                          columnNumber: 33
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "max-width-414px",
                          children: Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 1 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "slideVitrine"
                            }, settingsMobileSlide), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 340,
                                    columnNumber: 49
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 339,
                                  columnNumber: 47
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 349,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 350,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 348,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 347,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 336,
                              columnNumber: 37
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 335,
                            columnNumber: 35
                          }, this) : Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                            children: [Object.keys(dadosProduto.images).map(function (item, key) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                  src: dadosProduto.images[item].popup,
                                  alt: dadosProduto.name
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 364,
                                  columnNumber: 45
                                }, _this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 363,
                                columnNumber: 43
                              }, _this);
                            }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom item-video",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  onClick: "",
                                  "data-videoid": itemVideo.id_video,
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 373,
                                    columnNumber: 49
                                  }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-youtube-play",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 374,
                                    columnNumber: 49
                                  }, _this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 372,
                                  columnNumber: 47
                                }, _this)
                              }, keyVideo, false, {
                                fileName: _jsxFileName,
                                lineNumber: 371,
                                columnNumber: 45
                              }, _this);
                            }) : null]
                          }, void 0, true) : null
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 333,
                          columnNumber: 31
                        }, this)]
                      }, void 0, true, {
                        fileName: _jsxFileName,
                        lineNumber: 209,
                        columnNumber: 29
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-info",
                        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-actions container-padding container-padding-top",
                          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "nproduct-header",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
                              className: "nproduct-title",
                              children: dadosProduto.name
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 390,
                              columnNumber: 38
                            }, this), dadosProduto.model ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              className: "infosProduct",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "titleInfo",
                                  children: "REF:"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 394,
                                  columnNumber: 49
                                }, this), " ", dadosProduto.model]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 394,
                                columnNumber: 45
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 393,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "rateBox",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                className: "lk-avaliar",
                                onClick: "",
                                children: "Avaliar agora"
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 401,
                                columnNumber: 41
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 400,
                              columnNumber: 38
                            }, this), dadosProduto["short"] != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "infosArea",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "resumeProduct",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  dangerouslySetInnerHTML: {
                                    __html: dadosProduto["short"]
                                  }
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 408,
                                  columnNumber: 43
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 407,
                                columnNumber: 42
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 406,
                              columnNumber: 41
                            }, this) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 389,
                            columnNumber: 35
                          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "buyArea",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "nprodct-price",
                                children: [lalala ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "oldPrice",
                                  children: ["De ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "nproduct-price-max",
                                    children: mainValue
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 423,
                                    columnNumber: 50
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 422,
                                  columnNumber: 45
                                }, this) : '', /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "nproduct-price-value",
                                  children: lalala ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                                    children: ["Por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "specialValue",
                                      children: lalala
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 431,
                                      columnNumber: 53
                                    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "item-discount",
                                      children: dadosProduto.discount_percent
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 432,
                                      columnNumber: 48
                                    }, this)]
                                  }, void 0, true) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                    children: ["por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      children: mainValue
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 435,
                                      columnNumber: 54
                                    }, this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 435,
                                    columnNumber: 47
                                  }, this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 428,
                                  columnNumber: 43
                                }, this), selectedQtdParcel != '' && selectedValParcel != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                  className: "selectedParcel",
                                  children: ["Ou ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "numParc",
                                    children: [selectedQtdParcel, "x"]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 440,
                                    columnNumber: 78
                                  }, this), " de ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "valParc",
                                    children: selectedValParcel
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 440,
                                    columnNumber: 135
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 440,
                                  columnNumber: 45
                                }, this) : null]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 420,
                                columnNumber: 39
                              }, this), dadosProduto != '' ? parseInt(dadosProduto.has_option) != 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "optionsArea",
                                children: dadosProduto.options.map(function (item, key) {
                                  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                    className: "box-option",
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                      children: item.name
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 453,
                                      columnNumber: 53
                                    }, _this), item.type == 'text' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                      className: "txtOption",
                                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                        id: "fieldOption",
                                        maxlength: "3",
                                        type: "text",
                                        "data-group": item.product_option_id,
                                        className: "field",
                                        name: "txt-option"
                                      }, void 0, false, {
                                        fileName: _jsxFileName,
                                        lineNumber: 457,
                                        columnNumber: 57
                                      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                        className: "help",
                                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                          className: "fa fa-question-circle color2",
                                          "aria-hidden": "true"
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 459,
                                          columnNumber: 61
                                        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                          className: "infoHelp",
                                          children: "Insira at\xE9 3 letras para personalizar a camisa com um bordado exclusivo."
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 461,
                                          columnNumber: 61
                                        }, _this)]
                                      }, void 0, true, {
                                        fileName: _jsxFileName,
                                        lineNumber: 458,
                                        columnNumber: 57
                                      }, _this)]
                                    }, void 0, true, {
                                      fileName: _jsxFileName,
                                      lineNumber: 456,
                                      columnNumber: 55
                                    }, _this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                                      className: "listOptions",
                                      children: item.option_value.map(function (itemOption, key) {
                                        return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                            className: selectedOptionBuy !== item.product_option_id + '_' + itemOption.product_option_value_id ? 'option' : 'option selected',
                                            onClick: "",
                                            href: "javascript:;",
                                            children: itemOption.name
                                          }, void 0, false, {
                                            fileName: _jsxFileName,
                                            lineNumber: 471,
                                            columnNumber: 63
                                          }, _this)
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 470,
                                          columnNumber: 61
                                        }, _this);
                                      })
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 467,
                                      columnNumber: 55
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 452,
                                    columnNumber: 51
                                  }, _this);
                                })
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 449,
                                columnNumber: 44
                              }, this) : '' : '']
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 419,
                              columnNumber: 37
                            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "quantityArea",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                  children: "Quantidade"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 489,
                                  columnNumber: 44
                                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "buttonsQuantity",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnLess",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-minus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 491,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 491,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                    id: "txtQuantity",
                                    type: "text",
                                    name: "txt-quantity",
                                    value: quantityBuy,
                                    className: "txtQuantity"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 492,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnMore",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-plus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 493,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 493,
                                    columnNumber: 47
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 490,
                                  columnNumber: 44
                                }, this)]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 488,
                                columnNumber: 41
                              }, this), soldOutStock != null && !soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton btn_buy",
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fas fa fa-shopping-cart"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 500,
                                    columnNumber: 52
                                  }, this), " ", 'Comprar']
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 499,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 498,
                                columnNumber: 43
                              }, this) : soldOutStock != null && soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton notifyButton",
                                  "data-productid": dadosProduto.product_id,
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-envelope",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 506,
                                    columnNumber: 52
                                  }, this), " Avise-me"]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 505,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 504,
                                columnNumber: 43
                              }, this) : null]
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 487,
                              columnNumber: 39
                            }, this), dadosProduto.text_prevenda != null && dadosProduto.text_prevenda != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                className: "checkPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                  id: "ckprevenda",
                                  type: "checkbox",
                                  onChange: ""
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 516,
                                  columnNumber: 70
                                }, this), " Concordo com o prazo de entrega descrito abaixo."]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 516,
                                columnNumber: 41
                              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "infoPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
                                  className: "tit",
                                  children: "TERMO DE ACEITA\xC7\xC3O"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 518,
                                  columnNumber: 43
                                }, this), dadosProduto.text_prevenda]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 517,
                                columnNumber: 41
                              }, this)]
                            }, void 0, true) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 417,
                            columnNumber: 35
                          }, this), dadosProduto.guia_medidas ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "guiasMedida",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              children: dadosProduto.guia_medidas.map(function (itemGuia, keyGuia) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                  className: "itemGuia",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    className: "color2",
                                    title: itemGuia.title,
                                    "data-tituloguia": "",
                                    "data-conteudoguia": "",
                                    onClick: "",
                                    children: itemGuia.title
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 534,
                                    columnNumber: 47
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 533,
                                  columnNumber: 43
                                }, _this);
                              })
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 530,
                              columnNumber: 39
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 529,
                            columnNumber: 37
                          }, this) : null]
                        }, void 0, true, {
                          fileName: _jsxFileName,
                          lineNumber: 388,
                          columnNumber: 33
                        }, this)
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 387,
                        columnNumber: 29
                      }, this)]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 207,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 206,
                    columnNumber: 23
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 205,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 196,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 195,
              columnNumber: 17
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 194,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 193,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateFooter, {
          configs: configs.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 559,
          columnNumber: 13
        }, this)]
      }, void 0, true)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 180,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 179,
    columnNumber: 5
  }, this);
}

_s2(Product, "4ziBSee6/0ZHo0Uq+ig8AtGtzxc=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"]];
});

_c5 = Product;
;

var _c, _c2, _c3, _c4, _c5;

$RefreshReg$(_c, "TemplateHeader$dynamic");
$RefreshReg$(_c2, "TemplateHeader");
$RefreshReg$(_c3, "TemplateFooter$dynamic");
$RefreshReg$(_c4, "TemplateFooter");
$RefreshReg$(_c5, "Product");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvW3NsdWddLmpzIl0sIm5hbWVzIjpbImlkU3RvcmVBcHAiLCJUZW1wbGF0ZUhlYWRlciIsImR5bmFtaWMiLCJUZW1wbGF0ZUZvb3RlciIsInNsaWRlclRodW1icyIsImluZmluaXRlIiwidmVydGljYWwiLCJzbGlkZXNUb1Nob3ciLCJzbGlkZXNUb1Njcm9sbCIsInNwZWVkIiwicmVzcG9uc2l2ZSIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsInNldHRpbmdzTW9iaWxlU2xpZGUiLCJkb3RzIiwiUHJvZHVjdCIsImNvbmZpZ3MiLCJtYWluTWVudSIsImRhZG9zUHJvZHV0byIsImluaXRpYWxJbWFnZXMiLCJnZXRJbml0aWFsSW1hZ2UiLCJpbWdJbmljaWFsIiwidGVtcEltZ0luaWNpYWwiLCJpIiwiaW1hZ2VzIiwicHVzaCIsImluaXRpYWxLZXkiLCJpbWFnZSIsInN1YnN0cmluZyIsImxhc3RJbmRleE9mIiwidW5kZWZpbmVkIiwic2hvdyIsInBvcHVwIiwidGh1bWIiLCJzZWxlY3RlZFF0ZFBhcmNlbCIsInNlbGVjdGVkVmFsUGFyY2VsIiwicXVhbnRpdHlCdXkiLCJzaGlwcGluZ01ldGhvZHMiLCJzZWxlY3RlZE9wdGlvbkJ1eSIsImdldFNwZWNpYWxWYWx1ZSIsImluaXRpYWxTcGVjaWFsVmFsdWUiLCJwYXJzZUludCIsImhhc19vcHRpb24iLCJzcGVjaWFsIiwicHJpY2UiLCJzcGVjaWFsVmFsdWUiLCJ1c2VTdGF0ZSIsInNldFNwZWNpYWxWYWx1ZSIsImxhbGFsYSIsIm1haW5WYWx1ZSIsInNldE1haW5WYWx1ZSIsInVzZUVmZmVjdCIsImdldE1haW5WYWx1ZSIsImluaXRpYWxNYWluVmFsdWUiLCJpbml0aWFsU3RhdGUiLCJzb2xkT3V0U3RvY2siLCJzZXRTb2xkT3V0U3RvY2siLCJjaGFuZ2Vab29tIiwiZXYiLCJpbWdTaG93IiwiY3VycmVudFRhcmdldCIsImRhdGFzZXQiLCJzcmNzaG93IiwiaW1nUG9wdXAiLCJzcmNwb3B1cCIsInRlbXBJbWFnZSIsInJvdXRlciIsInVzZVJvdXRlciIsImlzRmFsbGJhY2siLCJwcm9kdWN0X2lkIiwibmFtZSIsInJlc3Bvc3RhIiwiT2JqZWN0Iiwia2V5cyIsImxlbmd0aCIsIm1hcCIsIml0ZW0iLCJrZXkiLCJ2aWRlb3MiLCJpdGVtVmlkZW8iLCJrZXlWaWRlbyIsImlkX3ZpZGVvIiwibGFiZWxzIiwicHJvbW9fdG9wX2xlZnQiLCJwcm9tb190b3BfcmlnaHQiLCJwcm9tb19ib3R0b21fbGVmdCIsInByb21vX2JvdHRvbV9yaWdodCIsInNtYWxsSW1hZ2UiLCJhbHQiLCJpc0ZsdWlkV2lkdGgiLCJzcmMiLCJsYXJnZUltYWdlIiwid2lkdGgiLCJ3aWR0aFpvb20iLCJoZWlnaHQiLCJtb2RlbCIsIl9faHRtbCIsImRpc2NvdW50X3BlcmNlbnQiLCJvcHRpb25zIiwidHlwZSIsInByb2R1Y3Rfb3B0aW9uX2lkIiwib3B0aW9uX3ZhbHVlIiwiaXRlbU9wdGlvbiIsInByb2R1Y3Rfb3B0aW9uX3ZhbHVlX2lkIiwidGV4dF9wcmV2ZW5kYSIsImd1aWFfbWVkaWRhcyIsIml0ZW1HdWlhIiwia2V5R3VpYSIsInRpdGxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUdBLElBQUlBLFVBQVUsR0FBRyxtQkFBakI7QUFDQSxJQUFJQyxjQUFjLEdBQUdDLG1EQUFPLE1BQUM7QUFBQSxTQUFNLDhGQUFPLElBQXlCLEdBQUNGLFVBQTFCLEdBQXFDLG9CQUE1QyxDQUFOO0FBQUEsQ0FBRDtBQUFBO0FBQUE7QUFBQSxrQ0FBYywwR0FBeUIsR0FBQ0EsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxjQUFjLDRCQUEwQkEsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxFQUE1QjtNQUFJQyxjO0FBQ0osSUFBSUUsY0FBYyxHQUFHRCxtREFBTyxPQUFDO0FBQUEsU0FBTSw4RkFBTyxJQUF5QixHQUFDRixVQUExQixHQUFxQyxvQkFBNUMsQ0FBTjtBQUFBLENBQUQ7QUFBQTtBQUFBO0FBQUEsa0NBQWMsMEdBQXlCLEdBQUNBLFVBQTFCLEdBQXFDLG9CQUFuRDtBQUFBO0FBQUEsY0FBYyw0QkFBMEJBLFVBQTFCLEdBQXFDLG9CQUFuRDtBQUFBO0FBQUEsRUFBNUI7TUFBSUcsYztBQUVKLElBQUlDLFlBQVksR0FBRztBQUNqQkMsVUFBUSxFQUFFLEtBRE87QUFFakJDLFVBQVEsRUFBRSxJQUZPO0FBR2pCQyxjQUFZLEVBQUUsQ0FIRztBQUlqQkMsZ0JBQWMsRUFBRSxDQUpDO0FBS2pCQyxPQUFLLEVBQUUsR0FMVTtBQU1qQkMsWUFBVSxFQUFFLENBQ1Y7QUFDRUMsY0FBVSxFQUFFLEdBRGQ7QUFFRUMsWUFBUSxFQUFFO0FBQ1JOLGNBQVEsRUFBRSxLQURGO0FBRVJDLGtCQUFZLEVBQUU7QUFGTjtBQUZaLEdBRFU7QUFOSyxDQUFuQjtBQWlCQSxJQUFJTSxtQkFBbUIsR0FBRztBQUN4Qk4sY0FBWSxFQUFFLENBRFU7QUFFeEJDLGdCQUFjLEVBQUUsQ0FGUTtBQUd4QkMsT0FBSyxFQUFFLEdBSGlCO0FBSXhCSyxNQUFJLEVBQUU7QUFKa0IsQ0FBMUI7O0FBT2UsU0FBU0MsT0FBVCxPQUFzRDtBQUFBOztBQUFBO0FBQUE7O0FBQUEsTUFBbkNDLE9BQW1DLFFBQW5DQSxPQUFtQztBQUFBLE1BQTFCQyxRQUEwQixRQUExQkEsUUFBMEI7QUFBQSxNQUFoQkMsWUFBZ0IsUUFBaEJBLFlBQWdCO0FBQ25FLE1BQUlDLGFBQWEsR0FBRyxFQUFwQjs7QUFDQSxXQUFTQyxlQUFULENBQXlCRixZQUF6QixFQUF1QztBQUVyQyxRQUFJRyxVQUFVLEdBQUcsRUFBakI7QUFDQSxRQUFJQyxjQUFjLEdBQUcsRUFBckI7O0FBRUEsU0FBSSxJQUFJQyxDQUFSLElBQWFMLFlBQVksQ0FBQ00sTUFBMUIsRUFBaUM7QUFDL0JGLG9CQUFjLENBQUNHLElBQWYsQ0FBb0IsQ0FBQ0YsQ0FBRCxFQUFJTCxZQUFZLENBQUNNLE1BQWIsQ0FBcUJELENBQXJCLENBQUosQ0FBcEI7QUFDRDs7QUFFRCxRQUFJRyxVQUFVLEdBQUdSLFlBQVksQ0FBQ1MsS0FBYixDQUFtQkMsU0FBbkIsQ0FBNkJWLFlBQVksQ0FBQ1MsS0FBYixDQUFtQkUsV0FBbkIsQ0FBK0IsR0FBL0IsSUFBb0MsQ0FBakUsQ0FBakI7O0FBRUEsUUFBR1gsWUFBWSxDQUFDTSxNQUFiLElBQXVCTSxTQUF2QixJQUFvQ1osWUFBWSxDQUFDTSxNQUFiLENBQW9CRSxVQUFwQixLQUFtQyxJQUExRSxFQUErRTtBQUM3RUwsZ0JBQVUsQ0FBQyxNQUFELENBQVYsR0FBcUJILFlBQVksQ0FBQ00sTUFBYixDQUFvQkUsVUFBcEIsRUFBZ0NLLElBQXJEO0FBQ0FWLGdCQUFVLENBQUMsT0FBRCxDQUFWLEdBQXNCSCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JFLFVBQXBCLEVBQWdDTSxLQUF0RDtBQUNBWCxnQkFBVSxDQUFDLE9BQUQsQ0FBVixHQUFzQkgsWUFBWSxDQUFDTSxNQUFiLENBQW9CRSxVQUFwQixFQUFnQ08sS0FBdEQ7QUFDRCxLQUpELE1BSU0sSUFBR1gsY0FBYyxDQUFDLENBQUQsQ0FBZCxJQUFxQixJQUF4QixFQUE2QjtBQUNqQ0QsZ0JBQVUsR0FBR0MsY0FBYyxDQUFDLENBQUQsQ0FBZCxDQUFrQixDQUFsQixDQUFiO0FBQ0Q7O0FBRUQsV0FBT0QsVUFBUDtBQUNEOztBQUNELE1BQUdILFlBQUgsRUFBZ0I7QUFDZEMsaUJBQWEsR0FBR0MsZUFBZSxDQUFDRixZQUFELENBQS9CO0FBQ0Q7O0FBRUQsTUFBSWdCLGlCQUFpQixHQUFHLENBQXhCO0FBQ0EsTUFBSUMsaUJBQWlCLEdBQUcsQ0FBeEI7QUFDQSxNQUFJQyxXQUFXLEdBQUcsQ0FBbEI7QUFDQSxNQUFJQyxlQUFlLEdBQUcsSUFBdEI7QUFDQSxNQUFJQyxpQkFBaUIsR0FBRyxDQUF4Qjs7QUFFQSxXQUFTQyxlQUFULENBQXlCckIsWUFBekIsRUFBdUM7QUFBQTs7QUFDckMsUUFBSXNCLG1CQUFtQixHQUFHLElBQTFCOztBQUNBLFFBQUd0QixZQUFZLElBQUlZLFNBQW5CLEVBQTZCO0FBQzNCLFVBQUdXLFFBQVEsQ0FBQ3ZCLFlBQVksQ0FBQ3dCLFVBQWQsQ0FBUixJQUFxQyxDQUF4QyxFQUEwQztBQUFFO0FBQzFDLFlBQUd4QixZQUFZLENBQUN5QixPQUFoQixFQUF3QjtBQUN0QkgsNkJBQW1CLEdBQUd0QixZQUFZLENBQUN5QixPQUFuQztBQUNELFNBRkQsTUFFSztBQUNISCw2QkFBbUIsR0FBR3RCLFlBQVksQ0FBQzBCLEtBQW5DO0FBQ0Q7QUFDRixPQU5ELE1BTUs7QUFDSCxZQUFHSixtQkFBbUIsSUFBSSxJQUF2QixJQUErQnRCLFlBQVksQ0FBQ3lCLE9BQS9DLEVBQXVEO0FBQ3JESCw2QkFBbUIsR0FBR3RCLFlBQVksQ0FBQ3lCLE9BQW5DO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFFBQU1FLFlBQVksR0FBR0Msc0RBQVEsQ0FBQ04sbUJBQUQsQ0FBN0I7QUFDQSxXQUFPSyxZQUFQO0FBQ0Q7O0FBbkRrRSxLQWlDMUROLGVBakMwRDs7QUFBQSx5QkFvRDNCQSxlQUFlLENBQUNyQixZQUFELENBcERZO0FBQUE7QUFBQSxNQW9ENUQyQixZQXBENEQ7QUFBQSxNQW9EOUNFLGVBcEQ4Qzs7QUFzRG5FLE1BQUlDLE1BQU0sR0FBRyxDQUFiOztBQXREbUUsa0JBdURuQ0Ysc0RBQVEsQ0FBQyxJQUFELENBdkQyQjtBQUFBLE1BdUQ5REcsU0F2RDhEO0FBQUEsTUF1RG5EQyxZQXZEbUQ7O0FBd0RuRUMseURBQVMsQ0FBQyxZQUFNO0FBQ2QsYUFBU0MsWUFBVCxDQUFzQmxDLFlBQXRCLEVBQW9DO0FBQ2xDLFVBQUltQyxnQkFBZ0IsR0FBRyxJQUF2Qjs7QUFDQSxVQUFHbkMsWUFBWSxJQUFJWSxTQUFuQixFQUE2QjtBQUMzQixZQUFHVyxRQUFRLENBQUN2QixZQUFZLENBQUN3QixVQUFkLENBQVIsSUFBcUMsQ0FBeEMsRUFBMEM7QUFBRTtBQUMxQyxjQUFHeEIsWUFBWSxDQUFDeUIsT0FBaEIsRUFBd0I7QUFDdEJVLDRCQUFnQixHQUFHbkMsWUFBWSxDQUFDMEIsS0FBaEM7QUFDRCxXQUZELE1BRUs7QUFDSFMsNEJBQWdCLEdBQUcsSUFBbkI7QUFDRDtBQUNGLFNBTkQsTUFNSztBQUNILGNBQUdBLGdCQUFnQixJQUFJLElBQXBCLElBQTRCbkMsWUFBWSxDQUFDMEIsS0FBNUMsRUFBa0Q7QUFDaERTLDRCQUFnQixHQUFHbkMsWUFBWSxDQUFDMEIsS0FBaEM7QUFDRDtBQUNGOztBQUNETSxvQkFBWSxDQUFDRyxnQkFBRCxDQUFaO0FBQ0Q7QUFHRjs7QUFDREQsZ0JBQVksQ0FBQ2xDLFlBQUQsQ0FBWjtBQUNELEdBckJRLENBQVQ7O0FBeERtRSxtQkErRTNCNEIsc0RBQVEsQ0FBQyxZQUFNO0FBQ3JELFFBQU1RLFlBQVksR0FBRyxDQUFyQjtBQUNBLFdBQU9BLFlBQVA7QUFDRCxHQUgrQyxDQS9FbUI7QUFBQSxNQStFNURDLFlBL0U0RDtBQUFBLE1BK0U5Q0MsZUEvRThDO0FBcUZuRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUlNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVFLE1BQUlDLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUNDLEVBQUQsRUFBUTtBQUN2QixRQUFNQyxPQUFPLEdBQUdELEVBQUUsQ0FBQ0UsYUFBSCxDQUFpQkMsT0FBakIsQ0FBeUJDLE9BQXpDO0FBQ0EsUUFBTUMsUUFBUSxHQUFHTCxFQUFFLENBQUNFLGFBQUgsQ0FBaUJDLE9BQWpCLENBQXlCRyxRQUExQztBQUNBLFFBQUlDLFNBQVMsR0FBRyxFQUFoQjtBQUNBQSxhQUFTLENBQUMsTUFBRCxDQUFULEdBQW9CTixPQUFwQjtBQUNBTSxhQUFTLENBQUMsT0FBRCxDQUFULEdBQXFCRixRQUFyQixDQUx1QixDQU12QjtBQUVELEdBUkQ7O0FBVUEsTUFBTUcsTUFBTSxHQUFHQyw2REFBUyxFQUF4Qjs7QUFDQSxNQUFJLENBQUNELE1BQU0sQ0FBQ0UsVUFBUixJQUFzQixFQUFDbEQsWUFBRCxhQUFDQSxZQUFELGVBQUNBLFlBQVksQ0FBRW1ELFVBQWYsQ0FBMUIsRUFBcUQ7QUFDbkQsd0JBQU8scUVBQUMsaURBQUQ7QUFBVyxnQkFBVSxFQUFFO0FBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFBUDtBQUNEOztBQUNELHNCQUNFLHFFQUFDLDJEQUFEO0FBQUEsMkJBQ0UscUVBQUMsOERBQUQ7QUFBQSxnQkFHRUgsTUFBTSxDQUFDRSxVQUFQLGdCQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZ0JBR0U7QUFBQSxnQ0FDRSxxRUFBQyxnREFBRDtBQUFBLGtDQUNFO0FBQUEsc0JBQVFsRCxZQUFZLENBQUNvRDtBQUFyQjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURGLGVBRUU7QUFBTSxnQkFBSSxFQUFDLGFBQVg7QUFBeUIsbUJBQU8sRUFBQztBQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUZGLGVBR0U7QUFBTSxnQkFBSSxFQUFDLFVBQVg7QUFBc0IsbUJBQU8sRUFBQztBQUE5QjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQU1FLHFFQUFDLGNBQUQ7QUFBZ0IsaUJBQU8sRUFBRXRELE9BQU8sQ0FBQ3VELFFBQWpDO0FBQTJDLGtCQUFRLEVBQUV0RCxRQUFRLENBQUNzRDtBQUE5RDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQU5GLGVBT0U7QUFBSyxjQUFJLEVBQUMsY0FBVjtBQUFBLGlDQUNFO0FBQUsscUJBQVMsRUFBQyxlQUFmO0FBQUEsbUNBQ0U7QUFBSyx1QkFBUyxFQUFDLHdEQUFmO0FBQUEscUNBQ0U7QUFBSyx5QkFBUyxFQUFDLFdBQWY7QUFBQSx3Q0FDRTtBQUFJLDJCQUFTLEVBQUMsWUFBZDtBQUFBLDBDQUNHO0FBQUksNkJBQVMsRUFBQyxpQkFBZDtBQUFBLDJDQUNHO0FBQUcsMEJBQUksRUFBQyxHQUFSO0FBQVksMkJBQUssRUFBQyxtQkFBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESDtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQURILGVBSUc7QUFBSSw2QkFBUyxFQUFDLGlCQUFkO0FBQUEsOEJBQ0lyRCxZQUFZLENBQUNvRDtBQURqQjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQUpIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFERixlQVNFO0FBQUssMkJBQVMsRUFBQyxhQUFmO0FBQUEseUNBQ0U7QUFBSyw2QkFBUyxFQUFDLFdBQWY7QUFBQSwyQ0FDSTtBQUFLLCtCQUFTLEVBQUMsZUFBZjtBQUFBLDhDQUVFO0FBQUssaUNBQVMsRUFBQyxrQkFBZjtBQUFBLGdEQUNFO0FBQUssbUNBQVMsRUFBQyxxQkFBZjtBQUFBLGlEQUNDO0FBQUsscUNBQVMsRUFBQywwQkFBZjtBQUFBLHNDQUVJcEQsWUFBWSxDQUFDTSxNQUFkLEdBQ0dnRCxNQUFNLENBQUNDLElBQVAsQ0FBWXZELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNrRCxNQUFqQyxHQUEwQyxDQUEzQyxnQkFDRSxxRUFBQyxrREFBRDtBQUFRLHVDQUFTLEVBQUM7QUFBbEIsK0JBQXFDdEUsWUFBckM7QUFBQSx5Q0FHTW9FLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdkQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ21ELEdBQWpDLENBQXFDLFVBQUNDLElBQUQsRUFBT0MsR0FBUDtBQUFBLG9EQUM3QjtBQUNFLDJDQUFTLEVBQUMsNkVBRFo7QUFBQSx5REFHSTtBQUFLLHVDQUFHLEVBQUUzRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JvRCxJQUFwQixFQUEwQjVDLEtBQXBDO0FBQ0UsdUNBQUcsRUFBRWQsWUFBWSxDQUFDb0QsSUFEcEI7QUFFRSxvREFBY3BELFlBQVksQ0FBQ00sTUFBYixDQUFvQm9ELElBQXBCLEVBQTBCN0MsSUFGMUM7QUFHRSxxREFBZWIsWUFBWSxDQUFDTSxNQUFiLENBQW9Cb0QsSUFBcEIsRUFBMEI1QyxLQUgzQztBQUlFLHlDQUFLLEVBQUMsSUFKUjtBQUlhLDBDQUFNLEVBQUMsSUFKcEI7QUFLRSwyQ0FBTyxFQUFDO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRDZCO0FBQUEsK0JBQXJDLENBSE4sRUFpQktkLFlBQVksQ0FBQzRELE1BQWIsQ0FBb0JKLE1BQXJCLEdBQ0V4RCxZQUFZLENBQUM0RCxNQUFiLENBQW9CSCxHQUFwQixDQUF3QixVQUFDSSxTQUFELEVBQVlDLFFBQVo7QUFBQSxvREFDdEI7QUFBSywyQ0FBUyxFQUFDLFlBQWY7QUFBQSx5REFDRTtBQUFHLHdDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBTyxFQUFDLEVBQS9CO0FBQWtDLG9EQUFjRCxTQUFTLENBQUNFLFFBQTFEO0FBQUEsNERBQ0U7QUFBSyx5Q0FBRyx1Q0FBZ0NGLFNBQVMsQ0FBQ0UsUUFBMUM7QUFBUjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQURGLGVBRUU7QUFBRywrQ0FBUyxFQUFDLG9CQUFiO0FBQWtDLHFEQUFZO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREYsbUNBQWlDRCxRQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQURzQjtBQUFBLCtCQUF4QixDQURGLEdBVUUsSUEzQk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLGdCQWlDRTtBQUFBLHlDQUVFUixNQUFNLENBQUNDLElBQVAsQ0FBWXZELFlBQVksQ0FBQ00sTUFBekIsRUFBaUNtRCxHQUFqQyxDQUFxQyxVQUFDQyxJQUFELEVBQU9DLEdBQVA7QUFBQSxvREFDN0I7QUFDRSwyQ0FBUyxFQUFDLDZFQURaO0FBQUEseURBR0k7QUFBSyx1Q0FBRyxFQUFFM0QsWUFBWSxDQUFDTSxNQUFiLENBQW9Cb0QsSUFBcEIsRUFBMEI1QyxLQUFwQztBQUNFLHVDQUFHLEVBQUVkLFlBQVksQ0FBQ29ELElBRHBCO0FBRUUsb0RBQWNwRCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JvRCxJQUFwQixFQUEwQjdDLElBRjFDO0FBR0UscURBQWViLFlBQVksQ0FBQ00sTUFBYixDQUFvQm9ELElBQXBCLEVBQTBCNUMsS0FIM0M7QUFJRSx5Q0FBSyxFQUFDLElBSlI7QUFJYSwwQ0FBTSxFQUFDLElBSnBCO0FBS0UsMkNBQU8sRUFBQztBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFISjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQUQ2QjtBQUFBLCtCQUFyQyxDQUZGLEVBaUJHZCxZQUFZLENBQUM0RCxNQUFiLENBQW9CSixNQUFyQixHQUNFeEQsWUFBWSxDQUFDNEQsTUFBYixDQUFvQkgsR0FBcEIsQ0FBd0IsVUFBQ0ksU0FBRCxFQUFZQyxRQUFaO0FBQUEsb0RBQ3RCO0FBQUssMkNBQVMsRUFBQyxZQUFmO0FBQUEseURBQ0U7QUFBRyx3Q0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQU8sRUFBQyxFQUEvQjtBQUFrQyxvREFBY0QsU0FBUyxDQUFDRSxRQUExRDtBQUFBLDREQUNFO0FBQUsseUNBQUcsdUNBQWdDRixTQUFTLENBQUNFLFFBQTFDO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FERixlQUVFO0FBQUcsK0NBQVMsRUFBQyxvQkFBYjtBQUFrQyxxREFBWTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLG1DQUFpQ0QsUUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FEc0I7QUFBQSwrQkFBeEIsQ0FERixHQVVFLElBM0JKO0FBQUEsNENBbENKLEdBZ0VFO0FBbEVMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdDQURGLGVBeUVJO0FBQUssbUNBQVMsRUFBQyxnQ0FBZjtBQUFBLGlEQUNFO0FBQUsscUNBQVMsRUFBQyxVQUFmO0FBQUEsdUNBRUs5RCxZQUFZLENBQUNnRSxNQUFiLENBQW9CQyxjQUFwQixDQUFtQ3hELEtBQW5DLElBQTRDLElBQTdDLGdCQUNFO0FBQUssdUNBQVMsRUFBQyx3QkFBZjtBQUFBLHFEQUNFO0FBQUssbUNBQUcsRUFBRVQsWUFBWSxDQUFDZ0UsTUFBYixDQUFvQkMsY0FBcEIsQ0FBbUN4RDtBQUE3QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixHQUtFLElBUE4sRUFVS1QsWUFBWSxDQUFDZ0UsTUFBYixDQUFvQkUsZUFBcEIsQ0FBb0N6RCxLQUFwQyxJQUE2QyxJQUE5QyxnQkFDRTtBQUFLLHVDQUFTLEVBQUMseUJBQWY7QUFBQSxxREFDRTtBQUFLLG1DQUFHLEVBQUVULFlBQVksQ0FBQ2dFLE1BQWIsQ0FBb0JFLGVBQXBCLENBQW9DekQ7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREYsR0FLRSxJQWZOLEVBa0JLVCxZQUFZLENBQUNnRSxNQUFiLENBQW9CRyxpQkFBcEIsQ0FBc0MxRCxLQUF0QyxJQUErQyxJQUFoRCxnQkFDRTtBQUFLLHVDQUFTLEVBQUMsMkJBQWY7QUFBQSxxREFDRTtBQUFLLG1DQUFHLEVBQUVULFlBQVksQ0FBQ2dFLE1BQWIsQ0FBb0JHLGlCQUFwQixDQUFzQzFEO0FBQWhEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLEdBS0UsSUF2Qk4sRUEwQktULFlBQVksQ0FBQ2dFLE1BQWIsQ0FBb0JJLGtCQUFwQixDQUF1QzNELEtBQXZDLElBQWdELElBQWpELGdCQUNFO0FBQUssdUNBQVMsRUFBQyw0QkFBZjtBQUFBLHFEQUNFO0FBQUssbUNBQUcsRUFBRVQsWUFBWSxDQUFDZ0UsTUFBYixDQUFvQkksa0JBQXBCLENBQXVDM0Q7QUFBakQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREYsR0FLRSxJQS9CTixlQWtDRSxxRUFBQywyREFBRCxvQkFBdUI7QUFDZjRELHdDQUFVLEVBQUU7QUFDWkMsbUNBQUcsRUFBRXRFLFlBQVksQ0FBQ29ELElBRE47QUFFWm1CLDRDQUFZLEVBQUUsSUFGRjtBQUdaQyxtQ0FBRyxFQUFFdkUsYUFBYSxDQUFDWTtBQUhQLCtCQURHO0FBTW5CNEQsd0NBQVUsRUFBRTtBQUNSRCxtQ0FBRyxFQUFFdkUsYUFBYSxDQUFDYSxLQURYO0FBRVI0RCxxQ0FBSyxFQUFFNUUsT0FBTyxDQUFDNkUsU0FGUDtBQUdSQyxzQ0FBTSxFQUFFOUUsT0FBTyxDQUFDNkU7QUFIUjtBQU5PLDZCQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQWxDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdDQXpFSixlQTRIRTtBQUFLLG1DQUFTLEVBQUMsaUJBQWY7QUFBQSxvQ0FDSXJCLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdkQsWUFBWSxDQUFDTSxNQUF6QixLQUFvQ2dELE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdkQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ2tELE1BQWpDLEdBQTBDLENBQS9FLGdCQUNDO0FBQUsscUNBQVMsRUFBQyxnQkFBZjtBQUFBLG1EQUNFLHFFQUFDLGtEQUFEO0FBQVEsdUNBQVMsRUFBQztBQUFsQiwrQkFBcUM3RCxtQkFBckM7QUFBQSx5Q0FFTTJELE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdkQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ21ELEdBQWpDLENBQXFDLFVBQUNDLElBQUQsRUFBT0MsR0FBUDtBQUFBLG9EQUNqQztBQUFLLDJDQUFTLEVBQUMsVUFBZjtBQUFBLHlEQUNFO0FBQUssdUNBQUcsRUFBRTNELFlBQVksQ0FBQ00sTUFBYixDQUFvQm9ELElBQXBCLEVBQTBCNUMsS0FBcEM7QUFBMkMsdUNBQUcsRUFBRWQsWUFBWSxDQUFDb0Q7QUFBN0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRGlDO0FBQUEsK0JBQXJDLENBRk4sRUFTS3BELFlBQVksQ0FBQzRELE1BQWIsQ0FBb0JKLE1BQXJCLEdBQ0V4RCxZQUFZLENBQUM0RCxNQUFiLENBQW9CSCxHQUFwQixDQUF3QixVQUFDSSxTQUFELEVBQVlDLFFBQVo7QUFBQSxvREFDdEI7QUFBSywyQ0FBUyxFQUFDLHFCQUFmO0FBQUEseURBQ0U7QUFBRyx3Q0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQU8sRUFBQyxFQUEvQjtBQUFrQyxvREFBY0QsU0FBUyxDQUFDRSxRQUExRDtBQUFBLDREQUNFO0FBQUsseUNBQUcsdUNBQWdDRixTQUFTLENBQUNFLFFBQTFDO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FERixlQUVFO0FBQUcsK0NBQVMsRUFBQyxvQkFBYjtBQUFrQyxxREFBWTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLG1DQUEwQ0QsUUFBMUM7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FEc0I7QUFBQSwrQkFBeEIsQ0FERixHQVVFLElBbkJOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBREQsR0F5QkVSLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdkQsWUFBWSxDQUFDTSxNQUF6QixLQUFvQ2dELE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdkQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ2tELE1BQWpDLEdBQTBDLENBQS9FLGdCQUNFO0FBQUEsdUNBRUlGLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdkQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ21ELEdBQWpDLENBQXFDLFVBQUNDLElBQUQsRUFBT0MsR0FBUDtBQUFBLGtEQUNuQztBQUFLLHlDQUFTLEVBQUMsVUFBZjtBQUFBLHVEQUNFO0FBQUsscUNBQUcsRUFBRTNELFlBQVksQ0FBQ00sTUFBYixDQUFvQm9ELElBQXBCLEVBQTBCNUMsS0FBcEM7QUFBMkMscUNBQUcsRUFBRWQsWUFBWSxDQUFDb0Q7QUFBN0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUNBRG1DO0FBQUEsNkJBQXJDLENBRkosRUFTS3BELFlBQVksQ0FBQzRELE1BQWIsQ0FBb0JKLE1BQXJCLEdBQ0V4RCxZQUFZLENBQUM0RCxNQUFiLENBQW9CSCxHQUFwQixDQUF3QixVQUFDSSxTQUFELEVBQVlDLFFBQVo7QUFBQSxrREFDdEI7QUFBSyx5Q0FBUyxFQUFDLHFCQUFmO0FBQUEsdURBQ0U7QUFBRyxzQ0FBSSxFQUFDLGNBQVI7QUFBdUIseUNBQU8sRUFBQyxFQUEvQjtBQUFrQyxrREFBY0QsU0FBUyxDQUFDRSxRQUExRDtBQUFBLDBEQUNFO0FBQUssdUNBQUcsdUNBQWdDRixTQUFTLENBQUNFLFFBQTFDO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQ0FERixlQUVFO0FBQUcsNkNBQVMsRUFBQyxvQkFBYjtBQUFrQyxtREFBWTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDJDQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLGlDQUEwQ0QsUUFBMUM7QUFBQTtBQUFBO0FBQUE7QUFBQSx1Q0FEc0I7QUFBQSw2QkFBeEIsQ0FERixHQVVFLElBbkJOO0FBQUEsMENBREYsR0F3QkE7QUFsREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0E1SEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDhCQUZGLGVBb0xFO0FBQUssaUNBQVMsRUFBQyxlQUFmO0FBQUEsK0NBQ0k7QUFBSyxtQ0FBUyxFQUFDLHlEQUFmO0FBQUEsa0RBQ0U7QUFBSyxxQ0FBUyxFQUFDLGlCQUFmO0FBQUEsb0RBQ0c7QUFBSSx1Q0FBUyxFQUFDLGdCQUFkO0FBQUEsd0NBQWdDOUQsWUFBWSxDQUFDb0Q7QUFBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FESCxFQUdNcEQsWUFBWSxDQUFDNkUsS0FBZCxnQkFDQztBQUFJLHVDQUFTLEVBQUMsY0FBZDtBQUFBLHFEQUNJO0FBQUEsd0RBQUk7QUFBTSwyQ0FBUyxFQUFDLFdBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQUFKLE9BQTZDN0UsWUFBWSxDQUFDNkUsS0FBMUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERCxHQUtDLElBUk4sZUFXRztBQUFLLHVDQUFTLEVBQUMsU0FBZjtBQUFBLHFEQUNHO0FBQUcseUNBQVMsRUFBQyxZQUFiO0FBQTBCLHVDQUFPLEVBQUMsRUFBbEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESDtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQVhILEVBZ0JNN0UsWUFBWSxTQUFaLElBQXNCLEVBQXZCLGdCQUNDO0FBQUssdUNBQVMsRUFBQyxXQUFmO0FBQUEscURBQ0M7QUFBSyx5Q0FBUyxFQUFDLGVBQWY7QUFBQSx1REFDQztBQUFLLHlEQUF1QixFQUFFO0FBQUU4RSwwQ0FBTSxFQUFFOUUsWUFBWTtBQUF0QjtBQUE5QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREQsR0FPQyxJQXZCTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBREYsZUE2QkU7QUFBSyxxQ0FBUyxFQUFDLFNBQWY7QUFBQSxvREFFRTtBQUFLLHVDQUFTLEVBQUMsWUFBZjtBQUFBLHNEQUNFO0FBQUsseUNBQVMsRUFBQyxlQUFmO0FBQUEsMkNBQ004QixNQUFELGdCQUNDO0FBQUssMkNBQVMsRUFBQyxVQUFmO0FBQUEsaUVBQ0s7QUFBTSw2Q0FBUyxFQUFDLG9CQUFoQjtBQUFBLDhDQUFzQ0M7QUFBdEM7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FETDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBREQsR0FLQyxFQU5OLGVBUUk7QUFBSywyQ0FBUyxFQUFDLHNCQUFmO0FBQUEsNENBQ0lELE1BQUQsZ0JBQ0M7QUFBQSxvRUFDTTtBQUFNLCtDQUFTLEVBQUMsY0FBaEI7QUFBQSxnREFBZ0NBO0FBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUEsNENBRE4sZUFFQztBQUFNLCtDQUFTLEVBQUMsZUFBaEI7QUFBQSxnREFBaUM5QixZQUFZLENBQUMrRTtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLDRDQUZEO0FBQUEsa0RBREQsZ0JBTUM7QUFBQSxvRUFBTztBQUFBLGdEQUFPaEQ7QUFBUDtBQUFBO0FBQUE7QUFBQTtBQUFBLDRDQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBUkosRUFtQk1mLGlCQUFpQixJQUFJLEVBQXJCLElBQTJCQyxpQkFBaUIsSUFBSSxFQUFqRCxnQkFDQztBQUFHLDJDQUFTLEVBQUMsZ0JBQWI7QUFBQSxpRUFBaUM7QUFBTSw2Q0FBUyxFQUFDLFNBQWhCO0FBQUEsK0NBQTJCRCxpQkFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUFqQyx1QkFBMEY7QUFBTSw2Q0FBUyxFQUFDLFNBQWhCO0FBQUEsOENBQTJCQztBQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUExRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBREQsR0FHQyxJQXRCTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREYsRUE0QktqQixZQUFZLElBQUksRUFBakIsR0FDR3VCLFFBQVEsQ0FBQ3ZCLFlBQVksQ0FBQ3dCLFVBQWQsQ0FBUixJQUFxQyxDQUF0QyxnQkFDQztBQUFLLHlDQUFTLEVBQUMsYUFBZjtBQUFBLDBDQUVLeEIsWUFBWSxDQUFDZ0YsT0FBYixDQUFxQnZCLEdBQXJCLENBQXlCLFVBQUNDLElBQUQsRUFBT0MsR0FBUDtBQUFBLHNEQUN2QjtBQUFLLDZDQUFTLEVBQUMsWUFBZjtBQUFBLDREQUNFO0FBQUEsZ0RBQVFELElBQUksQ0FBQ047QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQURGLEVBR0lNLElBQUksQ0FBQ3VCLElBQUwsSUFBYSxNQUFkLGdCQUNDO0FBQUssK0NBQVMsRUFBQyxXQUFmO0FBQUEsOERBQ0U7QUFBTywwQ0FBRSxFQUFDLGFBQVY7QUFBd0IsaURBQVMsRUFBQyxHQUFsQztBQUFzQyw0Q0FBSSxFQUFDLE1BQTNDO0FBQWtELHNEQUFZdkIsSUFBSSxDQUFDd0IsaUJBQW5FO0FBQXNGLGlEQUFTLEVBQUMsT0FBaEc7QUFBd0csNENBQUksRUFBQztBQUE3RztBQUFBO0FBQUE7QUFBQTtBQUFBLCtDQURGLGVBRUU7QUFBSyxpREFBUyxFQUFDLE1BQWY7QUFBQSxnRUFDSTtBQUFHLG1EQUFTLEVBQUMsOEJBQWI7QUFBNEMseURBQVk7QUFBeEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxpREFESixlQUdJO0FBQUssbURBQVMsRUFBQyxVQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlEQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwrQ0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREQsZ0JBWUM7QUFBSSwrQ0FBUyxFQUFDLGFBQWQ7QUFBQSxnREFFSXhCLElBQUksQ0FBQ3lCLFlBQUwsQ0FBa0IxQixHQUFsQixDQUFzQixVQUFDMkIsVUFBRCxFQUFhekIsR0FBYjtBQUFBLDREQUNwQjtBQUFBLGlFQUNFO0FBQU8scURBQVMsRUFBRXZDLGlCQUFpQixLQUFLc0MsSUFBSSxDQUFDd0IsaUJBQUwsR0FBdUIsR0FBdkIsR0FBMkJFLFVBQVUsQ0FBQ0MsdUJBQTVELEdBQXNGLFFBQXRGLEdBQWlHLGlCQUFuSDtBQUFzSSxtREFBTyxFQUFDLEVBQTlJO0FBQWlKLGdEQUFJLEVBQUMsY0FBdEo7QUFBQSxzREFBc0tELFVBQVUsQ0FBQ2hDO0FBQWpMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlEQURvQjtBQUFBLHVDQUF0QjtBQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBZko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJDQUR1QjtBQUFBLGlDQUF6QjtBQUZMO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREQsR0FrQ0EsRUFuQ0YsR0FvQ0QsRUFoRUg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQUZGLGVBc0VJO0FBQUssdUNBQVMsRUFBQyxZQUFmO0FBQUEsc0RBQ0U7QUFBSyx5Q0FBUyxFQUFDLGNBQWY7QUFBQSx3REFDRztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FESCxlQUVHO0FBQUssMkNBQVMsRUFBQyxpQkFBZjtBQUFBLDBEQUNHO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0MsNkNBQVMsRUFBQyxTQUE1QztBQUFBLDJEQUFzRDtBQUFHLCtDQUFTLEVBQUMsYUFBYjtBQUEyQixxREFBWTtBQUF2QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXREO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBREgsZUFFRztBQUFPLHNDQUFFLEVBQUMsYUFBVjtBQUF3Qix3Q0FBSSxFQUFDLE1BQTdCO0FBQW9DLHdDQUFJLEVBQUMsY0FBekM7QUFBd0QseUNBQUssRUFBR2xDLFdBQWhFO0FBQThFLDZDQUFTLEVBQUM7QUFBeEY7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FGSCxlQUdHO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0MsNkNBQVMsRUFBQyxTQUE1QztBQUFBLDJEQUFzRDtBQUFHLCtDQUFTLEVBQUMsWUFBYjtBQUEwQixxREFBWTtBQUF0QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXREO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBSEg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQUZIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERixFQVVJbUIsWUFBWSxJQUFJLElBQWhCLElBQXdCLENBQUNBLFlBQTFCLGdCQUNDO0FBQUsseUNBQVMsRUFBQyxlQUFmO0FBQUEsdURBQ007QUFBRyxzQ0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQVMsRUFBQyxtQkFBakM7QUFBcUQseUNBQU8sRUFBQyxFQUE3RDtBQUFBLDBEQUNHO0FBQUcsNkNBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBREgsT0FDZ0QsU0FEaEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRE47QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERCxHQU1FQSxZQUFZLElBQUksSUFBaEIsSUFBd0JBLFlBQXpCLGdCQUNBO0FBQUsseUNBQVMsRUFBQyxlQUFmO0FBQUEsdURBQ007QUFBRyxzQ0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQVMsRUFBQyx3QkFBakM7QUFBMEQsb0RBQWdCckMsWUFBWSxDQUFDbUQsVUFBdkY7QUFBbUcseUNBQU8sRUFBQyxFQUEzRztBQUFBLDBEQUNHO0FBQUcsNkNBQVMsRUFBQyxnQkFBYjtBQUE4QixtREFBWTtBQUExQztBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQURIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUROO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREEsR0FPQSxJQXZCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBdEVKLEVBaUdJbkQsWUFBWSxDQUFDc0YsYUFBYixJQUE2QixJQUE3QixJQUFxQ3RGLFlBQVksQ0FBQ3NGLGFBQWIsSUFBOEIsRUFBcEUsZ0JBQ0M7QUFBQSxzREFDRTtBQUFHLHlDQUFTLEVBQUMsZUFBYjtBQUFBLHdEQUE2QjtBQUFPLG9DQUFFLEVBQUMsWUFBVjtBQUF1QixzQ0FBSSxFQUFDLFVBQTVCO0FBQXVDLDBDQUFRLEVBQUM7QUFBaEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FBN0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURGLGVBRUU7QUFBSyx5Q0FBUyxFQUFDLGNBQWY7QUFBQSx3REFDRTtBQUFJLDJDQUFTLEVBQUMsS0FBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FERixFQUVHdEYsWUFBWSxDQUFDc0YsYUFGaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQUZGO0FBQUEsNENBREQsR0FTQyxJQTFHSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBN0JGLEVBNElJdEYsWUFBWSxDQUFDdUYsWUFBZCxnQkFDQztBQUFLLHFDQUFTLEVBQUMsYUFBZjtBQUFBLG1EQUNFO0FBQUEsd0NBRUV2RixZQUFZLENBQUN1RixZQUFiLENBQTBCOUIsR0FBMUIsQ0FBOEIsVUFBQytCLFFBQUQsRUFBV0MsT0FBWDtBQUFBLG9EQUM1QjtBQUFJLDJDQUFTLEVBQUMsVUFBZDtBQUFBLHlEQUNJO0FBQUcsNkNBQVMsRUFBQyxRQUFiO0FBQXNCLHlDQUFLLEVBQUVELFFBQVEsQ0FBQ0UsS0FBdEM7QUFBNkMsdURBQWdCLEVBQTdEO0FBQWdFLHlEQUFrQixFQUFsRjtBQUFxRiwyQ0FBTyxFQUFDLEVBQTdGO0FBQUEsOENBQWlHRixRQUFRLENBQUNFO0FBQTFHO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQUQ0QjtBQUFBLCtCQUE5QjtBQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQURELEdBYUMsSUF6Sko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSw4QkFwTEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBVEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQVBGLGVBcVhFLHFFQUFDLGNBQUQ7QUFBZ0IsaUJBQU8sRUFBRTVGLE9BQU8sQ0FBQ3VEO0FBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBclhGO0FBQUE7QUFOSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBbVlEOztJQTVnQnVCeEQsTztVQXFJUG9ELHFEOzs7TUFySU9wRCxPO0FBNGdCdkIiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvW3NsdWddLjg1ZDgwYzJhMTcxYzAxMDk3NzlhLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuaW1wb3J0IEVycm9yUGFnZSBmcm9tIFwibmV4dC9lcnJvclwiO1xyXG5pbXBvcnQgSGVhZCBmcm9tIFwibmV4dC9oZWFkXCI7XHJcbmltcG9ydCBkeW5hbWljIGZyb20gJ25leHQvZHluYW1pYydcclxuaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCB9IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQgUmVhY3RJbWFnZU1hZ25pZnkgZnJvbSAncmVhY3QtaW1hZ2UtbWFnbmlmeSdcclxuaW1wb3J0IFNsaWRlciBmcm9tIFwicmVhY3Qtc2xpY2tcIjtcclxuXHJcbmltcG9ydCBDb250YWluZXIgZnJvbSBcIkAvY29tcG9uZW50cy9jb250YWluZXJcIjtcclxuaW1wb3J0IExheW91dCBmcm9tIFwiQC9jb21wb25lbnRzL2xheW91dFwiO1xyXG5pbXBvcnQgeyBnZXRDb25maWdzLCBnZXRNYWluTWVudSwgZ2V0UHJvZHVjdEJ5U2x1ZywgZ2V0UHJvZHVjdHNCeUNhdGVnb3J5IH0gZnJvbSBcIkAvbGliL2FwaVwiO1xyXG5cclxudmFyIGlkU3RvcmVBcHAgPSAnbjQ5c2hvcHYyX3RyaWpvaWEnO1xyXG52YXIgVGVtcGxhdGVIZWFkZXIgPSBkeW5hbWljKCgpID0+IGltcG9ydCgnQC9jb21wb25lbnRzL3RlbXBsYXRlcy8nK2lkU3RvcmVBcHArJy9jb21wb25lbnRzL2hlYWRlcicpKVxyXG52YXIgVGVtcGxhdGVGb290ZXIgPSBkeW5hbWljKCgpID0+IGltcG9ydCgnQC9jb21wb25lbnRzL3RlbXBsYXRlcy8nK2lkU3RvcmVBcHArJy9jb21wb25lbnRzL2Zvb3RlcicpKVxyXG5cclxubGV0IHNsaWRlclRodW1icyA9IHtcclxuICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgdmVydGljYWw6IHRydWUsXHJcbiAgc2xpZGVzVG9TaG93OiA0LFxyXG4gIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gIHNwZWVkOiA1MDAsXHJcbiAgcmVzcG9uc2l2ZTogW1xyXG4gICAge1xyXG4gICAgICBicmVha3BvaW50OiA0MTUsXHJcbiAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgdmVydGljYWw6IGZhbHNlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzogM1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgXVxyXG59XHJcblxyXG52YXIgc2V0dGluZ3NNb2JpbGVTbGlkZSA9IHtcclxuICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgc3BlZWQ6IDUwMCxcclxuICBkb3RzOiB0cnVlXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFByb2R1Y3QoeyBjb25maWdzLCBtYWluTWVudSwgZGFkb3NQcm9kdXRvIH0pIHtcclxuICB2YXIgaW5pdGlhbEltYWdlcyA9IFtdO1xyXG4gIGZ1bmN0aW9uIGdldEluaXRpYWxJbWFnZShkYWRvc1Byb2R1dG8pIHtcclxuICAgIFxyXG4gICAgdmFyIGltZ0luaWNpYWwgPSBbXTtcclxuICAgIHZhciB0ZW1wSW1nSW5pY2lhbCA9IFtdO1xyXG4gICAgXHJcbiAgICBmb3IodmFyIGkgaW4gZGFkb3NQcm9kdXRvLmltYWdlcyl7XHJcbiAgICAgIHRlbXBJbWdJbmljaWFsLnB1c2goW2ksIGRhZG9zUHJvZHV0by5pbWFnZXMgW2ldXSk7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIGluaXRpYWxLZXkgPSBkYWRvc1Byb2R1dG8uaW1hZ2Uuc3Vic3RyaW5nKGRhZG9zUHJvZHV0by5pbWFnZS5sYXN0SW5kZXhPZignLycpKzEpXHJcbiAgICBcclxuICAgIGlmKGRhZG9zUHJvZHV0by5pbWFnZXMgIT0gdW5kZWZpbmVkICYmIGRhZG9zUHJvZHV0by5pbWFnZXNbaW5pdGlhbEtleV0gIT0gbnVsbCl7XHJcbiAgICAgIGltZ0luaWNpYWxbJ3Nob3cnXSA9IGRhZG9zUHJvZHV0by5pbWFnZXNbaW5pdGlhbEtleV0uc2hvdyBcclxuICAgICAgaW1nSW5pY2lhbFsncG9wdXAnXSA9IGRhZG9zUHJvZHV0by5pbWFnZXNbaW5pdGlhbEtleV0ucG9wdXBcclxuICAgICAgaW1nSW5pY2lhbFsndGh1bWInXSA9IGRhZG9zUHJvZHV0by5pbWFnZXNbaW5pdGlhbEtleV0udGh1bWJcclxuICAgIH1lbHNlIGlmKHRlbXBJbWdJbmljaWFsWzBdICE9IG51bGwpe1xyXG4gICAgICBpbWdJbmljaWFsID0gdGVtcEltZ0luaWNpYWxbMF1bMV1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gaW1nSW5pY2lhbDtcclxuICB9XHJcbiAgaWYoZGFkb3NQcm9kdXRvKXtcclxuICAgIGluaXRpYWxJbWFnZXMgPSBnZXRJbml0aWFsSW1hZ2UoZGFkb3NQcm9kdXRvKTtcclxuICB9XHJcblxyXG4gIHZhciBzZWxlY3RlZFF0ZFBhcmNlbCA9IDE7XHJcbiAgdmFyIHNlbGVjdGVkVmFsUGFyY2VsID0gMTtcclxuICB2YXIgcXVhbnRpdHlCdXkgPSAxO1xyXG4gIHZhciBzaGlwcGluZ01ldGhvZHMgPSBudWxsO1xyXG4gIHZhciBzZWxlY3RlZE9wdGlvbkJ1eSA9IDE7XHJcbiAgXHJcbiAgZnVuY3Rpb24gZ2V0U3BlY2lhbFZhbHVlKGRhZG9zUHJvZHV0bykge1xyXG4gICAgdmFyIGluaXRpYWxTcGVjaWFsVmFsdWUgPSBudWxsO1xyXG4gICAgaWYoZGFkb3NQcm9kdXRvICE9IHVuZGVmaW5lZCl7XHJcbiAgICAgIGlmKHBhcnNlSW50KGRhZG9zUHJvZHV0by5oYXNfb3B0aW9uKSA9PSAwKXsgLy8gUFJPRFVUTyBTRU0gT1BDT0VTXHJcbiAgICAgICAgaWYoZGFkb3NQcm9kdXRvLnNwZWNpYWwpe1xyXG4gICAgICAgICAgaW5pdGlhbFNwZWNpYWxWYWx1ZSA9IGRhZG9zUHJvZHV0by5zcGVjaWFsO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgaW5pdGlhbFNwZWNpYWxWYWx1ZSA9IGRhZG9zUHJvZHV0by5wcmljZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIGlmKGluaXRpYWxTcGVjaWFsVmFsdWUgPT0gbnVsbCAmJiBkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgICBpbml0aWFsU3BlY2lhbFZhbHVlID0gZGFkb3NQcm9kdXRvLnNwZWNpYWxcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgY29uc3Qgc3BlY2lhbFZhbHVlID0gdXNlU3RhdGUoaW5pdGlhbFNwZWNpYWxWYWx1ZSk7XHJcbiAgICByZXR1cm4gc3BlY2lhbFZhbHVlO1xyXG4gIH1cclxuICBjb25zdCBbc3BlY2lhbFZhbHVlLCBzZXRTcGVjaWFsVmFsdWVdID0gZ2V0U3BlY2lhbFZhbHVlKGRhZG9zUHJvZHV0byk7XHJcblxyXG4gIHZhciBsYWxhbGEgPSAwO1xyXG4gIHZhciBbbWFpblZhbHVlLCBzZXRNYWluVmFsdWVdID0gdXNlU3RhdGUobnVsbCk7XHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGZ1bmN0aW9uIGdldE1haW5WYWx1ZShkYWRvc1Byb2R1dG8pIHtcclxuICAgICAgdmFyIGluaXRpYWxNYWluVmFsdWUgPSBudWxsO1xyXG4gICAgICBpZihkYWRvc1Byb2R1dG8gIT0gdW5kZWZpbmVkKXtcclxuICAgICAgICBpZihwYXJzZUludChkYWRvc1Byb2R1dG8uaGFzX29wdGlvbikgPT0gMCl7IC8vIFBST0RVVE8gU0VNIE9QQ09FU1xyXG4gICAgICAgICAgaWYoZGFkb3NQcm9kdXRvLnNwZWNpYWwpe1xyXG4gICAgICAgICAgICBpbml0aWFsTWFpblZhbHVlID0gZGFkb3NQcm9kdXRvLnByaWNlO1xyXG4gICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIGluaXRpYWxNYWluVmFsdWUgPSBudWxsO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgaWYoaW5pdGlhbE1haW5WYWx1ZSA9PSBudWxsICYmIGRhZG9zUHJvZHV0by5wcmljZSl7XHJcbiAgICAgICAgICAgIGluaXRpYWxNYWluVmFsdWUgPSBkYWRvc1Byb2R1dG8ucHJpY2VcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgc2V0TWFpblZhbHVlKGluaXRpYWxNYWluVmFsdWUpO1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICBcclxuICAgIH1cclxuICAgIGdldE1haW5WYWx1ZShkYWRvc1Byb2R1dG8pO1xyXG4gIH0pXHJcblxyXG4gIGNvbnN0IFtzb2xkT3V0U3RvY2ssIHNldFNvbGRPdXRTdG9ja10gPSB1c2VTdGF0ZSgoKSA9PiB7XHJcbiAgICBjb25zdCBpbml0aWFsU3RhdGUgPSAwO1xyXG4gICAgcmV0dXJuIGluaXRpYWxTdGF0ZTtcclxuICB9KTtcclxuICBcclxuICBcclxuICAvKmlmKHBhcnNlSW50KGRhZG9zUHJvZHV0by5oYXNfb3B0aW9uKSA9PSAwKXsgLy8gUFJPRFVUTyBTRU0gT1BDT0VTXHJcbiAgICAgIFxyXG4gICAgY29uc3QgdmFsb3JBdHVhbCA9IGRhZG9zUHJvZHV0by5wcmljZVxyXG4gICAgaWYoZGFkb3NQcm9kdXRvLnNwZWNpYWwpe1xyXG4gICAgICBzZXRTZWxlY3RlZE1haW5WYWx1ZSh2YWxvckF0dWFsKTtcclxuICAgICAgc2V0U2VsZWN0ZWRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvLnNwZWNpYWwpO1xyXG4gICAgfWVsc2V7XHJcbiAgICAgIHNldFNlbGVjdGVkTWFpblZhbHVlKG51bGwpO1xyXG4gICAgICBzZXRTZWxlY3RlZFNwZWNpYWxWYWx1ZSh2YWxvckF0dWFsKTtcclxuICAgIH1cclxuXHJcbiAgICBpZighZGFkb3NQcm9kdXRvLnNvbGRfb3V0KXtcclxuICAgICAgc2V0U29sZE91dFN0b2NrKDApO1xyXG4gICAgfWVsc2V7XHJcbiAgICAgIHNldFNvbGRPdXRTdG9jaygxKTtcclxuICAgIH1cclxuICB9ZWxzZXsgLy8gUFJPRFVUTyBDT00gT1BDT0VTXHJcbiAgICBzZXRTb2xkT3V0U3RvY2soMCk7XHJcblxyXG4gICAgICBpZihzZWxlY3RlZE1haW5WYWx1ZSA9PSBudWxsKXtcclxuICAgICAgICBzZXRTZWxlY3RlZE1haW5WYWx1ZShkYWRvc1Byb2R1dG8ucHJpY2UpXHJcbiAgICAgIH1cclxuICAgICAgaWYoc2VsZWN0ZWRTcGVjaWFsVmFsdWUgPT0gbnVsbCAmJiBkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgc2V0U2VsZWN0ZWRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvLnNwZWNpYWwpXHJcbiAgICAgIH0qL1xyXG5cclxuICAgICAgLypmb3IodmFyIGkgaW4gZGFkb3NQcm9kdXRvLm9wdGlvbnMpe1xyXG4gICAgICAgIGdydXBvQXR1YWwgPSBkYWRvc1Byb2R1dG8ub3B0aW9uc1tpXVxyXG4gICAgICAgIGZvcih2YXIgb3B0IGluIGdydXBvQXR1YWwub3B0aW9uX3ZhbHVlKXtcclxuICAgICAgICAgIGlmKGdydXBvQXR1YWwub3B0aW9uX3ZhbHVlW29wdF0ub3B0aW9uX3ZhbHVlX2lkPT1kYWRvc1Byb2R1dG8ub3BjYW9fc2VsZWNpb25hZGEpe1xyXG4gICAgICAgICAgICBpbml0aWFsT3B0aW9uID0gZ3J1cG9BdHVhbC5wcm9kdWN0X29wdGlvbl9pZCsnXycrZ3J1cG9BdHVhbC5vcHRpb25fdmFsdWVbb3B0XS5wcm9kdWN0X29wdGlvbl92YWx1ZV9pZFxyXG4gICAgICAgICAgICB0aGlzLlNlbGVjdE9wdGlvbihpbml0aWFsT3B0aW9uLCBudWxsKVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAgICAgICBcclxuICB9Ki9cclxuIFxyXG4gIHZhciBjaGFuZ2Vab29tID0gKGV2KSA9PiB7XHJcbiAgICBjb25zdCBpbWdTaG93ID0gZXYuY3VycmVudFRhcmdldC5kYXRhc2V0LnNyY3Nob3dcclxuICAgIGNvbnN0IGltZ1BvcHVwID0gZXYuY3VycmVudFRhcmdldC5kYXRhc2V0LnNyY3BvcHVwXHJcbiAgICB2YXIgdGVtcEltYWdlID0ge307XHJcbiAgICB0ZW1wSW1hZ2VbJ3Nob3cnXSA9IGltZ1Nob3dcclxuICAgIHRlbXBJbWFnZVsncG9wdXAnXSA9IGltZ1BvcHVwXHJcbiAgICAvL3NldEluaXRpYWxJbWFnZSh0ZW1wSW1hZ2UpO1xyXG4gICAgXHJcbiAgfVxyXG4gIFxyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG4gIGlmICghcm91dGVyLmlzRmFsbGJhY2sgJiYgIWRhZG9zUHJvZHV0bz8ucHJvZHVjdF9pZCkge1xyXG4gICAgcmV0dXJuIDxFcnJvclBhZ2Ugc3RhdHVzQ29kZT17NDA0fSAvPjtcclxuICB9XHJcbiAgcmV0dXJuIChcclxuICAgIDxMYXlvdXQ+XHJcbiAgICAgIDxDb250YWluZXI+XHJcbiAgICAgIFxyXG4gICAgICAgIHtcclxuICAgICAgICByb3V0ZXIuaXNGYWxsYmFjayA/IChcclxuICAgICAgICAgIDxkaXY+TG9hZGluZ+KApjwvZGl2PlxyXG4gICAgICAgICkgOiAoXHJcbiAgICAgICAgICA8PlxyXG4gICAgICAgICAgICA8SGVhZD5cclxuICAgICAgICAgICAgICA8dGl0bGU+e2RhZG9zUHJvZHV0by5uYW1lfTwvdGl0bGU+XHJcbiAgICAgICAgICAgICAgPG1ldGEgbmFtZT1cImRlc2NyaXB0aW9uXCIgY29udGVudD1cImxlbGVsZVwiIC8+XHJcbiAgICAgICAgICAgICAgPG1ldGEgbmFtZT1cIm9nOmltYWdlXCIgY29udGVudD1cImxpbGlsaVwiIC8+XHJcbiAgICAgICAgICAgIDwvSGVhZD5cclxuICAgICAgICAgICAgPFRlbXBsYXRlSGVhZGVyIGNvbmZpZ3M9e2NvbmZpZ3MucmVzcG9zdGF9IG1haW5NZW51PXttYWluTWVudS5yZXNwb3N0YX0+PC9UZW1wbGF0ZUhlYWRlcj4gXHJcbiAgICAgICAgICAgIDxkaXYgY2xhcz1cIm1haW4tY29udGVudFwiPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicGFnZS1wcm9kdWN0c1wiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItcGFkZGluZyBsaWdodC1iYWNrZ3JvdW5kIG5wcm9kdWN0LWJyZWFkY3J1bWJcIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8b2wgY2xhc3NOYW1lPVwiYnJlYWRjcnVtYlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJicmVhZGNydW1iLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiL1wiIHRpdGxlPVwiUMOhZ2luYSBpbmljaWFsXCI+SG9tZTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJicmVhZGNydW1iLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICB7ZGFkb3NQcm9kdXRvLm5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8L29sPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY3AtcHJldmlldzNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZHVjdC1wYWdlXCI+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZHVjdC1nYWxsZXJ5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC10aHVtYm5haWxzIFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LWltYWdlcy1jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by5pbWFnZXMpID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLmxlbmd0aCA+IDQpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTbGlkZXIgY2xhc3NOYW1lPVwic2xpZGVyVGh1bWJzXCIgey4uLnNsaWRlclRodW1ic30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLm1hcCgoaXRlbSwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2UtdGh1bWIganMtY2Fyb3VzZWwtY29udHJvbC1pdGVtIHBvaW50ZXIganMtcHJvZHVjdC1pbWFnZS10aHVtYlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWx0PXtkYWRvc1Byb2R1dG8ubmFtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEtc3Jjc2hvdz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5zaG93fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1zcmNwb3B1cD17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPVwiMTBcIiBoZWlnaHQ9XCIxMFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPVwiXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLnZpZGVvcy5sZW5ndGgpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by52aWRlb3MubWFwKChpdGVtVmlkZW8sIGtleVZpZGVvKSA9PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIml0ZW0tdmlkZW9cIiBrZXk9e2tleVZpZGVvfT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGRhdGEtdmlkZW9pZD17aXRlbVZpZGVvLmlkX3ZpZGVvfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17YGh0dHBzOi8vaW1nLnlvdXR1YmUuY29tL3ZpLyR7aXRlbVZpZGVvLmlkX3ZpZGVvfS9tcWRlZmF1bHQuanBnYH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS15b3V0dWJlLXBsYXlcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1NsaWRlcj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLm1hcCgoaXRlbSwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwcm9kdWN0LWltYWdlLXRodW1iIGpzLWNhcm91c2VsLWNvbnRyb2wtaXRlbSBwb2ludGVyIGpzLXByb2R1Y3QtaW1hZ2UtdGh1bWJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD17ZGFkb3NQcm9kdXRvLm5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEtc3Jjc2hvdz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5zaG93fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLXNyY3BvcHVwPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjEwXCIgaGVpZ2h0PVwiMTBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPVwiXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLnZpZGVvcy5sZW5ndGgpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8udmlkZW9zLm1hcCgoaXRlbVZpZGVvLCBrZXlWaWRlbykgPT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaXRlbS12aWRlb1wiIGtleT17a2V5VmlkZW99PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGRhdGEtdmlkZW9pZD17aXRlbVZpZGVvLmlkX3ZpZGVvfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2BodHRwczovL2ltZy55b3V0dWJlLmNvbS92aS8ke2l0ZW1WaWRlby5pZF92aWRlb30vbXFkZWZhdWx0LmpwZ2B9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXlvdXR1YmUtcGxheVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZXMgbWluLXdpZHRoLTQxNXB4XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb190b3BfbGVmdC5pbWFnZSAhPSBudWxsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGFiZWxJdGVtIGxhYmVsVG9wTGVmdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb190b3BfbGVmdC5pbWFnZX0gLz4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb190b3BfcmlnaHQuaW1hZ2UgIT0gbnVsbCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxhYmVsSXRlbSBsYWJlbFRvcFJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX3RvcF9yaWdodC5pbWFnZX0gLz4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb19ib3R0b21fbGVmdC5pbWFnZSAhPSBudWxsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGFiZWxJdGVtIGxhYmVsQm90dG9tTGVmdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb19ib3R0b21fbGVmdC5pbWFnZX0gLz4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLmxhYmVscy5wcm9tb19ib3R0b21fcmlnaHQuaW1hZ2UgIT0gbnVsbCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxhYmVsSXRlbSBsYWJlbEJvdHRvbVJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX2JvdHRvbV9yaWdodC5pbWFnZX0gLz4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSZWFjdEltYWdlTWFnbmlmeSB7Li4ue1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNtYWxsSW1hZ2U6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ6IGRhZG9zUHJvZHV0by5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzRmx1aWRXaWR0aDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcmM6IGluaXRpYWxJbWFnZXMuc2hvd1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhcmdlSW1hZ2U6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcmM6IGluaXRpYWxJbWFnZXMucG9wdXAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IGNvbmZpZ3Mud2lkdGhab29tLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogY29uZmlncy53aWR0aFpvb21cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX0gLz4gICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1heC13aWR0aC00MTRweFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykgJiYgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubGVuZ3RoID4gMSkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LWltYWdlc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U2xpZGVyIGNsYXNzTmFtZT1cInNsaWRlVml0cmluZVwiIHsuLi5zZXR0aW5nc01vYmlsZVNsaWRlfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhcmVhWm9vbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH0gYWx0PXtkYWRvc1Byb2R1dG8ubmFtZX0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8udmlkZW9zLmxlbmd0aCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by52aWRlb3MubWFwKChpdGVtVmlkZW8sIGtleVZpZGVvKSA9PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhcmVhWm9vbSBpdGVtLXZpZGVvXCIga2V5PXtrZXlWaWRlb30+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgZGF0YS12aWRlb2lkPXtpdGVtVmlkZW8uaWRfdmlkZW99PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17YGh0dHBzOi8vaW1nLnlvdXR1YmUuY29tL3ZpLyR7aXRlbVZpZGVvLmlkX3ZpZGVvfS9tcWRlZmF1bHQuanBnYH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEteW91dHViZS1wbGF5XCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU2xpZGVyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiAoT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykgJiYgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubGVuZ3RoID4gMCkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKGRhZG9zUHJvZHV0by5pbWFnZXMpLm1hcCgoaXRlbSwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhcmVhWm9vbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfSBhbHQ9e2RhZG9zUHJvZHV0by5uYW1lfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by52aWRlb3MubGVuZ3RoKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLnZpZGVvcy5tYXAoKGl0ZW1WaWRlbywga2V5VmlkZW8pID0+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tIGl0ZW0tdmlkZW9cIiBrZXk9e2tleVZpZGVvfT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBkYXRhLXZpZGVvaWQ9e2l0ZW1WaWRlby5pZF92aWRlb30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvJHtpdGVtVmlkZW8uaWRfdmlkZW99L21xZGVmYXVsdC5qcGdgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS15b3V0dWJlLXBsYXlcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2R1Y3QtaW5mb1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1hY3Rpb25zIGNvbnRhaW5lci1wYWRkaW5nIGNvbnRhaW5lci1wYWRkaW5nLXRvcFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZHVjdC1oZWFkZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoMSBjbGFzc05hbWU9XCJucHJvZHVjdC10aXRsZVwiPntkYWRvc1Byb2R1dG8ubmFtZX08L2gxPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGRhZG9zUHJvZHV0by5tb2RlbCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwiaW5mb3NQcm9kdWN0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxzcGFuIGNsYXNzTmFtZT1cInRpdGxlSW5mb1wiPlJFRjo8L3NwYW4+IHtkYWRvc1Byb2R1dG8ubW9kZWx9PC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJhdGVCb3hcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cImxrLWF2YWxpYXJcIiBvbkNsaWNrPVwiXCI+QXZhbGlhciBhZ29yYTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhkYWRvc1Byb2R1dG8uc2hvcnQgIT0gJycpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpbmZvc0FyZWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJlc3VtZVByb2R1Y3RcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBkYW5nZXJvdXNseVNldElubmVySFRNTD17eyBfX2h0bWw6IGRhZG9zUHJvZHV0by5zaG9ydCB9fSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXlBcmVhXCI+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbFNlY3Rpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kY3QtcHJpY2VcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhsYWxhbGEpID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm9sZFByaWNlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBEZSA8c3BhbiBjbGFzc05hbWU9XCJucHJvZHVjdC1wcmljZS1tYXhcIj57bWFpblZhbHVlfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZHVjdC1wcmljZS12YWx1ZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsobGFsYWxhKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBQb3IgPHNwYW4gY2xhc3NOYW1lPVwic3BlY2lhbFZhbHVlXCI+e2xhbGFsYX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiaXRlbS1kaXNjb3VudFwiPntkYWRvc1Byb2R1dG8uZGlzY291bnRfcGVyY2VudH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cD5wb3IgPHNwYW4+e21haW5WYWx1ZX08L3NwYW4+PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KHNlbGVjdGVkUXRkUGFyY2VsICE9ICcnICYmIHNlbGVjdGVkVmFsUGFyY2VsICE9ICcnKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJzZWxlY3RlZFBhcmNlbFwiPk91IDxzcGFuIGNsYXNzTmFtZT1cIm51bVBhcmNcIj57c2VsZWN0ZWRRdGRQYXJjZWx9eDwvc3Bhbj4gZGUgPHNwYW4gY2xhc3NOYW1lPVwidmFsUGFyY1wiPntzZWxlY3RlZFZhbFBhcmNlbH08L3NwYW4+PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvICE9ICcnKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKHBhcnNlSW50KGRhZG9zUHJvZHV0by5oYXNfb3B0aW9uKSAhPSAwKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm9wdGlvbnNBcmVhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8ub3B0aW9ucy5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJib3gtb3B0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+e2l0ZW0ubmFtZX08L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGl0ZW0udHlwZSA9PSAndGV4dCcpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInR4dE9wdGlvblwiPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IGlkPVwiZmllbGRPcHRpb25cIiBtYXhsZW5ndGg9XCIzXCIgdHlwZT1cInRleHRcIiBkYXRhLWdyb3VwPXtpdGVtLnByb2R1Y3Rfb3B0aW9uX2lkfSBjbGFzc05hbWU9XCJmaWVsZFwiIG5hbWU9XCJ0eHQtb3B0aW9uXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhlbHBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtcXVlc3Rpb24tY2lyY2xlIGNvbG9yMlwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaW5mb0hlbHBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEluc2lyYSBhdMOpIDMgbGV0cmFzIHBhcmEgcGVyc29uYWxpemFyIGEgY2FtaXNhIGNvbSB1bSBib3JkYWRvIGV4Y2x1c2l2by4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwibGlzdE9wdGlvbnNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtLm9wdGlvbl92YWx1ZS5tYXAoKGl0ZW1PcHRpb24sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPXtzZWxlY3RlZE9wdGlvbkJ1eSAhPT0gaXRlbS5wcm9kdWN0X29wdGlvbl9pZCsnXycraXRlbU9wdGlvbi5wcm9kdWN0X29wdGlvbl92YWx1ZV9pZCA/ICdvcHRpb24nIDogJ29wdGlvbiBzZWxlY3RlZCd9IG9uQ2xpY2s9XCJcIiBocmVmPVwiamF2YXNjcmlwdDo7XCI+e2l0ZW1PcHRpb24ubmFtZX08L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6ICAnJyAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbFNlY3Rpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicXVhbnRpdHlBcmVhXCI+ICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+UXVhbnRpZGFkZTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJ1dHRvbnNRdWFudGl0eVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBjbGFzc05hbWU9XCJidG5MZXNzXCI+PGkgY2xhc3NOYW1lPVwiZmEgZmEtbWludXNcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IGlkPVwidHh0UXVhbnRpdHlcIiB0eXBlPVwidGV4dFwiIG5hbWU9XCJ0eHQtcXVhbnRpdHlcIiB2YWx1ZT17IHF1YW50aXR5QnV5IH0gY2xhc3NOYW1lPVwidHh0UXVhbnRpdHlcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBjbGFzc05hbWU9XCJidG5Nb3JlXCI+PGkgY2xhc3NOYW1lPVwiZmEgZmEtcGx1c1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT48L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoc29sZE91dFN0b2NrICE9IG51bGwgJiYgIXNvbGRPdXRTdG9jayk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV5QnV0dG9uQXJlYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgY2xhc3NOYW1lPVwiYnV5QnV0dG9uIGJ0bl9idXlcIiBvbkNsaWNrPVwiXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhcyBmYSBmYS1zaG9wcGluZy1jYXJ0XCI+PC9pPiB7J0NvbXByYXInfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiAoc29sZE91dFN0b2NrICE9IG51bGwgJiYgc29sZE91dFN0b2NrKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXlCdXR0b25BcmVhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBjbGFzc05hbWU9XCJidXlCdXR0b24gbm90aWZ5QnV0dG9uXCIgZGF0YS1wcm9kdWN0aWQ9e2RhZG9zUHJvZHV0by5wcm9kdWN0X2lkfSBvbkNsaWNrPVwiXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLWVudmVsb3BlXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPiBBdmlzZS1tZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhkYWRvc1Byb2R1dG8udGV4dF9wcmV2ZW5kYSE9IG51bGwgJiYgZGFkb3NQcm9kdXRvLnRleHRfcHJldmVuZGEgIT0gJycpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJjaGVja1ByZXZlbmRhXCI+PGlucHV0IGlkPVwiY2twcmV2ZW5kYVwiIHR5cGU9XCJjaGVja2JveFwiIG9uQ2hhbmdlPVwiXCIgLz4gQ29uY29yZG8gY29tIG8gcHJhem8gZGUgZW50cmVnYSBkZXNjcml0byBhYmFpeG8uPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpbmZvUHJldmVuZGFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cInRpdFwiPlRFUk1PIERFIEFDRUlUQcOHw4NPPC9oND5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2RhZG9zUHJvZHV0by50ZXh0X3ByZXZlbmRhfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoZGFkb3NQcm9kdXRvLmd1aWFfbWVkaWRhcyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZ3VpYXNNZWRpZGFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLmd1aWFfbWVkaWRhcy5tYXAoKGl0ZW1HdWlhLCBrZXlHdWlhKSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT1cIml0ZW1HdWlhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJjb2xvcjJcIiB0aXRsZT17aXRlbUd1aWEudGl0bGV9IGRhdGEtdGl0dWxvZ3VpYT1cIlwiIGRhdGEtY29udGV1ZG9ndWlhPVwiXCIgb25DbGljaz1cIlwiPntpdGVtR3VpYS50aXRsZX08L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8VGVtcGxhdGVGb290ZXIgY29uZmlncz17Y29uZmlncy5yZXNwb3N0YX0gLz5cclxuICAgICAgICAgIDwvPlxyXG4gICAgICAgICl9XHJcbiAgICAgIDwvQ29udGFpbmVyPlxyXG4gICAgPC9MYXlvdXQ+XHJcbiAgKTtcclxufTtcclxuXHJcbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBnZXRTdGF0aWNQcm9wcyh7IHBhcmFtcyB9KSB7XHJcbiAgdmFyIGNvbmZpZ3MgPSBhd2FpdCBnZXRDb25maWdzKCk7XHJcbiAgdmFyIG1haW5NZW51ID0gYXdhaXQgZ2V0TWFpbk1lbnUoKTtcclxuICB2YXIgcmVzUHJvZHVjdCA9IGF3YWl0IGdldFByb2R1Y3RCeVNsdWcocGFyYW1zLnNsdWcpO1xyXG5cclxuICB2YXIgZGFkb3NQcm9kdXRvID0gbnVsbDtcclxuICBpZihyZXNQcm9kdWN0LnN1Y2Nlc3Mpe1xyXG4gICAgZGFkb3NQcm9kdXRvID0gcmVzUHJvZHVjdC5yZXNwb3N0YS5wcm9kdXRvO1xyXG4gIH1cclxuICByZXR1cm4ge1xyXG4gICAgcHJvcHM6IHtcclxuICAgICAgY29uZmlncyxcclxuICAgICAgZGFkb3NQcm9kdXRvLFxyXG4gICAgICBtYWluTWVudVxyXG4gICAgfSxcclxuICAgIHJldmFsaWRhdGU6IDYwXHJcbiAgfTtcclxufVxyXG5cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFN0YXRpY1BhdGhzKCkge1xyXG4gIGNvbnN0IHByb2R1dG9zID0gYXdhaXQgZ2V0UHJvZHVjdHNCeUNhdGVnb3J5KCk7XHJcblxyXG4gIHJldHVybiB7XHJcbiAgICBwYXRoczpcclxuICAgICAgW10sXHJcbiAgICBmYWxsYmFjazogdHJ1ZSxcclxuICB9O1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=