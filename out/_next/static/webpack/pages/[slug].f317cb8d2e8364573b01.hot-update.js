webpackHotUpdate_N_E("pages/[slug]",{

/***/ "./pages/[slug].js":
/*!*************************!*\
  !*** ./pages/[slug].js ***!
  \*************************/
/*! exports provided: __N_SSG, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__N_SSG", function() { return __N_SSG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Product; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/error */ "./node_modules/next/error.js");
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_error__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_image_magnify__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-image-magnify */ "./node_modules/react-image-magnify/dist/es/ReactImageMagnify.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_container__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/components/container */ "./components/container.js");
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @/components/layout */ "./components/layout.js");





var _jsxFileName = "C:\\Users\\sauln49\\Desktop\\nextjs\\pages\\[slug].js",
    _s3 = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }










var idStoreApp = 'n49shopv2_trijoia';
var TemplateHeader = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c = function _c() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/header$")("./" + idStoreApp + "/components/header");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/header$").resolve("./" + idStoreApp + "/components/header"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/header']
  }
});
_c2 = TemplateHeader;
var TemplateFooter = next_dynamic__WEBPACK_IMPORTED_MODULE_6___default()(_c3 = function _c3() {
  return __webpack_require__("./components/templates lazy recursive ^\\.\\/.*\\/components\\/footer$")("./" + idStoreApp + "/components/footer");
}, {
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(__webpack_require__("./components/templates weak recursive ^\\.\\/.*\\/components\\/footer$").resolve("./" + idStoreApp + "/components/footer"))];
    },
    modules: ['@/components/templates/' + idStoreApp + '/components/footer']
  }
});
_c4 = TemplateFooter;
var sliderThumbs = {
  infinite: false,
  vertical: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  speed: 500,
  responsive: [{
    breakpoint: 415,
    settings: {
      vertical: false,
      slidesToShow: 3
    }
  }]
};
var settingsMobileSlide = {
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  dots: true
};
var __N_SSG = true;
function Product(_ref) {
  _s3();

  var _s = $RefreshSig$(),
      _s2 = $RefreshSig$(),
      _this = this;

  var configs = _ref.configs,
      mainMenu = _ref.mainMenu,
      dadosProduto = _ref.dadosProduto;
  var initialImages = [];

  function getInitialImage(dadosProduto) {
    var imgInicial = [];
    var tempImgInicial = [];

    for (var i in dadosProduto.images) {
      tempImgInicial.push([i, dadosProduto.images[i]]);
    }

    var initialKey = dadosProduto.image.substring(dadosProduto.image.lastIndexOf('/') + 1);

    if (dadosProduto.images != undefined && dadosProduto.images[initialKey] != null) {
      imgInicial['show'] = dadosProduto.images[initialKey].show;
      imgInicial['popup'] = dadosProduto.images[initialKey].popup;
      imgInicial['thumb'] = dadosProduto.images[initialKey].thumb;
    } else if (tempImgInicial[0] != null) {
      imgInicial = tempImgInicial[0][1];
    }

    return imgInicial;
  }

  if (dadosProduto) {
    initialImages = getInitialImage(dadosProduto);
  }

  var selectedQtdParcel = 1;
  var selectedValParcel = 1;
  var quantityBuy = 1;
  var shippingMethods = null;
  var selectedOptionBuy = 1;

  function getSpecialValue(dadosProduto) {
    _s();

    var initialSpecialValue = null;

    if (dadosProduto != undefined) {
      if (parseInt(dadosProduto.has_option) == 0) {
        // PRODUTO SEM OPCOES
        if (dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        } else {
          initialSpecialValue = dadosProduto.price;
        }
      } else {
        if (initialSpecialValue == null && dadosProduto.special) {
          initialSpecialValue = dadosProduto.special;
        }
      }
    }

    var specialValue = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(initialSpecialValue);
    return specialValue;
  }

  _s(getSpecialValue, "uOCI/u7Rhc0OEPJW7t3Rh1DIVCE=");

  var _getSpecialValue = getSpecialValue(dadosProduto),
      _getSpecialValue2 = Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_getSpecialValue, 2),
      specialValue = _getSpecialValue2[0],
      setSpecialValue = _getSpecialValue2[1];

  function getMainValue(dadosProduto) {
    _s2();

    var initialMainValue = null;

    if (dadosProduto != undefined) {
      if (parseInt(dadosProduto.has_option) == 0) {
        // PRODUTO SEM OPCOES
        if (dadosProduto.special) {
          initialMainValue = dadosProduto.price;
        } else {
          initialMainValue = null;
        }
      } else {
        if (initialMainValue == null && dadosProduto.price) {
          initialMainValue = dadosProduto.price;
        }
      }
    }

    var mainValueState = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(initialMainValue);
    return mainValueState;
  }

  _s2(getMainValue, "loXbXvMqcS2N0dBaSjwn2V8iaXI=");

  var _getMainValue = getMainValue(dadosProduto),
      _getMainValue2 = Object(C_Users_sauln49_Desktop_nextjs_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_getMainValue, 2),
      mainValue = _getMainValue2[0],
      setMainValue = _getMainValue2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_7__["useState"])(function () {
    var initialState = 0;
    return initialState;
  }),
      soldOutStock = _useState[0],
      setSoldOutStock = _useState[1];
  /*if(parseInt(dadosProduto.has_option) == 0){ // PRODUTO SEM OPCOES
      
    const valorAtual = dadosProduto.price
    if(dadosProduto.special){
      setSelectedMainValue(valorAtual);
      setSelectedSpecialValue(dadosProduto.special);
    }else{
      setSelectedMainValue(null);
      setSelectedSpecialValue(valorAtual);
    }
      if(!dadosProduto.sold_out){
      setSoldOutStock(0);
    }else{
      setSoldOutStock(1);
    }
  }else{ // PRODUTO COM OPCOES
    setSoldOutStock(0);
        if(selectedMainValue == null){
        setSelectedMainValue(dadosProduto.price)
      }
      if(selectedSpecialValue == null && dadosProduto.special){
        setSelectedSpecialValue(dadosProduto.special)
      }*/

  /*for(var i in dadosProduto.options){
    grupoAtual = dadosProduto.options[i]
    for(var opt in grupoAtual.option_value){
      if(grupoAtual.option_value[opt].option_value_id==dadosProduto.opcao_selecionada){
        initialOption = grupoAtual.product_option_id+'_'+grupoAtual.option_value[opt].product_option_value_id
        this.SelectOption(initialOption, null)
      }
    }
  }
        
  }*/


  var changeZoom = function changeZoom(ev) {
    var imgShow = ev.currentTarget.dataset.srcshow;
    var imgPopup = ev.currentTarget.dataset.srcpopup;
    var tempImage = {};
    tempImage['show'] = imgShow;
    tempImage['popup'] = imgPopup; //setInitialImage(tempImage);
  };

  var lalala = getSpecialValue(dadosProduto);
  var lelele = getMainValue(dadosProduto);
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])();

  if (!router.isFallback && !(dadosProduto !== null && dadosProduto !== void 0 && dadosProduto.product_id)) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_error__WEBPACK_IMPORTED_MODULE_4___default.a, {
      statusCode: 404
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 174,
      columnNumber: 12
    }, this);
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_layout__WEBPACK_IMPORTED_MODULE_11__["default"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_container__WEBPACK_IMPORTED_MODULE_10__["default"], {
      children: router.isFallback ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        children: "Loading\u2026"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 182,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_5___default.a, {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
            children: dadosProduto.name
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 186,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "description",
            content: "lelele"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 187,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
            name: "og:image",
            content: "lilili"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 188,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 185,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateHeader, {
          configs: configs.resposta,
          mainMenu: mainMenu.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 190,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          clas: "main-content",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            className: "page-products",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              className: "container-padding light-background nproduct-breadcrumb",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "container",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ol", {
                  className: "breadcrumb",
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                      href: "/",
                      title: "P\xE1gina inicial",
                      children: "Home"
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 197,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 196,
                    columnNumber: 24
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                    className: "breadcrumb-item",
                    children: dadosProduto.name
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 199,
                    columnNumber: 24
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 195,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                  className: "cp-preview3",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                    className: "container",
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                      className: "nproduct-page",
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-gallery",
                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-thumbnails ",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images-container",
                            children: dadosProduto.images ? Object.keys(dadosProduto.images).length > 4 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "sliderThumbs"
                            }, sliderThumbs), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 220,
                                    columnNumber: 57
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 217,
                                  columnNumber: 53
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 234,
                                      columnNumber: 51
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 235,
                                      columnNumber: 51
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 233,
                                    columnNumber: 49
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 232,
                                  columnNumber: 47
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 213,
                              columnNumber: 39
                            }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "product-image-thumb js-carousel-control-item pointer js-product-image-thumb",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name,
                                    "data-srcshow": dadosProduto.images[item].show,
                                    "data-srcpopup": dadosProduto.images[item].popup,
                                    width: "10",
                                    height: "10",
                                    onClick: ""
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 251,
                                    columnNumber: 53
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 248,
                                  columnNumber: 49
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 266,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 267,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 265,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 264,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }, void 0, true) : null
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 209,
                            columnNumber: 32
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 208,
                          columnNumber: 31
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-images min-width-415px",
                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "areaZoom",
                            children: [dadosProduto.labels.promo_top_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 285,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 284,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_top_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelTopRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_top_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 293,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 292,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_left.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomLeft",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_left.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 301,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 300,
                              columnNumber: 41
                            }, this) : null, dadosProduto.labels.promo_bottom_right.image != null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "labelItem labelBottomRight",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                src: dadosProduto.labels.promo_bottom_right.image
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 309,
                                columnNumber: 43
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 308,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_image_magnify__WEBPACK_IMPORTED_MODULE_8__["default"], _objectSpread({}, {
                              smallImage: {
                                alt: dadosProduto.name,
                                isFluidWidth: true,
                                src: initialImages.show
                              },
                              largeImage: {
                                src: initialImages.popup,
                                width: configs.widthZoom,
                                height: configs.widthZoom
                              }
                            }), void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 315,
                              columnNumber: 37
                            }, this)]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 281,
                            columnNumber: 35
                          }, this)
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 280,
                          columnNumber: 33
                        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "max-width-414px",
                          children: Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 1 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "product-images",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_slick__WEBPACK_IMPORTED_MODULE_9___default.a, _objectSpread(_objectSpread({
                              className: "slideVitrine"
                            }, settingsMobileSlide), {}, {
                              children: [Object.keys(dadosProduto.images).map(function (item, key) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: dadosProduto.images[item].popup,
                                    alt: dadosProduto.name
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 338,
                                    columnNumber: 49
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 337,
                                  columnNumber: 47
                                }, _this);
                              }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "areaZoom item-video",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    "data-videoid": itemVideo.id_video,
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                      src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 347,
                                      columnNumber: 49
                                    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-youtube-play",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 348,
                                      columnNumber: 49
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 346,
                                    columnNumber: 47
                                  }, _this)
                                }, keyVideo, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 345,
                                  columnNumber: 45
                                }, _this);
                              }) : null]
                            }), void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 334,
                              columnNumber: 37
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 333,
                            columnNumber: 35
                          }, this) : Object.keys(dadosProduto.images) && Object.keys(dadosProduto.images).length > 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                            children: [Object.keys(dadosProduto.images).map(function (item, key) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                  src: dadosProduto.images[item].popup,
                                  alt: dadosProduto.name
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 362,
                                  columnNumber: 45
                                }, _this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 361,
                                columnNumber: 43
                              }, _this);
                            }), dadosProduto.videos.length ? dadosProduto.videos.map(function (itemVideo, keyVideo) {
                              return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "areaZoom item-video",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  onClick: "",
                                  "data-videoid": itemVideo.id_video,
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("img", {
                                    src: "https://img.youtube.com/vi/".concat(itemVideo.id_video, "/mqdefault.jpg")
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 371,
                                    columnNumber: 49
                                  }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-youtube-play",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 372,
                                    columnNumber: 49
                                  }, _this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 370,
                                  columnNumber: 47
                                }, _this)
                              }, keyVideo, false, {
                                fileName: _jsxFileName,
                                lineNumber: 369,
                                columnNumber: 45
                              }, _this);
                            }) : null]
                          }, void 0, true) : null
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 331,
                          columnNumber: 31
                        }, this)]
                      }, void 0, true, {
                        fileName: _jsxFileName,
                        lineNumber: 207,
                        columnNumber: 29
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                        className: "nproduct-info",
                        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                          className: "product-actions container-padding container-padding-top",
                          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "nproduct-header",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
                              className: "nproduct-title",
                              children: dadosProduto.name
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 388,
                              columnNumber: 38
                            }, this), dadosProduto.model ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              className: "infosProduct",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                  className: "titleInfo",
                                  children: "REF:"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 392,
                                  columnNumber: 49
                                }, this), " ", dadosProduto.model]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 392,
                                columnNumber: 45
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 391,
                              columnNumber: 41
                            }, this) : null, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "rateBox",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                className: "lk-avaliar",
                                onClick: "",
                                children: "Avaliar agora"
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 399,
                                columnNumber: 41
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 398,
                              columnNumber: 38
                            }, this), dadosProduto["short"] != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "infosArea",
                              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "resumeProduct",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  dangerouslySetInnerHTML: {
                                    __html: dadosProduto["short"]
                                  }
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 406,
                                  columnNumber: 43
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 405,
                                columnNumber: 42
                              }, this)
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 404,
                              columnNumber: 41
                            }, this) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 387,
                            columnNumber: 35
                          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "buyArea",
                            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "nprodct-price",
                                children: [lalala ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "oldPrice",
                                  children: ["De ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "nproduct-price-max",
                                    children: lelele
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 421,
                                    columnNumber: 50
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 420,
                                  columnNumber: 45
                                }, this) : '', /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "nproduct-price-value",
                                  children: lalala ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                                    children: ["Por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "specialValue",
                                      children: lalala
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 429,
                                      columnNumber: 53
                                    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      className: "item-discount",
                                      children: dadosProduto.discount_percent
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 430,
                                      columnNumber: 48
                                    }, this)]
                                  }, void 0, true) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                    children: ["por ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                      children: lelele
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 433,
                                      columnNumber: 54
                                    }, this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 433,
                                    columnNumber: 47
                                  }, this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 426,
                                  columnNumber: 43
                                }, this), selectedQtdParcel != '' && selectedValParcel != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                  className: "selectedParcel",
                                  children: ["Ou ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "numParc",
                                    children: [selectedQtdParcel, "x"]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 438,
                                    columnNumber: 78
                                  }, this), " de ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
                                    className: "valParc",
                                    children: selectedValParcel
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 438,
                                    columnNumber: 135
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 438,
                                  columnNumber: 45
                                }, this) : null]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 418,
                                columnNumber: 39
                              }, this), dadosProduto != '' ? parseInt(dadosProduto.has_option) != 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "optionsArea",
                                children: dadosProduto.options.map(function (item, key) {
                                  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                    className: "box-option",
                                    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                      children: item.name
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 451,
                                      columnNumber: 53
                                    }, _this), item.type == 'text' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                      className: "txtOption",
                                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                        id: "fieldOption",
                                        maxlength: "3",
                                        type: "text",
                                        "data-group": item.product_option_id,
                                        className: "field",
                                        name: "txt-option"
                                      }, void 0, false, {
                                        fileName: _jsxFileName,
                                        lineNumber: 455,
                                        columnNumber: 57
                                      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                        className: "help",
                                        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                          className: "fa fa-question-circle color2",
                                          "aria-hidden": "true"
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 457,
                                          columnNumber: 61
                                        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                          className: "infoHelp",
                                          children: "Insira at\xE9 3 letras para personalizar a camisa com um bordado exclusivo."
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 459,
                                          columnNumber: 61
                                        }, _this)]
                                      }, void 0, true, {
                                        fileName: _jsxFileName,
                                        lineNumber: 456,
                                        columnNumber: 57
                                      }, _this)]
                                    }, void 0, true, {
                                      fileName: _jsxFileName,
                                      lineNumber: 454,
                                      columnNumber: 55
                                    }, _this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                                      className: "listOptions",
                                      children: item.option_value.map(function (itemOption, key) {
                                        return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                            className: selectedOptionBuy !== item.product_option_id + '_' + itemOption.product_option_value_id ? 'option' : 'option selected',
                                            onClick: "",
                                            href: "javascript:;",
                                            children: itemOption.name
                                          }, void 0, false, {
                                            fileName: _jsxFileName,
                                            lineNumber: 469,
                                            columnNumber: 63
                                          }, _this)
                                        }, void 0, false, {
                                          fileName: _jsxFileName,
                                          lineNumber: 468,
                                          columnNumber: 61
                                        }, _this);
                                      })
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 465,
                                      columnNumber: 55
                                    }, _this)]
                                  }, void 0, true, {
                                    fileName: _jsxFileName,
                                    lineNumber: 450,
                                    columnNumber: 51
                                  }, _this);
                                })
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 447,
                                columnNumber: 44
                              }, this) : '' : '']
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 417,
                              columnNumber: 37
                            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                              className: "colSection",
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "quantityArea",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
                                  children: "Quantidade"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 487,
                                  columnNumber: 44
                                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                  className: "buttonsQuantity",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnLess",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-minus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 489,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 489,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                    id: "txtQuantity",
                                    type: "text",
                                    name: "txt-quantity",
                                    value: quantityBuy,
                                    className: "txtQuantity"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 490,
                                    columnNumber: 47
                                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    href: "javascript:;",
                                    onClick: "",
                                    className: "btnMore",
                                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                      className: "fa fa-plus",
                                      "aria-hidden": "true"
                                    }, void 0, false, {
                                      fileName: _jsxFileName,
                                      lineNumber: 491,
                                      columnNumber: 101
                                    }, this)
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 491,
                                    columnNumber: 47
                                  }, this)]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 488,
                                  columnNumber: 44
                                }, this)]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 486,
                                columnNumber: 41
                              }, this), soldOutStock != null && !soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton btn_buy",
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fas fa fa-shopping-cart"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 498,
                                    columnNumber: 52
                                  }, this), " ", 'Comprar']
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 497,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 496,
                                columnNumber: 43
                              }, this) : soldOutStock != null && soldOutStock ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "buyButtonArea",
                                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                  href: "javascript:;",
                                  className: "buyButton notifyButton",
                                  "data-productid": dadosProduto.product_id,
                                  onClick: "",
                                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("i", {
                                    className: "fa fa-envelope",
                                    "aria-hidden": "true"
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 504,
                                    columnNumber: 52
                                  }, this), " Avise-me"]
                                }, void 0, true, {
                                  fileName: _jsxFileName,
                                  lineNumber: 503,
                                  columnNumber: 49
                                }, this)
                              }, void 0, false, {
                                fileName: _jsxFileName,
                                lineNumber: 502,
                                columnNumber: 43
                              }, this) : null]
                            }, void 0, true, {
                              fileName: _jsxFileName,
                              lineNumber: 485,
                              columnNumber: 39
                            }, this), dadosProduto.text_prevenda != null && dadosProduto.text_prevenda != '' ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
                              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
                                className: "checkPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
                                  id: "ckprevenda",
                                  type: "checkbox",
                                  onChange: ""
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 514,
                                  columnNumber: 70
                                }, this), " Concordo com o prazo de entrega descrito abaixo."]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 514,
                                columnNumber: 41
                              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                                className: "infoPrevenda",
                                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
                                  className: "tit",
                                  children: "TERMO DE ACEITA\xC7\xC3O"
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 516,
                                  columnNumber: 43
                                }, this), dadosProduto.text_prevenda]
                              }, void 0, true, {
                                fileName: _jsxFileName,
                                lineNumber: 515,
                                columnNumber: 41
                              }, this)]
                            }, void 0, true) : null]
                          }, void 0, true, {
                            fileName: _jsxFileName,
                            lineNumber: 415,
                            columnNumber: 35
                          }, this), dadosProduto.guia_medidas ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                            className: "guiasMedida",
                            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
                              children: dadosProduto.guia_medidas.map(function (itemGuia, keyGuia) {
                                return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
                                  className: "itemGuia",
                                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                                    className: "color2",
                                    title: itemGuia.title,
                                    "data-tituloguia": "",
                                    "data-conteudoguia": "",
                                    onClick: "",
                                    children: itemGuia.title
                                  }, void 0, false, {
                                    fileName: _jsxFileName,
                                    lineNumber: 532,
                                    columnNumber: 47
                                  }, _this)
                                }, void 0, false, {
                                  fileName: _jsxFileName,
                                  lineNumber: 531,
                                  columnNumber: 43
                                }, _this);
                              })
                            }, void 0, false, {
                              fileName: _jsxFileName,
                              lineNumber: 528,
                              columnNumber: 39
                            }, this)
                          }, void 0, false, {
                            fileName: _jsxFileName,
                            lineNumber: 527,
                            columnNumber: 37
                          }, this) : null]
                        }, void 0, true, {
                          fileName: _jsxFileName,
                          lineNumber: 386,
                          columnNumber: 33
                        }, this)
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 385,
                        columnNumber: 29
                      }, this)]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 205,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 204,
                    columnNumber: 23
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 203,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 194,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 193,
              columnNumber: 17
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 192,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 191,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(TemplateFooter, {
          configs: configs.resposta
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 557,
          columnNumber: 13
        }, this)]
      }, void 0, true)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 178,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 177,
    columnNumber: 5
  }, this);
}

_s3(Product, "9ivUjH6RR1GoW/oyO3lSZCyjhfw=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"]];
});

_c5 = Product;
;

var _c, _c2, _c3, _c4, _c5;

$RefreshReg$(_c, "TemplateHeader$dynamic");
$RefreshReg$(_c2, "TemplateHeader");
$RefreshReg$(_c3, "TemplateFooter$dynamic");
$RefreshReg$(_c4, "TemplateFooter");
$RefreshReg$(_c5, "Product");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvW3NsdWddLmpzIl0sIm5hbWVzIjpbImlkU3RvcmVBcHAiLCJUZW1wbGF0ZUhlYWRlciIsImR5bmFtaWMiLCJUZW1wbGF0ZUZvb3RlciIsInNsaWRlclRodW1icyIsImluZmluaXRlIiwidmVydGljYWwiLCJzbGlkZXNUb1Nob3ciLCJzbGlkZXNUb1Njcm9sbCIsInNwZWVkIiwicmVzcG9uc2l2ZSIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsInNldHRpbmdzTW9iaWxlU2xpZGUiLCJkb3RzIiwiUHJvZHVjdCIsImNvbmZpZ3MiLCJtYWluTWVudSIsImRhZG9zUHJvZHV0byIsImluaXRpYWxJbWFnZXMiLCJnZXRJbml0aWFsSW1hZ2UiLCJpbWdJbmljaWFsIiwidGVtcEltZ0luaWNpYWwiLCJpIiwiaW1hZ2VzIiwicHVzaCIsImluaXRpYWxLZXkiLCJpbWFnZSIsInN1YnN0cmluZyIsImxhc3RJbmRleE9mIiwidW5kZWZpbmVkIiwic2hvdyIsInBvcHVwIiwidGh1bWIiLCJzZWxlY3RlZFF0ZFBhcmNlbCIsInNlbGVjdGVkVmFsUGFyY2VsIiwicXVhbnRpdHlCdXkiLCJzaGlwcGluZ01ldGhvZHMiLCJzZWxlY3RlZE9wdGlvbkJ1eSIsImdldFNwZWNpYWxWYWx1ZSIsImluaXRpYWxTcGVjaWFsVmFsdWUiLCJwYXJzZUludCIsImhhc19vcHRpb24iLCJzcGVjaWFsIiwicHJpY2UiLCJzcGVjaWFsVmFsdWUiLCJ1c2VTdGF0ZSIsInNldFNwZWNpYWxWYWx1ZSIsImdldE1haW5WYWx1ZSIsImluaXRpYWxNYWluVmFsdWUiLCJtYWluVmFsdWVTdGF0ZSIsIm1haW5WYWx1ZSIsInNldE1haW5WYWx1ZSIsImluaXRpYWxTdGF0ZSIsInNvbGRPdXRTdG9jayIsInNldFNvbGRPdXRTdG9jayIsImNoYW5nZVpvb20iLCJldiIsImltZ1Nob3ciLCJjdXJyZW50VGFyZ2V0IiwiZGF0YXNldCIsInNyY3Nob3ciLCJpbWdQb3B1cCIsInNyY3BvcHVwIiwidGVtcEltYWdlIiwibGFsYWxhIiwibGVsZWxlIiwicm91dGVyIiwidXNlUm91dGVyIiwiaXNGYWxsYmFjayIsInByb2R1Y3RfaWQiLCJuYW1lIiwicmVzcG9zdGEiLCJPYmplY3QiLCJrZXlzIiwibGVuZ3RoIiwibWFwIiwiaXRlbSIsImtleSIsInZpZGVvcyIsIml0ZW1WaWRlbyIsImtleVZpZGVvIiwiaWRfdmlkZW8iLCJsYWJlbHMiLCJwcm9tb190b3BfbGVmdCIsInByb21vX3RvcF9yaWdodCIsInByb21vX2JvdHRvbV9sZWZ0IiwicHJvbW9fYm90dG9tX3JpZ2h0Iiwic21hbGxJbWFnZSIsImFsdCIsImlzRmx1aWRXaWR0aCIsInNyYyIsImxhcmdlSW1hZ2UiLCJ3aWR0aCIsIndpZHRoWm9vbSIsImhlaWdodCIsIm1vZGVsIiwiX19odG1sIiwiZGlzY291bnRfcGVyY2VudCIsIm9wdGlvbnMiLCJ0eXBlIiwicHJvZHVjdF9vcHRpb25faWQiLCJvcHRpb25fdmFsdWUiLCJpdGVtT3B0aW9uIiwicHJvZHVjdF9vcHRpb25fdmFsdWVfaWQiLCJ0ZXh0X3ByZXZlbmRhIiwiZ3VpYV9tZWRpZGFzIiwiaXRlbUd1aWEiLCJrZXlHdWlhIiwidGl0bGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBR0EsSUFBSUEsVUFBVSxHQUFHLG1CQUFqQjtBQUNBLElBQUlDLGNBQWMsR0FBR0MsbURBQU8sTUFBQztBQUFBLFNBQU0sOEZBQU8sSUFBeUIsR0FBQ0YsVUFBMUIsR0FBcUMsb0JBQTVDLENBQU47QUFBQSxDQUFEO0FBQUE7QUFBQTtBQUFBLGtDQUFjLDBHQUF5QixHQUFDQSxVQUExQixHQUFxQyxvQkFBbkQ7QUFBQTtBQUFBLGNBQWMsNEJBQTBCQSxVQUExQixHQUFxQyxvQkFBbkQ7QUFBQTtBQUFBLEVBQTVCO01BQUlDLGM7QUFDSixJQUFJRSxjQUFjLEdBQUdELG1EQUFPLE9BQUM7QUFBQSxTQUFNLDhGQUFPLElBQXlCLEdBQUNGLFVBQTFCLEdBQXFDLG9CQUE1QyxDQUFOO0FBQUEsQ0FBRDtBQUFBO0FBQUE7QUFBQSxrQ0FBYywwR0FBeUIsR0FBQ0EsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxjQUFjLDRCQUEwQkEsVUFBMUIsR0FBcUMsb0JBQW5EO0FBQUE7QUFBQSxFQUE1QjtNQUFJRyxjO0FBRUosSUFBSUMsWUFBWSxHQUFHO0FBQ2pCQyxVQUFRLEVBQUUsS0FETztBQUVqQkMsVUFBUSxFQUFFLElBRk87QUFHakJDLGNBQVksRUFBRSxDQUhHO0FBSWpCQyxnQkFBYyxFQUFFLENBSkM7QUFLakJDLE9BQUssRUFBRSxHQUxVO0FBTWpCQyxZQUFVLEVBQUUsQ0FDVjtBQUNFQyxjQUFVLEVBQUUsR0FEZDtBQUVFQyxZQUFRLEVBQUU7QUFDUk4sY0FBUSxFQUFFLEtBREY7QUFFUkMsa0JBQVksRUFBRTtBQUZOO0FBRlosR0FEVTtBQU5LLENBQW5CO0FBaUJBLElBQUlNLG1CQUFtQixHQUFHO0FBQ3hCTixjQUFZLEVBQUUsQ0FEVTtBQUV4QkMsZ0JBQWMsRUFBRSxDQUZRO0FBR3hCQyxPQUFLLEVBQUUsR0FIaUI7QUFJeEJLLE1BQUksRUFBRTtBQUprQixDQUExQjs7QUFPZSxTQUFTQyxPQUFULE9BQXNEO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBLE1BQW5DQyxPQUFtQyxRQUFuQ0EsT0FBbUM7QUFBQSxNQUExQkMsUUFBMEIsUUFBMUJBLFFBQTBCO0FBQUEsTUFBaEJDLFlBQWdCLFFBQWhCQSxZQUFnQjtBQUNuRSxNQUFJQyxhQUFhLEdBQUcsRUFBcEI7O0FBQ0EsV0FBU0MsZUFBVCxDQUF5QkYsWUFBekIsRUFBdUM7QUFFckMsUUFBSUcsVUFBVSxHQUFHLEVBQWpCO0FBQ0EsUUFBSUMsY0FBYyxHQUFHLEVBQXJCOztBQUVBLFNBQUksSUFBSUMsQ0FBUixJQUFhTCxZQUFZLENBQUNNLE1BQTFCLEVBQWlDO0FBQy9CRixvQkFBYyxDQUFDRyxJQUFmLENBQW9CLENBQUNGLENBQUQsRUFBSUwsWUFBWSxDQUFDTSxNQUFiLENBQXFCRCxDQUFyQixDQUFKLENBQXBCO0FBQ0Q7O0FBRUQsUUFBSUcsVUFBVSxHQUFHUixZQUFZLENBQUNTLEtBQWIsQ0FBbUJDLFNBQW5CLENBQTZCVixZQUFZLENBQUNTLEtBQWIsQ0FBbUJFLFdBQW5CLENBQStCLEdBQS9CLElBQW9DLENBQWpFLENBQWpCOztBQUVBLFFBQUdYLFlBQVksQ0FBQ00sTUFBYixJQUF1Qk0sU0FBdkIsSUFBb0NaLFlBQVksQ0FBQ00sTUFBYixDQUFvQkUsVUFBcEIsS0FBbUMsSUFBMUUsRUFBK0U7QUFDN0VMLGdCQUFVLENBQUMsTUFBRCxDQUFWLEdBQXFCSCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JFLFVBQXBCLEVBQWdDSyxJQUFyRDtBQUNBVixnQkFBVSxDQUFDLE9BQUQsQ0FBVixHQUFzQkgsWUFBWSxDQUFDTSxNQUFiLENBQW9CRSxVQUFwQixFQUFnQ00sS0FBdEQ7QUFDQVgsZ0JBQVUsQ0FBQyxPQUFELENBQVYsR0FBc0JILFlBQVksQ0FBQ00sTUFBYixDQUFvQkUsVUFBcEIsRUFBZ0NPLEtBQXREO0FBQ0QsS0FKRCxNQUlNLElBQUdYLGNBQWMsQ0FBQyxDQUFELENBQWQsSUFBcUIsSUFBeEIsRUFBNkI7QUFDakNELGdCQUFVLEdBQUdDLGNBQWMsQ0FBQyxDQUFELENBQWQsQ0FBa0IsQ0FBbEIsQ0FBYjtBQUNEOztBQUVELFdBQU9ELFVBQVA7QUFDRDs7QUFDRCxNQUFHSCxZQUFILEVBQWdCO0FBQ2RDLGlCQUFhLEdBQUdDLGVBQWUsQ0FBQ0YsWUFBRCxDQUEvQjtBQUNEOztBQUVELE1BQUlnQixpQkFBaUIsR0FBRyxDQUF4QjtBQUNBLE1BQUlDLGlCQUFpQixHQUFHLENBQXhCO0FBQ0EsTUFBSUMsV0FBVyxHQUFHLENBQWxCO0FBQ0EsTUFBSUMsZUFBZSxHQUFHLElBQXRCO0FBQ0EsTUFBSUMsaUJBQWlCLEdBQUcsQ0FBeEI7O0FBRUEsV0FBU0MsZUFBVCxDQUF5QnJCLFlBQXpCLEVBQXVDO0FBQUE7O0FBQ3JDLFFBQUlzQixtQkFBbUIsR0FBRyxJQUExQjs7QUFDQSxRQUFHdEIsWUFBWSxJQUFJWSxTQUFuQixFQUE2QjtBQUMzQixVQUFHVyxRQUFRLENBQUN2QixZQUFZLENBQUN3QixVQUFkLENBQVIsSUFBcUMsQ0FBeEMsRUFBMEM7QUFBRTtBQUMxQyxZQUFHeEIsWUFBWSxDQUFDeUIsT0FBaEIsRUFBd0I7QUFDdEJILDZCQUFtQixHQUFHdEIsWUFBWSxDQUFDeUIsT0FBbkM7QUFDRCxTQUZELE1BRUs7QUFDSEgsNkJBQW1CLEdBQUd0QixZQUFZLENBQUMwQixLQUFuQztBQUNEO0FBQ0YsT0FORCxNQU1LO0FBQ0gsWUFBR0osbUJBQW1CLElBQUksSUFBdkIsSUFBK0J0QixZQUFZLENBQUN5QixPQUEvQyxFQUF1RDtBQUNyREgsNkJBQW1CLEdBQUd0QixZQUFZLENBQUN5QixPQUFuQztBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxRQUFNRSxZQUFZLEdBQUdDLHNEQUFRLENBQUNOLG1CQUFELENBQTdCO0FBQ0EsV0FBT0ssWUFBUDtBQUNEOztBQW5Ea0UsS0FpQzFETixlQWpDMEQ7O0FBQUEseUJBb0QzQkEsZUFBZSxDQUFDckIsWUFBRCxDQXBEWTtBQUFBO0FBQUEsTUFvRDVEMkIsWUFwRDREO0FBQUEsTUFvRDlDRSxlQXBEOEM7O0FBc0RuRSxXQUFTQyxZQUFULENBQXNCOUIsWUFBdEIsRUFBb0M7QUFBQTs7QUFDbEMsUUFBSStCLGdCQUFnQixHQUFHLElBQXZCOztBQUNBLFFBQUcvQixZQUFZLElBQUlZLFNBQW5CLEVBQTZCO0FBQzNCLFVBQUdXLFFBQVEsQ0FBQ3ZCLFlBQVksQ0FBQ3dCLFVBQWQsQ0FBUixJQUFxQyxDQUF4QyxFQUEwQztBQUFFO0FBQzFDLFlBQUd4QixZQUFZLENBQUN5QixPQUFoQixFQUF3QjtBQUN0Qk0sMEJBQWdCLEdBQUcvQixZQUFZLENBQUMwQixLQUFoQztBQUNELFNBRkQsTUFFSztBQUNISywwQkFBZ0IsR0FBRyxJQUFuQjtBQUNEO0FBQ0YsT0FORCxNQU1LO0FBQ0gsWUFBR0EsZ0JBQWdCLElBQUksSUFBcEIsSUFBNEIvQixZQUFZLENBQUMwQixLQUE1QyxFQUFrRDtBQUNoREssMEJBQWdCLEdBQUcvQixZQUFZLENBQUMwQixLQUFoQztBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxRQUFJTSxjQUFjLEdBQUdKLHNEQUFRLENBQUNHLGdCQUFELENBQTdCO0FBQ0EsV0FBT0MsY0FBUDtBQUNEOztBQXhFa0UsTUFzRDFERixZQXREMEQ7O0FBQUEsc0JBeUVuQ0EsWUFBWSxDQUFDOUIsWUFBRCxDQXpFdUI7QUFBQTtBQUFBLE1BeUU5RGlDLFNBekU4RDtBQUFBLE1BeUVuREMsWUF6RW1EOztBQUFBLGtCQTJFM0JOLHNEQUFRLENBQUMsWUFBTTtBQUNyRCxRQUFNTyxZQUFZLEdBQUcsQ0FBckI7QUFDQSxXQUFPQSxZQUFQO0FBQ0QsR0FIK0MsQ0EzRW1CO0FBQUEsTUEyRTVEQyxZQTNFNEQ7QUFBQSxNQTJFOUNDLGVBM0U4QztBQWlGbkU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFJTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFRSxNQUFJQyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDQyxFQUFELEVBQVE7QUFDdkIsUUFBTUMsT0FBTyxHQUFHRCxFQUFFLENBQUNFLGFBQUgsQ0FBaUJDLE9BQWpCLENBQXlCQyxPQUF6QztBQUNBLFFBQU1DLFFBQVEsR0FBR0wsRUFBRSxDQUFDRSxhQUFILENBQWlCQyxPQUFqQixDQUF5QkcsUUFBMUM7QUFDQSxRQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFDQUEsYUFBUyxDQUFDLE1BQUQsQ0FBVCxHQUFvQk4sT0FBcEI7QUFDQU0sYUFBUyxDQUFDLE9BQUQsQ0FBVCxHQUFxQkYsUUFBckIsQ0FMdUIsQ0FNdkI7QUFFRCxHQVJEOztBQVVBLE1BQUlHLE1BQU0sR0FBRzFCLGVBQWUsQ0FBQ3JCLFlBQUQsQ0FBNUI7QUFDQSxNQUFJZ0QsTUFBTSxHQUFHbEIsWUFBWSxDQUFDOUIsWUFBRCxDQUF6QjtBQUNBLE1BQU1pRCxNQUFNLEdBQUdDLDZEQUFTLEVBQXhCOztBQUNBLE1BQUksQ0FBQ0QsTUFBTSxDQUFDRSxVQUFSLElBQXNCLEVBQUNuRCxZQUFELGFBQUNBLFlBQUQsZUFBQ0EsWUFBWSxDQUFFb0QsVUFBZixDQUExQixFQUFxRDtBQUNuRCx3QkFBTyxxRUFBQyxpREFBRDtBQUFXLGdCQUFVLEVBQUU7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUFQO0FBQ0Q7O0FBQ0Qsc0JBQ0UscUVBQUMsMkRBQUQ7QUFBQSwyQkFDRSxxRUFBQyw4REFBRDtBQUFBLGdCQUdFSCxNQUFNLENBQUNFLFVBQVAsZ0JBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixnQkFHRTtBQUFBLGdDQUNFLHFFQUFDLGdEQUFEO0FBQUEsa0NBQ0U7QUFBQSxzQkFBUW5ELFlBQVksQ0FBQ3FEO0FBQXJCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREYsZUFFRTtBQUFNLGdCQUFJLEVBQUMsYUFBWDtBQUF5QixtQkFBTyxFQUFDO0FBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRkYsZUFHRTtBQUFNLGdCQUFJLEVBQUMsVUFBWDtBQUFzQixtQkFBTyxFQUFDO0FBQTlCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBSEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBTUUscUVBQUMsY0FBRDtBQUFnQixpQkFBTyxFQUFFdkQsT0FBTyxDQUFDd0QsUUFBakM7QUFBMkMsa0JBQVEsRUFBRXZELFFBQVEsQ0FBQ3VEO0FBQTlEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBTkYsZUFPRTtBQUFLLGNBQUksRUFBQyxjQUFWO0FBQUEsaUNBQ0U7QUFBSyxxQkFBUyxFQUFDLGVBQWY7QUFBQSxtQ0FDRTtBQUFLLHVCQUFTLEVBQUMsd0RBQWY7QUFBQSxxQ0FDRTtBQUFLLHlCQUFTLEVBQUMsV0FBZjtBQUFBLHdDQUNFO0FBQUksMkJBQVMsRUFBQyxZQUFkO0FBQUEsMENBQ0c7QUFBSSw2QkFBUyxFQUFDLGlCQUFkO0FBQUEsMkNBQ0c7QUFBRywwQkFBSSxFQUFDLEdBQVI7QUFBWSwyQkFBSyxFQUFDLG1CQUFsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURIO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBREgsZUFJRztBQUFJLDZCQUFTLEVBQUMsaUJBQWQ7QUFBQSw4QkFDSXRELFlBQVksQ0FBQ3FEO0FBRGpCO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBSkg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURGLGVBU0U7QUFBSywyQkFBUyxFQUFDLGFBQWY7QUFBQSx5Q0FDRTtBQUFLLDZCQUFTLEVBQUMsV0FBZjtBQUFBLDJDQUNJO0FBQUssK0JBQVMsRUFBQyxlQUFmO0FBQUEsOENBRUU7QUFBSyxpQ0FBUyxFQUFDLGtCQUFmO0FBQUEsZ0RBQ0U7QUFBSyxtQ0FBUyxFQUFDLHFCQUFmO0FBQUEsaURBQ0M7QUFBSyxxQ0FBUyxFQUFDLDBCQUFmO0FBQUEsc0NBRUlyRCxZQUFZLENBQUNNLE1BQWQsR0FDR2lELE1BQU0sQ0FBQ0MsSUFBUCxDQUFZeEQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ21ELE1BQWpDLEdBQTBDLENBQTNDLGdCQUNFLHFFQUFDLGtEQUFEO0FBQVEsdUNBQVMsRUFBQztBQUFsQiwrQkFBcUN2RSxZQUFyQztBQUFBLHlDQUdNcUUsTUFBTSxDQUFDQyxJQUFQLENBQVl4RCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDb0QsR0FBakMsQ0FBcUMsVUFBQ0MsSUFBRCxFQUFPQyxHQUFQO0FBQUEsb0RBQzdCO0FBQ0UsMkNBQVMsRUFBQyw2RUFEWjtBQUFBLHlEQUdJO0FBQUssdUNBQUcsRUFBRTVELFlBQVksQ0FBQ00sTUFBYixDQUFvQnFELElBQXBCLEVBQTBCN0MsS0FBcEM7QUFDRSx1Q0FBRyxFQUFFZCxZQUFZLENBQUNxRCxJQURwQjtBQUVFLG9EQUFjckQsWUFBWSxDQUFDTSxNQUFiLENBQW9CcUQsSUFBcEIsRUFBMEI5QyxJQUYxQztBQUdFLHFEQUFlYixZQUFZLENBQUNNLE1BQWIsQ0FBb0JxRCxJQUFwQixFQUEwQjdDLEtBSDNDO0FBSUUseUNBQUssRUFBQyxJQUpSO0FBSWEsMENBQU0sRUFBQyxJQUpwQjtBQUtFLDJDQUFPLEVBQUM7QUFMVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSEo7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FENkI7QUFBQSwrQkFBckMsQ0FITixFQWlCS2QsWUFBWSxDQUFDNkQsTUFBYixDQUFvQkosTUFBckIsR0FDRXpELFlBQVksQ0FBQzZELE1BQWIsQ0FBb0JILEdBQXBCLENBQXdCLFVBQUNJLFNBQUQsRUFBWUMsUUFBWjtBQUFBLG9EQUN0QjtBQUFLLDJDQUFTLEVBQUMsWUFBZjtBQUFBLHlEQUNFO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0Msb0RBQWNELFNBQVMsQ0FBQ0UsUUFBMUQ7QUFBQSw0REFDRTtBQUFLLHlDQUFHLHVDQUFnQ0YsU0FBUyxDQUFDRSxRQUExQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREYsZUFFRTtBQUFHLCtDQUFTLEVBQUMsb0JBQWI7QUFBa0MscURBQVk7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixtQ0FBaUNELFFBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRHNCO0FBQUEsK0JBQXhCLENBREYsR0FVRSxJQTNCTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREYsZ0JBaUNFO0FBQUEseUNBRUVSLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZeEQsWUFBWSxDQUFDTSxNQUF6QixFQUFpQ29ELEdBQWpDLENBQXFDLFVBQUNDLElBQUQsRUFBT0MsR0FBUDtBQUFBLG9EQUM3QjtBQUNFLDJDQUFTLEVBQUMsNkVBRFo7QUFBQSx5REFHSTtBQUFLLHVDQUFHLEVBQUU1RCxZQUFZLENBQUNNLE1BQWIsQ0FBb0JxRCxJQUFwQixFQUEwQjdDLEtBQXBDO0FBQ0UsdUNBQUcsRUFBRWQsWUFBWSxDQUFDcUQsSUFEcEI7QUFFRSxvREFBY3JELFlBQVksQ0FBQ00sTUFBYixDQUFvQnFELElBQXBCLEVBQTBCOUMsSUFGMUM7QUFHRSxxREFBZWIsWUFBWSxDQUFDTSxNQUFiLENBQW9CcUQsSUFBcEIsRUFBMEI3QyxLQUgzQztBQUlFLHlDQUFLLEVBQUMsSUFKUjtBQUlhLDBDQUFNLEVBQUMsSUFKcEI7QUFLRSwyQ0FBTyxFQUFDO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBRDZCO0FBQUEsK0JBQXJDLENBRkYsRUFpQkdkLFlBQVksQ0FBQzZELE1BQWIsQ0FBb0JKLE1BQXJCLEdBQ0V6RCxZQUFZLENBQUM2RCxNQUFiLENBQW9CSCxHQUFwQixDQUF3QixVQUFDSSxTQUFELEVBQVlDLFFBQVo7QUFBQSxvREFDdEI7QUFBSywyQ0FBUyxFQUFDLFlBQWY7QUFBQSx5REFDRTtBQUFHLHdDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBTyxFQUFDLEVBQS9CO0FBQWtDLG9EQUFjRCxTQUFTLENBQUNFLFFBQTFEO0FBQUEsNERBQ0U7QUFBSyx5Q0FBRyx1Q0FBZ0NGLFNBQVMsQ0FBQ0UsUUFBMUM7QUFBUjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQURGLGVBRUU7QUFBRywrQ0FBUyxFQUFDLG9CQUFiO0FBQWtDLHFEQUFZO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREYsbUNBQWlDRCxRQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQURzQjtBQUFBLCtCQUF4QixDQURGLEdBVUUsSUEzQko7QUFBQSw0Q0FsQ0osR0FnRUU7QUFsRUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0NBREYsZUF5RUk7QUFBSyxtQ0FBUyxFQUFDLGdDQUFmO0FBQUEsaURBQ0U7QUFBSyxxQ0FBUyxFQUFDLFVBQWY7QUFBQSx1Q0FFSy9ELFlBQVksQ0FBQ2lFLE1BQWIsQ0FBb0JDLGNBQXBCLENBQW1DekQsS0FBbkMsSUFBNEMsSUFBN0MsZ0JBQ0U7QUFBSyx1Q0FBUyxFQUFDLHdCQUFmO0FBQUEscURBQ0U7QUFBSyxtQ0FBRyxFQUFFVCxZQUFZLENBQUNpRSxNQUFiLENBQW9CQyxjQUFwQixDQUFtQ3pEO0FBQTdDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURGLEdBS0UsSUFQTixFQVVLVCxZQUFZLENBQUNpRSxNQUFiLENBQW9CRSxlQUFwQixDQUFvQzFELEtBQXBDLElBQTZDLElBQTlDLGdCQUNFO0FBQUssdUNBQVMsRUFBQyx5QkFBZjtBQUFBLHFEQUNFO0FBQUssbUNBQUcsRUFBRVQsWUFBWSxDQUFDaUUsTUFBYixDQUFvQkUsZUFBcEIsQ0FBb0MxRDtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixHQUtFLElBZk4sRUFrQktULFlBQVksQ0FBQ2lFLE1BQWIsQ0FBb0JHLGlCQUFwQixDQUFzQzNELEtBQXRDLElBQStDLElBQWhELGdCQUNFO0FBQUssdUNBQVMsRUFBQywyQkFBZjtBQUFBLHFEQUNFO0FBQUssbUNBQUcsRUFBRVQsWUFBWSxDQUFDaUUsTUFBYixDQUFvQkcsaUJBQXBCLENBQXNDM0Q7QUFBaEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBREYsR0FLRSxJQXZCTixFQTBCS1QsWUFBWSxDQUFDaUUsTUFBYixDQUFvQkksa0JBQXBCLENBQXVDNUQsS0FBdkMsSUFBZ0QsSUFBakQsZ0JBQ0U7QUFBSyx1Q0FBUyxFQUFDLDRCQUFmO0FBQUEscURBQ0U7QUFBSyxtQ0FBRyxFQUFFVCxZQUFZLENBQUNpRSxNQUFiLENBQW9CSSxrQkFBcEIsQ0FBdUM1RDtBQUFqRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERixHQUtFLElBL0JOLGVBa0NFLHFFQUFDLDJEQUFELG9CQUF1QjtBQUNmNkQsd0NBQVUsRUFBRTtBQUNaQyxtQ0FBRyxFQUFFdkUsWUFBWSxDQUFDcUQsSUFETjtBQUVabUIsNENBQVksRUFBRSxJQUZGO0FBR1pDLG1DQUFHLEVBQUV4RSxhQUFhLENBQUNZO0FBSFAsK0JBREc7QUFNbkI2RCx3Q0FBVSxFQUFFO0FBQ1JELG1DQUFHLEVBQUV4RSxhQUFhLENBQUNhLEtBRFg7QUFFUjZELHFDQUFLLEVBQUU3RSxPQUFPLENBQUM4RSxTQUZQO0FBR1JDLHNDQUFNLEVBQUUvRSxPQUFPLENBQUM4RTtBQUhSO0FBTk8sNkJBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBbENGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0NBekVKLGVBNEhFO0FBQUssbUNBQVMsRUFBQyxpQkFBZjtBQUFBLG9DQUNJckIsTUFBTSxDQUFDQyxJQUFQLENBQVl4RCxZQUFZLENBQUNNLE1BQXpCLEtBQW9DaUQsTUFBTSxDQUFDQyxJQUFQLENBQVl4RCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDbUQsTUFBakMsR0FBMEMsQ0FBL0UsZ0JBQ0M7QUFBSyxxQ0FBUyxFQUFDLGdCQUFmO0FBQUEsbURBQ0UscUVBQUMsa0RBQUQ7QUFBUSx1Q0FBUyxFQUFDO0FBQWxCLCtCQUFxQzlELG1CQUFyQztBQUFBLHlDQUVNNEQsTUFBTSxDQUFDQyxJQUFQLENBQVl4RCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDb0QsR0FBakMsQ0FBcUMsVUFBQ0MsSUFBRCxFQUFPQyxHQUFQO0FBQUEsb0RBQ2pDO0FBQUssMkNBQVMsRUFBQyxVQUFmO0FBQUEseURBQ0U7QUFBSyx1Q0FBRyxFQUFFNUQsWUFBWSxDQUFDTSxNQUFiLENBQW9CcUQsSUFBcEIsRUFBMEI3QyxLQUFwQztBQUEyQyx1Q0FBRyxFQUFFZCxZQUFZLENBQUNxRDtBQUE3RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSx5Q0FEaUM7QUFBQSwrQkFBckMsQ0FGTixFQVNLckQsWUFBWSxDQUFDNkQsTUFBYixDQUFvQkosTUFBckIsR0FDRXpELFlBQVksQ0FBQzZELE1BQWIsQ0FBb0JILEdBQXBCLENBQXdCLFVBQUNJLFNBQUQsRUFBWUMsUUFBWjtBQUFBLG9EQUN0QjtBQUFLLDJDQUFTLEVBQUMscUJBQWY7QUFBQSx5REFDRTtBQUFHLHdDQUFJLEVBQUMsY0FBUjtBQUF1QiwyQ0FBTyxFQUFDLEVBQS9CO0FBQWtDLG9EQUFjRCxTQUFTLENBQUNFLFFBQTFEO0FBQUEsNERBQ0U7QUFBSyx5Q0FBRyx1Q0FBZ0NGLFNBQVMsQ0FBQ0UsUUFBMUM7QUFBUjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQURGLGVBRUU7QUFBRywrQ0FBUyxFQUFDLG9CQUFiO0FBQWtDLHFEQUFZO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREYsbUNBQTBDRCxRQUExQztBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQURzQjtBQUFBLCtCQUF4QixDQURGLEdBVUUsSUFuQk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQ0FERCxHQXlCRVIsTUFBTSxDQUFDQyxJQUFQLENBQVl4RCxZQUFZLENBQUNNLE1BQXpCLEtBQW9DaUQsTUFBTSxDQUFDQyxJQUFQLENBQVl4RCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDbUQsTUFBakMsR0FBMEMsQ0FBL0UsZ0JBQ0U7QUFBQSx1Q0FFSUYsTUFBTSxDQUFDQyxJQUFQLENBQVl4RCxZQUFZLENBQUNNLE1BQXpCLEVBQWlDb0QsR0FBakMsQ0FBcUMsVUFBQ0MsSUFBRCxFQUFPQyxHQUFQO0FBQUEsa0RBQ25DO0FBQUsseUNBQVMsRUFBQyxVQUFmO0FBQUEsdURBQ0U7QUFBSyxxQ0FBRyxFQUFFNUQsWUFBWSxDQUFDTSxNQUFiLENBQW9CcUQsSUFBcEIsRUFBMEI3QyxLQUFwQztBQUEyQyxxQ0FBRyxFQUFFZCxZQUFZLENBQUNxRDtBQUE3RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSx1Q0FEbUM7QUFBQSw2QkFBckMsQ0FGSixFQVNLckQsWUFBWSxDQUFDNkQsTUFBYixDQUFvQkosTUFBckIsR0FDRXpELFlBQVksQ0FBQzZELE1BQWIsQ0FBb0JILEdBQXBCLENBQXdCLFVBQUNJLFNBQUQsRUFBWUMsUUFBWjtBQUFBLGtEQUN0QjtBQUFLLHlDQUFTLEVBQUMscUJBQWY7QUFBQSx1REFDRTtBQUFHLHNDQUFJLEVBQUMsY0FBUjtBQUF1Qix5Q0FBTyxFQUFDLEVBQS9CO0FBQWtDLGtEQUFjRCxTQUFTLENBQUNFLFFBQTFEO0FBQUEsMERBQ0U7QUFBSyx1Q0FBRyx1Q0FBZ0NGLFNBQVMsQ0FBQ0UsUUFBMUM7QUFBUjtBQUFBO0FBQUE7QUFBQTtBQUFBLDJDQURGLGVBRUU7QUFBRyw2Q0FBUyxFQUFDLG9CQUFiO0FBQWtDLG1EQUFZO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkNBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREYsaUNBQTBDRCxRQUExQztBQUFBO0FBQUE7QUFBQTtBQUFBLHVDQURzQjtBQUFBLDZCQUF4QixDQURGLEdBVUUsSUFuQk47QUFBQSwwQ0FERixHQXdCQTtBQWxESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdDQTVIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsOEJBRkYsZUFvTEU7QUFBSyxpQ0FBUyxFQUFDLGVBQWY7QUFBQSwrQ0FDSTtBQUFLLG1DQUFTLEVBQUMseURBQWY7QUFBQSxrREFDRTtBQUFLLHFDQUFTLEVBQUMsaUJBQWY7QUFBQSxvREFDRztBQUFJLHVDQUFTLEVBQUMsZ0JBQWQ7QUFBQSx3Q0FBZ0MvRCxZQUFZLENBQUNxRDtBQUE3QztBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURILEVBR01yRCxZQUFZLENBQUM4RSxLQUFkLGdCQUNDO0FBQUksdUNBQVMsRUFBQyxjQUFkO0FBQUEscURBQ0k7QUFBQSx3REFBSTtBQUFNLDJDQUFTLEVBQUMsV0FBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBQUosT0FBNkM5RSxZQUFZLENBQUM4RSxLQUExRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQURELEdBS0MsSUFSTixlQVdHO0FBQUssdUNBQVMsRUFBQyxTQUFmO0FBQUEscURBQ0c7QUFBRyx5Q0FBUyxFQUFDLFlBQWI7QUFBMEIsdUNBQU8sRUFBQyxFQUFsQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURIO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBWEgsRUFnQk05RSxZQUFZLFNBQVosSUFBc0IsRUFBdkIsZ0JBQ0M7QUFBSyx1Q0FBUyxFQUFDLFdBQWY7QUFBQSxxREFDQztBQUFLLHlDQUFTLEVBQUMsZUFBZjtBQUFBLHVEQUNDO0FBQUsseURBQXVCLEVBQUU7QUFBRStFLDBDQUFNLEVBQUUvRSxZQUFZO0FBQXRCO0FBQTlCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FERCxHQU9DLElBdkJOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQ0FERixlQTZCRTtBQUFLLHFDQUFTLEVBQUMsU0FBZjtBQUFBLG9EQUVFO0FBQUssdUNBQVMsRUFBQyxZQUFmO0FBQUEsc0RBQ0U7QUFBSyx5Q0FBUyxFQUFDLGVBQWY7QUFBQSwyQ0FDTStDLE1BQUQsZ0JBQ0M7QUFBSywyQ0FBUyxFQUFDLFVBQWY7QUFBQSxpRUFDSztBQUFNLDZDQUFTLEVBQUMsb0JBQWhCO0FBQUEsOENBQXNDQztBQUF0QztBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQURMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FERCxHQUtDLEVBTk4sZUFRSTtBQUFLLDJDQUFTLEVBQUMsc0JBQWY7QUFBQSw0Q0FDSUQsTUFBRCxnQkFDQztBQUFBLG9FQUNNO0FBQU0sK0NBQVMsRUFBQyxjQUFoQjtBQUFBLGdEQUFnQ0E7QUFBaEM7QUFBQTtBQUFBO0FBQUE7QUFBQSw0Q0FETixlQUVDO0FBQU0sK0NBQVMsRUFBQyxlQUFoQjtBQUFBLGdEQUFpQy9DLFlBQVksQ0FBQ2dGO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsNENBRkQ7QUFBQSxrREFERCxnQkFNQztBQUFBLG9FQUFPO0FBQUEsZ0RBQU9oQztBQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUEsNENBQVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEo7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FSSixFQW1CTWhDLGlCQUFpQixJQUFJLEVBQXJCLElBQTJCQyxpQkFBaUIsSUFBSSxFQUFqRCxnQkFDQztBQUFHLDJDQUFTLEVBQUMsZ0JBQWI7QUFBQSxpRUFBaUM7QUFBTSw2Q0FBUyxFQUFDLFNBQWhCO0FBQUEsK0NBQTJCRCxpQkFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUFqQyx1QkFBMEY7QUFBTSw2Q0FBUyxFQUFDLFNBQWhCO0FBQUEsOENBQTJCQztBQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQUExRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0NBREQsR0FHQyxJQXRCTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREYsRUE0QktqQixZQUFZLElBQUksRUFBakIsR0FDR3VCLFFBQVEsQ0FBQ3ZCLFlBQVksQ0FBQ3dCLFVBQWQsQ0FBUixJQUFxQyxDQUF0QyxnQkFDQztBQUFLLHlDQUFTLEVBQUMsYUFBZjtBQUFBLDBDQUVLeEIsWUFBWSxDQUFDaUYsT0FBYixDQUFxQnZCLEdBQXJCLENBQXlCLFVBQUNDLElBQUQsRUFBT0MsR0FBUDtBQUFBLHNEQUN2QjtBQUFLLDZDQUFTLEVBQUMsWUFBZjtBQUFBLDREQUNFO0FBQUEsZ0RBQVFELElBQUksQ0FBQ047QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZDQURGLEVBR0lNLElBQUksQ0FBQ3VCLElBQUwsSUFBYSxNQUFkLGdCQUNDO0FBQUssK0NBQVMsRUFBQyxXQUFmO0FBQUEsOERBQ0U7QUFBTywwQ0FBRSxFQUFDLGFBQVY7QUFBd0IsaURBQVMsRUFBQyxHQUFsQztBQUFzQyw0Q0FBSSxFQUFDLE1BQTNDO0FBQWtELHNEQUFZdkIsSUFBSSxDQUFDd0IsaUJBQW5FO0FBQXNGLGlEQUFTLEVBQUMsT0FBaEc7QUFBd0csNENBQUksRUFBQztBQUE3RztBQUFBO0FBQUE7QUFBQTtBQUFBLCtDQURGLGVBRUU7QUFBSyxpREFBUyxFQUFDLE1BQWY7QUFBQSxnRUFDSTtBQUFHLG1EQUFTLEVBQUMsOEJBQWI7QUFBNEMseURBQVk7QUFBeEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxpREFESixlQUdJO0FBQUssbURBQVMsRUFBQyxVQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlEQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwrQ0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBREQsZ0JBWUM7QUFBSSwrQ0FBUyxFQUFDLGFBQWQ7QUFBQSxnREFFSXhCLElBQUksQ0FBQ3lCLFlBQUwsQ0FBa0IxQixHQUFsQixDQUFzQixVQUFDMkIsVUFBRCxFQUFhekIsR0FBYjtBQUFBLDREQUNwQjtBQUFBLGlFQUNFO0FBQU8scURBQVMsRUFBRXhDLGlCQUFpQixLQUFLdUMsSUFBSSxDQUFDd0IsaUJBQUwsR0FBdUIsR0FBdkIsR0FBMkJFLFVBQVUsQ0FBQ0MsdUJBQTVELEdBQXNGLFFBQXRGLEdBQWlHLGlCQUFuSDtBQUFzSSxtREFBTyxFQUFDLEVBQTlJO0FBQWlKLGdEQUFJLEVBQUMsY0FBdEo7QUFBQSxzREFBc0tELFVBQVUsQ0FBQ2hDO0FBQWpMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlEQURvQjtBQUFBLHVDQUF0QjtBQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkNBZko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJDQUR1QjtBQUFBLGlDQUF6QjtBQUZMO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREQsR0FrQ0EsRUFuQ0YsR0FvQ0QsRUFoRUg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9DQUZGLGVBc0VJO0FBQUssdUNBQVMsRUFBQyxZQUFmO0FBQUEsc0RBQ0U7QUFBSyx5Q0FBUyxFQUFDLGNBQWY7QUFBQSx3REFDRztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FESCxlQUVHO0FBQUssMkNBQVMsRUFBQyxpQkFBZjtBQUFBLDBEQUNHO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0MsNkNBQVMsRUFBQyxTQUE1QztBQUFBLDJEQUFzRDtBQUFHLCtDQUFTLEVBQUMsYUFBYjtBQUEyQixxREFBWTtBQUF2QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXREO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBREgsZUFFRztBQUFPLHNDQUFFLEVBQUMsYUFBVjtBQUF3Qix3Q0FBSSxFQUFDLE1BQTdCO0FBQW9DLHdDQUFJLEVBQUMsY0FBekM7QUFBd0QseUNBQUssRUFBR25DLFdBQWhFO0FBQThFLDZDQUFTLEVBQUM7QUFBeEY7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQ0FGSCxlQUdHO0FBQUcsd0NBQUksRUFBQyxjQUFSO0FBQXVCLDJDQUFPLEVBQUMsRUFBL0I7QUFBa0MsNkNBQVMsRUFBQyxTQUE1QztBQUFBLDJEQUFzRDtBQUFHLCtDQUFTLEVBQUMsWUFBYjtBQUEwQixxREFBWTtBQUF0QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXREO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBSEg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdDQUZIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERixFQVVJa0IsWUFBWSxJQUFJLElBQWhCLElBQXdCLENBQUNBLFlBQTFCLGdCQUNDO0FBQUsseUNBQVMsRUFBQyxlQUFmO0FBQUEsdURBQ007QUFBRyxzQ0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQVMsRUFBQyxtQkFBakM7QUFBcUQseUNBQU8sRUFBQyxFQUE3RDtBQUFBLDBEQUNHO0FBQUcsNkNBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsMENBREgsT0FDZ0QsU0FEaEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRE47QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FERCxHQU1FQSxZQUFZLElBQUksSUFBaEIsSUFBd0JBLFlBQXpCLGdCQUNBO0FBQUsseUNBQVMsRUFBQyxlQUFmO0FBQUEsdURBQ007QUFBRyxzQ0FBSSxFQUFDLGNBQVI7QUFBdUIsMkNBQVMsRUFBQyx3QkFBakM7QUFBMEQsb0RBQWdCcEMsWUFBWSxDQUFDb0QsVUFBdkY7QUFBbUcseUNBQU8sRUFBQyxFQUEzRztBQUFBLDBEQUNHO0FBQUcsNkNBQVMsRUFBQyxnQkFBYjtBQUE4QixtREFBWTtBQUExQztBQUFBO0FBQUE7QUFBQTtBQUFBLDBDQURIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUROO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBREEsR0FPQSxJQXZCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0NBdEVKLEVBaUdJcEQsWUFBWSxDQUFDdUYsYUFBYixJQUE2QixJQUE3QixJQUFxQ3ZGLFlBQVksQ0FBQ3VGLGFBQWIsSUFBOEIsRUFBcEUsZ0JBQ0M7QUFBQSxzREFDRTtBQUFHLHlDQUFTLEVBQUMsZUFBYjtBQUFBLHdEQUE2QjtBQUFPLG9DQUFFLEVBQUMsWUFBVjtBQUF1QixzQ0FBSSxFQUFDLFVBQTVCO0FBQXVDLDBDQUFRLEVBQUM7QUFBaEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FBN0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQURGLGVBRUU7QUFBSyx5Q0FBUyxFQUFDLGNBQWY7QUFBQSx3REFDRTtBQUFJLDJDQUFTLEVBQUMsS0FBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3Q0FERixFQUVHdkYsWUFBWSxDQUFDdUYsYUFGaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQUZGO0FBQUEsNENBREQsR0FTQyxJQTFHSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0NBN0JGLEVBNElJdkYsWUFBWSxDQUFDd0YsWUFBZCxnQkFDQztBQUFLLHFDQUFTLEVBQUMsYUFBZjtBQUFBLG1EQUNFO0FBQUEsd0NBRUV4RixZQUFZLENBQUN3RixZQUFiLENBQTBCOUIsR0FBMUIsQ0FBOEIsVUFBQytCLFFBQUQsRUFBV0MsT0FBWDtBQUFBLG9EQUM1QjtBQUFJLDJDQUFTLEVBQUMsVUFBZDtBQUFBLHlEQUNJO0FBQUcsNkNBQVMsRUFBQyxRQUFiO0FBQXNCLHlDQUFLLEVBQUVELFFBQVEsQ0FBQ0UsS0FBdEM7QUFBNkMsdURBQWdCLEVBQTdEO0FBQWdFLHlEQUFrQixFQUFsRjtBQUFxRiwyQ0FBTyxFQUFDLEVBQTdGO0FBQUEsOENBQWlHRixRQUFRLENBQUNFO0FBQTFHO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlDQUQ0QjtBQUFBLCtCQUE5QjtBQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtDQURELEdBYUMsSUF6Sko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSw4QkFwTEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBVEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQVBGLGVBcVhFLHFFQUFDLGNBQUQ7QUFBZ0IsaUJBQU8sRUFBRTdGLE9BQU8sQ0FBQ3dEO0FBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBclhGO0FBQUE7QUFOSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBbVlEOztJQTFnQnVCekQsTztVQW1JUHFELHFEOzs7TUFuSU9yRCxPO0FBMGdCdkIiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvW3NsdWddLmYzMTdjYjhkMmU4MzY0NTczYjAxLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuaW1wb3J0IEVycm9yUGFnZSBmcm9tIFwibmV4dC9lcnJvclwiO1xyXG5pbXBvcnQgSGVhZCBmcm9tIFwibmV4dC9oZWFkXCI7XHJcbmltcG9ydCBkeW5hbWljIGZyb20gJ25leHQvZHluYW1pYydcclxuaW1wb3J0IHsgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCdcclxuaW1wb3J0IFJlYWN0SW1hZ2VNYWduaWZ5IGZyb20gJ3JlYWN0LWltYWdlLW1hZ25pZnknXHJcbmltcG9ydCBTbGlkZXIgZnJvbSBcInJlYWN0LXNsaWNrXCI7XHJcblxyXG5pbXBvcnQgQ29udGFpbmVyIGZyb20gXCJAL2NvbXBvbmVudHMvY29udGFpbmVyXCI7XHJcbmltcG9ydCBMYXlvdXQgZnJvbSBcIkAvY29tcG9uZW50cy9sYXlvdXRcIjtcclxuaW1wb3J0IHsgZ2V0Q29uZmlncywgZ2V0TWFpbk1lbnUsIGdldFByb2R1Y3RCeVNsdWcsIGdldFByb2R1Y3RzQnlDYXRlZ29yeSB9IGZyb20gXCJAL2xpYi9hcGlcIjtcclxuXHJcbnZhciBpZFN0b3JlQXBwID0gJ240OXNob3B2Ml90cmlqb2lhJztcclxudmFyIFRlbXBsYXRlSGVhZGVyID0gZHluYW1pYygoKSA9PiBpbXBvcnQoJ0AvY29tcG9uZW50cy90ZW1wbGF0ZXMvJytpZFN0b3JlQXBwKycvY29tcG9uZW50cy9oZWFkZXInKSlcclxudmFyIFRlbXBsYXRlRm9vdGVyID0gZHluYW1pYygoKSA9PiBpbXBvcnQoJ0AvY29tcG9uZW50cy90ZW1wbGF0ZXMvJytpZFN0b3JlQXBwKycvY29tcG9uZW50cy9mb290ZXInKSlcclxuXHJcbmxldCBzbGlkZXJUaHVtYnMgPSB7XHJcbiAgaW5maW5pdGU6IGZhbHNlLFxyXG4gIHZlcnRpY2FsOiB0cnVlLFxyXG4gIHNsaWRlc1RvU2hvdzogNCxcclxuICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICBzcGVlZDogNTAwLFxyXG4gIHJlc3BvbnNpdmU6IFtcclxuICAgIHtcclxuICAgICAgYnJlYWtwb2ludDogNDE1LFxyXG4gICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgIHZlcnRpY2FsOiBmYWxzZSxcclxuICAgICAgICBzbGlkZXNUb1Nob3c6IDNcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIF1cclxufVxyXG5cclxudmFyIHNldHRpbmdzTW9iaWxlU2xpZGUgPSB7XHJcbiAgc2xpZGVzVG9TaG93OiAxLFxyXG4gIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gIHNwZWVkOiA1MDAsXHJcbiAgZG90czogdHJ1ZVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBQcm9kdWN0KHsgY29uZmlncywgbWFpbk1lbnUsIGRhZG9zUHJvZHV0byB9KSB7XHJcbiAgdmFyIGluaXRpYWxJbWFnZXMgPSBbXTtcclxuICBmdW5jdGlvbiBnZXRJbml0aWFsSW1hZ2UoZGFkb3NQcm9kdXRvKSB7XHJcbiAgICBcclxuICAgIHZhciBpbWdJbmljaWFsID0gW107XHJcbiAgICB2YXIgdGVtcEltZ0luaWNpYWwgPSBbXTtcclxuICAgIFxyXG4gICAgZm9yKHZhciBpIGluIGRhZG9zUHJvZHV0by5pbWFnZXMpe1xyXG4gICAgICB0ZW1wSW1nSW5pY2lhbC5wdXNoKFtpLCBkYWRvc1Byb2R1dG8uaW1hZ2VzIFtpXV0pO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciBpbml0aWFsS2V5ID0gZGFkb3NQcm9kdXRvLmltYWdlLnN1YnN0cmluZyhkYWRvc1Byb2R1dG8uaW1hZ2UubGFzdEluZGV4T2YoJy8nKSsxKVxyXG4gICAgXHJcbiAgICBpZihkYWRvc1Byb2R1dG8uaW1hZ2VzICE9IHVuZGVmaW5lZCAmJiBkYWRvc1Byb2R1dG8uaW1hZ2VzW2luaXRpYWxLZXldICE9IG51bGwpe1xyXG4gICAgICBpbWdJbmljaWFsWydzaG93J10gPSBkYWRvc1Byb2R1dG8uaW1hZ2VzW2luaXRpYWxLZXldLnNob3cgXHJcbiAgICAgIGltZ0luaWNpYWxbJ3BvcHVwJ10gPSBkYWRvc1Byb2R1dG8uaW1hZ2VzW2luaXRpYWxLZXldLnBvcHVwXHJcbiAgICAgIGltZ0luaWNpYWxbJ3RodW1iJ10gPSBkYWRvc1Byb2R1dG8uaW1hZ2VzW2luaXRpYWxLZXldLnRodW1iXHJcbiAgICB9ZWxzZSBpZih0ZW1wSW1nSW5pY2lhbFswXSAhPSBudWxsKXtcclxuICAgICAgaW1nSW5pY2lhbCA9IHRlbXBJbWdJbmljaWFsWzBdWzFdXHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGltZ0luaWNpYWw7XHJcbiAgfVxyXG4gIGlmKGRhZG9zUHJvZHV0byl7XHJcbiAgICBpbml0aWFsSW1hZ2VzID0gZ2V0SW5pdGlhbEltYWdlKGRhZG9zUHJvZHV0byk7XHJcbiAgfVxyXG5cclxuICB2YXIgc2VsZWN0ZWRRdGRQYXJjZWwgPSAxO1xyXG4gIHZhciBzZWxlY3RlZFZhbFBhcmNlbCA9IDE7XHJcbiAgdmFyIHF1YW50aXR5QnV5ID0gMTtcclxuICB2YXIgc2hpcHBpbmdNZXRob2RzID0gbnVsbDtcclxuICB2YXIgc2VsZWN0ZWRPcHRpb25CdXkgPSAxO1xyXG4gIFxyXG4gIGZ1bmN0aW9uIGdldFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8pIHtcclxuICAgIHZhciBpbml0aWFsU3BlY2lhbFZhbHVlID0gbnVsbDtcclxuICAgIGlmKGRhZG9zUHJvZHV0byAhPSB1bmRlZmluZWQpe1xyXG4gICAgICBpZihwYXJzZUludChkYWRvc1Byb2R1dG8uaGFzX29wdGlvbikgPT0gMCl7IC8vIFBST0RVVE8gU0VNIE9QQ09FU1xyXG4gICAgICAgIGlmKGRhZG9zUHJvZHV0by5zcGVjaWFsKXtcclxuICAgICAgICAgIGluaXRpYWxTcGVjaWFsVmFsdWUgPSBkYWRvc1Byb2R1dG8uc3BlY2lhbDtcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgIGluaXRpYWxTcGVjaWFsVmFsdWUgPSBkYWRvc1Byb2R1dG8ucHJpY2U7XHJcbiAgICAgICAgfVxyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICBpZihpbml0aWFsU3BlY2lhbFZhbHVlID09IG51bGwgJiYgZGFkb3NQcm9kdXRvLnNwZWNpYWwpe1xyXG4gICAgICAgICAgaW5pdGlhbFNwZWNpYWxWYWx1ZSA9IGRhZG9zUHJvZHV0by5zcGVjaWFsXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGNvbnN0IHNwZWNpYWxWYWx1ZSA9IHVzZVN0YXRlKGluaXRpYWxTcGVjaWFsVmFsdWUpO1xyXG4gICAgcmV0dXJuIHNwZWNpYWxWYWx1ZTtcclxuICB9XHJcbiAgY29uc3QgW3NwZWNpYWxWYWx1ZSwgc2V0U3BlY2lhbFZhbHVlXSA9IGdldFNwZWNpYWxWYWx1ZShkYWRvc1Byb2R1dG8pO1xyXG5cclxuICBmdW5jdGlvbiBnZXRNYWluVmFsdWUoZGFkb3NQcm9kdXRvKSB7XHJcbiAgICB2YXIgaW5pdGlhbE1haW5WYWx1ZSA9IG51bGw7XHJcbiAgICBpZihkYWRvc1Byb2R1dG8gIT0gdW5kZWZpbmVkKXtcclxuICAgICAgaWYocGFyc2VJbnQoZGFkb3NQcm9kdXRvLmhhc19vcHRpb24pID09IDApeyAvLyBQUk9EVVRPIFNFTSBPUENPRVNcclxuICAgICAgICBpZihkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgICBpbml0aWFsTWFpblZhbHVlID0gZGFkb3NQcm9kdXRvLnByaWNlO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgaW5pdGlhbE1haW5WYWx1ZSA9IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICBpZihpbml0aWFsTWFpblZhbHVlID09IG51bGwgJiYgZGFkb3NQcm9kdXRvLnByaWNlKXtcclxuICAgICAgICAgIGluaXRpYWxNYWluVmFsdWUgPSBkYWRvc1Byb2R1dG8ucHJpY2VcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgdmFyIG1haW5WYWx1ZVN0YXRlID0gdXNlU3RhdGUoaW5pdGlhbE1haW5WYWx1ZSk7XHJcbiAgICByZXR1cm4gbWFpblZhbHVlU3RhdGU7XHJcbiAgfVxyXG4gIHZhciBbbWFpblZhbHVlLCBzZXRNYWluVmFsdWVdID0gZ2V0TWFpblZhbHVlKGRhZG9zUHJvZHV0byk7XHJcblxyXG4gIGNvbnN0IFtzb2xkT3V0U3RvY2ssIHNldFNvbGRPdXRTdG9ja10gPSB1c2VTdGF0ZSgoKSA9PiB7XHJcbiAgICBjb25zdCBpbml0aWFsU3RhdGUgPSAwO1xyXG4gICAgcmV0dXJuIGluaXRpYWxTdGF0ZTtcclxuICB9KTtcclxuICBcclxuICBcclxuICAvKmlmKHBhcnNlSW50KGRhZG9zUHJvZHV0by5oYXNfb3B0aW9uKSA9PSAwKXsgLy8gUFJPRFVUTyBTRU0gT1BDT0VTXHJcbiAgICAgIFxyXG4gICAgY29uc3QgdmFsb3JBdHVhbCA9IGRhZG9zUHJvZHV0by5wcmljZVxyXG4gICAgaWYoZGFkb3NQcm9kdXRvLnNwZWNpYWwpe1xyXG4gICAgICBzZXRTZWxlY3RlZE1haW5WYWx1ZSh2YWxvckF0dWFsKTtcclxuICAgICAgc2V0U2VsZWN0ZWRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvLnNwZWNpYWwpO1xyXG4gICAgfWVsc2V7XHJcbiAgICAgIHNldFNlbGVjdGVkTWFpblZhbHVlKG51bGwpO1xyXG4gICAgICBzZXRTZWxlY3RlZFNwZWNpYWxWYWx1ZSh2YWxvckF0dWFsKTtcclxuICAgIH1cclxuXHJcbiAgICBpZighZGFkb3NQcm9kdXRvLnNvbGRfb3V0KXtcclxuICAgICAgc2V0U29sZE91dFN0b2NrKDApO1xyXG4gICAgfWVsc2V7XHJcbiAgICAgIHNldFNvbGRPdXRTdG9jaygxKTtcclxuICAgIH1cclxuICB9ZWxzZXsgLy8gUFJPRFVUTyBDT00gT1BDT0VTXHJcbiAgICBzZXRTb2xkT3V0U3RvY2soMCk7XHJcblxyXG4gICAgICBpZihzZWxlY3RlZE1haW5WYWx1ZSA9PSBudWxsKXtcclxuICAgICAgICBzZXRTZWxlY3RlZE1haW5WYWx1ZShkYWRvc1Byb2R1dG8ucHJpY2UpXHJcbiAgICAgIH1cclxuICAgICAgaWYoc2VsZWN0ZWRTcGVjaWFsVmFsdWUgPT0gbnVsbCAmJiBkYWRvc1Byb2R1dG8uc3BlY2lhbCl7XHJcbiAgICAgICAgc2V0U2VsZWN0ZWRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvLnNwZWNpYWwpXHJcbiAgICAgIH0qL1xyXG5cclxuICAgICAgLypmb3IodmFyIGkgaW4gZGFkb3NQcm9kdXRvLm9wdGlvbnMpe1xyXG4gICAgICAgIGdydXBvQXR1YWwgPSBkYWRvc1Byb2R1dG8ub3B0aW9uc1tpXVxyXG4gICAgICAgIGZvcih2YXIgb3B0IGluIGdydXBvQXR1YWwub3B0aW9uX3ZhbHVlKXtcclxuICAgICAgICAgIGlmKGdydXBvQXR1YWwub3B0aW9uX3ZhbHVlW29wdF0ub3B0aW9uX3ZhbHVlX2lkPT1kYWRvc1Byb2R1dG8ub3BjYW9fc2VsZWNpb25hZGEpe1xyXG4gICAgICAgICAgICBpbml0aWFsT3B0aW9uID0gZ3J1cG9BdHVhbC5wcm9kdWN0X29wdGlvbl9pZCsnXycrZ3J1cG9BdHVhbC5vcHRpb25fdmFsdWVbb3B0XS5wcm9kdWN0X29wdGlvbl92YWx1ZV9pZFxyXG4gICAgICAgICAgICB0aGlzLlNlbGVjdE9wdGlvbihpbml0aWFsT3B0aW9uLCBudWxsKVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAgICAgICBcclxuICB9Ki9cclxuIFxyXG4gIHZhciBjaGFuZ2Vab29tID0gKGV2KSA9PiB7XHJcbiAgICBjb25zdCBpbWdTaG93ID0gZXYuY3VycmVudFRhcmdldC5kYXRhc2V0LnNyY3Nob3dcclxuICAgIGNvbnN0IGltZ1BvcHVwID0gZXYuY3VycmVudFRhcmdldC5kYXRhc2V0LnNyY3BvcHVwXHJcbiAgICB2YXIgdGVtcEltYWdlID0ge307XHJcbiAgICB0ZW1wSW1hZ2VbJ3Nob3cnXSA9IGltZ1Nob3dcclxuICAgIHRlbXBJbWFnZVsncG9wdXAnXSA9IGltZ1BvcHVwXHJcbiAgICAvL3NldEluaXRpYWxJbWFnZSh0ZW1wSW1hZ2UpO1xyXG4gICAgXHJcbiAgfVxyXG4gIFxyXG4gIHZhciBsYWxhbGEgPSBnZXRTcGVjaWFsVmFsdWUoZGFkb3NQcm9kdXRvKTtcclxuICB2YXIgbGVsZWxlID0gZ2V0TWFpblZhbHVlKGRhZG9zUHJvZHV0byk7XHJcbiAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcbiAgaWYgKCFyb3V0ZXIuaXNGYWxsYmFjayAmJiAhZGFkb3NQcm9kdXRvPy5wcm9kdWN0X2lkKSB7XHJcbiAgICByZXR1cm4gPEVycm9yUGFnZSBzdGF0dXNDb2RlPXs0MDR9IC8+O1xyXG4gIH1cclxuICByZXR1cm4gKFxyXG4gICAgPExheW91dD5cclxuICAgICAgPENvbnRhaW5lcj5cclxuICAgICAgXHJcbiAgICAgICAge1xyXG4gICAgICAgIHJvdXRlci5pc0ZhbGxiYWNrID8gKFxyXG4gICAgICAgICAgPGRpdj5Mb2FkaW5n4oCmPC9kaXY+XHJcbiAgICAgICAgKSA6IChcclxuICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgIDxIZWFkPlxyXG4gICAgICAgICAgICAgIDx0aXRsZT57ZGFkb3NQcm9kdXRvLm5hbWV9PC90aXRsZT5cclxuICAgICAgICAgICAgICA8bWV0YSBuYW1lPVwiZGVzY3JpcHRpb25cIiBjb250ZW50PVwibGVsZWxlXCIgLz5cclxuICAgICAgICAgICAgICA8bWV0YSBuYW1lPVwib2c6aW1hZ2VcIiBjb250ZW50PVwibGlsaWxpXCIgLz5cclxuICAgICAgICAgICAgPC9IZWFkPlxyXG4gICAgICAgICAgICA8VGVtcGxhdGVIZWFkZXIgY29uZmlncz17Y29uZmlncy5yZXNwb3N0YX0gbWFpbk1lbnU9e21haW5NZW51LnJlc3Bvc3RhfT48L1RlbXBsYXRlSGVhZGVyPiBcclxuICAgICAgICAgICAgPGRpdiBjbGFzPVwibWFpbi1jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwYWdlLXByb2R1Y3RzXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1wYWRkaW5nIGxpZ2h0LWJhY2tncm91bmQgbnByb2R1Y3QtYnJlYWRjcnVtYlwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxvbCBjbGFzc05hbWU9XCJicmVhZGNydW1iXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT1cImJyZWFkY3J1bWItaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIvXCIgdGl0bGU9XCJQw6FnaW5hIGluaWNpYWxcIj5Ib21lPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT1cImJyZWFkY3J1bWItaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHtkYWRvc1Byb2R1dG8ubmFtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvb2w+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjcC1wcmV2aWV3M1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LXBhZ2VcIj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LWdhbGxlcnlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LXRodW1ibmFpbHMgXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2VzLWNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLmltYWdlcykgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubGVuZ3RoID4gNCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNsaWRlciBjbGFzc05hbWU9XCJzbGlkZXJUaHVtYnNcIiB7Li4uc2xpZGVyVGh1bWJzfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZS10aHVtYiBqcy1jYXJvdXNlbC1jb250cm9sLWl0ZW0gcG9pbnRlciBqcy1wcm9kdWN0LWltYWdlLXRodW1iXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ9e2RhZG9zUHJvZHV0by5uYW1lfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1zcmNzaG93PXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnNob3d9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLXNyY3BvcHVwPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCIxMFwiIGhlaWdodD1cIjEwXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9XCJcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8udmlkZW9zLmxlbmd0aCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLnZpZGVvcy5tYXAoKGl0ZW1WaWRlbywga2V5VmlkZW8pID0+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaXRlbS12aWRlb1wiIGtleT17a2V5VmlkZW99PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgZGF0YS12aWRlb2lkPXtpdGVtVmlkZW8uaWRfdmlkZW99PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvJHtpdGVtVmlkZW8uaWRfdmlkZW99L21xZGVmYXVsdC5qcGdgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXlvdXR1YmUtcGxheVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU2xpZGVyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2UtdGh1bWIganMtY2Fyb3VzZWwtY29udHJvbC1pdGVtIHBvaW50ZXIganMtcHJvZHVjdC1pbWFnZS10aHVtYlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17ZGFkb3NQcm9kdXRvLmltYWdlc1tpdGVtXS5wb3B1cH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWx0PXtkYWRvc1Byb2R1dG8ubmFtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1zcmNzaG93PXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnNob3d9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEtc3JjcG9wdXA9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPVwiMTBcIiBoZWlnaHQ9XCIxMFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9XCJcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8udmlkZW9zLmxlbmd0aCk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by52aWRlb3MubWFwKChpdGVtVmlkZW8sIGtleVZpZGVvKSA9PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpdGVtLXZpZGVvXCIga2V5PXtrZXlWaWRlb30+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBvbkNsaWNrPVwiXCIgZGF0YS12aWRlb2lkPXtpdGVtVmlkZW8uaWRfdmlkZW99PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17YGh0dHBzOi8vaW1nLnlvdXR1YmUuY29tL3ZpLyR7aXRlbVZpZGVvLmlkX3ZpZGVvfS9tcWRlZmF1bHQuanBnYH0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEteW91dHViZS1wbGF5XCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LWltYWdlcyBtaW4td2lkdGgtNDE1cHhcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb21cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX3RvcF9sZWZ0LmltYWdlICE9IG51bGwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsYWJlbEl0ZW0gbGFiZWxUb3BMZWZ0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX3RvcF9sZWZ0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX3RvcF9yaWdodC5pbWFnZSAhPSBudWxsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGFiZWxJdGVtIGxhYmVsVG9wUmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fdG9wX3JpZ2h0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX2JvdHRvbV9sZWZ0LmltYWdlICE9IG51bGwpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsYWJlbEl0ZW0gbGFiZWxCb3R0b21MZWZ0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX2JvdHRvbV9sZWZ0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8ubGFiZWxzLnByb21vX2JvdHRvbV9yaWdodC5pbWFnZSAhPSBudWxsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGFiZWxJdGVtIGxhYmVsQm90dG9tUmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5sYWJlbHMucHJvbW9fYm90dG9tX3JpZ2h0LmltYWdlfSAvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFJlYWN0SW1hZ2VNYWduaWZ5IHsuLi57XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc21hbGxJbWFnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdDogZGFkb3NQcm9kdXRvLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNGbHVpZFdpZHRoOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYzogaW5pdGlhbEltYWdlcy5zaG93XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFyZ2VJbWFnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYzogaW5pdGlhbEltYWdlcy5wb3B1cCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogY29uZmlncy53aWR0aFpvb20sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBjb25maWdzLndpZHRoWm9vbVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fSAvPiAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWF4LXdpZHRoLTQxNHB4XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKSAmJiBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5sZW5ndGggPiAxKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2VzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTbGlkZXIgY2xhc3NOYW1lPVwic2xpZGVWaXRyaW5lXCIgey4uLnNldHRpbmdzTW9iaWxlU2xpZGV9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5tYXAoKGl0ZW0sIGtleSkgPT4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtkYWRvc1Byb2R1dG8uaW1hZ2VzW2l0ZW1dLnBvcHVwfSBhbHQ9e2RhZG9zUHJvZHV0by5uYW1lfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRhZG9zUHJvZHV0by52aWRlb3MubGVuZ3RoKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGFkb3NQcm9kdXRvLnZpZGVvcy5tYXAoKGl0ZW1WaWRlbywga2V5VmlkZW8pID0+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tIGl0ZW0tdmlkZW9cIiBrZXk9e2tleVZpZGVvfT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIG9uQ2xpY2s9XCJcIiBkYXRhLXZpZGVvaWQ9e2l0ZW1WaWRlby5pZF92aWRlb30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPXtgaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvJHtpdGVtVmlkZW8uaWRfdmlkZW99L21xZGVmYXVsdC5qcGdgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS15b3V0dWJlLXBsYXlcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TbGlkZXI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IChPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKSAmJiBPYmplY3Qua2V5cyhkYWRvc1Byb2R1dG8uaW1hZ2VzKS5sZW5ndGggPiAwKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoZGFkb3NQcm9kdXRvLmltYWdlcykubWFwKChpdGVtLCBrZXkpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFyZWFab29tXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2RhZG9zUHJvZHV0by5pbWFnZXNbaXRlbV0ucG9wdXB9IGFsdD17ZGFkb3NQcm9kdXRvLm5hbWV9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZGFkb3NQcm9kdXRvLnZpZGVvcy5sZW5ndGgpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8udmlkZW9zLm1hcCgoaXRlbVZpZGVvLCBrZXlWaWRlbykgPT4gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJlYVpvb20gaXRlbS12aWRlb1wiIGtleT17a2V5VmlkZW99PiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGRhdGEtdmlkZW9pZD17aXRlbVZpZGVvLmlkX3ZpZGVvfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e2BodHRwczovL2ltZy55b3V0dWJlLmNvbS92aS8ke2l0ZW1WaWRlby5pZF92aWRlb30vbXFkZWZhdWx0LmpwZ2B9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXlvdXR1YmUtcGxheVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJucHJvZHVjdC1pbmZvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LWFjdGlvbnMgY29udGFpbmVyLXBhZGRpbmcgY29udGFpbmVyLXBhZGRpbmctdG9wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LWhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGgxIGNsYXNzTmFtZT1cIm5wcm9kdWN0LXRpdGxlXCI+e2RhZG9zUHJvZHV0by5uYW1lfTwvaDE+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoZGFkb3NQcm9kdXRvLm1vZGVsKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJpbmZvc1Byb2R1Y3RcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+PHNwYW4gY2xhc3NOYW1lPVwidGl0bGVJbmZvXCI+UkVGOjwvc3Bhbj4ge2RhZG9zUHJvZHV0by5tb2RlbH08L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmF0ZUJveFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwibGstYXZhbGlhclwiIG9uQ2xpY2s9XCJcIj5BdmFsaWFyIGFnb3JhPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGRhZG9zUHJvZHV0by5zaG9ydCAhPSAnJyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImluZm9zQXJlYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmVzdW1lUHJvZHVjdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXt7IF9faHRtbDogZGFkb3NQcm9kdXRvLnNob3J0IH19IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJ1eUFyZWFcIj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sU2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibnByb2RjdC1wcmljZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGxhbGFsYSkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwib2xkUHJpY2VcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIERlIDxzcGFuIGNsYXNzTmFtZT1cIm5wcm9kdWN0LXByaWNlLW1heFwiPntsZWxlbGV9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5wcm9kdWN0LXByaWNlLXZhbHVlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhsYWxhbGEpID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBvciA8c3BhbiBjbGFzc05hbWU9XCJzcGVjaWFsVmFsdWVcIj57bGFsYWxhfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJpdGVtLWRpc2NvdW50XCI+e2RhZG9zUHJvZHV0by5kaXNjb3VudF9wZXJjZW50fTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPnBvciA8c3Bhbj57bGVsZWxlfTwvc3Bhbj48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoc2VsZWN0ZWRRdGRQYXJjZWwgIT0gJycgJiYgc2VsZWN0ZWRWYWxQYXJjZWwgIT0gJycpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInNlbGVjdGVkUGFyY2VsXCI+T3UgPHNwYW4gY2xhc3NOYW1lPVwibnVtUGFyY1wiPntzZWxlY3RlZFF0ZFBhcmNlbH14PC9zcGFuPiBkZSA8c3BhbiBjbGFzc05hbWU9XCJ2YWxQYXJjXCI+e3NlbGVjdGVkVmFsUGFyY2VsfTwvc3Bhbj48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChkYWRvc1Byb2R1dG8gIT0gJycpP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAocGFyc2VJbnQoZGFkb3NQcm9kdXRvLmhhc19vcHRpb24pICE9IDApID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwib3B0aW9uc0FyZWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhZG9zUHJvZHV0by5vcHRpb25zLm1hcCgoaXRlbSwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJveC1vcHRpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbD57aXRlbS5uYW1lfTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsoaXRlbS50eXBlID09ICd0ZXh0Jyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidHh0T3B0aW9uXCI+ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgaWQ9XCJmaWVsZE9wdGlvblwiIG1heGxlbmd0aD1cIjNcIiB0eXBlPVwidGV4dFwiIGRhdGEtZ3JvdXA9e2l0ZW0ucHJvZHVjdF9vcHRpb25faWR9IGNsYXNzTmFtZT1cImZpZWxkXCIgbmFtZT1cInR4dC1vcHRpb25cIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVscFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYSBmYS1xdWVzdGlvbi1jaXJjbGUgY29sb3IyXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpbmZvSGVscFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgSW5zaXJhIGF0w6kgMyBsZXRyYXMgcGFyYSBwZXJzb25hbGl6YXIgYSBjYW1pc2EgY29tIHVtIGJvcmRhZG8gZXhjbHVzaXZvLiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJsaXN0T3B0aW9uc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0ub3B0aW9uX3ZhbHVlLm1hcCgoaXRlbU9wdGlvbiwga2V5KSA9PiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9e3NlbGVjdGVkT3B0aW9uQnV5ICE9PSBpdGVtLnByb2R1Y3Rfb3B0aW9uX2lkKydfJytpdGVtT3B0aW9uLnByb2R1Y3Rfb3B0aW9uX3ZhbHVlX2lkID8gJ29wdGlvbicgOiAnb3B0aW9uIHNlbGVjdGVkJ30gb25DbGljaz1cIlwiIGhyZWY9XCJqYXZhc2NyaXB0OjtcIj57aXRlbU9wdGlvbi5uYW1lfTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogICcnICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sU2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJxdWFudGl0eUFyZWFcIj4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbD5RdWFudGlkYWRlPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV0dG9uc1F1YW50aXR5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGNsYXNzTmFtZT1cImJ0bkxlc3NcIj48aSBjbGFzc05hbWU9XCJmYSBmYS1taW51c1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT48L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgaWQ9XCJ0eHRRdWFudGl0eVwiIHR5cGU9XCJ0ZXh0XCIgbmFtZT1cInR4dC1xdWFudGl0eVwiIHZhbHVlPXsgcXVhbnRpdHlCdXkgfSBjbGFzc05hbWU9XCJ0eHRRdWFudGl0eVwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiamF2YXNjcmlwdDo7XCIgb25DbGljaz1cIlwiIGNsYXNzTmFtZT1cImJ0bk1vcmVcIj48aSBjbGFzc05hbWU9XCJmYSBmYS1wbHVzXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhzb2xkT3V0U3RvY2sgIT0gbnVsbCAmJiAhc29sZE91dFN0b2NrKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXlCdXR0b25BcmVhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJqYXZhc2NyaXB0OjtcIiBjbGFzc05hbWU9XCJidXlCdXR0b24gYnRuX2J1eVwiIG9uQ2xpY2s9XCJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhIGZhLXNob3BwaW5nLWNhcnRcIj48L2k+IHsnQ29tcHJhcid9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IChzb2xkT3V0U3RvY2sgIT0gbnVsbCAmJiBzb2xkT3V0U3RvY2spP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJ1eUJ1dHRvbkFyZWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cImphdmFzY3JpcHQ6O1wiIGNsYXNzTmFtZT1cImJ1eUJ1dHRvbiBub3RpZnlCdXR0b25cIiBkYXRhLXByb2R1Y3RpZD17ZGFkb3NQcm9kdXRvLnByb2R1Y3RfaWR9IG9uQ2xpY2s9XCJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtZW52ZWxvcGVcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+IEF2aXNlLW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7KGRhZG9zUHJvZHV0by50ZXh0X3ByZXZlbmRhIT0gbnVsbCAmJiBkYWRvc1Byb2R1dG8udGV4dF9wcmV2ZW5kYSAhPSAnJyk/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImNoZWNrUHJldmVuZGFcIj48aW5wdXQgaWQ9XCJja3ByZXZlbmRhXCIgdHlwZT1cImNoZWNrYm94XCIgb25DaGFuZ2U9XCJcIiAvPiBDb25jb3JkbyBjb20gbyBwcmF6byBkZSBlbnRyZWdhIGRlc2NyaXRvIGFiYWl4by48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImluZm9QcmV2ZW5kYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwidGl0XCI+VEVSTU8gREUgQUNFSVRBw4fDg088L2g0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ZGFkb3NQcm9kdXRvLnRleHRfcHJldmVuZGF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhkYWRvc1Byb2R1dG8uZ3VpYV9tZWRpZGFzKT9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJndWlhc01lZGlkYVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYWRvc1Byb2R1dG8uZ3VpYV9tZWRpZGFzLm1hcCgoaXRlbUd1aWEsIGtleUd1aWEpID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwiaXRlbUd1aWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cImNvbG9yMlwiIHRpdGxlPXtpdGVtR3VpYS50aXRsZX0gZGF0YS10aXR1bG9ndWlhPVwiXCIgZGF0YS1jb250ZXVkb2d1aWE9XCJcIiBvbkNsaWNrPVwiXCI+e2l0ZW1HdWlhLnRpdGxlfTwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxUZW1wbGF0ZUZvb3RlciBjb25maWdzPXtjb25maWdzLnJlc3Bvc3RhfSAvPlxyXG4gICAgICAgICAgPC8+XHJcbiAgICAgICAgKX1cclxuICAgICAgPC9Db250YWluZXI+XHJcbiAgICA8L0xheW91dD5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFN0YXRpY1Byb3BzKHsgcGFyYW1zIH0pIHtcclxuICB2YXIgY29uZmlncyA9IGF3YWl0IGdldENvbmZpZ3MoKTtcclxuICB2YXIgbWFpbk1lbnUgPSBhd2FpdCBnZXRNYWluTWVudSgpO1xyXG4gIHZhciByZXNQcm9kdWN0ID0gYXdhaXQgZ2V0UHJvZHVjdEJ5U2x1ZyhwYXJhbXMuc2x1Zyk7XHJcblxyXG4gIHZhciBkYWRvc1Byb2R1dG8gPSBudWxsO1xyXG4gIGlmKHJlc1Byb2R1Y3Quc3VjY2Vzcyl7XHJcbiAgICBkYWRvc1Byb2R1dG8gPSByZXNQcm9kdWN0LnJlc3Bvc3RhLnByb2R1dG87XHJcbiAgfVxyXG4gIHJldHVybiB7XHJcbiAgICBwcm9wczoge1xyXG4gICAgICBjb25maWdzLFxyXG4gICAgICBkYWRvc1Byb2R1dG8sXHJcbiAgICAgIG1haW5NZW51XHJcbiAgICB9LFxyXG4gICAgcmV2YWxpZGF0ZTogNjBcclxuICB9O1xyXG59XHJcblxyXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0U3RhdGljUGF0aHMoKSB7XHJcbiAgY29uc3QgcHJvZHV0b3MgPSBhd2FpdCBnZXRQcm9kdWN0c0J5Q2F0ZWdvcnkoKTtcclxuXHJcbiAgcmV0dXJuIHtcclxuICAgIHBhdGhzOlxyXG4gICAgICBbXSxcclxuICAgIGZhbGxiYWNrOiB0cnVlLFxyXG4gIH07XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==